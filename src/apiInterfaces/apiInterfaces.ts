export interface SingleCenterDetailOfMember {
  memberID: string,
  firstName: string,
  middleName: string,
  lastName: string,
  emailAddress: string,
  phoneNumber: string,
  picture: string,
  memberStatus: string,
  memberType: string,
  companyID: string,
  tenantID: string,
  companyName: string,
  tenantName: string,
  tenantlocation: string,
  tenantLogo: string,
  tenantTermsOfserviceUrl: string,
  tenantPrivacyPolicyUrl: string
}

export interface MultiCenterDetailOfMember {
  memberDetails: Array<SingleCenterDetailOfMember>
}

export interface OTPResponseFields {
  id: number,
  jobid: number,
  smssentstatus: number,
  messagestatus: number
}

export interface OTPVerified {
  access_token: string,
  token_type: string,
  expires_in: number,
  issued: string,
  expires: string
}

export interface SingleDocumentArray {
  formName: string,
  formId: number,
  Available: boolean,
  FilePath: string
}

export interface SingleOfferArray {
  OfferName: string,
  OfferShortName: string,
  OfferDescription: string,
  OfferLogo: string,
  OfferValidTill: string,
  OfferBgImage: string,
  OfferDocumentsRequired: Array<SingleDocumentArray>
}

export interface Offers {
  offerList: Array<SingleOfferArray>
}

export interface IClasslist {
  ClassId: number,
  ClassLevel: string,
  StartTime: string,
  DurationEndtime: string,
  Rating: number,
  Ratingcount: number,
  CenterId: number,
  CenterName: string,
  CenterAddress: string,
  CentreLocation: string,
  CapicityStatus: string,
  MemberClassStatus: string,
  UserId: number,
  UserName: string,
  UserImage: string,
  PurposeId: string,
  PurposeName: string,
  PurposeLogo: string,
  BackgroundImage: string,
  Tags: Array<ITags>
}

export interface ITags {
  tag: string
}

export interface IClassdetails {
  ClassDiscription: string,
  UserBio: string
}

export interface AttendanceList {
  ClassAttendance: Array<IClassAttendance>;
  AppointmentAttendance: Array<IAppointmentAttendance>;
}

export interface IClassAttendance {
  Guid: number,
  Xrefid: number,
  PurposeName: string,
  PurposeId: number,
  TrainerName: string,
  TrainerId: number,
  TrainerPic: string,
  AppointmentLocation: string,
  AppointmentDateTime: string,
  AppointmentEndTime: string
}

export interface IAppointmentAttendance {
  Guid: number,
  Xrefid: number,
  PurposeName: string,
  PurposeId: number,
  TrainerName: string,
  TrainerId: number,
  TrainerPic: string,
  AppointmentLocation: string,
  AppointmentDateTime: string,
  AppointmentEndTime: string
}

import { ArrayType } from "@angular/compiler/src/output/output_ast";

export interface ScheduleList {
  ClassList: Array<Classlist>;
  Appointmentlist: Array<Schedulelist>;
}

export interface Classlist {
  XrefID: string,
  ClassId: string,
  ClassLevel: string,
  StartTime: string,
  Duration: string,
  Rating: string,
  Ratingcount: string,
  CenterId: string,
  CenterName: string,
  CenterAddress: string,
  CentreLocation: string,
  UserId: string,
  UserName: string,
  UserImage: string,
  PurposeId: string,
  PurposeName: string,
  PurposeLogo: string,
  Tags: ArrayType
}

export interface Schedulelist {
  XrefID: string,
  StartTime: string,
  Duration: string,
  CenterId: string,
  CenterName: string,
  CenterAddress: String,
  CentreLocation: string,
  UserId: string,
  UserName: string,
  UserImage: string,
  PurposeId: string,
  PurposeName: string,
  PurposeLogo: string
}

export interface SelectTrainer {
  selectTrainer: Array<trainers>
}

export interface trainers {
  UserId: number,
  TrainerName: string,
  TrainerPicture: string,
  TrainerRating: number,
  RatingSurveyCount: number
}

export interface SelectFacility {
  selectFacility: Array<facility>
}

export interface facility {
  FacilityName: string,
  FacilityPicture: string
}

export interface RecomendedProducts {
  PrductTypeName: string,
  ProductTypeId: string,
  ProductName: string,
  ProductId: string,
  ProductLogoPath: string,
  ProductPrice: number,
  ProductTax: Array<ProductTax>
}

export interface ProductTax {
  ProductTaxRateGroupName: string,
  Product1TaxRateId: string,
  Product1TaxRatepercent: number,
  Product2TaxRateId: string,
  Product2TaxRatePercent: number
}

export interface Offers {
  OfferCodeName: string,
  OfferShortName: string,
  OfferDescription: string,
  OfferLogo: string,
  OfferValidTill: string,
  OfferBgImage: string,
  OfferDiscount: string,
  OfferDocumentsRequired: Array<OfferDocumentsRequired>
}

export interface OfferDocumentsRequired {
  formName: string,
  formId: string,
  Available: boolean,
  FilePath: string
}

export interface Waiver {
  WaiverStatus: string,
  WaiverFilePath: string,
  WaiverTerms: string,
  WaiverSignAllowed: [
    {
      Otp: boolean,
      Esign: boolean,
      Aadhar: boolean
    }
  ]
}

export interface response {
  id: string,
  jobid: string,
  smssentstatus: number,
  messagestatus: number
}

export interface WaiverEsign {
  WaiverStatus: string,
  WaiverFilePath: string
}

export interface SelectPurpose {
  Purposes: Array<purpose>
}

export interface purpose {
  PurposeName: string,
    PurposeId: number,
    PurposeIcon: string,
    PurposeDiscription: string,
    PurposeDuration:string,
    PostSessionMaintenanceTime: string,
    SpecialMessage: string,
    membermanagable: boolean,
    BookedSession: number,
    TotalSession: number,
    SessionOwner: string,
    IfNoSessionOwnerLetMemberChooseSessionOwner: string,
    PurposeDescription: string,
    PurposeColour: string,
    MaxDailySessionBookingLimitReached: number,
}


export interface repeatBooking {
   bookedResponse: Array<repeat>
  }
  
  export interface repeat {
   xrefid:number,
   status:string,
   venue:string,
  }
 
  export interface Mywallet{
    myWallet: Array<mywallet>
  }

  export interface mywallet {
    transactionId:string,
    transactionamount:number,
    transactionStatus:number,
    transactionDate:string,
    transactionType:string
  }

  export interface Myprofile{
    myprofile: Array<myProfile>
  }

  export interface myProfile {
    profilePicture:string,
    ProfileName:string,
    clientId:string,
    membershipType:string,
    walletBalance:number,
    outstandingbalance:number, 
  }
 