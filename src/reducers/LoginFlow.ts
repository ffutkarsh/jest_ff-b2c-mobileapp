import * as a from '../constants/LoginActionTypes';

//INITIAL STATE.
const initialState = {

    dialCode: "",
    phoneNumber: "",
    centerName: "",
    subCenterName: "",
    enquiryType: "",
    memberType: "",
    userName: "",
    userPhoto: "",
    otp: "",
    verificationFlag: "",
    permissions: []
}

//REDUCER.
const rootReducer = (state = initialState, action) => {

    switch (action.type) {

        case a.ADD_DIALCODE:
            return {
                ...state, dialCode: action.payload
            }

        case a.ADD_PHONENUMBER:
            return {
                ...state, phoneNumber: action.payload
            }

        case a.ADD_CENTERNAME:
            return {
                ...state, centerName: action.payload
            }

        case a.ADD_SUBCENTERNAME:
            return {
                ...state, subCenterName: action.payload
            }

        case a.ADD_ENQUIRYTYPE:
            return {
                ...state, enquiryType: action.payload
            }

        case a.ADD_MEMBERTYPE:
            return {
                ...state, memberType: action.payload
            }

        case a.ADD_USERNAME:
            return {
                ...state, userName: action.payload
            }

        case a.ADD_USERPHOTO:
            return {
                ...state, userPhoto: action.payload
            }

        case a.ADD_OTP:
            return {
                ...state, otp: action.payload
            }

        case a.ADD_VERIFICATIONFLAG:
            return {
                ...state, verificationFlag: action.payload
            }

        case a.ADD_PERMISSIONS:
            return {
                ...state, permissions: action.payload
            }
    }
}

export default rootReducer;