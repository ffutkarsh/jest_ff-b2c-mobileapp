import * as a from '../constants/LoginActionTypes';

export const addDialCode = dialCode => ({
    type: a.ADD_DIALCODE,
    payload: dialCode
});

export const addPhoneNumber = phoneNumber => ({
    type: a.ADD_PHONENUMBER,
    payload: phoneNumber
});

export const addCenterName = centerName => ({
    type: a.ADD_CENTERNAME,
    payload: centerName
});

export const addSubCenterName = subCenterName => ({
    type: a.ADD_SUBCENTERNAME,
    payload: subCenterName
});

export const addEnquiryType = enquiryType => ({
    type: a.ADD_ENQUIRYTYPE,
    payload: enquiryType
});

export const addMemberType = memberType => ({
    type: a.ADD_MEMBERTYPE,
    payload: memberType
});

export const addUserName = userName => ({
    type: a.ADD_USERNAME,
    payload: userName
});

export const addUserPhoto = userPhoto => ({
    type: a.ADD_USERPHOTO,
    payload: userPhoto
});

export const addOTP = otp => ({
    type: a.ADD_OTP,
    payload: otp
});

export const addVerificationFlag = verificationFlag => ({
    type: a.ADD_VERIFICATIONFLAG,
    payload: verificationFlag
});

export const addPermissions = permissions => ({
    type: a.ADD_PERMISSIONS,
    payload: permissions
});