import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class BillingProvider {
  receivedGst = 0;
  receivedRegi = 0;
  receivedDiscount = 0;
  receivedTotal = 0;
  receivedTopay = 0;
  constructor(public http: Http) {
    console.log('Hello BillingProvider Provider');
  }

  gst(gstamount) {
    this.receivedGst = gstamount
    return this.receivedGst;
  }

  registration(regi) {
    this.receivedRegi = regi;
    return this.receivedRegi;
  }

  discount(discAmt) {
    this.receivedDiscount = discAmt;
    return this.receivedDiscount;
  }

  total(totalamt) {
    this.receivedTotal = totalamt
    return this.receivedTotal
  }

  toPay(topayamt) {
    this.receivedTopay = topayamt;
    return this.receivedTopay;
  }
}

