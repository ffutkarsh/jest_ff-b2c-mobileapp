import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class LanguageProvider {

  constructor(public http: HttpClient) {
    console.log('Hello LanguageProvider Provider');
  }

  //GETTING DATA FROM JSON.
  getLanguages() {
    return this.http.get('assets/Language.json');
  }
}
