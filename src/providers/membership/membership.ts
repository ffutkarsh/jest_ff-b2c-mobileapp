import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';

@Injectable()
export class MembershipProvider {

  constructor(public http: Http) {
    console.log('Hello MembershipProvider Provider');
  }
  //call from page comes here
  getMembershipDetails() {
    return this.http.get('assets/membership.json')
  }

}
