import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DashboardFlowAuthProvider {

  public isLoggedIn = false;
  public varDashboardToOtp = false;

  constructor(public http: Http) { }

  // Login a user
  // Normally make a server request and store
  // e.g. the auth token
  fromDashboardpage(): void {
    this.isLoggedIn = true;
  }

  // Logout a user, destroy token and remove
  // every information related to a user
  notfromDashboardpage(): void {
    this.isLoggedIn = false;
  }

  // Returns whether the user is currently authenticated
  // Could check if current token is still valid
  authenticated(): boolean {
    return this.isLoggedIn;
  }

  fromDashboardToOtp(): void {
    this.varDashboardToOtp = true;
  }

  fromOtpToDashboard(): void {
    this.varDashboardToOtp = false;
  }

  authenticated1(): boolean {
    return this.varDashboardToOtp;
  }

}
