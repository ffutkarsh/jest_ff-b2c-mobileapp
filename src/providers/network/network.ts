import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { AlertController, Events } from 'ionic-angular';

//TWO STATES OF NETWORK ONLINE/OFFLINE
export enum ConnectionStatusEnum {
    Online,
    Offline
}

@Injectable()
export class NetworkProvider {

    previousStatus; //STORES THE PREVIOUS STATE OF NETWORK.

    constructor(public alertCtrl: AlertController,
        public network: Network,
        public eventCtrl: Events) {
        console.log('Hello NetworkProvider Provider');
        this.previousStatus = ConnectionStatusEnum.Online;
    }

    //METHOD TO LISTEN NETWORK EVENTS. [PAGE SPECIFIC]
    public initializeNetworkEvents(): void {
        //AUTOMATICALLY FIRES WHEN CONNECTION IS TURNED OFF.
        this.network.onDisconnect().subscribe(() => {
            if (this.previousStatus === ConnectionStatusEnum.Online) {
                this.eventCtrl.publish('network:offline');
            }
            this.previousStatus = ConnectionStatusEnum.Offline;
        });
        //AUTOMATICALLY FIRES WHEN CONNECTION IS TURNED ON.
        this.network.onConnect().subscribe(() => {
            if (this.previousStatus === ConnectionStatusEnum.Offline) {
                this.eventCtrl.publish('network:online');
            }
            this.previousStatus = ConnectionStatusEnum.Online;
        });
    }

    //RETURNS THE CURRENT NETWORK STATE.
    public isConnected() {
        let conntype = this.network.type;
        return conntype && conntype !== 'unknown' && conntype !== 'none';
    }

}
