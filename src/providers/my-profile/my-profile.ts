import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Myprofile } from '../../apiInterfaces/apiInterfaces';

@Injectable()
export class MyProfileProvider {

  constructor(public http: HttpClient) {
    console.log('Hello MyProfileProvider Provider');
  }

  //call from page comes here
  getMyProfileDetails(): Observable<any> {
    return this.http.get<any>('assets/myProfileApi.json');
  }

}

 
 