import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

@Injectable()
export class CheckPermissionsProvider {

  //STRUCTURE OF INITIAL STATE OF USER OBJECT.
  public currentUserState = {
    dialCode: "",
    phoneNumber: "",
    centerName: "",
    subCenterName: "",
    enquiryType: "",
    memberType: "",
    userName: "",
    userPhoto: "",
    otp: "",
    verificationFlag: "",
    permissions: []
  };

  constructor(
    public http: Http,
    private storage: Storage) {
    this.storage.get('USERDETAILS').then((USERDETAILS) => { //FETCHING THE CURRENT USER STATE.
      this.currentUserState = USERDETAILS;
    });
  }

  checkAccessibility(claims) {
    if (this.currentUserState.memberType == "Registered Member") {
      let result = this.currentUserState.permissions.filter((page) => page == claims);
      if (result == claims) {
        return true; //ACCESS GIVEN.
      } else {
        return false; //ACCESS NOT GIVEN.
      }
    } else if (this.currentUserState.memberType == "Guest Member") {
      let result = this.currentUserState.permissions.filter((page) => page == claims);
      if (result == claims) {
        return true; //ACCESS GIVEN.
      } else {
        return false; //ACCESS NOT GIVEN.
      }
    }
  }

}
