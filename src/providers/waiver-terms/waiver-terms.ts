import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Waiver, response,WaiverEsign } from '../../apiInterfaces/apiInterfaces';

/*
  Generated class for the WaiverTermsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class WaiverTermsProvider {

  constructor(public http: Http) {
    console.log('Hello WaiverTermsProvider Provider');
  }
   //TO FETCH ABOUT CONTENT FROM JSON.
   getWaiverTerms() {
    console.log("inside getall waiverr")
    return this.http.get('assets/waiverTerms.json');
  } 

     //TO FETCH ABOUT CONTENT FROM JSON.
     getWaiverOtp(){
      console.log("inside getall waiverr")
      return this.http.get('assets/WaiverOtp.json');
    } 
    
      //TO FETCH ABOUT CONTENT FROM JSON.
      getWaiverEsign(){
        console.log("inside getall waiverr")
        return this.http.get('assets/WaiverEsign.json');
      } 
      
}
