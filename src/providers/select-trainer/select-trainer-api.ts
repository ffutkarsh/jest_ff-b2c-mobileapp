import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { SelectTrainer } from '../../apiInterfaces/apiInterfaces';

@Injectable()
export class SelectTrainerProvider {

  constructor(public http: HttpClient) {
    console.log('Hello SelectTrainerProvider Provider');
  }

  //call from page comes here
  getSpecificTrainerDetails(): Observable<SelectTrainer> {
    return this.http.get<SelectTrainer>('assets/selectTrainerApi.json');
  }

}


 