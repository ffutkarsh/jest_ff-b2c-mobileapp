import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AboutContentProvider {

  constructor(public http: Http) {
    console.log('Hello AboutContentProvider Provider');
  }

  //TO FETCH ABOUT CONTENT FROM JSON.
  getAboutContent() {
    return this.http.get('assets/AboutContent.json');
  }

}
