import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { SingleCenterDetailOfMember, MultiCenterDetailOfMember, OTPResponseFields, OTPVerified } from '../../apiInterfaces/apiInterfaces';

@Injectable()
export class LoginFlowApiProvider {
  public d=  new Date();

  public RatinWidgetData = [
    {
      'ratingDate':  this.d.getDate()+5,
    
      'ratingStar': null,
      'ratingFlag': false
    },
    {
      'ratingDate': this.d.getDate()+1,
      'ratingStar': null,
      'ratingFlag': true
    },
    {
      'ratingDate': this.d.getDate(),
      'ratingStar': null,
      'ratingFlag': true
    },
    {
      'ratingDate': this.d.getDate()+2,
      'ratingStar': null,
      'ratingFlag': false
    },
    {
      'ratingDate': this.d.getDate()+4,
      'ratingStar': null,
      'ratingFlag': false
    }
  ];


  constructor(public http: HttpClient) {
    console.log('Hello LoginFlowApiProvider Provider');
  }

  //FETCHING API FOR MEMBER DETAILS.
  public getMemberDetails(): Observable<MultiCenterDetailOfMember> {
    return this.http.get<MultiCenterDetailOfMember>('assets/MemberDetailsApi.json');
  }

  //FETCHING API FOR OTP RESPONSE.
  public getOtpResponse(): Observable<OTPResponseFields> {
    return this.http.get<OTPResponseFields>('assets/OtpResponseApi.json');
  }

  //FETCHING API FOR OTP VERIFIED TOKEN.
  public getOtpVerified(): Observable<OTPVerified> {
    return this.http.get<OTPVerified>('assets/OtpVerifiedApi.json');
  }

  // /* rating widget modal popup based on flag  */
  //  public getRating(): Observable<any>{
  // return 
  //  }

}
