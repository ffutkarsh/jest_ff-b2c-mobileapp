import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { RecomendedProducts,ProductTax } from '../../apiInterfaces/apiInterfaces';

/*
  Generated class for the RelatedItemsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RelatedItemsProvider {
  obectOfItem = [];
  booleanRelatedItem;
  constructor(public http: HttpClient) {
    console.log('Hello RelatedItemsProvider Provider');
  }

  storeRelatedItems(item) {
    console.log("iSTORE RELATYED ITEMS", this.obectOfItem)
    return this.obectOfItem
  }

  //call from page comes here
  getAllRelatedItems(): Observable<RecomendedProducts> {
    return this.http.get<RecomendedProducts>('assets/RelatedItems.json');
  }

  showRelatedItem(): boolean {
    return this.booleanRelatedItem = true
  }


  showRelatedItem1(): boolean {
    return this.booleanRelatedItem = false
  }



}
