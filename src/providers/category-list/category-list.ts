import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the CategoryListProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CategoryListProvider {

  constructor(public http: HttpClient) {
    console.log('Hello CategoryListProvider Provider');
  }

  //call from page comes here
  getVideoDetails(): Observable<any> {
    return this.http.get<any>('assets/video.json');
  }


  
}
