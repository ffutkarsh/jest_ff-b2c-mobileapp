import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';

/*
  Generated class for the OtpHelperProvider provider.

  
*/
@Injectable()
export class OtpHelperProvider {
  otpsize =4; // Can be switched to 6 or 4

  constructor() {
    
  }

  getLastDigits(){
    return 6787; // Will Get this value from json or nav Params
  }

  getOtpSize()
  {
    return this.otpsize; // Will return the OTP size 4 or 6 , can be used to
  }



}
