import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { IClasslist, IClassdetails, IClassAttendance, IAppointmentAttendance } from '../../apiInterfaces/apiInterfaces';

@Injectable()
export class ClassesFlowApiProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ClassesFlowApiProvider Provider');
  }

  //Fetching Api for Class List Page.
  public getClassList(): Observable<IClasslist[]> {
    return this.http.get<IClasslist[]>('assets/classlist.json');
  }

  //Fetching Api for Class Details Page.
  public getClassDetails(): Observable<IClassdetails> {
    return this.http.get<IClassdetails>('assets/classdetails.json');
  }

  //Fetching Api for Class Attendance Page.
  public getClassAttendance(): Observable<IClassAttendance[]> {
    return this.http.get<IClassAttendance[]>('assets/ClassAttendance.json');
  }

  //Fetching Api for Appointment Attendance Page.
  public getApptAttendance(): Observable<IAppointmentAttendance[]> {
    return this.http.get<IAppointmentAttendance[]>('assets/ClassAttendance.json');
  }

  public getPurposeSessions(): Observable<any> {
    return this.http.get<any>('assets/PurposeSessions.json');
  }

  public getFilterTags(): Observable<any> {
    return this.http.get<any>('assets/FilterTags.json');
  }

  public getPopularLocations(): Observable<any> {
    return this.http.get<any>('assets/popularlocation.json');
  }
}
