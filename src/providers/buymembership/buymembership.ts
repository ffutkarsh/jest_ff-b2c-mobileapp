import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class BuymembershipProvider {

  constructor(public http: HttpClient) {
    console.log('Hello BuymembershipProvider Provider');
  }

  //call from page comes here
  getBuyMembershipDetails(): Observable<any> {
    return this.http.get<any>('assets/buyMembership.json');
  }

}


 
