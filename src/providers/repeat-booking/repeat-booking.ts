import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { repeatBooking } from '../../apiInterfaces/apiInterfaces';

@Injectable()
export class RepeatBookingProvider {

  constructor(public http: HttpClient) {
    console.log('Hello RepeatBookingProvider Provider');
  }
  repeatBookingDetails(): Observable<repeatBooking> {
    return this.http.get<repeatBooking>('assets/repeatBooking.json');
  }
}