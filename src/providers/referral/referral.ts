import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the ReferralProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ReferralProvider {

  constructor(public http: Http) {
    console.log('Hello ReferralProvider Provider');
  }
  getReferralDetails() {
    return this.http.get('assets/referral.json');
  }

}

 

