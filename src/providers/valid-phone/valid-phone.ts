import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ValidPhoneProvider {

  constructor(public http: Http) {
    console.log('Hello ProductProvider Provider');
  }

  //GETTING DATA FROM JSON.
  getCountries() {
    return this.http.get('assets/CountryCodeFinal.json')
  }

  //GETTING DATA FROM JSON.
  getPurposes() {
    return this.http.get('assets/PurposeList.json');
    
  }

  //GETTING DATA FROM JSON.
  getSlots() {
    return this.http.get('assets/Slots.json');
    
  }

}
