import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the IpgeofetchProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class IpgeofetchProvider {

  constructor(public http: Http,public http2:HttpClient) {
    console.log('Hello IpgeofetchProvider Provider');
  }

  getLocfromIP(): Observable<any> {
    return this.http2.get<any>('http://ipinfo.io/json');
  }

  reverseGeo(lat, long)
  {
    return this.http2.get<any>('https://nominatim.openstreetmap.org/reverse?format=json&lat='+lat+'&lon='+long);
  }

}
