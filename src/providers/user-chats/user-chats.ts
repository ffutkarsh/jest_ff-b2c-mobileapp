import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';


/*
  Generated class for the UserChatsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserChatsProvider {

  constructor(public http: HttpClient) {
    console.log('Hello UserChatsProvider Provider');
  }
  getUserData():Observable<any>{
    return this.http.get('assets/user.json');
  }

  gettarinerData():Observable<any>{
    return this.http.get('assets/trainer.json');
  }

}
