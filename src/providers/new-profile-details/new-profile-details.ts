import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the NewProfileDetailsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NewProfileDetailsProvider {

  constructor(public http: Http) {
    console.log('Hello NewProfileDetailsProvider Provider');
  }
  //call from page comes here
  getNewProfileDetails() {
    return this.http.get('assets/NewProfileDetails.json')
  }
}
