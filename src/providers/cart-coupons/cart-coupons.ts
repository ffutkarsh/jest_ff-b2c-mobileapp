import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Offers,OfferDocumentsRequired } from '../../apiInterfaces/apiInterfaces';

/*
  Generated class for the CartCouponsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CartCouponsProvider {

  constructor(public http: HttpClient) {
    console.log('Hello CartCouponsProvider Provider');
  }
  //call from page comes here
  getAllCouponsDetails(): Observable<Offers> {
    console.log("inside getall")
    return this.http.get<Offers>('assets/CartCoupon.json');
  }

}


