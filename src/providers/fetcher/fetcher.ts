import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class FetcherProvider {

  constructor(public http: Http,public http2:HttpClient) {
    console.log('Hello ProductProvider Provider');
  }


 
  getPurposes(): Observable<any> {
    return this.http2.get<any>('assets/PurposeListApi.json');
  }

  getBookedOrder() {
    return this.http.get('assets/BookedResponse.json');
  }

  getProfileData() {
    return this.http.get('assets/ProfileDetails.json');
  }

  getPopularLocations() {
    return this.http.get('assets/popularlocation.json');
  }

  getRatingTags() {
    return this.http.get('assets/ratingtags.json');
  }

  getCities() {
    return this.http.get('assets/location.json');
  }
  
  



}









  
