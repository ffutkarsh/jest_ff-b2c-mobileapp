import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class VideoCategoryProvider {

  constructor(public http: HttpClient) {
    console.log('Hello VideoCategoryProvider Provider');
  }

  //FETCHING API RESPONSE OF VIDEO CATEGORIES.
  public getVideoCategory(): Observable<any> {
    return this.http.get<any>('assets/VideoCategory.json');
  }

}
