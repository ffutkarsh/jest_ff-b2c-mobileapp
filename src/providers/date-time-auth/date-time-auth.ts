import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the DateTimeAuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DateTimeAuthProvider {
  public isLoggedIn = false;
  constructor(public http: Http) {
    console.log('Hello DateTimeAuthProvider Provider');
  }

     // Login a user
  // Normally make a server request and store
  // e.g. the auth token
  fromDateTimepage(): void {
    this.isLoggedIn = true;
  }

  // Logout a user, destroy token and remove
  // every information related to a user
  notfromDateTimepage(): void {
    this.isLoggedIn = false;
  }

  // Returns whether the user is currently authenticated
  // Could check if current token is still valid
  authenticated(): boolean {
    return this.isLoggedIn;
  }
}
