import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Mywallet } from '../../apiInterfaces/apiInterfaces';

@Injectable()
export class MyWalletProvider {

  constructor(public http: HttpClient) {
    console.log('Hello MyWalletProvider Provider');
  }

  //call from page comes here
  getWalletDetails(): Observable<Mywallet> {
    return this.http.get<Mywallet>('assets/myWallet.json');
  }

}

 
 