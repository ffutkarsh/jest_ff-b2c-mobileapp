import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SignuptermsAuthProvider {

  public isLoggedIn = false;

  constructor(public http: Http) { }

  // Login a user
  // Normally make a server request and store
  // e.g. the auth token
  fromSignuptermspage(): void {
    this.isLoggedIn = true;
  }

  // Logout a user, destroy token and remove
  // every information related to a user
  notfromSignuptermspage(): void {
    this.isLoggedIn = false;
  }

  // Returns whether the user is currently authenticated
  // Could check if current token is still valid
  authenticated(): boolean {
    return this.isLoggedIn;
  }

}
