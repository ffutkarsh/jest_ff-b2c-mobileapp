import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { ToastController } from 'ionic-angular';
/*
  Generated class for the ErrorProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ErrorProvider {

  constructor(public http: Http ,public toastCtrl:ToastController ) {
    console.log('Hello ErrorProvider Provider');
  }




error(mssg){
  
    let toast = this.toastCtrl.create({
      message: mssg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  
  }
}
