import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { promise } from 'selenium-webdriver';


@Injectable()
export class CartBillingProvider {
  public res = 0;
  constructor(public http: HttpClient, private storage: Storage) {
    console.log('Hello BillingProvider Provider');
  }

  ionViewDidLeave(){

  }

  async getBill(): Promise<any> {
    return this.calc();
  }

  async calc() {
    this.storage.get('CARTITEM').then((value) => {
      if (value == null || value.length == 0) {
        return this.res;
      }
      else {
        for (let i = 0; i < value.length; i++) {
          console.log(Number(value[i].amount), Number(value[i].quantity))
          this.res = Number(this.res) + (Number(value[i].amount) * Number(value[i].quantity));
          console.log(this.res)
       
        }
        console.log(value.length)

      }
    });
    return await Promise.resolve(this.res);
  }


  updateQuantity(i,newQuantity)
{
  if(this.res[i].type == "freeze")
  {
    console.log("Quantity cannot be changed for freezing memebership");
  }
  else
  {
    this.res[i].quantity = newQuantity;

    // Push updates res object
  }
}
}


