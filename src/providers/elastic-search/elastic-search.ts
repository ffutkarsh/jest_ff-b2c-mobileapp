import { Injectable } from '@angular/core';
import * as elasticsearch from 'elasticsearch';
import 'rxjs/add/operator/map';

/*
  Generated class for the ElasticSearchProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ElasticSearchProvider {
  public client: elasticsearch.Client
  
  private searchVar :any;
  constructor() {
    console.log('Hello ElasticSearchProvider Provider');
  }

  
  public CheckNo(q){
  
    let re =/^[0-9]*$/;

    console.log(re.test(q))
    return re.test(q);
  
  }

  SetBody(q){

    
    let body ={};
  
  
    if(q!=''){  
    if(this.CheckNo(q)){
      
          body = {
         
          "query":{  
             "bool":{  
                "must":[  
                   {  
                      "multi_match":{  
                         "query":q,
                         "fields":[    "meco_commonid", "meco_mobileno",  "meco_secondmobileno", "meco_officetelno",
                          "meco_fax", "meco_homeno", "meco_enrollmentkey_new"  ]
                      }
                   }
                ],
                 "should" : [
               { "term" : { "meco_type" : "member" } }
             ],
               "minimum_should_match" : 1,
             "boost" : 1.0,
                     
                "filter":
                  {  
                        "terms": {
                             "meco_tenantid": []
                                                   }
          }
                    } }
             } }
  
        else{
          body = 
            {
       "query": 
            {"bool": {"must": [ { "multi_match": {"query": q , "fields":
             [ "meco_nameoforg","meco_firstname^3","meco_lastname^3",
            "meco_emailaddress","meco_middlename","meco_nationalid","meco_enrollmentkey"],"fuzziness":1}}],
            "should": [ { "term": { "meco_homeclubtenantid": 45
           } },
            { "term": { "meco_type": "member" } }],"boost": 2,
            
            "filter":
            
          { "terms": {"meco_tenantid": [45] }}}}
          }
        
        
        
        
        }
      }
  return body
  
  
  }
  
  
    
   public   search(q,count){
     this.client=this.connect();
       count=this.searchVar;
  
       let body=this.SetBody(q);
   
  return  this.client.search({
          index: 'ffcustomer_local',
          size:10,
          from:count,
          type:'customer',
          body:body
        });
  
      }
  
  
  
  
      public connect() {
  return new elasticsearch.Client({
          host: 'https://search-comapny-r4kn7bsm4pfp6fqfktssgft6t4.us-east-1.es.amazonaws.com',
          log: 'trace'
        });
  
       // return 
      }
  
  
  }
