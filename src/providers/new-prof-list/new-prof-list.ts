import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the NewProfListProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NewProfListProvider {

  constructor(public http: Http) {
    console.log('Hello NewProfListProvider Provider');
  }

  //call from page comes here
  getProfList() {
    return this.http.get('assets/NewProfPurposeList.json')
  }


}
