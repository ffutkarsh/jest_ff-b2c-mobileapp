import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class MyMembershipProvider {
public len=0;
public arr : any;
  constructor(public http: HttpClient) {
    console.log('Hello MyMembershipProvider Provider');
  }

  //call from page comes here
  getMyMembershipDetails(): Observable<any> {
    return this.http.get<any>('assets/myMembership.json');
  }

  getPurchaseDetails(){
    return this.http.get('assets/purchase-history-list.json');
  }



}

 