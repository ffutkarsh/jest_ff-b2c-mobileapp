
import { Settings } from './settings/settings';
import { CartItemsProvider } from '../providers/cart-items/cart-items';
import { CartCouponsProvider } from '../providers/cart-coupons/cart-coupons';
import { RelatedItemsProvider } from '../providers/related-items/related-items';
import {BillingProvider} from '../providers/billing/billing';
 import {ClubLocatorProvider} from '../providers/club-locator/club-locator'
 import { MyMembershipProvider } from '../providers/my-membership/my-membership';
 import { CategoryListProvider } from '../providers/category-list/category-list';
 import { VideoCategoryProvider } from '../providers/video-category/video-category';
 import { ReferralProvider } from '../providers/referral/referral';
 import { FetchprofileProvider } from "../providers/fetchprofile/fetchprofile";

export {
    MyMembershipProvider,
    CartItemsProvider,
    CartCouponsProvider,
    RelatedItemsProvider,
    BillingProvider,
    ClubLocatorProvider,
    Settings,
    CategoryListProvider,
    VideoCategoryProvider, 
    ReferralProvider,
    FetchprofileProvider
};
