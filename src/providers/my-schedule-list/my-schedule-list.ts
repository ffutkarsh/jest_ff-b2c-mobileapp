import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { from } from 'rxjs/observable/from';
import { HttpClient } from '@angular/common/http';
import { ScheduleList } from '../../apiInterfaces/apiInterfaces';


@Injectable()
export class MyScheduleListProvider {

  constructor(public http: HttpClient) {
    console.log('Hello MyScheduleListProvider Provider');
  }

  getSchedulelist():Observable<ScheduleList[]> {
    return this.http.get<ScheduleList[]>('assets/Myschedules.json');
  }

}
