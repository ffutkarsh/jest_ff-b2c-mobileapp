import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Offers, ScheduleList, IClasslist, SelectPurpose, SelectFacility } from '../../apiInterfaces/apiInterfaces';

@Injectable()
export class DashboardFlowApiProvider {

  constructor(public http: HttpClient) { }

  //FETCHING API FOR OFFER DETAILS.
  public getOfferDetails(): Observable<Offers[]> {
    return this.http.get<Offers[]>('assets/OffersApi.json');
  }

  //FETCHING API FOR SCHEDULE DETAILS.
  public getSchedulelist(): Observable<ScheduleList[]> {
    return this.http.get<ScheduleList[]>('assets/Myschedules.json');
  }

  //FETCHING API FOR CLASSES DETAILS.
  public getClassList(): Observable<IClasslist[]> {
    return this.http.get<IClasslist[]>('assets/classlist.json');
  }

  //FETCHING API FOR PURPOSES DETAILS.
  public getPurposeList(): Observable<SelectPurpose> {
    return this.http.get<SelectPurpose>('assets/PurposeListApi.json');
  }

  //FETCHING API FOR FACILITY DETAILS.
  getSpecificFacilityDetails(): Observable<SelectFacility> {
    return this.http.get<SelectFacility>('assets/selectFacilityApi.json');
  }
}
