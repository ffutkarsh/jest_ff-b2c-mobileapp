import { Component, ViewChild, OnInit } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';
import { Config, Nav, Platform, ToastController, App } from 'ionic-angular';
import { Events, MenuController } from 'ionic-angular';
import { FirstRunPage, DashboardPage, MainPage, RatingWidgetModalPage } from '../pages/pages';
import { Settings } from '../providers/providers';
import { configs } from '../config/config'
import { Http } from '@angular/http';
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';
import { NetworkProvider } from '../providers/network/network';
import { LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ModalController, AlertController } from 'ionic-angular';
import { LoginFlowApiProvider } from '../providers/login-flow-api/login-flow-api';
import { Item } from 'ionic-angular/components/item/item';
import { count } from 'rxjs/operators/count';
import { FCM } from '@ionic-native/fcm';
import { zip } from 'rxjs/operators/zip';

export interface PageInterface {
  title: string;
  name: string;
  component: any;
  icon: string;
  logsOut?: boolean;
}

@Component({
  template: `<ion-menu id="myMenu" [content]="content">

    <ion-content style="background-color:#F8F9FD">
      <ion-list style="background-color:#F8F9FD" >

     <!-- <ion-img width="80" height="80" src="../../assets/img/ff.png">-->
     <img  padding width="300px" height="100px" class="footimg" src="assets/img/fflogo.png" alt="Fitness Force" />
      
      
    <!--  </ion-img>-->
        <button style="background-color:#F8F9FD "  menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">
        <ion-icon text-center style=" -moz-border-radius:50%;
        position: relative;
        display: inline-block;

        -webkit-border-radius:50%;
        border-radius: 50%;
        width: 40px;
        height: 40px; 
        /* width and height can be anything, as long as they're equal */
        text-align:center;
        background-color:#D1D1DA;
        color:white;
        font-size:30px;
        margin-top:8px !important ;
        
        "name="{{p.icon}}"
        ></ion-icon>
        &nbsp;
         <span    style="margin-top:8px !important ; padding-top 10px;" 
          >  {{p.title}}  </span> 
        </button>
      </ion-list>
    </ion-content>

  </ion-menu>
  <ion-nav #content [root]="rootPage"></ion-nav>`
})

export class MyApp {
  public counter = 0;
  rootPage: any;
  loader: any;
  popflag = false;

  @ViewChild(Nav) nav: Nav;
  pages: any[] = [
    { title: 'My Profile', component: 'ProfileNewPage', icon: 'ios-person' },
    { title: 'Dashboard', component: 'DashboardPage', icon: 'md-trending-up' },
    { title: 'Schedule', component: 'SchedulingPage', icon: 'ios-clock' },
    { title: 'Buy Memberships', component: 'BuyMainPage', icon: 'ios-cart' },
    { title: 'My Memberships', component: 'NewMyMembershipsPage', icon: 'ios-person' },
    { title: 'Feedback', component: 'FeedbackPage', icon: 'ios-chatbubbles' },
    { title: 'About', component: 'AboutPage', icon: 'ios-create' },
    { title: 'Logout', component: 'LogoutconfirmPage', icon: 'md-power' },
    { title: 'ClubLocator', component: 'ClubLocatorPage', icon: 'md-power' },
    { title: 'Class Rating', component: 'ClassRatingPage', icon: 'md-power' },
    { title: 'Class Attendance', component: 'ClassAttendancePopupPage', icon: 'md-power' },
    { title: 'ChatListPage', component: 'ChatListPage', icon: 'md-power' },
    { title: 'Videos', component: 'VideoCategoryPage', icon: 'md-power' }
  ]

  constructor(
    // private ionicApp: IonicApp,
    public app: App,
    public alertCtrl: AlertController,
    private fcm: FCM,
    private translate: TranslateService,
    public network: Network,
    public networkProvider: NetworkProvider,
    private http: Http,
    private httpmodule: HttpModule,
    public events: Events,
    public menu: MenuController,
    public settings: Settings,
    public toastCtrl: ToastController,
    private config: Config, private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    public platform: Platform,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    public modalCtrl: ModalController,
    public LFP: LoginFlowApiProvider) {
    platform.ready().then(() => {
      splashScreen.hide();
      this.hideSplashScreen()
      statusBar.styleDefault();

      platform.registerBackButtonAction(() => {
        let nav = app.getActiveNav();
        let activeView = nav.getActive();
        if (nav.canGoBack()) { //Can we go back?
          nav.pop();
        }
        else if (activeView.isOverlay) {
          activeView.dismiss();
        }
        else {
          // let activePortal = this.ionicApp._modalPortal.getActive();
          // if (activePortal) {
          //   activePortal.dismiss();
          // } else {
          const alert = this.alertCtrl.create({
            title: 'Do you want to close the app?',
            buttons: [{
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                alert.dismiss();
                console.log('Application exit prevented!');
              }
            }, {
              text: 'Close App',
              handler: () => {
                this.platform.exitApp(); // Close this application
              }
            }]
          });
          alert.present();
        }
      });

      // platform.registerBackButtonAction(()=> {
      //   if(this.counter == 0) {
      //     this.counter++;

      //     this.presentToast();
      //     setTimeout(() => { this.counter = 0 }, 3000)
      //   }
      //   if(this.counter > 0)
      //   {
      //     platform.exitApp()
      //   }
      //   if(this.nav.canGoBack())
      //   {
      //     this.nav.pop();
      //   }
      //   else {
      //     platform.exitApp()
      //   }
      // },0)


      // this.platform.registerBackButtonAction(() => {
      //     if (this.navCtrl.canGoBack()) { // CHECK IF THE USER IS IN THE ROOT PAGE.
      //       this.navCtrl.pop(); // IF IT'S NOT THE ROOT, POP A PAGE.
      //     } else {
      //       this.platform.exitApp(); // IF IT'S THE ROOT, EXIT THE APP.
      //     }
      //   });



    })

    this.initSettingsandconf()
    this.listenToLoginEvents();
    this.initTranslate();

    this.settings.getKeys("companyid").then((val) => { console.log(val) });

    this.presentLoading(); //CALLING LOADER FUNCTION.

    this.platform.ready().then(() => {

      this.storage.get('signuplangShown').then((result) => { //GETTING THE 'SIGNUPLANGSHOWN' KEY TO SET THE ROOT PAGE.

        if (result) {
          //ONCE THE 'SIGNUPLANGSHOWN' KEY IS SET TO TRUE, THE ROOT PAGE WILL BE LOGIN PAGE OR DASHBOARD PAGE.
          this.storage.get('loggedIn').then((login) => { //GETTING THE 'LOGGEDIN' KEY TO SET THE ROOT PAGE.
            if (login) {
              //IF USER HAS LOGGED IN.
              this.rootPage = DashboardPage;
              this.storage.get('langSelect').then((langSel) => { //GETTING THE KEY OF SELECTED LANGUAGE TO USE THE TRANSLATE SERVICE.
                this.translate.use(langSel); //USE OF TRANSLATE SERVICE.
              });
            } else {
              //IF USER HAS NOT LOGGED IN OR IS LOGGED OUT.
              this.rootPage = MainPage;
              this.storage.get('langSelect').then((langSel) => { //GETTING THE KEY OF SELECTED LANGUAGE TO USE THE TRANSLATE SERVICE.
                this.translate.use(langSel); //USE OF TRANSLATE SERVICE.
              });
            }
          });

        } else {
          // [FIRSTRUNPAGE - SIGNUPLANGUAGEPAGE]
          this.rootPage = FirstRunPage; //INITIAL ROOT PAGE [ONE TIME RUN FOR A APPLICATION].

        }

        this.loader.dismiss(); //DISMISSING THE LOADER.

      });




      //Notifications
      fcm.subscribeToTopic('all');
      fcm.getToken().then(token => {
        console.log(token);

        let postdata = {
          "client_id": "007",
          "FcmKey": "" + token
        };

        this.http.post("http://35.165.61.223:3000/RegisterClient", postdata).subscribe(data => {
          console.log(data);
        }, error => {
          console.log(error);
        });

      })
      fcm.onNotification().subscribe(data => {
        console.log("printing data", { 'notificationData': data });
        // alert("printing data"+JSON.stringify(data));

        this.storage.get('loggedIn').then((login) => {
          if (login) {

            if (data.payload == "rating") {
              this.popflag = true;
              setTimeout(() => {
                this.nav.push("ClassRatingPage", { 'notificationData': data });

              }, 1500);

            }
            else
              if (data.type == "appointment") {
                this.popflag = true;
                setTimeout(() => {
                  this.nav.push("AppointmentAttendancePopupPage", { 'notificationData': data });
                }, 1500);
              }
              else
                if (data.type == "class") {
                  this.popflag = true;
                  setTimeout(() => {
                    this.nav.push("ClassAttendancePopupPage", { 'notificationData': data });
                  }, 1500);
                }





          }
        })


        if (data.wasTapped) {
          console.log("Received in background");
        } else {
          console.log("Received in foreground");
        };
      })
      fcm.onTokenRefresh().subscribe(token => {
        console.log(token);

        let postdata = {
          "client_id": "007",
          "FcmKey": "" + token
        };

        this.http.post("http://35.165.61.223:3000/RegisterClient", postdata).subscribe(data => {
          console.log(data);
        }, error => {
          console.log(error);
        });
      });
      //end notifications.
    }
    );

    this.storage.get('loggedIn').then((val) => {
      if (val && !this.popflag) {
        //this.widgetRatingModal();
      }
    });

    /* calling rating widget model on constructor loading */
    // this.presentModal();



  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Press again to exit",
      duration: 3000,
      position: "middle"
    });
    toast.present();
  }
  hideSplashScreen() {
    if (this.splashScreen) {
      setTimeout(() => {
        this.splashScreen.hide();
      }, 100);
    }
  }



  //DISPLAY LOADER.
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Authenticating..."
    });
    this.loader.present();
  }

  initSettingsandconf() {
    let flag
    this.settings.checkkeys().then((val) => {
      if (val) {
        flag = false;
      } else { flag = true; }
      console.log(flag)
      if (flag) {
        let keyarr = Object.keys(configs);
        keyarr.forEach(element => {
          this.settings.setKeys(element, configs[element]);
        });
      }
    });
  }

  listenToLoginEvents() {
    this.events.subscribe('user:login', () => {
      this.enableMenu(true);
    });

    this.events.subscribe('logout', () => {
      this.enableMenu(false);
    });
  }

  enableMenu(loggedIn: boolean) {
    this.menu.enable(loggedIn);
  }

  //INITIALIZING NgX TRANSLATE FUNCTIONALITY.
  initTranslate() {
    this.translate.setDefaultLang('en');

    if (this.translate.getBrowserLang() !== undefined) {
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.translate.use('en');
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

  //TODO
  //TO PASS SEARCH PARAMETER TO CLIENT SEARCH PAGE 
  openPage(page) {
    this.nav.setRoot(page.component);
  }

  /* widget model rating method */
  widgetRatingModal() {
    this.storage.set('WidgetRating', this.LFP.RatinWidgetData).then(() => {
      this.storage.get('WidgetRating').then((val) => {
        console.log(val);
        let widgetlocalData = val;
        //moment 
        function compare(a, b) {
          const dateA = a.ratingDate;
          const dateB = b.ratingDate;
          let comparison = 0;
          if (dateA > dateB) {
            comparison = 1;
          } else if (dateA < dateB) {
            comparison = -1;
          }
          return comparison;
        }
        console.log(widgetlocalData.sort(compare));
        let localData = widgetlocalData.sort(compare);

        const result = localData.filter(res => res.ratingFlag == false);
        console.log(result);
        this.storage.set('WidgetRating', result)
        if (result[result.length - 1].ratingFlag == false) {
          const modal = this.modalCtrl.create(RatingWidgetModalPage);
          modal.present();
        }
      });
    })
  }




  /* widget model rating method */
  // presentModal() {
  //   this.storage.set('WidgetRating', this.LFP.RatinWidgetData).then(() => {
  //     this.storage.get('WidgetRating').then((val) => {
  //       console.log(val);
  //       let widgetlocalData = val;
  //       function compare(a, b) {
  //         const dateA = a.ratingDate;
  //         const dateB = b.ratingDate;
  //         let comparison = 0;
  //         if (dateA > dateB) {
  //           comparison = 1;
  //         } else if (dateA < dateB) {
  //           comparison = -1;
  //         }
  //         return comparison;
  //       }
  //       console.log(widgetlocalData.sort(compare));

  //       if (widgetlocalData[widgetlocalData.length-1].ratingFlag == false) {
  //         const modal = this.modalCtrl.create(RatingWidgetModalPage);
  //         modal.present().then(() => {
  //           widgetlocalData[widgetlocalData.length-1].ratingFlag = true;
  //           this.LFP.RatinWidgetData[this.LFP.RatinWidgetData.length-1].ratingFlag = widgetlocalData[widgetlocalData.length-1].ratingFlag;
  //           this.storage.set('WidgetRating', this.LFP.RatinWidgetData)
  //         })
  //       }
  //     });
  //   })
  // }

}