import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { CallNumber } from '@ionic-native/call-number';
import { RepeatBookingProvider } from '../providers/repeat-booking/repeat-booking';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { RepeatBookingAuthProvider } from '../providers/repeat-booking-auth/repeat-booking-auth';
import { EmailComposer } from '@ionic-native/email-composer';
import { OtpHelperProvider } from '../providers/otp-helper/otp-helper';
import { ReferralProvider } from '../providers/providers';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { AboutPage } from '../pages/about/about';
import { Geolocation } from '@ionic-native/geolocation';
import { BuymembershipProvider } from '../providers/buymembership/buymembership';
import { MomentModule } from 'angular2-moment';
import { Settings } from '../providers/providers';
import { Camera } from '@ionic-native/camera';
import {  Storage } from '@ionic/storage';
import { ClubLocatorProvider } from '../providers/providers';
import { UserChatsProvider } from '../providers/user-chats/user-chats';
import { CartCouponsProvider, RelatedItemsProvider, BillingProvider } from '../providers/providers';
import { DateTimeAuthProvider } from '../providers/date-time-auth/date-time-auth';
import { MyMembershipProvider } from '../providers/providers';
import { CheckPermissionsProvider } from '../providers/check-permissions/check-permissions';
import { ClassesFlowApiProvider } from '../providers/classes-flow-api/classes-flow-api';
import { LanguageProvider } from '../providers/language/language';
import { ChooseTrainerAuthProvider } from '../providers/choose-trainer-auth/choose-trainer-auth';
import { DashboardFlowApiProvider } from '../providers/dashboard-flow-api/dashboard-flow-api';
import { DashboardAuthProvider } from '../providers/dashboard-auth/dashboard-auth';
import { CountryModalPage } from '../pages/country-modal/country-modal';
import { LoginFlowApiProvider } from '../providers/login-flow-api/login-flow-api';
import { CenterSelectAuthProvider } from '../providers/center-select-auth/center-select-auth';
import {IpgeofetchProvider} from "../providers/ipgeofetch/ipgeofetch"
import { ValidPhoneProvider } from '../providers/valid-phone/valid-phone';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { DashboardFlowAuthProvider } from '../providers/dashboard-flow-auth/dashboard-flow-auth';
import { GeogetterProvider } from '../providers/geogetter/geogetter';
import { NetworkProvider } from '../providers/network/network';
import { CartBillingProvider } from '../providers/cart-billing/cart-billing';
import { File } from '@ionic-native/file';
import { CartItemsProvider } from '../providers/providers';
import { FCM } from '@ionic-native/fcm';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Network } from '@ionic-native/network';
import { Screenshot } from '@ionic-native/screenshot';
import { SelectTrainerProvider } from '../providers/select-trainer/select-trainer-api';
import { ScheduleAuthProvider } from '../providers/schedule-auth/schedule-auth';
import { IonicStorageModule } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AboutContentProvider } from '../providers/about-content/about-content';
import { IonicImageLoader } from 'ionic-image-loader';
import { CacheModule } from "ionic-cache";
import { SQLite } from '@ionic-native/sqlite';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { ErrorProvider } from '../providers/error/error';                       
import { MembershipProvider } from '../providers/membership/membership';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ElasticSearchProvider } from '../providers/elastic-search/elastic-search';
import { LOCALE_ID } from '@angular/core';
import { PopoverController } from 'ionic-angular';
import { FormsModule } from '@angular/forms';
import { AnyTrainerPage } from '../pages/any-trainer/any-trainer';
import { NewProfileDetailsProvider } from '../providers/new-profile-details/new-profile-details';
import { NewProfListProvider } from '../providers/new-prof-list/new-prof-list';
import { CartItemsPage } from '../pages/cart-items/cart-items';  
import { ApplyCouponModalPage } from '../pages/apply-coupon-modal/apply-coupon-modal'; 
import { SignuptermsAuthProvider } from '../providers/signupterms-auth/signupterms-auth';
import { WaiverTermsProvider } from '../providers/waiver-terms/waiver-terms';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { MyScheduleListProvider } from '../providers/my-schedule-list/my-schedule-list';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { MyProfileProvider } from '../providers/my-profile/my-profile';  
import { MyWalletProvider } from '../providers/my-wallet/my-wallet';
import { VideoCategoryProvider } from '../providers/video-category/video-category';
import { CategoryListProvider } from '../providers/category-list/category-list';
import { NavController } from 'ionic-angular'

const config: SocketIoConfig = { url: 'http://35.165.61.223:3001', options: {} }

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
 
export function provideSettings(storage: Storage) {
  return new Settings(storage, {
    option1: true,
    option2: 'Ionitron J. Framework',
    option3: '3',
    option4: 'Hello'
  });
}

@NgModule({
  declarations: [
    CountryModalPage,
    AnyTrainerPage,
    MyApp
 
  
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    MomentModule,
    HttpModule,
    IonicImageLoader.forRoot(),
    CacheModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    HttpClientModule,
    IonicStorageModule.forRoot(),
    SocketIoModule.forRoot(config),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    CountryModalPage,
    CartItemsPage,ApplyCouponModalPage,AnyTrainerPage,
   
   
  ],
  providers: [
    StatusBar,
    SelectTrainerProvider,
    PopoverController,
    SplashScreen,
    EmailComposer,
    LanguageProvider,
    ClubLocatorProvider,
    SQLite,
    CallNumber,
    ErrorProvider,MembershipProvider,ElasticSearchProvider,NewProfileDetailsProvider,
    ChooseTrainerAuthProvider,
    UserChatsProvider,
    Camera,
    Screenshot,
    GoogleAnalytics,
    TranslateService,
    MyScheduleListProvider,
    CartBillingProvider,
    BuymembershipProvider,
    InAppBrowser,
    ReferralProvider,
    Geolocation,
    SocialSharing,
    File,
    OpenNativeSettings,
    MyMembershipProvider,
    CountryModalPage,
    AnyTrainerPage,
    DateTimeAuthProvider,
    LaunchNavigator,
    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    CenterSelectAuthProvider,
    ClassesFlowApiProvider,
    RelatedItemsProvider,
    CheckPermissionsProvider,
    NativeGeocoder,
    ValidPhoneProvider,
    RepeatBookingAuthProvider,
    BillingProvider,
    CartCouponsProvider,
    OtpHelperProvider,
    LoginFlowApiProvider,
    MyProfileProvider,MyWalletProvider,VideoCategoryProvider,CategoryListProvider,
    NetworkProvider,
    DashboardAuthProvider,
    NewProfListProvider,SignuptermsAuthProvider,WaiverTermsProvider,ScreenOrientation,
    Network,
    FCM,
    DashboardFlowAuthProvider,
    GeogetterProvider,
    IpgeofetchProvider,
    AuthServiceProvider,
    CartItemsProvider,
    DashboardFlowApiProvider,
    RepeatBookingProvider,
    ScheduleAuthProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AboutContentProvider,
  ]
})
export class AppModule {}
