// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { FacilityBookedDetailsPage } from './facility-booked-details';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';
// import { Screenshot } from '@ionic-native/screenshot';
// import { SocialSharing } from '@ionic-native/social-sharing';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// export function provideStorage() { return new Storage(); }

// describe('FacilityBookedDetailsPage - UI Part', () => {
//     let de: DebugElement;
//     let comp: FacilityBookedDetailsPage;
//     let fixture: ComponentFixture<FacilityBookedDetailsPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 FacilityBookedDetailsPage
//             ],
//             imports: [
//                 IonicModule.forRoot(FacilityBookedDetailsPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 Screenshot,
//                 SocialSharing
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Header to be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".header")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Share Icon should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".iconshare")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Edit Icon should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".iconEdit")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Facility Details row should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".tselection")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Facility Details div should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".trainerDetails")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Facility Avatar should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".anytavatar1")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Facility Iamge should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".anytavatarimg")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Facility Name should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".FacilityName")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Date and Time icon should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".DateAndTimeIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Date and Time should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".AppointmentDateAndTime")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Facility icon should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".PurposeIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Facility should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".AppointmentPurpose")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Facility Court should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".FacilityCourt")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Venue icon should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".VenueIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Facility Venue should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".AppointmentVenue")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Facility status icon should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".AppointmentStatusIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Facility status should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".AppointmentStatus")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Gotoschedule button column should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".GoToScheduleColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Gotoschedule button should be present.", async () => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage);
//         de = fixture.debugElement.query(By.css(".GoToScheduleButton")).nativeElement;
//         expect(de).toBeDefined();
//     });

// });

// describe('FacilityBookedDetailsPage - Methods', () => {
//     let de: DebugElement;
//     let comp: FacilityBookedDetailsPage;
//     let fixture: ComponentFixture<FacilityBookedDetailsPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 FacilityBookedDetailsPage
//             ],
//             imports: [
//                 IonicModule.forRoot(FacilityBookedDetailsPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 Screenshot,
//                 SocialSharing
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(FacilityBookedDetailsPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Screenshot method should be present.", async () => {
//         expect(comp.screenShot).toBeDefined();
//     });

//     it("Reset method should be present.", async () => {
//         expect(comp.reset).toBeDefined();
//     });

//     it("Appointment Popover Method should be present.", async () => {
//         expect(comp.presentPopover).toBeDefined();
//     });

// });






