import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { FacilityBookedDetailsPage } from './facility-booked-details';
import { Screenshot } from '@ionic-native/screenshot';
import { SocialSharing } from '@ionic-native/social-sharing';

@NgModule({
  declarations: [
    FacilityBookedDetailsPage
  ],
  imports: [
    IonicPageModule.forChild(FacilityBookedDetailsPage),
    TranslateModule
  ],
  providers: [
    Screenshot,
    SocialSharing
  ]
})
export class FacilityBookedDetailsPageModule { }
