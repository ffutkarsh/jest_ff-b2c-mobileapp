import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import {
  FacilityPopoverPage,
  SchedulingPage
} from '../../pages/pages';
import { Storage } from '@ionic/storage';
import { Screenshot } from '@ionic-native/screenshot';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-facility-booked-details',
  templateUrl: 'facility-booked-details.html',
})
export class FacilityBookedDetailsPage {

  public fetchdetails; //APPOINTMENT DETAILS OBJECT.

  state: boolean = false; //SET THE STATE OF SCREENSHOT TO FALSE AFTER CERTAIN TIME OUT.

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public translateService: TranslateService,
    private storage: Storage,
    private screenshot: Screenshot,
    private socialSharing: SocialSharing) {

    this.fetchdetails = this.navParams.get('objectOfF');

  }

  ionViewDidLoad() { }

  ///showing time format 
  showtime() {
    let start_Time = moment(this.fetchdetails.StartTime, "hh:mm A");
    let startstring = start_Time.format("hh:mm A");
    let duration = (this.fetchdetails.Duration + "").replace(" h", "");
    let array = duration.split(".");
    let result = start_Time.add(array[0], 'hours');
    result = start_Time.add(array[1], 'minutes');
    return startstring + "  -  " + result.format("hh:mm A");
  }

  //TO TAKE SCREENSHOT.
  screenShot() {
    this.screenshot.URI(80)
      .then((res) => {
        this.socialSharing.share('', '', res.URI, '')
          .then(() => { },
            () => {
              alert('SocialSharing failed');
            });
      },
        () => {
          alert('Screenshot failed');
        });
  }

  // Reset function we will use to hide the screenshot preview after 1 second
  reset() {
    var self = this;
    setTimeout(function () {
      self.state = false;
    }, 2000);
  }

  //EDIT APPOINTMENT POPOVER.
  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(FacilityPopoverPage, {
      'purpose': this.fetchdetails.PurposeName,
      'icon': this.fetchdetails.PurposeLogo,
      'trainername': this.fetchdetails.UserName,
      'trainerIcon': this.fetchdetails.UserImage,
      'date': this.fetchdetails.Date,
      'availableSlot': this.fetchdetails.StartTime
    });
    popover.present({
      ev: myEvent
    });
  }

  //NAVIGATING TO SCHEDULING PAGE.
  GotoSchedule() {
    this.navCtrl.setRoot(SchedulingPage);
  }

}
