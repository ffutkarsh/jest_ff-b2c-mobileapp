// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { CartWaiverEsignPage } from './cart-waiver-esign';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { IonicStorageModule } from '@ionic/storage';
// import { SignaturePad } from 'angular2-signaturepad/signature-pad';
// import { SignaturePadModule } from  'angular2-signaturepad';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }







// describe('Login Page - UI Part', () => {
//     let de: DebugElement;
//     let comp: CartWaiverEsignPage;
//     let fixture: ComponentFixture<CartWaiverEsignPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [CartWaiverEsignPage],
//             imports: [
//                 IonicModule.forRoot(CartWaiverEsignPage),
//                 IonicStorageModule.forRoot(),SignaturePadModule
              
//             ],
//             providers: [
//                 NavController,
//                 SignaturePad,
//                 { provide: NavParams, useClass: NavParamsMock },
               
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(CartWaiverEsignPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
     
//     });

//     it('Navbar have a class', () => {
//                 const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.esignNavbar')).nativeElement;
//                 console.log(inputfield.className);
//                 expect(inputfield.className).toMatch('esignNavbar');
//             });
//         });

// //     it("Welcome Div to be present.", async () => {
// //         fixture = TestBed.createComponent(LoginPage);
// //         de = fixture.debugElement.query(By.css(".welcomeDiv")).nativeElement;
// //         expect(de).toBeDefined();
// //     });

// //     it("Welcome Div should contain title class.", async () => {
// //         fixture = TestBed.createComponent(LoginPage);
// //         de = fixture.debugElement.query(By.css(".welcomeTitle")).nativeElement;
// //         expect(de).toBeDefined();
// //     });

// //     it('Welcome Div title should match to "Welcome"', () => {
// //         const title: HTMLElement = fixture.debugElement.query(By.css('.welcomeTitle')).nativeElement;
// //         expect(title.textContent).toContain("Welcome");
// //     });

// //     it("Welcome Div should contain subtitle class.", async () => {
// //         fixture = TestBed.createComponent(LoginPage);
// //         de = fixture.debugElement.query(By.css(".welcomeSubTitle")).nativeElement;
// //         expect(de).toBeDefined();
// //     });

// //     it('Welcome Div subtitle should match to "Login with your mobile number"', () => {
// //         const title: HTMLElement = fixture.debugElement.query(By.css('.welcomeSubTitle')).nativeElement;
// //         expect(title.textContent).toContain("Login with your mobile number");
// //     });

// //     it("Input box to be present.", async () => {
// //         fixture = TestBed.createComponent(LoginPage);
// //         de = fixture.debugElement.query(By.css(".inputBox")).nativeElement;
// //         expect(de).toBeDefined();
// //     });

// //     it("Input box placeholder to be present.", async () => {
// //         const inputfieldplaceholder: HTMLInputElement = fixture.debugElement.query(By.css('.inputBox')).nativeElement;
// //         expect(inputfieldplaceholder.getAttribute('placeholder')).toMatch("Enter your mobile number");
// //     });

// //     it("Continue button to be present.", async () => {
// //         fixture = TestBed.createComponent(LoginPage);
// //         de = fixture.debugElement.query(By.css(".continueButton")).nativeElement;
// //         expect(de).toBeDefined();
// //     });

// //     it("Continue button text to be present.", async () => {
// //         const title: HTMLElement = fixture.debugElement.query(By.css('.continueButton')).nativeElement;
// //         expect(title.textContent).toContain("Continue");
// //     });

// // });

// // describe('Login Page - Validating Phone Number', () => {
// //     let de: DebugElement;
// //     let comp: LoginPage;
// //     let fixture: ComponentFixture<LoginPage>;

// //     beforeEach(async(() => {
// //         TestBed.configureTestingModule({
// //             declarations: [LoginPage],
// //             imports: [
// //                 IonicModule.forRoot(LoginPage),
// //                 IonicStorageModule.forRoot(),
// //                 HttpModule
// //             ],
// //             providers: [
// //                 NavController,
// //                 Http,
// //                 ConnectionBackend,
// //                 { provide: NavParams, useClass: NavParamsMock },
// //                 ValidPhoneProvider,
// //                 GeogetterProvider,
// //                 Geolocation,
// //                 NativeGeocoder,
// //                 AuthServiceProvider,
// //                 DashboardFlowAuthProvider
// //             ]
// //         });
// //     }));

// //     beforeEach(() => {
// //         fixture = TestBed.createComponent(LoginPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
// //         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
// //         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
// //     });

// //     it('Validating Phone Number - dynamic() - Trimming Character', () => {
// //         comp.phoneNumber = "123456a";
// //         comp.dynamic();
// //         expect(comp.phoneNumber).toMatch("123456");
// //     });

// //     it('Validating Phone Number - dynamic() - Trimming Space ', () => {
// //         comp.phoneNumber = "123 4";
// //         comp.dynamic();
// //         expect(comp.phoneNumber).toMatch("123");
// //     });

// //     it('Validating Phone Number - checkifNo() - Valid Number ', () => {
// //         comp.phoneNumber = "123456";
// //         expect(comp.checkifNo(comp.phoneNumber)).toBe(true);
// //     });

// //     it('Validating Phone Number - checkifNo() - Invalid Number', () => {
// //         comp.phoneNumber = "123456a$";
// //         expect(comp.checkifNo(comp.phoneNumber)).toBe(false);
// //     });

// //     it('Phone Number Length Format (Single Integer)', () => {
// //         comp.getPhoneLengthWhenInteger(10);
// //         expect(comp.defaultMin == 10 && comp.defaultMax == 10).toBe(true);
// //     });

// //     it('Phone Number Length Format (to)', () => {
// //         comp.getPhoneLengthWhenTo("5 to 10");
// //         expect(comp.defaultMin == 5 && comp.defaultMax == 10).toBe(true);
// //     });

// //     it('Phone Number Length Format (comma)', () => {
// //         comp.getPhoneLengthWhenComma("6, 8, 2009");
// //         expect(comp.replaceDefaultLengthArray[0] == 6 && comp.replaceDefaultLengthArray[1] == 8 && comp.replaceDefaultLengthArray[2] == 2009).toBe(true);
// //     });

// //     it('Phone Number Length Format ()', () => {
// //         comp.getPhoneLengthWhenBracket("(684)+7");
// //         expect(comp.replaceDefaultLengthArray[0] == 684 && comp.replaceDefaultLengthArray[1] == 7).toBe(true);
// //     });

// //     it('Entered Number Length matches the actual length', () => {
// //         expect(comp.phoneNumber == 1234567890).toBe(false);
// //     });

// //     it('Space in the phone number should be trimmed', () => {
// //         comp.phoneNumber = '123 456';
// //         expect(comp.TrimSpace(comp.phoneNumber)).toMatch('123456');
// //     });

// // });