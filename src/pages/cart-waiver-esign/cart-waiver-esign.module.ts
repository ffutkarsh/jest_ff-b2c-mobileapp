import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CartWaiverEsignPage } from './cart-waiver-esign';
import { SignaturePadModule } from  'angular2-signaturepad';

@NgModule({
  declarations: [
    CartWaiverEsignPage,
  ],
  imports: [
    IonicPageModule.forChild(CartWaiverEsignPage), SignaturePadModule,
  ],
})
export class CartWaiverEsignPageModule {}
