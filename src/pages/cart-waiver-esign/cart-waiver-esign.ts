import { Component, ViewChild } from '@angular/core';
import { NavController, IonicPage, Platform, NavParams ,Loading,  LoadingController} from 'ionic-angular';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Storage } from '@ionic/storage';
import { WaiverTermsProvider } from '../../providers/waiver-terms/waiver-terms';


@IonicPage()
@Component({
  selector: 'page-cart-waiver-esign',
  templateUrl: 'cart-waiver-esign.html',
})
export class CartWaiverEsignPage {

  signature = ''; //initialiser for for storing signature data. 
  isDrawing = false; // drawing status 
  pltFlag = false; // flag for ngif condition 
  responseEsign;

  public cartProducts;
  public additionalProducts;
  public totalProducts;
  public totalPrice;
  public discountAmount;
  public loading: Loading;
  

  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  private signaturePadOptions: Object;
  public appFlowCart;
  constructor(
    private screenOrientation: ScreenOrientation,
    public waiverterms: WaiverTermsProvider,
    private plt: Platform,
    public navController: NavController,
    public storage: Storage,
    public navParams: NavParams,
    public loadingCtrl: LoadingController) {

    this.presentLoading();
    this.appFlowCart = this.navParams.get("appFlow"); //from app flow.
    //platform ready 
    this.plt.ready().then(() => {
      //setting orientation to landscape 
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE).then(()=>
    {
      this.loading.dismiss();
    }    
    );
      this.signaturePadOptions = { // Check out https://github.com/szimek/signature_pad
        'minWidth': 1, //width of the pen
        'canvasWidth': (this.plt.height()), //height of the canvas area
        'canvasHeight': (this.plt.width() - 150),  //width of the canvas area
        'backgroundColor': '#fff', //background colour for the canvas area
        'penColor': '#000' //pen colour for writing on canvas area
      };
      this.pltFlag = true;
    });

    this.cartProducts = this.navParams.get('cartProducts');
    this.additionalProducts = this.navParams.get('additionalProducts');
    this.totalProducts = this.navParams.get('totalProducts');
    this.totalPrice = this.navParams.get('totalPrice');
    this.discountAmount = this.navParams.get('discountAmount');


  }



  ionViewDidEnter() {
    // clear the signaturepad (canvas area)
    this.signaturePad.clear()
  }

  drawComplete() {
    this.isDrawing = false;
  }

  drawStart() {
    this.isDrawing = true;
  }

  cancelPad() {
    this.navController.pop()
  }

  savePad() {
    this.presentLoading();
    this.signature = this.signaturePad.toDataURL();
    console.log(this.signature);
    this.storage.set('savedSignature', this.signature);
    this.signaturePad.clear();
    this.navController.push('WaiverSuccessPage', {
      'cartProducts': this.cartProducts,
      'additionalProducts': this.additionalProducts,
      'totalProducts': this.totalProducts,
      'totalPrice': this.totalPrice,
      'discountAmount': this.discountAmount,
      'appFlow': this.appFlowCart
    }).then(()=>
    {
      console.log(this.navController.getPrevious().index);
      console.log(this.navController.getPrevious().name);

      this.navController.remove(this.navController.getPrevious().index);
      this.navController.remove(this.navController.getPrevious().index -1);
      
    }
    );
  }

  clearPad() {
    this.signaturePad.clear();
  }

  ionViewDidLeave() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT).then(
      ()=>
      {
        this.loading.dismiss();
      }
    );
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
    content: 'Please wait...'
    });
    return this.loading.present()


  }

}