import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupTermsEulaPage } from './signup-terms-eula';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SignupTermsEulaPage
  ],
  imports: [
    IonicPageModule.forChild(SignupTermsEulaPage),
    TranslateModule
  ]
})
export class SignupTermsEulaPageModule { }
