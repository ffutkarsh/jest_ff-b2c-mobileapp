import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController, MenuController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import rootReducer from '../../reducers/LoginFlow';
import * as a from '../../actions/LoginAction';
import { NetworkProvider } from '../../providers/network/network';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { Subject } from 'rxjs/Subject';
import { LoginFlowApiProvider } from '../../providers/login-flow-api/login-flow-api';
import { OtpPage } from '../../pages/pages';

@IonicPage()
@Component({
  selector: 'page-signup-terms-eula',
  templateUrl: 'signup-terms-eula.html',
})
export class SignupTermsEulaPage {

  public fetchDialCode //FROM WELCOME SCREEN PAGE.
  public fetchPhoneNumber; //FROM WELCOME SCREEN PAGE.
  public selectedCenterObject; //FROM WELCOME SCREEN PAGE.

  public otpResponse; //STORING API RESPONSE OF OTP.

  public link: string; //VARIABLE TO STORE THE LINK. [TERMS OF SERVICE/PRIVACY POLICY]

  //STRUCTURE OF INITIAL STATE OF USER OBJECT.
  public currentUserState = {
    dialCode: "",
    phoneNumber: "",
    centerName: "",
    subCenterName: "",
    enquiryType: "",
    memberType: "",
    userName: "",
    userPhoto: "",
    otp: "",
    verificationFlag: "",
    permissions: []
  };

  //CHECKING NETWORK ABSENCE.
  public myObservable = new Subject<boolean>();
  public networkAvailable;
  public networkAlert;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public menuCtrl: MenuController,
    public translateService: TranslateService,
    private storage: Storage,
    private openNativeSettings: OpenNativeSettings,
    public events: Events,
    public networkProvider: NetworkProvider,
    private loginFlowApi: LoginFlowApiProvider) {

    this.menuCtrl.enable(false, 'myMenu'); //DISABLING MENU ON SIGNUPTERMSEULA PAGE.

    this.fetchDialCode = navParams.get("defaultDialcode"); //FROM WELCOME SCREEN PAGE.
    this.fetchPhoneNumber = navParams.get("phoneNumber"); //FROM WELCOME SCREEN PAGE.
    this.selectedCenterObject = this.navParams.get('memberDetails'); //FROM WELCOME SCREEN PAGE.
    console.log("Member Details on Sign Up Terms EULA Page");
    console.log(this.selectedCenterObject);

    //CHECKING NETWORK ABSENCE.
    this.networkProvider.initializeNetworkEvents();//check network condition
    this.networkChangesSubscribe();
    this.networkAvailable = (this.networkProvider.isConnected() ? true : false);
    this.myObservable.next(this.networkAvailable);
    this.myObservable.subscribe(val => {
      if (!val) {
        this.showNetworkAlert();
      }
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupTermsEulaPage');
    this.storage.get('USERDETAILS').then((USERDETAILS) => { //FETCHING THE CURRENT USER STATE.
      this.currentUserState = USERDETAILS;
    });
  }

  ionViewDidLeave() {
    this.networkChangesUnsubscribe(); //UNSUBSCRIBE THE CHANGES.
  }

  //IN APP BROWSER FOR TERMS OF SERVICE.
  gotoTerms() {
    this.link = 'https://www.google.com/'; //LINK TO TERMS OF SERVICE.
    this.navCtrl.push('InAppBrowserPage', { 'link': this.link }); //PASSING THE LINK TO IN APP BROWSER PAGE.
  }

  //IN APP BROWSER FOR PRIVACY POLICY.
  gotoPolicy() {
    this.link = 'https://www.fitnessforce.com/'; //LINK TO PRIVACY POLICY.
    this.navCtrl.push('InAppBrowserPage', { 'link': this.link }); //PASSING THE LINK TO IN APP BROWSER PAGE.
  }

  //MOVING TO OTP PAGE.
  gotoOTP() {
    // this.addUserName();  //ADDING USERNAME TO CURRENT USER STATE.
    // this.addUserPhoto();  //ADDING USERPHOTO TO CURRENT USER STATE.
    this.getOtpResponse(this.fetchDialCode, this.fetchPhoneNumber, this.selectedCenterObject.companyID, this.selectedCenterObject.tenantID)
  }

  // //ADDING USERNAME TO CURRENT USER STATE.
  // addUserName() {
  //   this.currentUserState = rootReducer(this.currentUserState, a.addUserName(this.userName));
  //   console.log("Add Username: ");
  //   console.log(this.currentUserState);
  //   this.storelocally();
  // }

  // //ADDING USERPHOTO TO CURRENT USER STATE.
  // addUserPhoto() {
  //   this.currentUserState = rootReducer(this.currentUserState, a.addUserPhoto(this.userPhoto));
  //   console.log("Add Userphoto: ");
  //   console.log(this.currentUserState);
  //   this.storelocally();
  // }

  //SAVES DATA TO LOCAL DATABASE.
  storelocally() {
    this.storage.set('USERDETAILS', this.currentUserState).then((val) => {
      this.storage.get('USERDETAILS').then((val) => {
        console.log(val);
      });
    });
  }

  //SUBSCRIBE TO NETWORK CHANGES.
  networkChangesSubscribe() {
    // Offline event
    this.events.subscribe('network:offline', () => {  //method to check internet
      this.myObservable.next(false);
      this.networkAvailable = false;
    });

    // Online event
    this.events.subscribe('network:online', () => {
      this.myObservable.next(true);
      this.networkAvailable = true;
    });
  }

  //UNSUBSCRIBE TO NETWORK CHANGES.
  networkChangesUnsubscribe() {
    this.events.unsubscribe('network:offline');
    this.events.unsubscribe('network:online');
  }

  //SHOWING ALERT ON NETWORK ABSENCE.
  showNetworkAlert() {
    this.networkAlert = this.alertCtrl.create({
      title: 'No Internet Connection',
      message: 'Please check your internet connection.',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Retry',
          handler: () => {
            if (this.networkAvailable == false) {
              this.showNetworkAlert();
            }
          }
        },
        {
          text: 'Open Settings',
          handler: () => {
            this.networkAlert.dismiss().then(() => {
              this.showSettings();
              if (this.networkAvailable == false) {
                this.showNetworkAlert();
              }
            });
          }
        }
      ]
    });
    this.networkAlert.present();
  }

  //MOVING TO SETTINGS ON CLICKING SHOW SETTINGS.
  showSettings() {
    if ((<any>window).cordova && (<any>window).cordova.plugins.settings) {
      console.log('openNativeSettingsTest is active');
      (<any>window).cordova.plugins.settings.open("settings", function () {
        console.log('opened settings');
      },
        function () {
          console.log('failed to open settings');
        }
      );
    } else {
      console.log('openNativeSettingsTest is not active!');
    }
  }

  //FETCHING OTP RESPONSE FROM API.
  getOtpResponse(defaultDialcode, phoneNumber, companyID, tenantID) {
    if (defaultDialcode == "+91" && phoneNumber == 1002003000) {
      this.loginFlowApi.getOtpResponse().map((response) => response).subscribe((response) => {
        this.otpResponse = response;
        console.log("Fetched OTP Response From Api: ");
        console.log(this.otpResponse);
        this.navCtrl.push(OtpPage, {
          'defaultDialcode': this.fetchDialCode,
          'phoneNumber': this.fetchPhoneNumber,
          'memberDetails': this.selectedCenterObject,
          'otpResponse': this.otpResponse
        })
      });
    }
  }

}
