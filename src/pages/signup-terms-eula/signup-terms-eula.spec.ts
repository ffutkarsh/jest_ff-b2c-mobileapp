// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { SignupTermsEulaPage } from './signup-terms-eula';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from "@angular/http";
// import { Http, ConnectionBackend } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';
// import { ValidPhoneProvider } from '../../providers/valid-phone/valid-phone';
// import { GeogetterProvider } from '../../providers/geogetter/geogetter';
// import { Geolocation } from '@ionic-native/geolocation';
// import { NativeGeocoder } from '@ionic-native/native-geocoder';
// import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
// import { DashboardFlowAuthProvider } from '../../providers/dashboard-flow-auth/dashboard-flow-auth';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { OpenNativeSettings } from '@ionic-native/open-native-settings';
// import { NetworkProvider } from '../../providers/network/network';
// import { Network } from '@ionic-native/network';
// import { LoginFlowApiProvider } from '../../providers/login-flow-api/login-flow-api';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// describe('SignupTermsEula Page - UI Part', () => {
//     let de: DebugElement;
//     let comp: SignupTermsEulaPage;
//     let fixture: ComponentFixture<SignupTermsEulaPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [SignupTermsEulaPage],
//             imports: [
//                 IonicModule.forRoot(SignupTermsEulaPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 ValidPhoneProvider,
//                 GeogetterProvider,
//                 Geolocation,
//                 NativeGeocoder,
//                 AuthServiceProvider,
//                 DashboardFlowAuthProvider,
//                 TranslateService,
//                 TranslateStore,
//                 OpenNativeSettings,
//                 NetworkProvider,
//                 Network,
//                 LoginFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Page component should be created.", () => expect(comp).toBeDefined());

//     it("Ion content should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".MainContent")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section grid should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".HeadingGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section row should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".HeadingGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section column should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".HeadingGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section grid should contain the row for FitnessForce Logo.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".FFLogoRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("FitnessForce Logo should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".FFLogo")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("FitnessForce Logo source should match.", async () => {
//         const imagefield: HTMLInputElement = fixture.debugElement.query(By.css('.FFLogo')).nativeElement;
//         expect(imagefield.getAttribute('src')).toMatch('assets/img/SignupLogo.png');
//     });

//     it("Heading Section grid should contain the “Welcome” text.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".WelcomeText")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Progress Indicator grid should be present.'", () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".ProgressGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Progress Indicator row should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".ProgressGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Progress Indicator column should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".ProgressGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Progress Indicator should be present.", () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".ProgressIndicator")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Details grid should be present.'", () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".ClientDetailsGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Details row should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".ClientDetailsGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Photo column should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".ClientPhotoColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Photo row should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".ClientPhotoRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Profile Photo avatar should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".ClientProfilePhotoAvatar")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Profile Photo should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".ClientProfilePhoto")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Name and Contact column should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".ClientNameContactColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Name should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".ClientName")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Contact should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".ClientContact")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Terms and Policy grid should be present.'", () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".TermsPolicyGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Terms and Policy row should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".TermsPolicyGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Terms and Policy column should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".TermsPolicyGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Terms and Policy should be present.", () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".TermsPolicy")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Terms and Policy para should be present.", () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".para")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Continue button should be present.", async () => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage);
//         de = fixture.debugElement.query(By.css(".continueButton")).nativeElement;
//         expect(de).toBeDefined();
//     });

// });

// describe('SignupTermsEula Page - Methods and Logic Part', () => {
//     let de: DebugElement;
//     let comp: SignupTermsEulaPage;
//     let fixture: ComponentFixture<SignupTermsEulaPage>;
//     let loginFlowApi: LoginFlowApiProvider;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [SignupTermsEulaPage],
//             imports: [
//                 IonicModule.forRoot(SignupTermsEulaPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 ValidPhoneProvider,
//                 GeogetterProvider,
//                 Geolocation,
//                 NativeGeocoder,
//                 AuthServiceProvider,
//                 DashboardFlowAuthProvider,
//                 TranslateService,
//                 TranslateStore,
//                 OpenNativeSettings,
//                 NetworkProvider,
//                 Network,
//                 LoginFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(SignupTermsEulaPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//         loginFlowApi = fixture.debugElement.injector.get(LoginFlowApiProvider); //INJECTING PROVIDER.
//     });

//     it('Instance of LoginFlowApiProvider created', () => {
//         expect(loginFlowApi instanceof LoginFlowApiProvider).toBe(true);
//     });

//     it('Ionviewdidload method should be present.', () => {
//         expect(comp.ionViewDidLoad).toBeDefined(true);
//     });

//     it('Go to terms method should be present.', () => {
//         expect(comp.gotoTerms).toBeDefined(true);
//     });

//     it('Go to policy method should be present.', () => {
//         expect(comp.gotoPolicy).toBeDefined(true);
//     });

//     it('Go to otp method should be present.', () => {
//         expect(comp.gotoOTP).toBeDefined(true);
//     });

// });

