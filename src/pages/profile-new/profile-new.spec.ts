// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By }           from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams, Platform } from 'ionic-angular/index';
// import { StatusBar } from '@ionic-native/status-bar';
// import {FetchprofileProvider} from "../../providers/fetchprofile/fetchprofile"
// import { SplashScreen } from '@ionic-native/splash-screen';
// import { ProfileNewPage } from './profile-new';
// import { HttpModule } from "@angular/http";
// import { Http, ConnectionBackend } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";

// class NavParamsMock {
//   static returnParam = null;
//   public get(key): any {
//       if (NavParamsMock.returnParam) {
//           return NavParamsMock.returnParam
//       }
//       return 'default';
//   }
//   static setParams(value) {
//       NavParamsMock.returnParam = value;
//   }
// }

// describe('Page1', () => {
//   let de: DebugElement;
//   let comp: ProfileNewPage;
//   let fixture: ComponentFixture<ProfileNewPage>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [ProfileNewPage],
//       imports: [
//         IonicModule.forRoot(ProfileNewPage),
//         HttpModule,
//         HttpClientModule,
//         TranslateModule.forChild()
//       ],
//       providers: [
//         NavController,
//         FetchprofileProvider,
//         { provide: NavParams, useClass: NavParamsMock },
//         TranslateService,
//         TranslateStore
//       ]
//     });
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(ProfileNewPage);
//     comp = fixture.componentInstance;
    
//   });


//   beforeEach(() => {
//     fixture = TestBed.createComponent(ProfileNewPage);
//     comp = fixture.componentInstance;


//   });



//   it("Page component should be created.", () => expect(comp).toBeDefined());


//   it("Title should be present.", async () => {
//       fixture = TestBed.createComponent(ProfileNewPage);
//       de = fixture.debugElement.query(By.css(".titlesclass")).nativeElement;
//       expect(de).toBeDefined();
//   });

//   it("Edit Button should be present.", async () => {
//       fixture = TestBed.createComponent(ProfileNewPage);
//       de = fixture.debugElement.query(By.css(".editbtn")).nativeElement;
//       expect(de).toBeDefined();
//   });

//   it("Backround color should be present.", async () => {
//       fixture = TestBed.createComponent(ProfileNewPage);
//       de = fixture.debugElement.query(By.css(".bgclr")).nativeElement;
//       expect(de).toBeDefined();
//   });

//   it("Profile Picture should be present.", async () => {
//     fixture = TestBed.createComponent(ProfileNewPage);
//     de = fixture.debugElement.query(By.css(".profilepic")).nativeElement;
//     expect(de).toBeDefined();
//   });

//   it("Name should be present.", async () => {
//     fixture = TestBed.createComponent(ProfileNewPage);
//     de = fixture.debugElement.query(By.css(".namerow")).nativeElement;
//     expect(de).toBeDefined();
//   });

  
//   it("Client Id div should be present.", async () => {
//     fixture = TestBed.createComponent(ProfileNewPage);
//     de = fixture.debugElement.query(By.css(".clsseg")).nativeElement;
//     expect(de).toBeDefined();
//   });
  

//   it("Balance div should be present.", async () => {
//     fixture = TestBed.createComponent(ProfileNewPage);
//     de = fixture.debugElement.query(By.css(".bal")).nativeElement;
//     expect(de).toBeDefined();
//   });
  

//   it("Class Icon should be present.", async () => {
//     fixture = TestBed.createComponent(ProfileNewPage);
//     de = fixture.debugElement.query(By.css(".clsicon")).nativeElement;
//     expect(de).toBeDefined();
//   });
  

//   it("Options Cards should be present.", async () => {
//     fixture = TestBed.createComponent(ProfileNewPage);
//     de = fixture.debugElement.query(By.css(".optioncrds")).nativeElement;
//     expect(de).toBeDefined();
//   });

  
//   it("Options Cards icon should be present.", async () => {
//     fixture = TestBed.createComponent(ProfileNewPage);
//     de = fixture.debugElement.query(By.css(".signout")).nativeElement;
//     expect(de).toBeDefined();
//   });

  

//   it("Purchase Options Cards icon should be present.", async () => {
//     fixture = TestBed.createComponent(ProfileNewPage);
//     de = fixture.debugElement.query(By.css(".purchasecrd")).nativeElement;
//     expect(de).toBeDefined();
//   });
  

//   it("Cards text should be present.", async () => {
//     fixture = TestBed.createComponent(ProfileNewPage);
//     de = fixture.debugElement.query(By.css(".crdtxt")).nativeElement;
//     expect(de).toBeDefined();
//   });


// });
