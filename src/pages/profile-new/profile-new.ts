import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { Chart } from 'chart.js';
import { FetchprofileProvider } from '../../providers/providers';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { WalletPage, LogoutconfirmPage,NewMyMembershipsPage ,MyrefferalPage,TransactionHistoryListPage} from '../../pages/pages'

@IonicPage()
@Component({
  selector: 'page-profile-new',
  templateUrl: 'profile-new.html',
})
export class ProfileNewPage {
  public profileDetails = {  

    "ProfileName": "",
    "profilePicture": "",
    "membershiptype": "",
    "clientId": "",
    "walletBalance": "",
    "outstandingbalance": ""
  };

  @ViewChild('barCanvas') lineCanvas;

  lineChart: any;  /// variable for displaying line on graph showing details 
  dir: any;      //  variable for dieclaring direction on graph showing details 
  testing: string     /// variable for storing value 
  // echarts:any; 


  constructor(public modalctrl: ModalController, private storage: Storage, public translateService: TranslateService, public fp: FetchprofileProvider, public navCtrl: NavController, public modalCtrl: ModalController, public navp: NavParams, public plt: Platform) {
    this.testing = "val";  
    this.lineChart = '';     
    this.getData();      //FETCHING API RESPONSE OF PROFILE DETAILS
    //SetDirection
    this.dir = 'ltr';      ///set direction 

  }

  ionViewDidLoad() {

    // reverse array  and set postion to right  for rtl 

    let labelArray = ["January", "February", "March", "April", "May", "June", "July","Aug","Sept","Oct","Nov",""];
    let YPostion = 'left'
    let dataArray = [65, 59, 80, 81, 56, 45, 48,63,52,61,45]  
    // if (
    //   this.dir == 'rtl'
    // ) {
    //   YPostion = 'right'
    //   dataArray.reverse();
    //   labelArray.reverse();
    // }
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {

      type: 'line',
      data: {
        labels: labelArray,
        datasets: [
          {
            label: false,
            fill: true,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: dataArray,
            spanGaps: false,
          }

        ],


        // tooltips: {
        //     callbacks: {
        //        label: function(tooltipItem) {
        //               return tooltipItem.yLabel;
        //        }
        //     }
        // }
      },

      options: {

        legend: {
          display: false,

        },


        scales: {
          yAxes: [{            /// position for y-axis
            position: YPostion,
            ticks: {
              beginAtZero: false,
              // fontSize: 10,
              //  / max: 80

            }
          }],
          xAxes: [{           /// position for x-axis

            ticks: {
              autoSkip: false,
              maxRotation: 90,
              minRotation: 90,
              beginAtZero: false,
              //   fontSize: 9
            }
          }]
        }

      }


    });
  }



  //FETCHING API RESPONSE OF PROFILE DETAILS
  getData() {

    this.fp.getProfileDetails().map((response) => response.json()).subscribe((response) => {
      this.profileDetails = response;      
    });
  }

    /// NAVIGATING TO PURCHASE HISTORY PAGE
  goToPurchaseHistory() {
    this.navCtrl.push(TransactionHistoryListPage);
  }

    /// NAVIGATING TO MY REFERRAL PAGE
  goToMyReferrals() {
    this.navCtrl.push(MyrefferalPage);
  }

  /// NAVIGATING TO MY MEMBERSHIP PAGE
  goToMyMembership() {
    this.navCtrl.push(NewMyMembershipsPage);
  }

     /// METHOD FOR EDIT PROFILE PAGE
  goToEditProfile() {
   
  }

  /// NAVIGATING TO MY WALLET PAGE
  goToMyWallets() {
    this.navCtrl.push(WalletPage, {
      'key': this.profileDetails.walletBalance
    });
  }

    // METHOD FOR SIGNOUT FROM THE APP
  goToSignOut() {
    let logOutModal = this.modalctrl.create(LogoutconfirmPage);
    logOutModal.present();
  }
}
