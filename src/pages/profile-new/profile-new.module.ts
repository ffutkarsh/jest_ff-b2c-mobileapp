import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfileNewPage } from './profile-new';
import { TranslateModule } from '@ngx-translate/core';
import {FetchprofileProvider} from "../../providers/fetchprofile/fetchprofile"
import { MomentModule } from 'angular2-moment';
@NgModule({
  declarations: [
    ProfileNewPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfileNewPage),
    TranslateModule,
    MomentModule
  ],
  providers:[FetchprofileProvider]
})
export class ProfileNewPageModule {}
