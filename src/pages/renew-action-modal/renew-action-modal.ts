import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FetcherProvider } from '../../providers/fetcher/fetcher';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-renew-action-modal',
  templateUrl: 'renew-action-modal.html',
})
export class RenewActionModalPage {

  city = [];

  renewPageData: any;
  renewData;
  setofferLocally;
  constructor(public navCtrl: NavController, public navParams: NavParams, public fetcher: FetcherProvider, public viewCtrl: ViewController,private storage: Storage) {

    this.getcity();

    console.log(navParams.get('renew'));
    this.renewPageData = navParams.get('renew');
    this.navParams.get("renewJsonData");
    console.log(this.renewPageData);
    this.renewData = this.navParams.get("renewJsonData")[0];
    console.log(this.renewData);
  }

  ionViewDidLoad() {
    this.renewPageData = this.navParams.get('renew');
    this.navParams.get("renewJsonData");
    console.log('ionViewDidLoad RenewActionModalPage');
  }
  closeModal() {
    // this.navCtrl.pop()
    this.viewCtrl.dismiss();

  }
  getcity() {
    this.fetcher.getCities().map((response) => response.json()).subscribe((response) => {
      this.city = response;
    });
  }



  goToCart() {

    let obj= {
      
                    expiry: this.renewData.expiry,
                    frozenicon: this.renewData.frozenicon,
                    frozenstatus: this.renewData.frozenstatus,
                    location: this.renewData.location,
                    purposedescription: this.renewData.purposedescription,
                    totalCost: this.renewData.totalCost,
                    totalSessions: this.renewData.totalSessions,
                    usedSessions: this.renewData.usedSessions,
                    quantity: 1,
                    type: "renewFrozen"
                  };

    this.storage.get('CARTITEM').then((val) => {
      this.setofferLocally = val;
    }).then(
      () => {
        if ((this.setofferLocally) || (this.setofferLocally = [])) {

          this.setofferLocally.push(obj);
          this.storage.set('CARTITEM', this.setofferLocally);


              this.navCtrl.push("CartItemsPage",
        {
          'renewFlowCart': true,
        });
        }
      });}

    // goToCart() {
    //   this.navCtrl.push("CartItemsPage",
    //     {
    //       'renewFlowCart': true, "renewTotalcost": this.renewPageData.passportRate, 'renewCartData': this.navParams.get("renewJsonData")
    //     });
    // }
  }

