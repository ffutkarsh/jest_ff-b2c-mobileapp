import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RenewActionModalPage } from './renew-action-modal';
import { FetcherProvider } from '../../providers/fetcher/fetcher';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    RenewActionModalPage,
  ],
  imports: [
    IonicPageModule.forChild(RenewActionModalPage),
    TranslateModule
  ],
  providers:[
    FetcherProvider
  ]
})
export class RenewActionModalPageModule {}
