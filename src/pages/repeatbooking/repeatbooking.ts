import { Component } from '@angular/core';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, LoadingController, NavParams } from 'ionic-angular';
import { IfObservable } from 'rxjs/observable/IfObservable';
import { RepeatBookingAuthProvider } from '../../providers/repeat-booking-auth/repeat-booking-auth';
import { DateTimeAuthProvider } from '../../providers/date-time-auth/date-time-auth';
import { CartItemsPage } from '../../pages/pages';
import { CartItemsProvider } from '../../providers/cart-items/cart-items';
import { AppointmentBookedPage } from '../../pages/pages';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-repeatbooking',
  templateUrl: 'repeatbooking.html',
})

export class Repeatbooking {

  public appointFull; //FROM DATE AND TIME PAGE OR ANY TRAINER PAGE.
  public purpose; //FROM DATE AND TIME PAGE OR ANY TRAINER PAGE.
  public purposeTotalSessions; //FROM DATE AND TIME PAGE OR ANY TRAINER PAGE.
  public icon; //FROM DATE AND TIME PAGE OR ANY TRAINER PAGE.
  public cartOrbooked; //FROM DATE AND TIME PAGE OR ANY TRAINER PAGE.
  public trainername; //FROM DATE AND TIME PAGE OR ANY TRAINER PAGE.
  public trainerType; //FROM DATE AND TIME PAGE OR ANY TRAINER PAGE.
  public trainerIcon; //FROM DATE AND TIME PAGE OR ANY TRAINER PAGE.
  public mastertrainerlist; //FROM DATE AND TIME PAGE OR ANY TRAINER PAGE.
  public trianerrating; //FROM DATE AND TIME PAGE OR ANY TRAINER PAGE.
  public ratingsurveycount; //FROM DATE AND TIME PAGE OR ANY TRAINER PAGE.
  public date; //FROM DATE AND TIME PAGE OR ANY TRAINER PAGE.
  public availableSlot; //FROM DATE AND TIME PAGE OR ANY TRAINER PAGE.

  public apptype; //VARIABLE TO STORE THE APPOINTMENT TYPE "ONLY ONCE" OR "REPEATS".
  public repeattime; //VARIABLE TO STORE THE REPEAT TIME IF "REPEATS" IS SELECTED.

  sessionten;
  session20;
  session30; session40;
  price10; price20; price30; price40;
  now; //VARIABLE TO STORE THE CURRENT DATE.
  dFormat; //VARIABLE TO STORE THE DATE IN THE GIVEN FORMAT.
  time; //VARIABLE TO STORE THE TIME.

  day; //VARIABLE TO STORE THE DAY.
  displaySlot; //VARIABLE FOR DISPLAYING SLOTS.

  repeatSelected = false; //STORING BOOLEAN VALUE BASED ON CHOICE SELECTED ["ONLY ONCE" OR "REPEATS"] - BY DEFAULT ITS "ONLY ONCE"

  buttonColor: string; //BUTTON COLOR ACCORDING TO CHOICE SELECTED.

  DateforTTForMWF; //VARIABLE TO STORE THE REPEATS DAY'S. [TUESDAY-THURSDAY OR MONDAY-WEDNESDAY-FRIDAY]

  public isButtondisabled; //USED TO SAVE STATE (BOOLEAN) OF DONE BUTTON ENABLE.

  setofferLocally;

  constructor(
    private storage: Storage,
    public repeatAuthService: RepeatBookingAuthProvider,
    public cartProvider: CartItemsProvider,
    public dateTimeAuthService: DateTimeAuthProvider,
    public navCtrl: NavController,
    public navParams: NavParams) {

    this.appointFull = this.navParams.get('appointFull');
    this.purpose = this.navParams.get("purpose");
    this.purposeTotalSessions = this.navParams.get("purposeTotalSessions");
    this.icon = this.navParams.get("icon");
    this.cartOrbooked = this.navParams.get('cartOrbooked');
    this.trainername = this.navParams.get("trainername");
    this.trainerType = this.navParams.get("trainerType");
    this.trainerIcon = this.navParams.get('trainerIcon');
    this.mastertrainerlist = this.navParams.get('mastertrainerlist');
    this.trianerrating = this.navParams.get('trainerrating');
    this.ratingsurveycount = this.navParams.get('ratingsurveycount');
    this.date = this.navParams.get('date');
    this.availableSlot = this.navParams.get('availableSlot');

    this.apptype = ""; //VARIABLE TO STORE THE VALUE OF CHOICE GRID - "ONLY ONCE" OR "REPEATS"
    this.repeattime = ""; //VARIABLE TO STORE THE VALUE OF REPEAT TIME IF "REPEATS" IS SELECTED.

    this.DateforTTForMWF = this.checkingday(); //STORING THE VALUE GIVEN BY CHECKINGDAY().
    this.date = this.navParams.get("date"); //RECEIVING DATE.
    var newd = this.navParams.get("date"); //RECEIVING DATE.
    this.now = moment(); //CURRENT DATE.
    this.dFormat = moment(this.now) //FORMATTING THE DATE.
    this.time = moment(this.now).format("hh:mm a"); //TIME.

    this.buttonColor = "#0FB14A"; //BUTTON COLOR.

    this.sessionten = 10;
    this.session20 = 20;
    this.session30 = 30;
    this.session40 = 40;
    this.price10 = 5000;
    this.price20 = 8000;
    this.price30 = 15000;
    this.price40 = 20000;

  }

  ionViewDidLoad() { }

  //CHECKING THE DAYS FOR REPEATS OPTION.
  checkingday() {
    this.day = moment(this.now).format("dddd");
    if (this.day == 'Monday' || this.day == 'Wednesday' || this.day == 'Friday') {
      // return moment().day("Monday");
      this.displaySlot = "(Mon-Wed-Fri)"
    }
    else {
      //return moment().day("Tuesday");
      this.displaySlot = "(Tue-Thurs-Sat)"
    }
  }

  //SETTING BUTTON COLOR TO GREEN ON SELECTING ONLY ONCE.
  onlyOnce(sess, price) {
    var session = sess;
    var price = price
    console.log(session, "session")
    this.repeatSelected = false;
    this.buttonColor = "#0FB14A";
    this.isButtondisabled = false;
    console.log(this.repeatSelected);
    var CartObj = {
      'appointFull': this.appointFull,
      'purpose': this.purpose,
      'purposeTotalSessions': this.purposeTotalSessions,
      'icon': this.icon,
      'cartOrbooked': this.cartOrbooked,
      'trainername': this.trainername,
      'trainerType': this.trainerType,
      'trainerIcon': this.trainerIcon,
      'mastertrainerlist': this.mastertrainerlist,
      'trainerrating': this.trianerrating,
      'ratingsurveycount': this.ratingsurveycount,
      'date': this.date,
      'availableSlot': this.availableSlot,
      'apptype': "Repeats",
      'repeattime': this.repeattime,
      'price': price,
      'fromCart': true
    }

let obj={
  purposename:this.purpose, 
  amout:price,
  purposeTotalSessions:session,
  sessiontag:this.purpose + " " + session + " " + "session",
  purposelogo:this.appointFull.PurposeIcon, 
  date:this.date,
  timeslot:"",
  type:"apponitment",
}
console.log(obj);

console.log(CartObj);

this.storage.get('CARTITEM').then((val) => {
  this.setofferLocally = val;
}).then(
 () => {
   if ((this.setofferLocally) || (this.setofferLocally = [])) {

     this.setofferLocally.push(obj);
     this.storage.set('CARTITEM', this.setofferLocally);

   }
 });


this.navCtrl.push("CartItemsPage",
 {
  'appFlow': true
 });





    // this.cartProvider.storeItems(CartObj)
    // this.navCtrl.push(CartItemsPage, {
    //   'appFlow': true
    // });
  }

  once() {
    this.isButtondisabled = false;
    this.buttonColor = "#0FB14A";
    this.repeatSelected = false;
  }

  //SETTING BUTTON COLOR TO GREY INITIALLY ON SELECTING REPEAT.
  repeats() {
    this.buttonColor = "#A3A3A4";
    this.isButtondisabled = true;
    this.repeatSelected = true;
    this.repeattime = "a";
    this.isFilled();
    //console.log(this.repeatSelected);
  }

  //WILL SLICE THE INPUT IF IT IS CHARACTER.
  isFilled() {
    this.buttonColor = "#0FB14A";
    this.isButtondisabled = false;
    let a = this.repeattime.slice(-1);
    if (!this.checkifNo(a)) {
      this.repeattime = this.repeattime.substring(0, this.isFilled.length - 1);
      if (this.repeattime.length == 0) {
        this.buttonColor = "#A3A3A4";
        this.isButtondisabled = true;
      }
    } else if (parseInt(this.repeattime) > this.purposeTotalSessions || parseInt(this.repeattime) == 0) {
      this.repeattime = this.repeattime.substring(0, this.isFilled.length - 1);
      this.buttonColor = "#A3A3A4";
      this.isButtondisabled = true;
    }
  }

  //CHECK IF THE ENTERED CHARACTER IS A DIGIT.
  checkifNo(repeattime) {
    let reg = new RegExp('^\\d+$');
    return reg.test(repeattime);
  }

  gotoSuccess() {
    //IF REPEATS IS SELECTED APPOINTMENT TYPE WILL BE PASSED AS "REPEATS".
    if (this.repeatSelected == true) {
      this.navCtrl.push('AppointmentBookedPage', {
        'appointFull': this.appointFull,
        'purpose': this.purpose,
        'purposeTotalSessions': this.purposeTotalSessions,
        'icon': this.icon,
        'cartOrbooked': this.cartOrbooked,
        'trainername': this.trainername,
        'trainerType': this.trainerType,
        'trainerIcon': this.trainerIcon,
        'mastertrainerlist': this.mastertrainerlist,
        'trainerrating': this.trianerrating,
        'ratingsurveycount': this.ratingsurveycount,
        'date': this.date,
        'availableSlot': this.availableSlot,
        'apptype': "Repeats",
        'repeattime': this.repeattime
      });
    }
    //IF ONLY ONCE IS SELECTED APPOINMENT TYPE WILL BE PASSED AS "ONLY ONCE".
    else {
      this.navCtrl.push('AppointmentBookedPage', {
        'appointFull': this.appointFull,
        'purpose': this.purpose,
        'purposeTotalSessions': this.purposeTotalSessions,
        'icon': this.icon,
        'cartOrbooked': this.cartOrbooked,
        'trainername': this.trainername,
        'trainerType': this.trainerType,
        'trainerIcon': this.trainerIcon,
        'mastertrainerlist': this.mastertrainerlist,
        'trainerrating': this.trianerrating,
        'ratingsurveycount': this.ratingsurveycount,
        'date': this.date,
        'availableSlot': this.availableSlot,
        'apptype': "Only Once",
        'repeattime': this.repeattime
      });
    }
    this.repeatAuthService.fromRepeatBookingpage();
  }

}
