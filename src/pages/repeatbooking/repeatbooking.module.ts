import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Repeatbooking } from './repeatbooking';

@NgModule({
  declarations: [
    Repeatbooking,
  ],
  imports: [
    IonicPageModule.forChild(Repeatbooking),
  ],
})
export class RepeatbookingModule {}
