// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, ViewChild } from '@angular/core';
// import { Repeatbooking } from './repeatbooking';
// import { IonicModule, Platform, NavController, NavParams } from 'ionic-angular/index';
// import { TranslateService } from '@ngx-translate/core';
// import { MomentModule } from 'angular2-moment';
// import * as moment from 'moment';
// import { Ionic2RatingModule } from 'ionic2-rating';
// import { Http, ConnectionBackend } from '@angular/http';
// import { RepeatBookingAuthProvider } from '../../providers/repeat-booking-auth/repeat-booking-auth';
// import { DateTimeAuthProvider } from '../../providers/date-time-auth/date-time-auth';
// import { HttpModule } from "@angular/http";

// class NavParamsMock {
//     static returnParam = {
//         'domain': '',
//         'phone': '',
//         'otp': ''
//     };
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// describe('Repeat Booking Page - UI Part', () => {
//     let de: DebugElement;
//     let comp: Repeatbooking;
//     let fixture: ComponentFixture<Repeatbooking>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [Repeatbooking],
//             imports: [
//                 IonicModule.forRoot(Repeatbooking),
//                 MomentModule,
//                 HttpModule
//             ],
//             providers: [
//                 { provide: NavParams, useClass: NavParamsMock },
//                 Ionic2RatingModule,
//                 NavController,
//                 TranslateService,
//                 Http,
//                 RepeatBookingAuthProvider,
//                 DateTimeAuthProvider,
//                 ConnectionBackend
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(Repeatbooking); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     //HEADER TEST CASE -1.
//     it("Header to be present.", async () => {
//         fixture = TestBed.createComponent(Repeatbooking);
//         de = fixture.debugElement.query(By.css(".repeatBookingHeader")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //HEADER TEST CASE -2.
//     it("Title of the header to present.", async () => {
//         const title: HTMLElement = fixture.debugElement.query(By.css('.headerTitle')).nativeElement;
//         expect(title.textContent).toContain("Repeat Booking");
//     });

//     //CARD SECTION TEST CASE -1.
//     it("Grid for cards to be present.", async () => {
//         fixture = TestBed.createComponent(Repeatbooking);
//         de = fixture.debugElement.query(By.css(".mygrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //CARD SECTION TEST CASE -2.
//     it("Div for cards to be present.", async () => {
//         fixture = TestBed.createComponent(Repeatbooking);
//         de = fixture.debugElement.query(By.css(".belowheader")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //CARD SECTION TEST CASE -3.
//     it("Row for cards to be present.", async () => {
//         fixture = TestBed.createComponent(Repeatbooking);
//         de = fixture.debugElement.query(By.css(".row")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //CARD SECTION TEST CASE -4.
//     it("Card 1 to be present.", async () => {
//         fixture = TestBed.createComponent(Repeatbooking);
//         de = fixture.debugElement.query(By.css(".card1")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //CARD SECTION TEST CASE -5.
//     it("Card 1 heading to be present.", async () => {
//         fixture = TestBed.createComponent(Repeatbooking);
//         de = fixture.debugElement.query(By.css(".card-header1")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //CARD SECTION TEST CASE -6.
//     it("Card 2 to be present.", async () => {
//         fixture = TestBed.createComponent(Repeatbooking);
//         de = fixture.debugElement.query(By.css(".card2")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //CARD SECTION TEST CASE -7.
//     it("Card 2 heading to be present.", async () => {
//         fixture = TestBed.createComponent(Repeatbooking);
//         de = fixture.debugElement.query(By.css(".card-header2")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //DATE AND TIME SECTION TEST CASE -1.
//     it("Div for Date and Time Should be present.", async () => {
//         fixture = TestBed.createComponent(Repeatbooking);
//         de = fixture.debugElement.query(By.css(".DateandTime")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //CHOICE GRID TEST CASE -1.
//     it("Choice Grid should be present.", async () => {
//         fixture = TestBed.createComponent(Repeatbooking);
//         de = fixture.debugElement.query(By.css(".choicegrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //CHOICE GRID TEST CASE -2.
//     it("“Only Once” option to be present.", async () => {
//         fixture = TestBed.createComponent(Repeatbooking);
//         de = fixture.debugElement.query(By.css(".onlyOnce")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //CHOICE GRID TEST CASE -3.
//     it("“Repeats” option to be present.", async () => {
//         fixture = TestBed.createComponent(Repeatbooking);
//         de = fixture.debugElement.query(By.css(".repeats")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //CHOICE GRID TEST CASE -4.
//     it("Slots should be present.", async () => {
//         fixture = TestBed.createComponent(Repeatbooking);
//         de = fixture.debugElement.query(By.css(".displayslots")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //BOOK OR DONE BUTTON TEST CASE -1.
//     it("“Book/Done” button to be present.", async () => {
//         fixture = TestBed.createComponent(Repeatbooking);
//         de = fixture.debugElement.query(By.css(".donebutton")).nativeElement;
//         expect(de).toBeDefined();
//     });
// });

// describe('Repeat Booking Page - Logic Part', () => {
//     let de: DebugElement;
//     let comp: Repeatbooking;
//     let fixture: ComponentFixture<Repeatbooking>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [Repeatbooking],
//             imports: [
//                 IonicModule.forRoot(Repeatbooking),
//                 MomentModule,
//                 HttpModule
//             ],
//             providers: [
//                 { provide: NavParams, useClass: NavParamsMock },
//                 Ionic2RatingModule,
//                 NavController,
//                 TranslateService,
//                 Http,
//                 RepeatBookingAuthProvider,
//                 DateTimeAuthProvider,
//                 ConnectionBackend
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(Repeatbooking); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     //CARD 1 TEST CASE -1.
//     it("Card 1 purpose icon to be present.", async () => {
//         expect(comp.icon).toBeDefined();
//     });

//     //CARD 1 TEST CASE -2.
//     it("Card 1 purpose name to be present.", async () => {
//         expect(comp.purpose).toBeDefined();
//     });

//     //CARD 2 TEST CASE -1.
//     it("Card 2 trainer image to be present.", async () => {
//         expect(comp.trainerImage).toBeDefined();
//     });

//     //CARD 2 TEST CASE -2.
//     it("Card 2 trainer name to be present.", async () => {
//         expect(comp.trainer).toBeDefined();
//     });

//     //DATE AND TIME TEST CASE -1.
//     it("Date of Appoinment to be defined.", async () => {
//         expect(comp.date).toBeDefined();
//     });

//     //DATE AND TIME TEST CASE -2.
//     it("Time of Appointment to be defined.", async () => {
//         expect(comp.availableSlot).toBeDefined();
//     });

//     //SLOTS TEST CASE -1.
//     it("Days of Slots to be defined (Mon-Wed-Fri or Tues-Thurs-Sat).", async () => {
//         expect(comp.displaySlot).toBeDefined();
//     });

//     //TOTAL NUMBER OF SESSIONS OF A PARTICULAR PURPOSE TEST CASE -1.
//     it("Number of sessions of a particular purpose to be defined.", async () => {
//         expect(comp.purposeTotalSessions).toBeDefined();
//     });
// }); 