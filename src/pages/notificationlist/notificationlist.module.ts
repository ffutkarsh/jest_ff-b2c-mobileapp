import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationlistPage } from './notificationlist';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    NotificationlistPage,
  ],
  imports: [
    IonicPageModule.forChild(NotificationlistPage),
    TranslateModule
  ],
})
export class NotificationlistPageModule {}
