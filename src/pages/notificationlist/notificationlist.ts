import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NotificationPage } from '../notification/notification';

@IonicPage()
@Component({
  selector: 'page-notificationlist',
  templateUrl: 'notificationlist.html',
})
export class NotificationlistPage {

  public notification;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {

    this.notification = [
      { img: "sarah-avatar.png.jpeg", name: "Jonathon Doe", role: "Admin Admin", status: "2 h ago", desc: "Vestibulum tincidunt tempor finibus. Cras eget rutrum est, sed sagittis sapien.", selected: false },
      { img: "sarah-avatar.png.jpeg", name: "David Gatner", role: "Developer", status: "3 d ago", desc: "Vestibulum tincidunt tempor finibus. Cras eget rutrum est, sed sagittis sapien.", selected: false },
      { img: "sarah-avatar.png.jpeg", name: "Jonathon Doe", role: "Receptionist", status: "3 d ago", desc: "Vestibulum tincidunt tempor finibus. Cras eget rutrum est, sed sagittis sapien.", selected: false },
      { img: "sarah-avatar.png.jpeg", name: "David Gatner", role: "Trainer", status: "3 d ago", desc: "Vestibulum tincidunt tempor finibus. Cras eget rutrum est, sed sagittis sapien.", selected: false }
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationlistPage');
  }

  gotodetail(n) {
    n.selected = true;
    this.navCtrl.push("NotificationPage", {
      'notificationDetails': n
    });
  }
}
