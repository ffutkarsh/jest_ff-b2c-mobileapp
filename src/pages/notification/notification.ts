import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {

  public notificationDetails;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.notificationDetails = this.navParams.get('notificationDetails');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationPage');
  }

}
