// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, style } from '@angular/core';
// import { ApplyCouponModalPage } from './apply-coupon-modal';
// import { IonicModule, Platform, NavController, NavParams, ViewController, Item } from 'ionic-angular/index';
// import { CartCouponsProvider } from '../../providers/cart-coupons/cart-coupons';
// import { mockApp, mockConfig, mockPlatform, mockView } from "ionic-angular/util/mock-providers";
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { MomentModule } from 'angular2-moment';
// import { HttpModule } from '@angular/http'
// import { BillingProvider } from '../../providers/providers';

// import {  HttpClientModule } from '@angular/common/http';
// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }
// //The describe function is for grouping related specs
// describe('---------APPLY COUPON MODAL PAGE UI----------', () => {
//     let de: DebugElement;
//     let comp: ApplyCouponModalPage;
//     let fixture: ComponentFixture<ApplyCouponModalPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [ApplyCouponModalPage],
//             imports: [
//                 HttpModule,
//                 HttpClientModule,
            
//                 TranslateModule.forChild(),
//                 MomentModule,
//                 IonicModule.forRoot(ApplyCouponModalPage),
//             ],
//             providers: [
//                 CartCouponsProvider,
//                 TranslateService,
//                 BillingProvider,
//                 TranslateStore,
//                 { provide: ViewController, useValue: mockView() },
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(ApplyCouponModalPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });
//     //Creating the main Component
//     it('should create component', () => expect(comp).toBeDefined());

//     //checking the content
//     it('checking the content using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.main-content')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('main-content');
//     });

//     //checking the modal 
//     it('checking the modal using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.modal')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('modal');
//     });

//     //checking the grid using class name 
//     it('checking the grid using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.mainGrid')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('mainGrid');
//     });

//     //checking the 1st row using class name 
//     it('checking the 1st row using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.couponApplied')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('couponApplied');
//     });



//     //checking the 2nd row using class name 
//     it('checking the 2nd row using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.row2')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('row2');
//     });

//     //checking avatar class in 2nd row
//     it('checking the avatar in 2nd row using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.avatarClass')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('avatarClass');
//     });

//     //checking avatar text div in 2nd row
//     it('checking the avatar text div in 2nd row using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.couponDiv')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('couponDiv');
//     });

//     //checking the row contining coupon title 
//     it('checking the row containing coupon title ', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.couponDetails')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('couponDetails');
//     });

//     //checking the row contining coupon description 
//     it('checking the row containing coupon description ', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.couponDetails')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('couponDetails');
//     });

//     //checking the row contining redirecting text 
//     it('checking the row containing coupon description ', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.redirecting')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('redirecting');
//     });

//     //checking the row contining spinner  
//     it('checking the row contining spinner ', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.spinnerClass')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('spinnerClass');
//     });

//     //checking the row contining cancel button  
//     it('checking the row containing cancel button  ', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.cancelCoupon')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('cancelCoupon');
//     });


//     //checking the row contining cancel button 
//     it('checking the row containing cancel button  ', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.CancelLabel')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('CancelLabel');
//     });


// });

// //The describe function is for grouping related specs
// describe('---------APPLY COUPON MODAL PAGE LOGIC - METHODS----------', () => {
//     let de: DebugElement;
//     let comp: ApplyCouponModalPage;
//     let fixture: ComponentFixture<ApplyCouponModalPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [ApplyCouponModalPage],
//             imports: [
//                 HttpModule,
//                 TranslateModule.forChild(),
//                 MomentModule,
//                 HttpClientModule,             
//                 IonicModule.forRoot(ApplyCouponModalPage),
//             ],
//             providers: [
//                 CartCouponsProvider,
//                 TranslateService,
//                 TranslateStore,
//                 BillingProvider,
//                 { provide: ViewController, useValue: mockView() },
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));
//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(ApplyCouponModalPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //checking the  provider method
//     it('checking the provider method to fetch json - coupon list ', () => {
//         expect(comp.fetchCouponDetails).toBeTruthy();
//     });

//     //checking the method to check conditions of pushing navparams
//     it('checking the method to check conditions of pushing navparams ', () => {
//         expect(comp.pushToCart).toBeTruthy();
//     });

//     //checking the method which generates random color 
//     it('checking the method which generates random color ', () => {
//         expect(comp.getColor).toBeTruthy();
//     });

//     //checking the method of timer
//     it('checking the method of timer ', () => {
//         expect(comp.startTimer).toBeTruthy();
//     });

//     //checking the method of cancelling coupon request
//     it('checking the method of cancelling coupon request ', () => {
//         expect(comp.cancelCoupon).toBeTruthy();
//     });
//     //checking the method of setMyStyles for setting style as background color
//     it('checking the method of setMyStyles for setting style as background color ', () => {
//         expect(comp.setMyStyles).toBeTruthy();
//     });
//     //checking the method of setMyStyles for setting style as color 
//     it('checking the method of setMyStyles for setting style as color ', () => {
//         expect(comp.setMyStyles1).toBeTruthy();
//     });

// });

// //The describe function is for grouping related specs
// describe('---------APPLY COUPON MODAL PAGE LOGIC - VARIABLES ----------', () => {
//     let de: DebugElement;
//     let comp: ApplyCouponModalPage;
//     let fixture: ComponentFixture<ApplyCouponModalPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [ApplyCouponModalPage],
//             imports: [
//                 HttpModule,
//                 TranslateModule.forChild(),
//                 MomentModule,
//                 HttpClientModule,             
//                 IonicModule.forRoot(ApplyCouponModalPage),
//             ],
//             providers: [
//                 CartCouponsProvider,
//                 TranslateService,
//                 BillingProvider,
//                 TranslateStore,
//                 { provide: ViewController, useValue: mockView() },
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(ApplyCouponModalPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //checking Array of coupon list to store json file.
//     it('checking Array of coupon list to store json file. ', () => {
//         expect(comp.couponDetails).toBeUndefined();
//     });

//     //Storing single coupon
//     it('Storing single coupon', () => {
//         expect(comp.oneRecord).toBeTruthy();
//     });

//     //Storing randomly generate color
//     it('checking Array OF randomly generate color ', () => {
//         expect(comp.colorarray).toBeTruthy();
//     });

//     ///to store index 
//     it('checking the variable to store index  ', () => {
//         expect(comp.index).toBeUndefined();
//     });


//     //checking the var from nav params to fetch coupon index 
//     it('checking the var from nav params to fetch coupon index ', () => {
//         expect(comp.couponIndex).toBeTruthy();
//     });

//     //boolean to check condition whether to navigate on cart page or coupon page.
//     it('boolean to check condition whether to navigate on cart page or coupon page.', () => {
//         expect(comp.cancelBoolean).toBeFalsy();
//     });

//     //checking the var from nav params to fetch coupon index 
//     it('checking the var to store text. Data binding! ', () => {
//         expect(comp.redirecting).toBeTruthy();
//     });

//     //checking the var to store timer
//     it('checking the var to store timer', () => {
//         expect(comp.timer).toBeTruthy();
//     });

//     //checking the var to store tick
//     it('checking the var to store tick', () => {
//         expect(comp.tick).toBeUndefined();
//     });
// });