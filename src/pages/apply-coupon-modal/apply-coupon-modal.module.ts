import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ApplyCouponModalPage } from './apply-coupon-modal';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ApplyCouponModalPage,
  ],
  imports: [
    IonicPageModule.forChild(ApplyCouponModalPage),
    TranslateModule
  ],
})
export class ApplyCouponModalPageModule {}
