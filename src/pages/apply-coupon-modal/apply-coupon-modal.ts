import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { CartCouponsProvider,BillingProvider } from '../../providers/providers';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { CartItemsPage, CartCouponPage } from '../../pages/pages'
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-apply-coupon-modal',
  templateUrl: 'apply-coupon-modal.html',
})
export class ApplyCouponModalPage {

  public couponDetails; //Array of coupon list to store json file.
  public CouponArray = []; //to store coupon offers in an array from response

  public oneRecord = []; //Storing single coupon
  public colorarray = []; // Storing randomly generate color
  public index; //to store index 
  public couponIndex; //from nav params to fetch coupon index
  public itemReceived;
  public cancelBoolean = false; //boolean to check condition whether to navigate on cart page or coupon page.
  public redirecting; //var to store text. Data binding!

  public timer; //timer
  public tick: number; //TIMER
  public dis;
  appliedCop;
  appliedCopArr=[];
  constructor(public navCtrl: NavController,
    public couponService: CartCouponsProvider,
    public navParams: NavParams,
    public translateService: TranslateService,
    public billingProvider: BillingProvider,
    private viewController: ViewController) {

    this.redirecting = "Redirecting in "  //data binding
    this.couponIndex = this.navParams.get('indexOfCoupon'); //fetching index of respective coupon from coupon list page
    this.itemReceived = this.navParams.get('itemreceived'); //fetching from coupon list
   // this.appliedCop = this.navParams.get('appliedCoupon'); //fetching from coupon list

    console.log(this.appliedCop,"array of coupon")
    this.fetchCouponDetails() //calling provider method to fetch json - coupon list
    this.pushToCart() //condition checking method on where to redirect.

  }

  ionViewWillEnter()
  {
    this.appliedCop = this.navParams.get('appliedCoupon'); //fetching from coupon list

    this.appliedCopArr.push(this.appliedCop)
    console.log(this.appliedCop,"array of coupon----")
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApplyCouponModalPage');
  }

  //from json using its provider
  fetchCouponDetails() {
    this.couponService.getAllCouponsDetails().map((response) => response).subscribe((response) => {
      this.couponDetails = response;
      this.CouponArray = this.couponDetails.Offers;
      console.log(this.CouponArray, "COUPONSSS")
      //creating  random colors and stroing it in an array
      for (this.index = 0; this.index < this.CouponArray.length; this.index++) {
        this.colorarray.push(this.getColor(this.index));
      }
      //fetching a single record from list using index from coupon list
      this.oneRecord = this.CouponArray[this.couponIndex];
    });
  }

  
  //generating random color
  getColor(i) {
    return "rgb(" + 360 * Math.random() + ',' +
      (25 + 70 * Math.random()) + '%,' +
      (85 + 10 * Math.random()) + '%)';
  }

  //condition checking method.
  pushToCart() {
    this.startTimer(5);
  }
  
  startTimer(timeoutt) {
    this.timer = TimerObservable.create(100, 1000).subscribe(t => {
      this.tick = timeoutt - t;
      if (this.tick <= 0 && this.cancelBoolean == false) {
        this.timer.unsubscribe();
       
        this.navCtrl.push(CartItemsPage, { //else go to cart item page
          'arrayOfCoupon': this.oneRecord,
          'boolean': false,
          'itemreceived': this.itemReceived 
        });
      }
      else if (this.cancelBoolean == true && this.tick >= 1) { //if cancel button is pressed then stay on coupon list page
        this.timer.unsubscribe();
        this.dis = 0;
        this.billingProvider.discount(this.dis) 
        this.navCtrl.pop()
      }
      console.log(this.billingProvider.receivedTotal,"THIS IS RECEIVED IN apply")
    });
 
  }


  cancelCoupon() { //on click of cancel set boolean true to check condition
    this.cancelBoolean = true;
    
    this.redirecting = " Cancelling coupon please wait "//changing text from "redirecting" to "Cancelling"
    //this.navCtrl.pop()
  }

  //from html ngStyle
  setMyStyles() {
    let style = {
      'background-color': this.colorarray[0],
    };
    return style; // returning randomnly generated color 
  }

  //from html ngStyle
  setMyStyles1() {
    let style = {
      'color': this.colorarray[0],
    };
    return style; // returning randomnly generated color 
  }
}
