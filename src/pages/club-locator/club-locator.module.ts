import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClubLocatorPage } from './club-locator';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ClubLocatorPage,
  ],
  imports: [
    IonicPageModule.forChild(ClubLocatorPage),
    TranslateModule
  ],
})
export class ClubLocatorPageModule {}
