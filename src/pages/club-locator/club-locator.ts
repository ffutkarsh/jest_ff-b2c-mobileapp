import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ClubLocatorProvider } from '../../providers/providers';
import { TranslateService } from '@ngx-translate/core';
import  { ClubDetailsPage } from '../../pages/pages';

@IonicPage()
@Component({
  selector: 'page-club-locator',
  templateUrl: 'club-locator.html',
})
export class ClubLocatorPage {
  clubArray = [];
  constructor(public navCtrl: NavController, public clubLocator: ClubLocatorProvider,public translateService: TranslateService, public navParams: NavParams) {
    this.fetchClubLocator() //calling cart items provider method
  }

  ionViewDidLoad() {  
  }

  fetchClubLocator() {
    this.clubLocator.getClubLocations().map((response) => response.json()).subscribe((response) => {
      this.clubArray = response;     
    });

  }


  goToClubDetails(item)
  {
    this.navCtrl.push(ClubDetailsPage, {
      'objectOfClub' : item
    })
  }

}
