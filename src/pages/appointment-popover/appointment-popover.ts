import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-appointment-popover',
  templateUrl: 'appointment-popover.html',
})
export class AppointmentPopoverPage {

  public purpose; //FROM APPOINTMENT BOOKED PAGE OR BOOKED APPOINTMENT DETAILS PAGE.
  public icon; //FROM APPOINTMENT BOOKED PAGE OR BOOKED APPOINTMENT DETAILS PAGE.
  public trainername; //FROM APPOINTMENT BOOKED PAGE OR BOOKED APPOINTMENT DETAILS PAGE.
  public trainerIcon; //FROM APPOINTMENT BOOKED PAGE OR BOOKED APPOINTMENT DETAILS PAGE.
  public trianerrating; //FROM APPOINTMENT BOOKED PAGE OR BOOKED APPOINTMENT DETAILS PAGE.
  public ratingsurveycount; //FROM APPOINTMENT BOOKED PAGE OR BOOKED APPOINTMENT DETAILS PAGE.
  public date; //FROM APPOINTMENT BOOKED PAGE OR BOOKED APPOINTMENT DETAILS PAGE.
  public availableSlot; //FROM APPOINTMENT BOOKED PAGE OR BOOKED APPOINTMENT DETAILS PAGE.

  constructor(
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams) {

    this.purpose = this.navParams.get("purpose");
    this.icon = this.navParams.get("icon");
    this.trainername = this.navParams.get("trainername");
    this.trainerIcon = this.navParams.get("trainerIcon");
    this.ratingsurveycount = this.navParams.get("ratingsurveycount");
    this.trianerrating = this.navParams.get("trainerrating");
    this.date = this.navParams.get("date");
    this.availableSlot = this.navParams.get("availableSlot");

  }

  ionViewDidLoad() { }

  gotoDateTime() {
    this.navCtrl.push("Datetime", {
      'booleanFromEdit': true,
      'purpose': this.purpose,
      'icon': this.icon,
      'trainername': this.trainername,
      'trainerIcon': this.trainerIcon,
      'ratingsurveycount': this.ratingsurveycount,
      'trainerrating': this.trianerrating,
      'date': this.date,
      'availableSlot': this.availableSlot,
    });
  }

  cancelApointment() {
    this.viewCtrl.dismiss();
    let myModal = this.modalCtrl.create("CancelAppointmentModalPage");
    myModal.present();
  }

}
