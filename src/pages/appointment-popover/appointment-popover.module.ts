import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentPopoverPage } from './appointment-popover';

@NgModule({
  declarations: [
    AppointmentPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentPopoverPage),
  ],
})
export class AppointmentPopoverPageModule {}
