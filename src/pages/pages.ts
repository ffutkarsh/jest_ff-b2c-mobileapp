// The page the user lands on after opening the app and without a session
export const FirstRunPage = 'SignupLanguagePage'; //ONE TIME RUN PAGE FOR APPLICATION.

// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
export const MainPage = 'LoginPage'; //LOGIN_FLOW PAGE 1.
export const WelcomeScreenPage = 'WelcomeScreenPage'; //LOGIN_FLOW PAGE 2.
export const SignupTermsEulaPage = 'SignupTermsEulaPage'; //LOGIN_FLOW PAGE 3.
export const OtpPage = 'OtpPage'; //LOGIN_FLOW PAGE 4.

export const LoginErrorPage = 'LoginErrorPage'; //IF USER ENTERS INCORRECT NUMBER.
export const LoginErrorFeedbackPage = 'LoginErrorFeedbackPage'; //PAGE AFTER SUBMITTING THE DETAILS ON LOGIN ERROR PAGE.

export const DashboardPage = 'DashboardPage'; //DASHBOARD PAGE ONCE THE LOGIN IS COMPLETED.
export const OfferPage = 'OfferPage'; //OFFER PAGE FROM DASHBOARD.
export const OfferDetailPage = 'OfferDetailPage'; //OFFER DETAIL PAGE FROM OFFER PAGE.

export const SchedulingPage = 'SchedulingPage';
export const AppointmentPopoverPage = "AppointmentPopoverPage";
export const FacilityPopoverPage = "FacilityPopoverPage";

export const MembershipPage = 'MembershipPage';

export const ClassesPage = 'ClassesPage'; //CLASSES PAGE FROM DASHBOARD.
export const FilterPage = "FilterPage";

export const AppointmentModalPage = 'AppointmentModalPage'; //APPOINTMENT_FLOW PAGE 1.
export const AppointmentBookedPage = 'AppointmentBookedPage';

export const FacilityBookedDetailsPage = 'FacilityBookedDetailsPage';

//cart flow
export const ApplyCouponModalPage = 'ApplyCouponModalPage';
export const CartCouponPage = 'CartCouponPage';
export const CartItemsPage = 'CartItemsPage';
export const EmployeeModalPage = 'EmployeeModalPage';
export const WaiverSuccessPage = 'WaiverSuccessPage';
export const CartDateModalPage = 'CartDateModalPage';

export const OopsPage = 'OopsPage'; //WHENEVER FLOW IS INCORRECT OOPS PAGE WILL BE DISPLAYED.
export const ClassDetailsPage = 'ClassDetailsPage';      // After Selecting "Join" , "Joined" Or "Buy And Join" 
export const WithDrawModalPage = 'WithDrawModalPage';     // Modal will popup 
export const ClassRatingPage = 'ClassRatingPage';

export const PaymentOptionsPage = 'PaymentOptionsPage'; //PAYMENT INITIAL PAGE.
export const PaymentSuccessfulPurchasePage = 'PaymentSuccessfulPurchasePage'; //PAYMENT SUCCESSFUL PAGE.
export const PaymentFailedPage = 'PaymentFailedPage'; //PAYMENT FAILURE PAGE.
export const LocationModalPage = 'LocationModalPage';
export const RatingWidgetModalPage = 'RatingWidgetModalPage';

export const SelectFacilityModalPage = 'SelectFacilityModalPage'; //FACILITY BOOKING PAGE 1.
export const FacilityDateTimePage = 'FacilityDateTimePage'; //FACILITY BOOKING PAGE 2.
export const WithdrawModalPage = 'WithdrawModalPage';
export const SeatSelectionMoadalPage = 'SeatSelectionMoadalPage';
export const PurchaseHistoryPage = 'PurchaseHistoryPage'; /// Purchase History Page
export const TransactionHistoryListPage = 'TransactionHistoryListPage';
export const NewMyMembershipsPage = 'NewMyMembershipsPage';
export const BuyMainPage = 'BuyMainPage';
export const BookedAppointmentPage = 'BookedAppointmentPage';
export const SpecificorAnyTrainerPage = 'SpecificorAnyTrainerPage';
export const AnyTrainerPage = 'AnyTrainerPage';
export const FeedbackPage = "FeedbackPage";
export const FeedbackFlowPage = "FeedbackFlowPage";

export const ClubLocatorPage = "ClubLocatorPage";
export const ClubDetailsPage = "ClubDetailsPage";
export const VideoCategoryPage = "VideoCategoryPage";
export const VideoListPage = "VideoListPage";
export const VideoPlayingPage = "VideoPlayingPage";
export const VirtualTourPage = "VirtualTourPage";

export const FreezePage = "FreezePage";
export const UpgradePage = "UpgradePage";
export const RenewPage = "RenewPage";
export const NotificationlistPage = "NotificationlistPage";
export const NotificationPage = "NotificationPage";

export const ProfileNewPage = "ProfileNewPage";
export const MyrefferalPage = "MyrefferalPage";
export const LogoutconfirmPage = "LogoutconfirmPage";
export const WalletPage = "WalletPage";