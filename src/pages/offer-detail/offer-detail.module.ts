import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { OfferDetailPage } from './offer-detail';

@NgModule({
  declarations: [
    OfferDetailPage
  ],
  imports: [
    IonicPageModule.forChild(OfferDetailPage),
    TranslateModule
  ],
})
export class OfferDetailPageModule { }
