import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import {
  NotificationlistPage,
  MainPage
} from '../../pages/pages';
import { CheckPermissionsProvider } from '../../providers/check-permissions/check-permissions';
import { CartItemsProvider } from '../../providers/cart-items/cart-items';
import { CartBillingProvider } from '../../providers/cart-billing/cart-billing';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-offer-detail',
  templateUrl: 'offer-detail.html',
})
export class OfferDetailPage {

  public fetchDialCode; //FROM OFFER PAGE OR OFFER CARD DASHBOARD PAGE.
  public fetchPhoneNumber; //FROM OFFER PAGE OR OFFER CARD DASHBOARD PAGE.
  public selectedCenterObject; //FROM OFFER PAGE OR OFFER CARD DASHBOARD PAGE.
  public otpResponse; //FROM OFFER PAGE OR OFFER CARD DASHBOARD PAGE.
  public otpVerified; //FROM OFFER PAGE OR OFFER CARD DASHBOARD PAGE.

  public verificationFlag; //FROM OFFER PAGE OR OFFER CARD DASHBOARD PAGE.
  public memberType; //FROM OFFER PAGE OR OFFER CARD DASHBOARD PAGE.
  public permissions; //FROM OFFER PAGE OR OFFER CARD DASHBOARD PAGE.
  public checkAccess; //FROM OFFER PAGE OR OFFER CARD DASHBOARD PAGE.

  public claimedOffer; //FROM OFFER PAGE OR OFFER CARD DASHBOARD PAGE.
  public day; //FOR STORING DAY IN GIVEN FORMAT.
  public date; //FOR STORING DATE IN GIVEN FORMAT.

  public oneItemSelected: boolean = false; //TO KEEP TRACK OF ITEMS SELECTED.

  public login; //TO CHECK WHETHER USER IS LOGGED IN.

  public offerObject: any = []; //OFFER'S SUB PRODUCTS ARRAY.

  public itemCount = 0; //TOTAL NUMBER OF PRODUCTS SELECTED.
  public totalAmount = 0; //TOTAL PRICE OF THE PRODUCTS.

  public setofferLocally: any = []; //STORE THE OFFER LOCALLY.

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public translateService: TranslateService,
    public checkPermissions: CheckPermissionsProvider,
    public CBP: CartBillingProvider,
    public cartProvider: CartItemsProvider,
    private storage: Storage) {

    this.fetchDialCode = navParams.get("defaultDialcode");
    this.fetchPhoneNumber = navParams.get("phoneNumber");
    this.selectedCenterObject = this.navParams.get('memberDetails');
    this.otpResponse = this.navParams.get('otpResponse');
    this.otpVerified = this.navParams.get('otpVerified');

    this.verificationFlag = navParams.get('verificationFlag');
    this.memberType = navParams.get('memberType');
    this.permissions = navParams.get('permissions');

    this.claimedOffer = this.navParams.get('claimedOffer');

    this.storage.get('loggedIn').then((value) => {
      this.login = value;
    });

  }

  ionViewDidLoad() {
    //CALCULATING DAY AND DATE FROM OFFER VALID TILL USING MOMENT JS
    this.day = moment(this.claimedOffer.OfferValidTill).format('ddd');
    this.date = moment(this.claimedOffer.OfferValidTill).format('DD MMM YYYY');
  }

  ionViewWillEnter() {
    //OFFER'S SUB PRODUCT ARRAY.
    this.offerObject = [{
      "packageType": "Gym floor 3 month",
      "strickPrice": "6000",
      "discountPrice": 5000,
      "purposeIcon": "gm.svg",
      "status": false
    },
    {
      "packageType": "Gym floor 3 month",
      "strickPrice": "6000",
      "discountPrice": 7000,
      "purposeIcon": "gm.svg",
      "status": false

    },
    {
      "packageType": "Gym floor 3 month",
      "strickPrice": "6000",
      "discountPrice": 8000,
      "purposeIcon": "gm.svg",
      "status": false
    }
    ];
  }

  ionViewDidLeave() {
    //CLEARING EVERYTHING ONCE LEFT FROM THE PAGE.
    this.itemCount = 0;
    this.totalAmount = 0;
    this.setofferLocally = [];
    this.oneItemSelected = false;
  }

  //TO ADD THE PRODUCT TO CART.
  addToCart(index) {
    this.storage.get('CARTITEM').then((val) => {
      this.setofferLocally = val;
    }).then(
      () => {
        if ((this.setofferLocally) || (this.setofferLocally = [])) {
          this.offerObject[index].status = !this.offerObject[index].status;
          this.oneItemSelected = true;
          let Offer = {
            "img": this.offerObject[index].purposeIcon,
            "purpose": this.offerObject[index].packageType,
            "amount": this.offerObject[index].discountPrice,
            "quantity": 1,
            "type": "offer"
          };
          this.setofferLocally.push(Offer)
          this.storage.set('CARTITEM', this.setofferLocally);
          this.itemCount++;
          this.totalAmount = this.totalAmount + this.offerObject[index].discountPrice;
        }
      });
  }

  //TO REMOVE THE PRODUCT FROM CART.
  removeFromCart(index) {
    this.offerObject[index].status = !this.offerObject[index].status;
    this.storage.get('CARTITEM').then((val) => {
      val.splice(val.length - 1, 1);
      this.setofferLocally = val;
      this.storage.set('CARTITEM', this.setofferLocally);
    });
    this.itemCount--;
    this.totalAmount = this.totalAmount - this.offerObject[index].discountPrice;
    if (this.totalAmount == 0) {
      this.oneItemSelected = false;
    }
  }

  //NAVIGATING TO CARTS ITEM PAGE.
  goToCart1() {
    if (this.oneItemSelected == true) {
      this.checkAccess = this.checkPermissions.checkAccessibility("CartItemsPage");
      if (this.login == true) { //IF USER IS A REGISTERED MEMBER. [I.E USER HAS LOGGED IN]
        if (this.checkAccess) {
          var CartObj = {
            'claimedOffer': this.claimedOffer,
            'price': 5000,
            'couponCode': "MIT 2002",
            'purpose': "Anniversary Offer",
            'purposeIcon': "gm.svg"
          }
          this.cartProvider.storeItems(CartObj)
          this.navCtrl.push("CartItemsPage", {
            'offerFlowCart': true,
            'offername': this.claimedOffer.OfferName
          });
        }
      } else {
        this.navCtrl.setRoot(MainPage); //IF USER IS A GUEST MEMBER. [I.E USER HAS NOT LOGGED IN]
      }
    }
  }

  //NAVIGATING TO NOTIFICATION LIST PAGE.
  goToNotification() {
    this.navCtrl.push(NotificationlistPage);
  }

}
