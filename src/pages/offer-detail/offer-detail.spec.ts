// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { OfferDetailPage } from './offer-detail';
// import { CheckPermissionsProvider } from '../../providers/check-permissions/check-permissions';
// import { CartItemsProvider } from '../../providers/cart-items/cart-items';
// import { CartBillingProvider } from '../../providers/cart-billing/cart-billing';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// describe('Offer Details Page UI Testing', () => {
//     let de: DebugElement;
//     let comp: OfferDetailPage;
//     let fixture: ComponentFixture<OfferDetailPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 OfferDetailPage
//             ],
//             imports: [
//                 IonicModule.forRoot(OfferDetailPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 CheckPermissionsProvider,
//                 CartItemsProvider,
//                 CartBillingProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(OfferDetailPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Page component should be created.", () => expect(comp).toBeDefined());

//     it("Header should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".Header")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Navigation Bar should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".NavigationBar")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Nav Bar Title should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".Title")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Notification Icon should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".NotificationIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Offer Card should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".offercards")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Offer Background Image should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".cbackground")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Offer Title should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".ctitle")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Offer Short Name should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".ctxtheader1")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Offer Validity should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".cvalidity")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Offer Description should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".cdescription")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Radio Buttons for product should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".radiobuttonlist")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Package for product should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(OfferDetailPage);
//             de = fixture.debugElement.query(By.css(".package")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Combined Prices of product should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(OfferDetailPage);
//             de = fixture.debugElement.query(By.css(".combinedprices")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Striked of prices should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(OfferDetailPage);
//             de = fixture.debugElement.query(By.css(".strickedprice")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Product Price should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(OfferDetailPage);
//             de = fixture.debugElement.query(By.css(".price")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Checked Icon should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(OfferDetailPage);
//             de = fixture.debugElement.query(By.css(".checked")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Footer should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".foot")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Footer row should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".footrow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Cart Items Column should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".cartitems")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Cart Logo Should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".cartlogo")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Number of items should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".chkoutinnerrow1")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Amount of Cart should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".chkoutinnerrow2")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Taxes should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".chkoutinnerrow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Cart Column should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(OfferDetailPage);
//             de = fixture.debugElement.query(By.css(".viewcart")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Cart div should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".vtxt")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Arrow icon in cart column should be present.", async () => {
//         fixture = TestBed.createComponent(OfferDetailPage);
//         de = fixture.debugElement.query(By.css(".arrowicon")).nativeElement;
//         expect(de).toBeDefined();
//     });
// });





