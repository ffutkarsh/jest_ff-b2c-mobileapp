// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams, FabContainer, ViewController } from 'ionic-angular/index';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { DashboardPage } from './dashboard';
// import { OfferCardDashboardPage } from '../../pages/offer-card-dashboard/offer-card-dashboard';
// import { ScheduleCardDashboardPage } from '../../pages/schedule-card-dashboard/schedule-card-dashboard';
// import { CheckPermissionsProvider } from '../../providers/check-permissions/check-permissions';
// import { DashboardFlowApiProvider } from '../../providers/dashboard-flow-api/dashboard-flow-api';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';

// export class ViewControllerMock {
//     readReady = {
//         subscribe() { }
//     };
//     writeReady = {
//         subscribe() { }
//     };
//     dismiss() { }
//     _setHeader() { }
//     _setNavbar() { }
//     _setIONContent() { }
//     _setIONContentRef() { }
// }

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// //The describe function is for grouping related specs
// describe('---------DashboardPage PAGE UI----------', () => {
//     let de: DebugElement;
//     let comp: DashboardPage;
//     let fixture: ComponentFixture<DashboardPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 DashboardPage,
//                 OfferCardDashboardPage,
//                 ScheduleCardDashboardPage
//             ],
//             imports: [
//                 IonicModule.forRoot(DashboardPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 FabContainer,
//                 { provide: ViewController, useClass: ViewControllerMock },
//                 TranslateService,
//                 TranslateStore,
//                 CheckPermissionsProvider,
//                 DashboardFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(DashboardPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //Creating the main Component
//     it('should create component', () => expect(comp).toBeDefined());

//     it('----------HEADER----------', () => {

//     });

//     //Checking header 
//     it('checking the header using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.NavigationBar')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('NavigationBar');
//     });

//     //header icon using class name
//     it('checking the header ICON using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.MenuToggleIcon')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('MenuToggleIcon');
//     });

//     //title tag using class name
//     it('checking the title using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.Title')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('Title');
//     });

//     it('----------CONTENT----------', () => {

//     });

//     //SEGMENT 1 tag using class name
//     it('checking the Icons Segment using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.SegmentIcons')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('SegmentIcons');
//     });

//     //ClassesSvg into segment 1.
//     it('checking the ClassesSvg using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.ClassesSvg')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('ClassesSvg');
//     });

//     //MySchedulesSvg into segment 1.
//     it('checking the MySchedulesSvg using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.MySchedulesSvg')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('MySchedulesSvg');
//     });

//     //MembershipsSvg into segment 1.
//     it('checking the MembershipsSvg using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.MembershipsSvg')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('MembershipsSvg');
//     });

//     //SEGMENT 2 tag using class name
//     it('checking the Labels Segment using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.SegmentLabel')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('SegmentLabel');
//     });

//     //ClassesLabel into segment 2.
//     it('checking the ClassesLabel using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.ClassesLabel')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('ClassesLabel');
//     });

//     //MySchedulesLabel into segment 2.
//     it('checking the MySchedulesLabel using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.MySchedulesLabel')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('MySchedulesLabel');
//     });

//     //MembershipsLabel into segment 2.
//     it('checking the MembershipsLabel using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.MembershipsLabel')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('MembershipsLabel');
//     });

//     //to check row 1 of offers
//     it('checking row 1 tag using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.OfferRow')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('OfferRow');
//     });

//     //to check offers heading present
//     it('checking offers heading using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.Offer')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('Offer');
//     });

//     //to check row 2 of schedules
//     it('checking the row 2 tag using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.MyScheduleRow')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('MyScheduleRow');
//     });

//     //to check my schedule heading present
//     it('checking my schedule heading using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.MySchedule')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('MySchedule');
//     });

//     //to check row 3 of classes
//     it('checking the row 3 tag using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.UpcomingClassesRow')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('UpcomingClassesRow');
//     });

//     //to check upcoming classes heading present
//     it('checking upcoming classes heading using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.UpcomingClasses')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('UpcomingClasses');
//     });

// });

// describe('---------Upcoming Classes Cards----------', () => {
//     let de: DebugElement;
//     let comp: DashboardPage;
//     let fixture: ComponentFixture<DashboardPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 DashboardPage,
//                 OfferCardDashboardPage,
//                 ScheduleCardDashboardPage
//             ],
//             imports: [
//                 IonicModule.forRoot(DashboardPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 FabContainer,
//                 { provide: ViewController, useClass: ViewControllerMock },
//                 TranslateService,
//                 TranslateStore,
//                 CheckPermissionsProvider,
//                 DashboardFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(DashboardPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //Creating the main Component
//     it('should create component', () => expect(comp).toBeDefined());

//     it('checking if card present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(DashboardPage);
//             de = fixture.debugElement.query(By.css(".UpcomingClassesListColumn1")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('checking if avatar present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(DashboardPage);
//             de = fixture.debugElement.query(By.css(".TrainerAvatar")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('checking if purpose icon present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(DashboardPage);
//             de = fixture.debugElement.query(By.css(".PurposeIcon")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('checking if name slot present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(DashboardPage);
//             de = fixture.debugElement.query(By.css(".SeesionName")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('checking if time slot present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(DashboardPage);
//             de = fixture.debugElement.query(By.css(".SessionDateTime")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('checking if duration slot present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(DashboardPage);
//             de = fixture.debugElement.query(By.css(".SessionDuration")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

// });

// describe('---------Fab Button----------', () => {
//     let de: DebugElement;
//     let comp: DashboardPage;
//     let fixture: ComponentFixture<DashboardPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 DashboardPage,
//                 OfferCardDashboardPage,
//                 ScheduleCardDashboardPage
//             ],
//             imports: [
//                 IonicModule.forRoot(DashboardPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 FabContainer,
//                 { provide: ViewController, useClass: ViewControllerMock },
//                 TranslateService,
//                 TranslateStore,
//                 CheckPermissionsProvider,
//                 DashboardFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(DashboardPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //FAB Button - Test Case 1
//     it('Fab button should be present.', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.FabButton')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('FabButton');
//     });

//     //FAB Button - Test Case 2
//     it('Main Favicon button should be present.', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.MainFavicon')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('MainFavicon');
//     });

//     //FAB Button - Test Case 3
//     it('Fab top button should be present.', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.TopFab')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('TopFab');
//     });

//     //FAB Button - Test Case 4
//     it('Fab middle button should be present.', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.MiddleFab')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('MiddleFab');
//     });

//     //FAB Button - Test Case 5
//     it('Fab bottom button should be present.', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.BottomFab')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('BottomFab');
//     });

// });

// describe('----------Dashboard PAGE METHODS----------', () => {
//     let de: DebugElement;
//     let comp: DashboardPage;
//     let fixture: ComponentFixture<DashboardPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 DashboardPage,
//                 OfferCardDashboardPage,
//                 ScheduleCardDashboardPage
//             ],
//             imports: [
//                 IonicModule.forRoot(DashboardPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 FabContainer,
//                 { provide: ViewController, useClass: ViewControllerMock },
//                 TranslateService,
//                 TranslateStore,
//                 CheckPermissionsProvider,
//                 DashboardFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(DashboardPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //checking go to offer method
//     it('checking gotoOffer method ', () => {
//         expect(comp.gotoOffersPage).toBeTruthy();
//     });

//     //checking goToMySchedules method
//     it('checking goToMySchedules method ', () => {
//         expect(comp.goToMySchedules).toBeTruthy();
//     });

//     //checking goToMemberships method
//     it('checking goToMemberships method ', () => {
//         expect(comp.goToMemberships).toBeTruthy();
//     });

//     //checking goToBookAppoint method
//     it('checking goToBookAppoint method ', () => {
//         expect(comp.goToBookAppoint).toBeTruthy();
//     });

//     //checking goToSearchClasses method
//     it('checking goToSearchClasses method ', () => {
//         expect(comp.goToSearchClasses).toBeTruthy();
//     });

//     //checking goToBookFacility method
//     it('checking goToBookFacility method ', () => {
//         expect(comp.goToBookFacility).toBeTruthy();
//     });

// });