import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, FabContainer, ViewController, MenuController } from 'ionic-angular';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import {
  NotificationlistPage,
  ClassesPage,
  SchedulingPage,
  NewMyMembershipsPage,
  OfferPage,
  ClassDetailsPage,
  AppointmentModalPage,
  SelectFacilityModalPage,
  MainPage
} from '../../pages/pages';
import { CheckPermissionsProvider } from '../../providers/check-permissions/check-permissions';
import { DashboardFlowApiProvider } from '../../providers/dashboard-flow-api/dashboard-flow-api';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {

  public fetchDialCode; //FROM OTP PAGE.
  public fetchPhoneNumber; //FROM OTP PAGE.
  public selectedCenterObject; //FROM OTP PAGE.
  public otpResponse; //FROM OTP PAGE.
  public otpVerified; //FROM OTP PAGE.

  public verificationFlag; //FROM OTP PAGE.
  public memberType; //FROM OTP PAGE.
  public permissions; //FROM OTP PAGE.
  public checkAccess; //FOR CHECKING ACCESS PERMISSIONS.

  public offersList = []; //STORING API RESPONSE OF OFFERS.
  public scheduleList = []; //STORING API RESPONSE OF SCHEDULES.
  public classList = []; //STORING API RESPONSE OF CLASSES.
  public purposeList = []; //STORING API RESPONSE OF PURPOSES.
  public facilityList = []; //STORING API RESPONSE OF FACILITIES.

  public classArray = []; //TO STORE THE ARRAY OF CLASSES. 
  public date = []; //FOR STORING DATE IN GIVEN FORMAT.
  public timeToGo = []; //FOR STORING HOURS TO GO FOR THE SESSION.

  public colorarray = []; //ARRAY TO STORE COLOR.

  public toggle: boolean = false; //TO TOGGLE THE BACKGROUND ON FAB CLOSE-OPEN.

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalctrl: ModalController,
    public fab: FabContainer,
    public viewCtrl: ViewController,
    public menuCtrl: MenuController,
    public translateService: TranslateService,
    public checkPermissions: CheckPermissionsProvider,
    private dashboardFlowApi: DashboardFlowApiProvider,
    private storage: Storage) {

    this.menuCtrl.enable(true, 'myMenu'); //ENABLING MENU.

    this.fetchDialCode = navParams.get("defaultDialcode");
    this.fetchPhoneNumber = navParams.get("phoneNumber");
    this.selectedCenterObject = this.navParams.get('memberDetails');
    this.otpResponse = this.navParams.get('otpResponse');
    this.otpVerified = this.navParams.get('otpVerified');

    this.verificationFlag = navParams.get('verificationFlag');
    this.memberType = navParams.get('memberType');
    this.permissions = navParams.get('permissions');

    this.getOfferDetails();
    this.getSchedulelist();
    this.getClassList();
    this.getPurposeList();
    this.getSpecificFacilityDetails();

    //CREATING CARDS WITH RANDOM COLOR AS BACKGROUND
    this.colorarray = [
      '#1abc9c', '#2980b9', '#e74c3c', '#a0a5a8',
      '#9b59b6', '#34495e', '#d35400', '#39a7f1',
      '#1abc9c', '#2980b9', '#e74c3c', '#a0a5a8',
      '#9b59b6', '#34495e', '#d35400', '#39a7f1',
      '#1abc9c', '#2980b9', '#e74c3c', '#a0a5a8',
      '#9b59b6', '#34495e', '#d35400', '#39a7f1',
      '#1abc9c', '#2980b9', '#e74c3c', '#a0a5a8',
      '#9b59b6', '#34495e', '#d35400', '#39a7f1'
    ];

  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true, 'myMenu'); //ENABLING MENU.
  }

  ionViewDidLeave() {
    this.fab.close(); //CLOSE THE FAB ONCE LEFT THE PAGE.
  }

  //API RESPONSE FOR OFFER LIST.
  getOfferDetails() {
    this.dashboardFlowApi.getOfferDetails().map((response) => response).subscribe((response) => {
      this.offersList = response;
    });
  }

  //API RESPONSE FOR SCHEDULE LIST.
  getSchedulelist() {
    this.dashboardFlowApi.getSchedulelist().map((response) => response).subscribe((response) => {
      this.scheduleList = response;
    });
  }

  //API RESPONSE FOR CLASS LIST.
  getClassList() {
    this.dashboardFlowApi.getClassList().map((response) => response).subscribe((response) => {
      this.classList = response;
      this.classArray = this.classList['ClassList'];
      for (let i = 0; i < this.classArray.length; i++) {
        this.date[i] = moment(this.classArray[i].Date).format('DD MMM');
        let combinedDateTime = this.classArray[i].Date + " " + this.classArray[i].StartTime;
        this.timeToGo[i] = moment(combinedDateTime, 'DD MMM YYYY HH:mm A').fromNow();
      }
    });
  }

  //API RESPONSE FOR PURPOSE LIST.
  getPurposeList() {
    this.dashboardFlowApi.getPurposeList().map((response) => response).subscribe((response) => {
      this.purposeList = response['Purposes'];
    });
  }

  //API RESPONSE FOR FACILITY LIST.
  getSpecificFacilityDetails() {
    this.dashboardFlowApi.getSpecificFacilityDetails().map((response) => response).subscribe((response) => {
      this.facilityList = response['selectFacilityApi'];
    });
  }

  //CLOSE THE FAB ACCORDING TO THE BACKGROUND.
  closeFab(fab: FabContainer) {
    this.toggleBackground()
    fab.close();
  }

  //TOGGLE THE BACKGROUND.
  toggleBackground() {
    this.toggle = !this.toggle;
  }

  //NAVIGATING TO NOTIFICATION LIST PAGE.
  goToNotification() {
    this.navCtrl.push(NotificationlistPage);
  }

  //NAVIGATING TO CLASSES PAGE.
  goToSearchClasses() {
    this.checkAccess = this.checkPermissions.checkAccessibility("ClassesPage");
    if (this.checkAccess && this.toggle == false) {
      this.navCtrl.setRoot(ClassesPage); //IF USER IS A REGISTERED MEMBER. [I.E USER HAS LOGGED IN]
    }
    else if (this.checkAccess && this.toggle == true) {

    }
    else if (this.checkAccess == false) {
      this.navCtrl.setRoot(MainPage); //IF USER IS A GUEST MEMBER. [I.E USER HAS NOT LOGGED IN]
    }
  }

  //NAVIGATING TO SCHEDULING PAGE.
  goToMySchedules() {
    this.checkAccess = this.checkPermissions.checkAccessibility("SchedulingPage");
    if (this.checkAccess && this.toggle == false) {
      this.navCtrl.setRoot(SchedulingPage); //IF USER IS A REGISTERED MEMBER. [I.E USER HAS LOGGED IN]
    }
    else if (this.checkAccess && this.toggle == true) {

    }
    else if (this.checkAccess == false) {
      this.navCtrl.setRoot(MainPage); //IF USER IS A GUEST MEMBER. [I.E USER HAS NOT LOGGED IN]
    }
  }

  //NAVIGATING TO MEMBERSHIP PAGE.
  goToMemberships() {
    this.checkAccess = this.checkPermissions.checkAccessibility("NewMyMembershipsPage");
    if (this.checkAccess && this.toggle == false) {
      this.navCtrl.push(NewMyMembershipsPage); //IF USER IS A REGISTERED MEMBER. [I.E USER HAS LOGGED IN]
    } else if (this.checkAccess && this.toggle == true) {

    }
    else if (this.checkAccess == false) {
      this.navCtrl.setRoot(MainPage); //IF USER IS A GUEST MEMBER. [I.E USER HAS NOT LOGGED IN]
    }
  }

  //NAVIGATING TO OFFERS PAGE.
  gotoOffersPage() {
    this.checkAccess = this.checkPermissions.checkAccessibility("OfferPage");
    if (this.checkAccess) {
      this.navCtrl.push(OfferPage, {
        'defaultDialcode': this.fetchDialCode,
        'phoneNumber': this.fetchPhoneNumber,
        'memberDetails': this.selectedCenterObject,
        'otpResponse': this.otpResponse,
        'otpVerified': this.otpVerified,
        'verificationFlag': this.verificationFlag,
        'memberType': this.memberType,
        'permissions': this.permissions,
        'offersList': this.offersList
      });
    }
  }

  //NAVIGATING TO CLASS DETAILS PAGE.
  goToClassDetails(c) {
    this.checkAccess = this.checkPermissions.checkAccessibility("AppointmentModalPage");
    if (this.checkAccess) {
      this.navCtrl.push(ClassDetailsPage, { //IF USER IS A REGISTERED MEMBER. [I.E USER HAS LOGGED IN]
        'objectOfC': c
      });
    } else {
      this.navCtrl.setRoot(MainPage); //IF USER IS A GUEST MEMBER. [I.E USER HAS NOT LOGGED IN]
    }
  }

  //NAVIGATING TO APPOINTMENT MODAL PAGE.
  goToBookAppoint(fab: FabContainer) {
    this.checkAccess = this.checkPermissions.checkAccessibility("AppointmentModalPage");
    if (this.checkAccess) {
      let appointModal = this.modalctrl.create(AppointmentModalPage, {
        "purposeApiResponse": this.purposeList
      }); //IF USER IS A REGISTERED MEMBER. [I.E USER HAS LOGGED IN]
      appointModal.present();
    } else {
      this.navCtrl.setRoot(MainPage); //IF USER IS A GUEST MEMBER. [I.E USER HAS NOT LOGGED IN]
    }
    fab.close();
    this.toggleBackground();
  }

  //NAVIGATING TO SEARCH CLASSES PAGE.
  goToSearchClassesbyFab(fab: FabContainer) {
    this.checkAccess = this.checkPermissions.checkAccessibility("ClassesPage");
    if (this.checkAccess) {
      this.navCtrl.setRoot(ClassesPage); //IF USER IS A REGISTERED MEMBER. [I.E USER HAS LOGGED IN]
    } else {
      this.navCtrl.setRoot(MainPage); //IF USER IS A GUEST MEMBER. [I.E USER HAS NOT LOGGED IN]
    }
    fab.close();
    this.toggleBackground();
  }

  //NAVIGATING TO FACILITY MODAL PAGE.
  goToBookFacility(fab: FabContainer) {
    this.checkAccess = this.checkPermissions.checkAccessibility("SelectFacilityModalPage");
    if (this.checkAccess) {
      let facilityModal = this.modalctrl.create(SelectFacilityModalPage, {
        "facilityApiResponse": this.facilityList
      }); //IF USER IS A REGISTERED MEMBER. [I.E USER HAS LOGGED IN]
      facilityModal.present();
    } else {
      this.navCtrl.setRoot(MainPage); //IF USER IS A GUEST MEMBER. [I.E USER HAS NOT LOGGED IN]
    }
    fab.close();
    this.toggleBackground();
  }

}
