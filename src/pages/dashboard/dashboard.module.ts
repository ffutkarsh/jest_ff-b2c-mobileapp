import { NgModule } from '@angular/core';
import { IonicPageModule, FabContainer } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { DashboardPage } from './dashboard';
import { OfferCardDashboardPage } from '../../pages/offer-card-dashboard/offer-card-dashboard';
import { ScheduleCardDashboardPage } from '../../pages/schedule-card-dashboard/schedule-card-dashboard';

@NgModule({
  declarations: [
    DashboardPage,
    OfferCardDashboardPage,
    ScheduleCardDashboardPage
  ],
  imports: [
    IonicPageModule.forChild(DashboardPage),
    TranslateModule
  ],
  providers: [
    FabContainer
  ]
})
export class DashboardPageModule { }
