import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import {
  NotificationlistPage,
  OfferDetailPage
} from '../../pages/pages';
import { CheckPermissionsProvider } from '../../providers/check-permissions/check-permissions';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-offer',
  templateUrl: 'offer.html',
})
export class OfferPage {

  public fetchDialCode; //FROM DASHBOARD PAGE.
  public fetchPhoneNumber; //FROM DASHBOARD PAGE.
  public selectedCenterObject; //FROM DASHBOARD PAGE.
  public otpResponse; //FROM DASHBOARD PAGE.
  public otpVerified; //FROM DASHBOARD PAGE.

  public verificationFlag; //FROM DASHBOARD PAGE.
  public memberType; //FROM DASHBOARD PAGE.
  public permissions; //FROM DASHBOARD PAGE.
  public checkAccess; //FOR CHECKING ACCESS PERMISSIONS.

  public offersList; //FROM DASHBOARD PAGE.

  public offerArray = []; //TO STORE THE ARRAY OF OFFERS. 
  public day = []; //FOR STORING DAY IN GIVEN FORMAT.
  public date = []; //FOR STORING DATE IN GIVEN FORMAT.

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public translateService: TranslateService,
    public checkPermissions: CheckPermissionsProvider,
    private storage: Storage) {

    this.fetchDialCode = navParams.get("defaultDialcode");
    this.fetchPhoneNumber = navParams.get("phoneNumber");
    this.selectedCenterObject = this.navParams.get('memberDetails');
    this.otpResponse = this.navParams.get('otpResponse');
    this.otpVerified = this.navParams.get('otpVerified');

    this.verificationFlag = navParams.get('verificationFlag');
    this.memberType = navParams.get('memberType');
    this.permissions = navParams.get('permissions');

    this.offersList = this.navParams.get('offersList');

  }

  ionViewDidLoad() {
    //CALCULATING DAY AND DATE FROM OFFER VALID TILL USING MOMENT JS
    this.offerArray = this.offersList['Offers'];
    for (let i = 0; i < this.offerArray.length; i++) {
      this.day[i] = moment(this.offerArray[i].OfferValidTill).format('ddd');
      this.date[i] = moment(this.offerArray[i].OfferValidTill).format('DD MMM YYYY');
    }
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true, 'myMenu'); //ENABLING MENU.
  }

  //NAVIGATING TO OFFER DETAILS PAGE.
  goToOfferDetails(i) {
    this.checkAccess = this.checkPermissions.checkAccessibility("OfferDetailPage");
    if (this.checkAccess) {
      this.navCtrl.push(OfferDetailPage, {
        'defaultDialcode': this.fetchDialCode,
        'phoneNumber': this.fetchPhoneNumber,
        'memberDetails': this.selectedCenterObject,
        'otpResponse': this.otpResponse,
        'otpVerified': this.otpVerified,
        'verificationFlag': this.verificationFlag,
        'memberType': this.memberType,
        'permissions': this.permissions,
        'claimedOffer': this.offersList['Offers'][i]
      });
    }
  }

  //NAVIGATING TO NOTIFICATION LIST PAGE.
  goToNotification() {
    this.navCtrl.push(NotificationlistPage);
  }

}
