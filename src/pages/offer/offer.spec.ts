// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { OfferPage } from './offer';
// import { CheckPermissionsProvider } from '../../providers/check-permissions/check-permissions';
// import { DashboardFlowApiProvider } from '../../providers/dashboard-flow-api/dashboard-flow-api';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// describe('Offer Page UI Testing', () => {
//     let de: DebugElement;
//     let comp: OfferPage;
//     let fixture: ComponentFixture<OfferPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 OfferPage
//             ],
//             imports: [
//                 IonicModule.forRoot(OfferPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 CheckPermissionsProvider,
//                 DashboardFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(OfferPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Page component should be created.", () => expect(comp).toBeDefined());

//     it("Header should be present.", async () => {
//         fixture = TestBed.createComponent(OfferPage);
//         de = fixture.debugElement.query(By.css(".Header")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Navigation Bar should be present.", async () => {
//         fixture = TestBed.createComponent(OfferPage);
//         de = fixture.debugElement.query(By.css(".NavigationBar")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Nav Bar Title should be present.", async () => {
//         fixture = TestBed.createComponent(OfferPage);
//         de = fixture.debugElement.query(By.css(".Title")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Notification Icon should be present.", async () => {
//         fixture = TestBed.createComponent(OfferPage);
//         de = fixture.debugElement.query(By.css(".NotificationIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Offer Card should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(OfferPage);
//             de = fixture.debugElement.query(By.css(".offercards")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Offer Background Image should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(OfferPage);
//             de = fixture.debugElement.query(By.css(".cbackground")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Offer Title should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(OfferPage);
//             de = fixture.debugElement.query(By.css(".ctitle")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Offer Short Name should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(OfferPage);
//             de = fixture.debugElement.query(By.css(".ctxtheader1")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Offer Validity should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(OfferPage);
//             de = fixture.debugElement.query(By.css(".cvalidity")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Offer Description should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(OfferPage);
//             de = fixture.debugElement.query(By.css(".cdescription")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

// });





