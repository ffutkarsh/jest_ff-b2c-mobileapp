import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { WaiverTermsProvider } from '../../providers/waiver-terms/waiver-terms';
import { ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-cart-waiver-action-modal',
  templateUrl: 'cart-waiver-action-modal.html',
})
export class CartWaiverActionModalPage {

  public termsHTML; //VARIABLE TO BIND JSON CONTENT INTO HTML.
  public waiverArray = [];
  public waiver = [];

  public cartProducts;
  public additionalProducts;
  public totalProducts;
  public totalPrice;
  public discountAmount;
  public appFlowCart;
  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public ModalController: ModalController,
    public waiverterms: WaiverTermsProvider) {
    this.cartProducts = this.navParams.get('cartProducts');
    this.additionalProducts = this.navParams.get('additionalProducts');
    this.totalProducts = this.navParams.get('totalProducts');
    this.totalPrice = this.navParams.get('totalPrice');
    this.discountAmount = this.navParams.get('discountAmount');
    this.appFlowCart = this.navParams.get("appFlow"); //from app flow.
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartWaiverActionModalPage');
    this.GetWaiverTermsContent(); //CALLING TO GET FETCHED DATA FROM JSON.
  }

  //FETCHING STRINGIFIED DATA FROM ABOUTCONTENT JSON WITH THE HELP OF ABOUTCONTENT PROVIDER.
  GetWaiverTermsContent() {
    this.waiverterms.getWaiverTerms().map((response) => response.json()).subscribe((response) => {
      this.termsHTML = response;
      this.waiverArray = this.termsHTML.Waiver
      this.waiver = this.waiverArray[0].WaiverTerms
    });
  }

  //closes the CartWaiverActionModalPage and opens the modal "pageCartWaiverOtpPage"
  waiverOtp() {
   // this.navCtrl.pop()
    const waiver = this.ModalController.create('CartWaiverOtpPage', {
      'cartProducts': this.cartProducts,
      'additionalProducts': this.additionalProducts,
      'totalProducts': this.totalProducts,
      'totalPrice': this.totalPrice,
      'discountAmount': this.discountAmount,
      'appFlow': this.appFlowCart
    })
    waiver.present({ animate: false });
  }

  waiverEsign() {
    this.navCtrl.push('CartWaiverEsignPage', {
      'cartProducts': this.cartProducts,
      'additionalProducts': this.additionalProducts,
      'totalProducts': this.totalProducts,
      'totalPrice': this.totalPrice,
      'discountAmount': this.discountAmount,
      'appFlow': this.appFlowCart
    })
  }


  // closeModal()
  // {
    closeModal(e)
    {
      console.log(e," value of event")
      if((e.path[1].className) == 'Modal content content-md'  && (e.path[0].className) == 'scroll-content')
      {
        //this.viewCtrl.dismiss();
        console.log("this is modal content")
      }
      else if((e.path[0].className) == 'scroll-content')
      {
        console.log("this is inner button")
        this.viewCtrl.dismiss();
      }
  
    }
  //   this.viewCtrl.dismiss();
  // }
}
