import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CartWaiverActionModalPage } from './cart-waiver-action-modal';


@NgModule({
  declarations: [
    CartWaiverActionModalPage,
  ],
  imports: [
 
    IonicPageModule.forChild(CartWaiverActionModalPage)
  ],
})
export class CartWaiverActionModalPageModule {}
