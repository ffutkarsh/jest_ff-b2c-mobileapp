// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { CartWaiverActionModalPage } from './cart-waiver-action-modal'
// import { IonicModule, NavController,NavParams, ModalController } from 'ionic-angular/index';
// import { WaiverTermsProvider } from '../../providers/waiver-terms/waiver-terms';
// import { IonicStorageModule} from '@ionic/storage';
// import { HttpModule } from '@angular/http';
// import {  HttpClientModule } from '@angular/common/http';


// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }


// describe('cart waiver modal page UI Testing', () => {
//     let comp: CartWaiverActionModalPage;
//     let fixture: ComponentFixture<CartWaiverActionModalPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [CartWaiverActionModalPage],
//             imports: [
//                 HttpModule,
//   HttpClientModule,
//                 IonicModule.forRoot(CartWaiverActionModalPage),
//                 IonicStorageModule.forRoot()
//             ],
//             providers: [
//                 NavController,
//                 WaiverTermsProvider,
//                 { provide: NavParams, useClass: NavParamsMock },
                
               
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(CartWaiverActionModalPage);
//         comp = fixture.componentInstance;
//     });

//     it('Main content', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.main-content')).nativeElement;
//         expect(inputfield.className).toMatch('main-content');
//     });

  
//     it('Modal content', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.Modal')).nativeElement;
//         expect(inputfield.className).toMatch('Modal');
//     });

//     it('Modal terms header', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.termsHeading')).nativeElement;
//         expect(inputfield.className).toMatch('termsHeading');
//     });

//     it('Modal terms content', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.termsContent')).nativeElement;
//         expect(inputfield.className).toMatch('termsContent');
//     });

//     it('Modal terms buttons grid', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.termsButton')).nativeElement;
//         expect(inputfield.className).toMatch('termsButton');
//     });


//     it('OTP button', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.otpButton')).nativeElement;
//         expect(inputfield.className).toMatch('otpButton');
//     });

//     it('E-sign button', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.esignButton')).nativeElement;
//         expect(inputfield.className).toMatch('esignButton');
//     });

//     it('Header content for the modal', () => {
//         let element2 = fixture.debugElement.nativeElement.querySelector(".termsHeading");
//         expect(element2.textContent.trim()).toContain("In order to proceed, you first need to complete Waiver terms & conditions");
//     });

//     it('OTP button text', () => {
//         let element2 = fixture.debugElement.nativeElement.querySelector(".otpButton");
//         expect(element2.textContent.trim()).toContain("OTP");
//     });

//     it('E-sign button text', () => {
//         let element2 = fixture.debugElement.nativeElement.querySelector(".esignButton");
//         expect(element2.textContent.trim()).toContain("E-sign");
//     });

//     it('Waiver title text', () => {
//         let element2 = fixture.debugElement.nativeElement.querySelector(".waiverTitle");
//         expect(element2.textContent.trim()).toContain("Waiver Terms");
//     });

    
//     it("Background image should be present", async () => {
//                 let fixture = TestBed.createComponent(CartWaiverActionModalPage);
//                 let element1 = fixture.debugElement.nativeElement.querySelector(".termsContent");
//                 expect(element1.getAttribute("scrollY")).toContain("true");
          
//             });
            
//     });




// describe('Cart waiver action modal Page logic Testing', () => {
//     let component: CartWaiverActionModalPage;
//     let fixture: ComponentFixture<CartWaiverActionModalPage>;
//     let waiverService: WaiverTermsProvider;
    

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [CartWaiverActionModalPage],
//             imports: [
//                 HttpModule,
//    HttpClientModule,
//                 IonicModule.forRoot(CartWaiverActionModalPage),
//                 IonicStorageModule.forRoot()
//             ],
//             providers: [
//                 NavController,
//                 WaiverTermsProvider,
//                 ModalController,
//                 { provide: NavParams, useClass: NavParamsMock },
                
               
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(CartWaiverActionModalPage);
//         component = fixture.componentInstance;
//         waiverService = fixture.debugElement.injector.get(WaiverTermsProvider);
        
//     });


 

  


//      it('Waiver terms Provider should be created', () => {
//          expect(waiverService).toBeDefined();
//      });
    

//      it('respons matches the content ', () => {
//         let termsHTML = "123";
//         component.GetWaiverTermsContent();
//         expect(termsHTML).toMatch("123");
//     });

//     // it('Waiver E-sign method is present', async () => {
//     //     expect(component.waiverEsign).toBeDefined();
//     // }); 

//     // it('Waiver E-sign method is present', async () => {
//     //     expect(component.waiverOtp).toBeDefined();
//     // }); 

// });
