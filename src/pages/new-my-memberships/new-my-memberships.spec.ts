// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { NewMyMembershipsPage } from './new-my-memberships';
// import { HttpModule } from "@angular/http";
// import { Http, ConnectionBackend } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';
// import { Component } from '@angular/core';
// import { IonicPage,IonicModule, NavController, NavParams } from 'ionic-angular';
// import { MyMembershipProvider } from '../../providers/my-membership/my-membership';
// import { findIndex } from 'rxjs/operators/findIndex';



// describe('Offer Page UI Testing', () => {
//     let de: DebugElement;
//     let comp: NewMyMembershipsPage;
//     let fixture: ComponentFixture<NewMyMembershipsPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [NewMyMembershipsPage],
//             imports: [
//                 HttpModule,
//                 IonicModule.forRoot(NewMyMembershipsPage),
//                 HttpModule,
//                  HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams },
//                 MyMembershipProvider,
//               ConnectionBackend,
//                 TranslateService,
//                 TranslateStore
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(NewMyMembershipsPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
   
//     });

//     it("Page component should be created.", () => expect(comp).toBeDefined());



//     it("Navigation Bar should be present.", async () => {
//         de = fixture.debugElement.query(By.css(".NavigationBar")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Nav Bar Title should be present.", async () => {
//         de = fixture.debugElement.query(By.css(".Title")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Notification Icon should be present.", async () => {
//         de = fixture.debugElement.query(By.css(".NotificationIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Offer Card should be present.", async () => {
//             de = fixture.debugElement.query(By.css(".offercards")).nativeElement;
//             expect(de).toBeDefined();
//         });

//     it("MOL header should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             de = fixture.debugElement.query(By.css(".ctxtheader1")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });


//     it("Validity should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             de = fixture.debugElement.query(By.css(".cvalidity")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });


//     it("Claim button should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             de = fixture.debugElement.query(By.css(".claimbutton")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

    
//     it("Grid should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             de = fixture.debugElement.query(By.css(".emptygrid")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Membership card be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             de = fixture.debugElement.query(By.css(".MembershipsCard")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Refresh icon should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             de = fixture.debugElement.query(By.css(".RefreshIconColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Card heading should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             de = fixture.debugElement.query(By.css(".CardHeading")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

    
//     it("Card Location price should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".CardLocationPrice")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Location price should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".LocationPrice")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Active label should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".LabelActive")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Start date column should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".StartDateColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Start date heading should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".StartDateHeading")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Start date should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".StartDate")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Package ADT should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".PackageAdtColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

    
//     it("Package ADT column should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".PackageAdtColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

    
//     it("Package ADT card should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".PackageAdtCard")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

    
//     it("Package ADT should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".PackageAdt")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Current membership package name should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".PackageName")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("End date column should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".EndDateColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("End date heading name should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".EndDateHeading")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("End date name should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".EndDate")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Contract package name should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".PackageName")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Freeze button should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".FreezeButton")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Upgrade button should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".UpgradeButton")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

    
//     it("Empty Grid row should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".EmptyGridRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Empty Grid column should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".EmptyGridColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

    

    
//     it("Suspended card should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".SuspendedCard")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Suspended card heading should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".SuspendedCardHeading")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Expiry date should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".Date")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

    
//     it("Card location should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".CardLocation")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Suspended card amount should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".Amount")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Suspended Reason row amount should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".SuspendedReasonRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Suspended reason amount should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".SuspendedReason")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Suspended sub text should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".Subtext")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Suspended label should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".LabelSuspended")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Freeze icon should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".FreezeIconColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Freeze label should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".LabelFreeze")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });
    
//     it("Resume button should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".Resume")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Renew button should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".Renew")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Upgrade button should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".Upgrade")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });
// });
    


// describe('My membership method testing', () => {
//     let de: DebugElement;
//     let comp: NewMyMembershipsPage;
//     let fixture: ComponentFixture<NewMyMembershipsPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [NewMyMembershipsPage],
//             imports: [
//                 IonicModule.forRoot(NewMyMembershipsPage),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 MyMembershipProvider,
//                 { provide: NavParams },
//                 TranslateService,
//                 TranslateStore
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(NewMyMembershipsPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//        // de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it('gotoNotifications must be defined', () => {
//         expect(comp.goToNotification).toBeTruthy();
//     });

    
//     it('Initial value of membership should be current', () => {
//         expect(comp.memberShip).toMatch("current");
//     });

//     it('gotoDetails must be defined', () => {
//         expect(comp.gotoDetails).toBeTruthy();
//     });

//     it('fetch membership details must be defined', () => {
//         expect(comp.fetchMembershipDetails).toBeTruthy();
//     });
  

//     it('go to freeze must be defined', () => {
//         expect(comp.openFreezPage).toBeTruthy();
//     });

//     it('go to freeze must be defined', () => {
//         expect(comp.openRenewPage).toBeTruthy();
//     });

//     it('go to freeze must be defined', () => {
//         expect(comp.openUpgradePage).toBeTruthy();
//     });
   
//      });




 






