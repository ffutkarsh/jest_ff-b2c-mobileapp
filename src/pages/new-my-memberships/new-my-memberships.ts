import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { MyMembershipProvider } from '../../providers/my-membership/my-membership';
import { findIndex } from 'rxjs/operators/findIndex';
import { FreezePage, UpgradePage,RenewPage,OfferDetailPage,NotificationlistPage } from '../../pages/pages'
import { TranslateService } from '@ngx-translate/core';
import * as numeral from 'numeral';
import {Pipe,PipeTransform,Injectable} from "@angular/core";
@IonicPage()
@Component({
  selector: 'page-new-my-memberships',
  templateUrl: 'new-my-memberships.html',
})
export class NewMyMembershipsPage {
  numeral:any
  public membershipOffername: any; // var to store offername from json
  public membershipOfferValidTill: any;  // var to store offer validity from json
  public membershipOfferBgImage: any;  // var to store offer bk img from json
  public memberShip; //ngModel
  public membershiOfferlist; // var to store offer list from json
  public currentMembershipData; //contract or non contract
  public membershipOwnedList; //num of memberships
  public membershipFrozenList; //frozen membership list
  public myMembershipData: any = []; //array to store memberhsip
  public currentMembershipitem:any; //var to store package

  public recurringamt; //var to store recurring amt


  constructor(
    public translateService: TranslateService,
    private modalController: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public MyMembershipPrvd: MyMembershipProvider) {
    this.fetchMembershipDetails(); //fetch details from json
    this.memberShip = 'current';  //ngModel
   var currency = numeral(1234.567).format('0,0.[00]');

  }

  //go to Freeze page on button click
  openFreezPage(F) {
    this.navCtrl.push(FreezePage,{'freeze':F});
  }
 //go to upgrade page on button click
  openUpgradePage(U) {
      this.navCtrl.push(UpgradePage,{'upgrade':U,"upgradeflag":true}); 
  }
 //go to renew page on button click
  openRenewPage(R) {
      this.navCtrl.push(RenewPage,{'renew':R,"renewflag":true});
  }

  //fetching details from json
  fetchMembershipDetails() {
    this.MyMembershipPrvd.getMyMembershipDetails().map(response => response).subscribe((response) => {
      this.membershipOffername = response['membership'][0].offer[0].OfferName;
      this.membershipOfferValidTill = response['membership'][0].offer[0].OfferValidTill;
      this.membershipOfferBgImage = response['membership'][0].offer[0].OfferBgImage;
      this.myMembershipData.push(response.membership); //storing details in array

      //fetching frozen and oher minute details
      this.myMembershipData.forEach((element, index) => {
        this.membershiOfferlist = element[index].offer;
        this.currentMembershipData = element[index + 1].currentMembership.Contract;
        for(let i=0;i<this.currentMembershipData.length; i++)
        {
          this.currentMembershipData[i].RecurringAmount = numeral(this.currentMembershipData[i].RecurringAmount).format('0,0.[00]');
        }
        this.currentMembershipitem = element[index + 1].currentMembership.Contract[index].package;
        this.recurringamt = 
        this.membershipOwnedList = element[index+2].membershipowned;
        this. membershipFrozenList = element[index+2].frozen;
      });
  
    });
  }

  //on click of offer button go to offer page
  gotoDetails() {
    let claimedOffer = {
      'OfferName': this.membershipOffername,
      'OfferShortName': this.membershipOffername,
      'OfferValidTill': this.membershipOfferValidTill,
      'OfferDescription': this.membershipOffername,
      'OfferBgImage': this.membershipOfferBgImage
    }
    this.navCtrl.push(OfferDetailPage
    , {
      'claimedOffer': claimedOffer
    });
  }

  //on click of bell icon - open noti page
  goToNotification(){
    this.navCtrl.push(NotificationlistPage);
  }
}



