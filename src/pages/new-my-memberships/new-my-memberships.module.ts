import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewMyMembershipsPage } from './new-my-memberships';
import { TranslateModule } from '@ngx-translate/core';
import { MomentModule } from 'angular2-moment';
@NgModule({
  declarations: [
    NewMyMembershipsPage
  ],
  imports: [
    IonicPageModule.forChild(NewMyMembershipsPage),
    TranslateModule,
    MomentModule
  ],
})
export class NewMyMembershipsPageModule { }
