import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FacilityDateTimePage } from './facility-date-time';
import { MomentModule } from 'angular2-moment';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FacilityDateTimePage,
  ],
  imports: [
    IonicPageModule.forChild(FacilityDateTimePage),
    MomentModule,
    TranslateModule,
  ],
})
export class FacilityDateTimePageModule {}
