import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import * as moment from 'moment';
import { ValidPhoneProvider } from '../../providers/valid-phone/valid-phone';
import { SelectTrainerProvider } from '../../providers/select-trainer/select-trainer-api';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
    selector: 'page-facility-date-time',
    templateUrl: 'facility-date-time.html',
})
export class FacilityDateTimePage {

    slotSelect;
    purpose: any; //var from nav-params
    icon;//var from nav params
    purposeTotalSessions; //var from nav-params
    trainertype; //var from nav-params
    trainername; //var from nav-params
    trainerImage; //var from nav-params
    date; //finding date
    availableSlot; //var to store available slots
    datelist: any; //array to store list columns
    indexofSlides: number; //var to store index of slide in slider
    fetchDate; //fetching date from slider
    arrayForInfinite = [];
    customArray = [];
    showInfiniteScroll =true;

    //initialising 3 tabs
    tab1Title = " ";
    tab2Title = " ";
    tab3Title = " ";

    now: any; //variable for current date
    displayDate: any; //displaying current date of slider
    calcD: any //calculating date
    beforethat: any; //var to store day before yesterday's date
    before: any; //var to store yesterday's date
    dates: any; //array to store dates
    week: any
    indexOfSlot; //this is a variable to store index of slot
    public trainerList = []; //Array of objects to store json file.
    dFormat: any; //storing today's date
    dddFormat: any; //storing today's day
    TotalSeriesArray: any
    public current: any; //STORES CURRENT DATE AND TIME.
    public startTime: any; //STARTNG TIME OF TRANING.
    public slot: any; //TO CHECK WHETHER SLOT IS AVAILABLE OR NOT.
    public slots: any; //OBJECT OF ARRAYS OF OBJECTS IN SLOTS JSON.
    public sObj: any; //STARTING TIME.
    public addHour: any; //TIME AFTER ADDING INTERVAL.
    public slotTime = []; //TO STORE THE SLOTS AND RETURN IT FOR BINDING WITH THE BUTTONS.
    public halfSlots = []; //ARRAY TO DISPLAY HALF SLOTS.
    public onnLoadMore: boolean = true; //ENABLING THE LOAD MORE BUTTON INITIALLY.

    beforeDays;
    afterDays;
    DisplayafterDays;
    YearArrayl;
    YearArray;
    currentUpcomingDayAfterWeek;

    public selectedFacility = {};

    public selectedSlots;

    public datea;

    setofferLocally;

    @ViewChild(Slides) slides: Slides;

    constructor(
        private validphone: ValidPhoneProvider,
        public navCtrl: NavController,
        public navParams: NavParams,
        private trainer: SelectTrainerProvider,
        public modalctrl: ModalController,
        public translateService: TranslateService,
        private storage: Storage
     ) {

        this.selectedSlots = [];

        this.datelist = [, , , , , , , , , , , , , , ,] //DEFAULT ARRAY OF SLOTS.

        //Fetching from nav params
        //   this.purpose = this.navParams.get("purpose");
        //   this.icon = this.navParams.get("purposeIcon");
        //   this.facilityCourt = this.navParams.get("facilityCourt");
        this.selectedFacility = this.navParams.get("selectedFacility");
        console.log("dfgdgdgdg",this.selectedFacility);
        // this.cartOrbooked = this.navParams.get('cartOrbooked');
        this.beforeDays = [];
        this.afterDays = [];
        //this.DisplayafterDays = [];
        this.YearArray = [];

        this.indexofSlides = 0;

        this.now = moment(); //find date

        this.dates = new Array; //today's date in array
        this.before = moment(this.now).subtract(1, 'days'); //yesterday
        this.beforethat = moment(this.before).subtract(1, 'days'); //day before yesterday

        let temp = {
            'isColor': 'transparent',
            'textColor': '#E5E5E5',
            'date': this.before, //changing yesterdays color
        }

        let temp2 = {
            'isColor': 'transparent',
            'textColor': '#E5E5E5',
            'date': this.beforethat  //changing day before yesterdays color
        }

        let tempNow = {
            'isColor': '#69C258',
            'textColor': 'white',
            'date': this.now //changing todays color
        }

        this.dates.push(temp2); //pushing day before ysterday in array
        this.dates.push(temp); //pushing yesterday date in array
        this.dates.push(tempNow); //pushing todays date in array

        this.calcD = this.now; //calculating future dates
        let day = moment(this.now).format('dddd');
        let start = this.now;
        this.displayDate = this.now;

        for (let i = 3; i <= 360; i++) {
            this.calcD = moment(this.calcD).add(1, 'days'); //adding +1 as date
            let calc = {
                'isColor': 'transparent',
                'textColor': 'black', //changing current dates color
                'date': this.calcD
            }
            this.dates.push(calc); //pushing to date array
        }


        if (day == 'Monday') {
            this.computePrevious(this.beforeDays, 1)
        }
        else if (day == 'Tuesday') {
            this.computePrevious(this.beforeDays, 2);
        }

        else if (day == 'Wednesday') {
            this.computePrevious(this.beforeDays, 3);
        }
        else if (day == 'Thursday') {
            this.computePrevious(this.beforeDays, 4);
        }
        else if (day == 'Friday') {
            this.computePrevious(this.beforeDays, 5);
        } else if (day == 'Saturday') {
            this.computePrevious(this.beforeDays, 6);
        }
        else {
            console.log('start');
        }

        //reverse to start from sunday
        this.beforeDays = this.beforeDays.reverse();
     //   console.log(this.beforeDays)

        // this.afterDays=this.beforeDays;
        // this.afterDays.push(this.now);

        this.computeNext(this.afterDays, this.beforeDays.length)

     //   console.log(this.afterDays)

        this.currentUpcomingDayAfterWeek = this.afterDays[this.afterDays.length - 1];
        //Last Day 

        let beForeLength = this.beforeDays.length
        let FirstWeek = [];
        let afterLength = this.afterDays.length;
        let increment = 0;

        while (beForeLength > 0) {
            let temp = {
                'isColor': 'transparent',
                'textColor': '#E5E5E5',
                'date': this.beforeDays[increment], //changing yesterdays color
            }
            FirstWeek.push(temp);
            increment = increment + 1;
            beForeLength = beForeLength - 1;
        }

        let afterCounter = Math.abs(afterLength - beForeLength);
        let increment2 = 0;
        let tempafter;
        while (afterCounter > 0) {

            if (
                increment2 == 0
            ) {
                tempafter = {
                    'isColor': '#8FDCE7',
                    'textColor': '#8FDCE7',
                    'date': this.afterDays[increment2], //changing yesterdays color
                }
            } else {
                tempafter = {
                    'isColor': 'transparent',
                    'textColor': 'black',
                    'date': this.afterDays[increment2], //changing yesterdays color
                }
            }
            FirstWeek.push(tempafter);
            increment2 = increment2 + 1;
            afterCounter = afterCounter - 1;
        }

        this.YearArray.push(FirstWeek);
      //  console.log("yearrr", this.YearArray);

        this.computeYear(this.currentUpcomingDayAfterWeek);

    }


    ionViewDidLoad() {
     //   console.log('ionViewDidLoad DateAndTimePage');
        this.Getslots(); //CALL FOR GETTING DATA FROM JSON ON VIEW LOAD.
    }



    //GET THE SLOTS FROM JSON.
    Getslots() {
        this.validphone.getSlots().map((response) => response.json()).subscribe((response) => {
            this.slots = response;
            if (this.slots.slots0.length == 0) {
                console.log("No Data");
            }
            else {
                this.datelist = this.slots.slots0; //ASSIGNING VALUES OF SLOTS0 ARRAY FROM JSON TO DEFAULT DATELIST ARRAY.
                for (let i = 0; i < this.slots.slots0.length; i++) {

                    //STARTING TIME.
                    this.sObj = moment(this.slots.slots0[i].startTime, 'hh:mm').format('hh:mm');
                   // console.log(this.sObj);

                    //TIME AFTER ADDING INTERVAL.
                    this.addHour = moment(this.slots.slots0[i].startTime, 'hh:mm').add('hours', 1).format('hh:mm');
                   // console.log(this.addHour);

                    //MAINTAINING SLOTS ARRAY CONTAINING START TIME AND END TIME OF SESSION.
                    this.slotTime[i] = this.sObj + "-" + this.addHour;

                } //END OF FOR.

                for (let i = 0; i < this.slotTime.length; i++) { //displaying buttons on screen
                    this.halfSlots.push(this.slotTime[i]);

                   
                }
                console.log("saving in hs ", this.halfSlots);
                
                this.showBtnArray();
               
                
               
            }
        });
    }

    //DISPLAYING THE SLOTS INTERVAL.
    slotInterval(index) {
       // this.halfSlots = this.slotTime;
        return this.customArray[index];
    }

    chckAvailAbility(slotval) {
        this.slot = false;
        //slotval array present Json Api
        if (slotval) {
            this.slot = true;
        }
        return this.slot
    }

    selectDate(index) {  //displaying a loader on current date selected.
        let l = this.dates.length;

        for (var n = 2; n < l; n++) {
            this.dates[n].isColor = 'transparent';
            this.dates[n].textColor = 'black';
        }

        if (index != 0 && index != 1) {
            this.dates[index].isColor = '#69C258'; //MAKING GREEN COLOR.
            this.dates[index].textColor = 'white';
            this.displayDate = this.dates[index].date;
            this.fetchDate = moment(this.displayDate, 'hh:mm').format('ddd, DD MMM YYYY')

        }
    }

    goToSlide(dir, flag) { //Slider section
        if (flag == 1) {
            if (dir == 'l') { //on press of left arrow
                ++this.indexofSlides;
            }
            if (dir == 'r') { ///on press of right arrow
                this.indexofSlides >= 0 ? --this.indexofSlides : this.indexofSlides = 0;
            }
        }
        else {
            if (dir == 'r') { ///on press of right arrow
                this.slides.slideTo(++this.indexofSlides);
            }
            if (dir == 'l') { //on press of left arrow
                this.slides.slideTo(this.indexofSlides >= 0 ? --this.indexofSlides : this.indexofSlides = 0);
            }
        }
    }

    // nav params to go to new page -  RepeatBooking
    gotoRepeat(i) {
        this.indexOfSlot = i;
        for (let s = 0; s < this.slots.slots0.length; s++) {
            if (s == i) {
                this.slots.slots0[s].status = !this.slots.slots0[s].status;
                if (this.slots.slots0[s].status == true) {
                    //STARTING TIME.
                    this.sObj = moment(this.slots.slots0[this.indexOfSlot].startTime, 'hh:mm').format('hh:mm A');
                    //TIME AFTER ADDING INTERVAL.
                    this.addHour = moment(this.slots.slots0[this.indexOfSlot].startTime, 'hh:mm').add('hours', 1).format('hh:mm A');
                    //MAINTAINING SLOTS ARRAY CONTAINING START TIME AND END TIME OF SESSION.
                    let slottosend = this.sObj + " - " + this.addHour;
                    //ADDING SLOT INTO ARRAY OF SELECTED SLOTS.
                    this.selectedSlots[s] = slottosend;
                    console.log('AFTER SLOT SELECTION');
                    console.log(this.selectedSlots);
                }
                if (this.slots.slots0[s].status == false) {
                    this.selectedSlots[s] = "";
                    console.log('AFTER SLOT DESELECTION');
                    console.log(this.selectedSlots);
                }
            }
        }

    }

    startTimer1() {
        let timer = setTimeout(x => {
            this.goToSchedule();
        }, 20000);
    }

    goToSchedule() {
        this.navCtrl.push("SpecificorAnyTrainerPage", {}, { animate: false });
    }

    slideNext() {
        this.slides.slideNext();
    }

    slidePrev() {
        this.slides.slidePrev();
    }

    selectDateNew(item) {
        console.log(item);
        this.datea = moment(item.date).format('ddd, DD MMM YYYY');
        for (let j = 0; j < 53; j++) {
         //   console.log(item)

            if (j == 0) {
                for (let i = this.beforeDays.length; i < 7; i++) {

                    if (this.YearArray[j][i].date == item.date) {
                        this.YearArray[j][i].isColor = '#8FDCE7';
                        this.YearArray[j][i].textColor = '#8FDCE7';
                    }
                    else {
                        this.YearArray[j][i].isColor = 'transparent';
                        this.YearArray[j][i].textColor = 'black';
                    }
                }
            }
            else {
                for (let i = 0; i < 7; i++) {

                    if (this.YearArray[j][i].date == item.date) {
                        this.YearArray[j][i].isColor = '#8FDCE7';
                        this.YearArray[j][i].textColor = '#8FDCE7';
                    } else {
                        this.YearArray[j][i].isColor = 'transparent';
                        this.YearArray[j][i].textColor = 'black';
                    }
                }
            }
        }
        if (this.date != item) {
            let facilityWarningModal = this.modalctrl.create("FacilityModalPage");
            facilityWarningModal.present();
            this.date = item;
        }
    }

    computePrevious(arr, count) {
        for (let i = 0; i < count; i++) {
            let addDay = moment(this.now).subtract(i + 1, 'days');
            arr.push(addDay);
        }
    }

    computeNext(arr, count) {
        for (let i = 0; i < 7 - count; i++) {
            let addDay = moment(this.now).add(i, 'days');
            arr.push(addDay);
        }
    }

    computeYear(date) {
        //Logic For First week 

        let TempArray = []

        let Dateholder;

        for (let j = 0; j < 52; j++) {

            for (let i = 1; i <= 7; i++) {

                let DatetoBeAdded = moment(date).add(i, 'days');
                let temp = {
                    'isColor': 'transparent',
                    'textColor': 'black',
                    'date': DatetoBeAdded, //changing yesterdays color
                }

                TempArray.push(temp);

                Dateholder = DatetoBeAdded
            }

            this.YearArray.push(TempArray)
            TempArray = []
            date = Dateholder;
        }

    }



    gotoCart() {
       
        const date =  moment().format('llll');
    
        let obj = {
            Amount:5000 ,
            type: "facility",
            purposeName:this.selectedFacility['FacilityName'],
            startduratin: "",
            purposelogo: this.selectedFacility['FacilityPicture'],
            bookdate: date,
            selectedSlots: this.selectedSlots
          };
          console.log(obj)
      
          this.storage.get('CARTITEM').then((val) => {
            this.setofferLocally = val;
          }).then(
            () => {
              if ((this.setofferLocally) || (this.setofferLocally = [])) {
      
                this.setofferLocally.push(obj);
                this.storage.set('CARTITEM', this.setofferLocally);
              }
            });
      
        this.navCtrl.push('CartItemsPage', {
            'facilityFlow': true,
            'selectedFacility': this.selectedFacility,
            'date': this.date,
            'selectedSlots': this.selectedSlots
        })
    }

    doInfinite(infiniteScroll) {
       
        console.log("reached endd !!");
        
        setTimeout(() => {
            
            this.showBtnArray();
            console.log('Async operation has ended');
          
                infiniteScroll.complete();
            
          
          }, 100);
        }

       
       
                //  for(let i=0; i<this.data.data.length; i++) {
                //    this.users.push(this.data.data[i]);
                //  }
             
    

    showBtnArray()
    {
        if(this.customArray.length == 0)
        {
            for(let i=0;i<3;i++)
            {
                this.arrayForInfinite.push("");
                
                this.customArray.push(this.halfSlots[this.customArray.length]);
                this.customArray.push(this.halfSlots[this.customArray.length]);
                this.customArray.push(this.halfSlots[this.customArray.length]);
            }

               
        }
        else
        {
            if(this.arrayForInfinite.length < Math.floor(this.halfSlots.length / 3))
            {
            this.arrayForInfinite.push("");
            this.customArray.push(this.halfSlots[this.customArray.length]);
            this.customArray.push(this.halfSlots[this.customArray.length]);
            this.customArray.push(this.halfSlots[this.customArray.length]);
            }
            if(this.arrayForInfinite.length == Math.floor(this.halfSlots.length / 3))
            {
                this.showInfiniteScroll = false;
            }

        }

    }
}
