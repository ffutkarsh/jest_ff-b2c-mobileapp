// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams, ViewController } from 'ionic-angular/index';
// import { mockView } from "ionic-angular/util/mock-providers";
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { LocationModalPage } from './location-modal';
// import { ClassesFlowApiProvider } from '../../providers/classes-flow-api/classes-flow-api';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';
// import { AndroidPermissions } from '@ionic-native/android-permissions';
// import { Geolocation } from '@ionic-native/geolocation';
// import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
// import { NetworkProvider } from '../../providers/network/network';
// import { Network } from '@ionic-native/network';
// import { OpenNativeSettings } from '@ionic-native/open-native-settings';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }


// describe('Location Modal Page UI Testing', () => {
//     let comp: LocationModalPage;
//     let fixture: ComponentFixture<LocationModalPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 LocationModalPage
//             ],
//             imports: [
//                 IonicModule.forRoot(LocationModalPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 { provide: ViewController, useValue: mockView() },
//                 TranslateService,
//                 TranslateStore,
//                 ClassesFlowApiProvider,
//                 AndroidPermissions,
//                 Geolocation,
//                 NativeGeocoder,
//                 NetworkProvider,
//                 Network,
//                 OpenNativeSettings
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(LocationModalPage);
//         comp = fixture.componentInstance;
//     });

//     it('Nav Bar should be present', () => {
//         const c: HTMLInputElement = fixture.debugElement.query(By.css('.NavigationBar')).nativeElement;
//         expect(c.className).toMatch('NavigationBar');
//     });

//     it('Close Button should be present', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.close')).nativeElement;
//         expect(inputfield.className).toMatch('close');
//     });

//     it('Close Button icon should match', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.closeIcon')).nativeElement;
//         expect(inputfield.className).toMatch('closeIcon');
//     });

//     it("Input should be present", async () => {
//         const c: HTMLInputElement = fixture.debugElement.query(By.css('.locinput')).nativeElement;
//         expect(c.className).toMatch('locinput');
//     });

//     it("Use Current Location Button should be present", async () => {
//         const c: HTMLInputElement = fixture.debugElement.query(By.css('.currentloctxt')).nativeElement;
//         expect(c.className).toMatch('currentloctxt');
//     })

//     it("Recent Search Header should be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const c: HTMLInputElement = fixture.debugElement.query(By.css('.recentheader')).nativeElement;
//             expect(c.className).toMatch('recentheader');
//         });
//     })

//     it("Recent Search List should be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const c: HTMLInputElement = fixture.debugElement.query(By.css('.recentList')).nativeElement;
//             expect(c.className).toMatch('recentList');
//         });
//     })

//     it("Popular Location Header should be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const c: HTMLInputElement = fixture.debugElement.query(By.css('.popularheader')).nativeElement;
//             expect(c.className).toMatch('popularheader');
//         });
//     })

//     it("Popular Location List should be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const c: HTMLInputElement = fixture.debugElement.query(By.css('.popularList')).nativeElement;
//             expect(c.className).toMatch('popularList');
//         });
//     })

// });

// describe('Location Modal Page Logic Testing', () => {
//     let comp: LocationModalPage;
//     let fixture: ComponentFixture<LocationModalPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 LocationModalPage
//             ],
//             imports: [
//                 IonicModule.forRoot(LocationModalPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 { provide: ViewController, useValue: mockView() },
//                 TranslateService,
//                 TranslateStore,
//                 ClassesFlowApiProvider,
//                 AndroidPermissions,
//                 Geolocation,
//                 NativeGeocoder,
//                 NetworkProvider,
//                 Network,
//                 OpenNativeSettings
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(LocationModalPage);
//         comp = fixture.componentInstance;
//     });

//     it('Method to store in Ionic Storage', () => {
//         expect(comp.storelocally).toBeDefined();
//     });

//     it('Method to add New Location in Location History', () => {
//         expect(comp.addNewHistoryItem).toBeDefined();
//     });

//     it('Method to fetch Locations from Location History', () => {
//         expect(comp.getRecentLocations).toBeDefined();
//     });

//     it('Method to Dismiss Modal', () => {
//         expect(comp.closeModal).toBeDefined();
//     });

//     it('Method to fetch recent location data', () => {
//         expect(comp.getData).toBeDefined();
//     });

//     it('Method to fetch Address using current location co-ordinates', () => {
//         expect(comp.chooseCurrentLoc).toBeDefined();
//     });

// });