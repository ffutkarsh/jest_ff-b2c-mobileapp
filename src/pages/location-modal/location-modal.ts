import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events, AlertController, LoadingController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { ClassesFlowApiProvider } from '../../providers/classes-flow-api/classes-flow-api';
import { Storage } from '@ionic/storage';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Geolocation } from '@ionic-native/geolocation';
import {
  NativeGeocoder,
  NativeGeocoderReverseResult,
  NativeGeocoderOptions
} from '@ionic-native/native-geocoder';
import { NetworkProvider } from '../../providers/network/network';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { Subject } from 'rxjs/Subject';

@IonicPage()
@Component({
  selector: 'page-location-modal',
  templateUrl: 'location-modal.html',
})
export class LocationModalPage {

  public locations = []; // Stores list of Popular locations
  public listBackup = []; // COPY OF POPULAR LOCATION.

  public hasLocationAccess: boolean = false; // Stores status of Location Permission

  public sameflag: boolean; // Flag to check if String (location) already in History

  public myInput: any; //VARIABLE TO BIND THE STRING WRITTEN IN SEARCH BAR TO NG-MODEL.

  public userSelection; // Stores User's location Selection

  public recent = {
    recentarray: []
  }; // Object to store recent location

  //USED FOR NETWORK ABSENCE.
  public myObservable = new Subject<boolean>();
  public networkAvailable;
  public networkAlert;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewController: ViewController,
    public events: Events,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public translateService: TranslateService,
    public classesFlowApi: ClassesFlowApiProvider,
    private storage: Storage,
    private androidPermissions: AndroidPermissions,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    public networkProvider: NetworkProvider,
    private openNativeSettings: OpenNativeSettings) {

    this.getData(); // Gets List of Popular Locations
    this.getRecentLocations(); // Gets List of Recent Locations

    this.myInput = ""; //INITIALLY SETTING IT TO NULL.

    this.networkProvider.initializeNetworkEvents();
    this.networkChangesSubscribe();
    this.networkAvailable = (this.networkProvider.isConnected() ? true : false);
    this.myObservable.next(this.networkAvailable);
    this.myObservable.subscribe(val => {
      if (!val) {
        this.showNetworkAlert();
      }
    });

  }

  ionViewDidLeave() {
    this.networkChangesUnsubscribe();
  }

  // Method to get list of available activities
  getData() {
    this.classesFlowApi.getPopularLocations().map((response) => response).subscribe((response) => {
      this.locations = response;
      this.listBackup = this.locations;
    });
  }

  // Gets List of Recent Locations
  getRecentLocations() {
    this.storage.get('recentLocations').then((val) => {
      if (val != null)
        this.recent = val;
    });
  }

  networkChangesSubscribe() {
    // Offline event
    this.events.subscribe('network:offline', () => {  //method to check internet
      this.myObservable.next(false);
      this.networkAvailable = false;
    });
    // Online event
    this.events.subscribe('network:online', () => {
      this.myObservable.next(true);
      this.networkAvailable = true;
    });
  }

  showNetworkAlert() {
    this.networkAlert = this.alertCtrl.create({
      title: 'No Internet Connection',
      message: 'Please check your internet connection.',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Retry',
          handler: () => {
            if (this.networkAvailable == false) {
              this.showNetworkAlert();
            }
          }
        },
        {
          text: 'Open Settings',
          handler: () => {
            this.networkAlert.dismiss().then(() => {
              this.showSettings();
              if (this.networkAvailable == false) {
                this.showNetworkAlert();
              }
            })
          }
        }
      ]
    });
    this.networkAlert.present();
  }

  showSettings() {
    if ((<any>window).cordova && (<any>window).cordova.plugins.settings) {
      (<any>window).cordova.plugins.settings.open("settings", function () { //OPENED SETTINGS

      },
        function () { //FAILED TO OPEN SETTINGS

        }
      );
    } else { //OPEN NATIVE SETTINGS IS NOT ACTIVE

    }
  }

  networkChangesUnsubscribe() {
    this.events.unsubscribe('network:offline');
    this.events.unsubscribe('network:online');
  }

  // Closes Current Modal and sends User's Selection as Param
  closeModal() {
    this.viewController.dismiss(this.userSelection); // Sending User's Selection as Param while closing modal
  }

  //TAKES THE INPUT AND BASED ON IT FILTERS THE LIST.
  onInput($event) {
    this.setFilteredItems(this.myInput)
  }

  //FILTERS THE LIST BY MODIFYING THE LIST AS PER INPUT.
  setFilteredItems(term) {
    this.locations = this.filterItemsSearch(term);
  }

  //MODIFIES THE LIST FROM THE SEARCH TEXT.
  filterItemsSearch(searchText) {
    if (!this.locations) return [];
    if (!searchText) return this.locations;
    searchText = searchText.toLowerCase();
    return this.locations.filter(it => {
      return it.name.toLowerCase().includes(searchText);
    });
  }

  //HANDLES THE BACKPRESSED OR DELETE AND DISPLAYS THE LIST ACCORDING TO CURRENT SEARCH TEXT.
  onBackPress(event) {
    if (event.keyCode == 8 || event.keyCode == 46) {
      this.locations = this.listBackup;
    }
  }

  // Fetches Current Address using current location co-ordinates
  chooseCurrentLoc() {
    // Asking Permission For Android Location access
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      success => {
        console.log('Permission granted');
        this.hasLocationAccess = true;
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
    );
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION]);
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    // Showing Loader while GPS gets a lock
    let loading = this.loadingCtrl.create({
      content: 'Fetching Location...'
    });
    if (this.hasLocationAccess) {
      loading.present();
    }
    this.geolocation.getCurrentPosition().then((resp) => {
      this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude, options)
        .then((result: NativeGeocoderReverseResult[]) => {
          console.log(JSON.stringify(result[0]));
          this.userSelection = result[0].subLocality;
          this.addNewHistoryItem(this.userSelection); // Add name to Recent Location (History)
          loading.dismiss(); // Close Loader after location found
          this.closeModal(); // Close Current Modal
        }
        )
        .catch((error: any) => console.log(error));
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  // Method to add new product to History (Recent Locations) , Takes input Location Name String
  addNewHistoryItem(name) {
    this.userSelection = name;
    this.sameflag = false;
    // Checking if already present in Recent Location
    this.recent.recentarray.forEach((e) => {
      if (e.name == name) {
        this.sameflag = true; // Setting Flag if duplicate found
      }
    });
    // Store only if previously not stored
    if (this.sameflag == false) {
      // Pushin Name to recent Location array
      if (this.recent.recentarray.length <= 3) {
        this.recent.recentarray.push({ name });
      }
      else if (this.recent.recentarray.length > 3) {
        this.recent.recentarray.splice(0, 1);
        this.recent.recentarray.push({ name });
      }
      this.storelocally(this.recent); //  method to store locally on database.
    }
    this.closeModal(); // Closes Current Modal
  }

  //method to store locally on database.
  storelocally(obj) {
    this.storage.set('recentLocations', obj); // Saves to local storage with key "recentLocations"
  }

}