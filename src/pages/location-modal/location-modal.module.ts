import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { LocationModalPage } from './location-modal';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Geolocation } from '@ionic-native/geolocation';

@NgModule({
  declarations: [
    LocationModalPage
  ],
  imports: [
    IonicPageModule.forChild(LocationModalPage),
    TranslateModule
  ],
  providers: [
    AndroidPermissions,
    Geolocation
  ]
})
export class LocationModalPageModule { }
