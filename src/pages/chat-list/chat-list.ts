import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserChatsProvider } from '../../providers/user-chats/user-chats';
import { Injectable } from '@angular/core';
import { HttpClient ,HttpClientModule} from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
/**
 * Generated class for the ChatListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat-list',
  templateUrl: 'chat-list.html',
})
export class ChatListPage {
  msg;
  public userData:any;
  public listToDisplay:any;
  constructor(public http: HttpClient , public navCtrl: NavController, public navParams: NavParams,private UD:UserChatsProvider) {
    this. GetTrainer();
    this.fetchmsgList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatListPage');
  }

  GetTrainer() {
    console.log("hi")
    this.UD.gettarinerData().subscribe(res => {
      console.log(res);
this.userData=res;
console.log(this.userData);
for(let i=0;i<1;i++)
{
  this.userData[i].unread = this.getRandomInt(5);
}

this.listToDisplay = this.userData;
    }
    ,
    err => {
      console.log("Error occured");
    });
  }



  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }


  fetchmsgList() {
    this.getChats().map(response => response ).subscribe((response) => {
      this.msg = response['msgList'];
      console.log(this.msg,"this is the return")


    });
  }



  getChats(): Observable<any> {
    return this.http.get<any>('assets/chat.json');
  }



  gotoChat() {
  
    this.navCtrl.push('ChatWindowPage', {
      'msgList':this.msg
    })}

}
