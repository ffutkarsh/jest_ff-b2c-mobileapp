import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { FilterPage } from './filter';

@NgModule({
  declarations: [
    FilterPage
  ],
  imports: [
    IonicPageModule.forChild(FilterPage),
    TranslateModule
  ],
})
export class FilterPageModule { }
