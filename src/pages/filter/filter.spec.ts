// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams, ViewController } from 'ionic-angular/index';
// import { mockView } from "ionic-angular/util/mock-providers";
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { FilterPage } from './filter';
// import { ClassesFlowApiProvider } from '../../providers/classes-flow-api/classes-flow-api';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }


// describe('FilterPage UI Testing', () => {
//     let de: DebugElement;
//     let comp: FilterPage;
//     let fixture: ComponentFixture<FilterPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 FilterPage
//             ],
//             imports: [
//                 IonicModule.forRoot(FilterPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 { provide: ViewController, useValue: mockView() },
//                 TranslateService,
//                 TranslateStore,
//                 ClassesFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(FilterPage);
//         comp = fixture.componentInstance;
//     });

//     it("Header should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".Header")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Navigation Bar should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".NavigationBar")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Title should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".Title")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Notification button should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".noti")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Notification Icon should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".NotificationIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Time Range grid should be present..", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".fixedbg")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Time Range Label should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".TimeRangeLabel")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Time Slider should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".timeslider")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Selected Start Time should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".selectedStartTime")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Selected End TIme should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".selectedEndTime")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Level Grid should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".LevelGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Level Label should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".TimeRangeLabel")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Level - [Beginner, Medium, Advanced] should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".LevelLabel")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Tags Grid should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".TagsGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Tags Label should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".TagsLabel")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Tags Button should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".tagbuttons")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Tags Button column should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const c: HTMLInputElement = fixture.debugElement.query(By.css('.tagsautocol')).nativeElement;
//             expect(c.className).toMatch('tagsautocol');
//         });
//     });

//     it("Rating Slider Grid should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".RatingSliderGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Rating Slider Label should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".RatingSliderLabel")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Rating Slides group should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".RatingSlides")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Rating Slides should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const c: HTMLInputElement = fixture.debugElement.query(By.css('.Slide')).nativeElement;
//             expect(c.className).toMatch('Slide');
//         });
//     });

//     it("Div for the slider should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const c: HTMLInputElement = fixture.debugElement.query(By.css('.width-slide')).nativeElement;
//             expect(c.className).toMatch('width-slide');
//         });
//     });

//     it("Rating should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const c: HTMLInputElement = fixture.debugElement.query(By.css('.durationtxt')).nativeElement;
//             expect(c.className).toMatch('durationtxt');
//         });
//     });

//     it("Circle around selected rating should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const c: HTMLInputElement = fixture.debugElement.query(By.css('.circlearound')).nativeElement;
//             expect(c.className).toMatch('circlearound');
//         });
//     });

//     it("Location Slider Grid should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".LocationSliderGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Location Slider Label should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".LocationSliderLabel")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Distance Slider should be present.", async () => {
//         fixture = TestBed.createComponent(FilterPage);
//         de = fixture.debugElement.query(By.css(".distanceslider")).nativeElement;
//         expect(de).toBeDefined();
//     });

// });

// describe('FilterPage - Method Test', () => {
//     let comp: FilterPage;
//     let fixture: ComponentFixture<FilterPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 FilterPage
//             ],
//             imports: [
//                 IonicModule.forRoot(FilterPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 { provide: ViewController, useValue: mockView() },
//                 TranslateService,
//                 TranslateStore,
//                 ClassesFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(FilterPage);
//         comp = fixture.componentInstance;
//     });

//     it('Ion View Did Load Method should be present.', () => {
//         expect(comp.ionViewDidLoad).toBeDefined();
//     });

//     it('GotoNotification Method should be present.', () => {
//         expect(comp.goToNotification).toBeDefined();
//     });

// });

// describe('FilterPage - TIme Range Methods', () => {
//     let comp: FilterPage;
//     let fixture: ComponentFixture<FilterPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 FilterPage
//             ],
//             imports: [
//                 IonicModule.forRoot(FilterPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 { provide: ViewController, useValue: mockView() },
//                 TranslateService,
//                 TranslateStore,
//                 ClassesFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(FilterPage);
//         comp = fixture.componentInstance;
//     });

//     it('Slider Changer Method should be present.', () => {
//         expect(comp.sliderChanger).toBeDefined();
//     });

//     it('Generate hours method should be present.', () => {
//         expect(comp.generateHours).toBeDefined();
//     });

//     it('Generate Series method should be present.', () => {
//         expect(comp.generateSeries).toBeDefined();
//     });

// });

// describe('FilterPage - Level Methods', () => {
//     let comp: FilterPage;
//     let fixture: ComponentFixture<FilterPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 FilterPage
//             ],
//             imports: [
//                 IonicModule.forRoot(FilterPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 { provide: ViewController, useValue: mockView() },
//                 TranslateService,
//                 TranslateStore,
//                 ClassesFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(FilterPage);
//         comp = fixture.componentInstance;
//     });

//     it('GotoBeginner method should be present.', () => {
//         expect(comp.goToBeginner).toBeDefined();
//     });

//     it('GotoMedium method should be present.', () => {
//         expect(comp.goToMedium).toBeDefined();
//     });

//     it('GotoAdvanced method should be present.', () => {
//         expect(comp.goToAdvanced).toBeDefined();
//     });

// });

// describe('FilterPage - Tags Methods', () => {
//     let comp: FilterPage;
//     let fixture: ComponentFixture<FilterPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 FilterPage
//             ],
//             imports: [
//                 IonicModule.forRoot(FilterPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 { provide: ViewController, useValue: mockView() },
//                 TranslateService,
//                 TranslateStore,
//                 ClassesFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(FilterPage);
//         comp = fixture.componentInstance;
//     });

//     it('Get Filter Tags method should be present.', () => {
//         expect(comp.getFilterTags).toBeDefined();
//     });

//     it('Click Tag method should be present.', () => {
//         expect(comp.ClickTag).toBeDefined();
//     });

// });

// describe('FilterPage - Rating Methods', () => {
//     let comp: FilterPage;
//     let fixture: ComponentFixture<FilterPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 FilterPage
//             ],
//             imports: [
//                 IonicModule.forRoot(FilterPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 { provide: ViewController, useValue: mockView() },
//                 TranslateService,
//                 TranslateStore,
//                 ClassesFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(FilterPage);
//         comp = fixture.componentInstance;
//     });

//     it('Rating select Method should be present.', () => {
//         expect(comp.SelectDuration).toBeDefined();
//     });

//     it('Generate rating series should be present.', () => {
//         expect(comp.generateRatingSeries).toBeDefined();
//     });

//     it('Display half Rating Method should be present.', () => {
//         expect(comp.displayHalf).toBeDefined();
//     });

// });

// describe('FilterPage - Location Distance Methods', () => {
//     let comp: FilterPage;
//     let fixture: ComponentFixture<FilterPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 FilterPage
//             ],
//             imports: [
//                 IonicModule.forRoot(FilterPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 { provide: ViewController, useValue: mockView() },
//                 TranslateService,
//                 TranslateStore,
//                 ClassesFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(FilterPage);
//         comp = fixture.componentInstance;
//     });

//     it('Distance Slider Changer method should be present.', () => {
//         expect(comp.sliderChangerDis).toBeDefined();
//     });

//     it('Generate distance in Kilometers method should be present.', () => {
//         expect(comp.generateKilometers).toBeDefined();
//     });

// });