import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { ClassesFlowApiProvider } from '../../providers/classes-flow-api/classes-flow-api';
import { Storage } from '@ionic/storage';
import { Slides } from 'ionic-angular';
import { extendMoment } from 'moment-range';
const momentt = extendMoment(moment);

@IonicPage()
@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html',
})
export class FilterPage {

  @ViewChild(Slides) slides: Slides;

  //------------------------------------------------TIME RANGE-----------------------------------------------------------//
  TotalSeriesArray: any;

  public knobValue = { lower: 15, upper: 32 }; // Initial values of dual knob (ion range)
  public leftknob; // Store value of left knob
  public rightknob; // Store value of right knob
  public startTime;  // Saves String of moment time object
  public endTime;  // Saves String of moment time object
  public hours;  // Saves all possible hours of day according to given interval

  //------------------------------------------------LEVEL SECTION--------------------------------------------------------//
  selectedBeginner: boolean;
  selectedMedium: boolean;
  selectedAdvanced: boolean;

  //------------------------------------------------TAGS SECTION---------------------------------------------------------//
  public FilterTags = {};
  public tagsToShow = [];

  //------------------------------------------------RATING SLIDER SECTION------------------------------------------------//
  RatingSeriesArray: any

  //------------------------------------------------LOCATION DISTANCE SLIDER---------------------------------------------//
  DistanceSeriesArray: any;

  public knobValueDis = { lower: 15, upper: 32 }; // Initial values of dual knob (ion range)
  public leftknobDis; // Store value of left knob
  public rightknobDis; // Store value of right knob
  public startDistance; // Saves String of moment time object
  public endDistance; // Saves String of moment time object
  public kilometers; // Saves all possible hours of day according to given interval

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public translateService: TranslateService,
    public classesFlowApi: ClassesFlowApiProvider,
    private storage: Storage) {

    //------------------------------------------------TIME RANGE-----------------------------------------------------------//
    this.TotalSeriesArray = [];
    this.generateSeries();

    this.hours = this.generateHours(); // Generates all possible hours of day according to given interval
    this.sliderChanger(); // Sets intial time string to knob pins
    this.leftknob = ((this.knobValue.lower / 48) * 100) - 2; // Sets intial value to knob
    this.rightknob = ((this.knobValue.upper / 48) * 100) - 1; // Sets intial value to knob

    //------------------------------------------------LEVEL SECTION--------------------------------------------------------//
    this.selectedBeginner = false;
    this.selectedMedium = false;
    this.selectedAdvanced = false;

    //------------------------------------------------TAGS SECTION---------------------------------------------------------//
    this.getFilterTags();

    //------------------------------------------------RATING SLIDER SECTION------------------------------------------------//
    this.RatingSeriesArray = [];
    this.generateRatingSeries();

    //------------------------------------------------LOCATION DISTANCE SLIDER---------------------------------------------//
    this.DistanceSeriesArray = [];
    this.generateDistanceSeries();

    this.kilometers = this.generateKilometers(); // Generates all possible hours of day according to given interval
    this.sliderChanger(); // Sets intial time string to knob pins
    this.leftknobDis = ((this.knobValueDis.lower / 48) * 100) - 2; // Sets intial value to knob
    this.rightknobDis = ((this.knobValueDis.upper / 48) * 100) - 1; // Sets intial value to knob

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FilterPage');
  }

  //------------------------------------------------TIME RANGE-----------------------------------------------------------//
  // Sets time string to knob pins , Fires every time any knob position is changed
  sliderChanger() {

    if (this.knobValue.lower > 0 && this.knobValue.lower <= 41) {
      this.leftknob = ((this.knobValue.lower / 48) * 100) - 2; // Calculating position for Left knob Time String
    }

    if (this.knobValue.upper < 41) {
      this.rightknob = ((this.knobValue.upper / 48) * 100) - 1; // Calculating position for Left knob Time String
    }
    this.startTime = this.hours[this.knobValue.lower]; // Setting Time String for Left knob
    this.endTime = this.hours[this.knobValue.upper]; // Setting Time String for Right knob

    //this.sortByPurpose(this.selectedCategory); // Updating List as per selected purpose and time slot
  }

  // Generates all possible hours of day according to given interval
  generateHours() {
    let result = []; //empty array to store result
    let start = moment("12:00 AM", "hh:mm A"); // Starting at 12:00 AM
    // Generating Slots with 1/2 hour interval
    for (let i = 0; i <= 47; i++) {
      let mdate = start.format("hh:mm A");
      result.push(mdate);
      mdate = start.add(30, 'minutes').format("hh:mm A");
    }
    result.push("11:59 PM"); // Last Entry to array
    return result;
  }


  generateSeries() {

    let currentCount = moment.duration({ 'minutes': 30 });

    for (let i = 0; i < 4; i++) {
      let seriesarray = []

      for (let j = 0; j < 5; j++) {

        let duration = currentCount.clone();
        seriesarray.push(duration)
        duration.add(30, 'minutes');
        currentCount = duration.clone();
      }

      console.log(seriesarray)
      this.TotalSeriesArray.push(seriesarray);

    }

    console.log(this.TotalSeriesArray)
  }

  //------------------------------------------------LEVEL SECTION--------------------------------------------------------//
  goToBeginner() {
    this.selectedBeginner = true;
    this.selectedMedium = false;
    this.selectedAdvanced = false;
  }

  goToMedium() {
    this.selectedBeginner = false;
    this.selectedMedium = true;
    this.selectedAdvanced = false;
  }

  goToAdvanced() {
    this.selectedBeginner = false;
    this.selectedMedium = false;
    this.selectedAdvanced = true;
  }

  //------------------------------------------------TAGS SECTION---------------------------------------------------------//
  getFilterTags() {
    this.classesFlowApi.getFilterTags().map((response) => response).subscribe((response) => {
      this.FilterTags = response;
      this.tagsToShow = this.FilterTags['FilterTags'];
    });
  }

  ClickTag(i) {
    this.tagsToShow[i].TagStatus = !this.tagsToShow[i].TagStatus;
  }

  //------------------------------------------------RATING SLIDER SECTION------------------------------------------------//
  SelectDuration(item) {
    for (let i = 0; i < 4; i++) {
      for (let j = 0; j < 5; j++) {
        if (this.RatingSeriesArray[i][j].duration == item.duration) {
          this.RatingSeriesArray[i][j].isColor = '#99DDE8';
          this.RatingSeriesArray[i][j].textColor = '#8F6CB7';
        } else {
          this.RatingSeriesArray[i][j].isColor = 'transparent';
          this.RatingSeriesArray[i][j].textColor = 'black';
        }
      }
    }
  }

  generateRatingSeries() {
    let currentCount = moment.duration({ 'minutes': 30 });
    for (let i = 0; i < 4; i++) {
      let seriesarray = []
      let Displayseriesarray = [];
      for (let j = 0; j < 5; j++) {
        let duration = currentCount.clone();
        seriesarray.push(duration)
        duration.add(30, 'minutes');
        currentCount = duration.clone();
      }
      for (let j = 0; j < 5; j++) {
        let n = moment.utc(seriesarray[j].as('milliseconds')).format('HH:mm')
        let tempdDur = {
          'duration': n,
          'isColor': "transparent",
          'textColor': 'black',
          'StrDuration': ''
        }
        Displayseriesarray.push(tempdDur)
      }
      console.log(seriesarray)
      this.RatingSeriesArray.push(Displayseriesarray);
    }
    console.log(this.RatingSeriesArray)
  }

  displayHalf(dur, item) {
    let d;
    if (String(dur).includes(':30')) {
      if (String(dur).startsWith('0')) {
        d = String(dur).substr(1, 1);
      } else {
        d = String(dur).substr(0, 2);
      }
      d = d + '.5';
    } else {
      if (String(dur).startsWith('0')) {
        d = String(dur).substr(1, 1);
      } else {
        d = String(dur).substr(0, 2);
      }
    }
    item.StrDuration = d;
    return d
  }

  //------------------------------------------------LOCATION DISTANCE SLIDER---------------------------------------------//

  // Sets time string to knob pins , Fires every time any knob position is changed
  sliderChangerDis() {

    if (this.knobValueDis.lower > 0 && this.knobValueDis.lower <= 41) {
      this.leftknobDis = ((this.knobValueDis.lower / 48) * 100) - 2; // Calculating position for Left knob Time String
    }

    if (this.knobValueDis.upper < 41) {
      this.rightknobDis = ((this.knobValueDis.upper / 48) * 100) - 1; // Calculating position for Left knob Time String
    }
    this.startDistance = this.kilometers[this.knobValueDis.lower]; // Setting Time String for Left knob
    this.endDistance = this.kilometers[this.knobValueDis.upper]; // Setting Time String for Right knob

    //this.sortByPurpose(this.selectedCategory); // Updating List as per selected purpose and time slot
  }

  // Generates all possible Kilometers according to given interval
  generateKilometers() {
    let result = []; //empty array to store result
    let start = moment("12:00 AM", "hh:mm A"); // Starting at 12:00 AM
    // Generating Slots with 1/2 hour interval
    for (let i = 0; i <= 47; i++) {
      let mdate = start.format("hh:mm A");
      result.push(mdate);
      mdate = start.add(30, 'minutes').format("hh:mm A");
    }
    result.push("11:59 PM"); // Last Entry to array
    return result;
  }


  generateDistanceSeries() {

    let currentCount = moment.duration({ 'minutes': 30 });

    for (let i = 0; i < 4; i++) {
      let seriesarray = []

      for (let j = 0; j < 5; j++) {

        let duration = currentCount.clone();
        seriesarray.push(duration)
        duration.add(30, 'minutes');
        currentCount = duration.clone();
      }

      console.log(seriesarray)
      this.DistanceSeriesArray.push(seriesarray);

    }

    console.log(this.DistanceSeriesArray)
  }

  goToNotification() {
    this.navCtrl.push("NotificationlistPage");
  }

}
