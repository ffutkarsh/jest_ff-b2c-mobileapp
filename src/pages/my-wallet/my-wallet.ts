import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { MyWalletProvider } from '../../providers/my-wallet/my-wallet';

@IonicPage()
@Component({
  selector: 'page-my-wallet',
  templateUrl: 'my-wallet.html',
})
export class MyWalletPage {

  public walletdesc = [];
  public balance;
  public input = 0;

  constructor(
    public alertCtrl: AlertController,
    public mywalletprvd: MyWalletProvider,
    public navCtrl: NavController,
    public navParams: NavParams) {
    this.MywalletFetcher();
    this.balance = this.navParams.get('key');
    console.log(this.balance, "this is the value of the wallet balance");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyWalletPage');
  }

  goToPayment(input) {
    this.input = Number(input);
    console.log(this.input,'INPUT')
    if(this.input == 0)
    {
      const alert = this.alertCtrl.create({
        title: 'Input cannot be null',
        buttons: [{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
                console.log('Application exit prevented!');
            }
        }]
    });
    alert.present();
    }
    else{
      this.navCtrl.push("PaymentOptionsPage", {
        'totalPrice': this.input,
        'booleanWallet': true
      })
    }
  
  }

  MywalletFetcher() {
    this.mywalletprvd.getWalletDetails().map(response => response).subscribe((response) => {
      this.walletdesc = response['myWallet'];
      console.log(this.walletdesc, "this is the wallet description response");
    })
  }
}
