import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MyMembershipProvider } from '../../providers/providers';

import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { PurchaseHistoryPage, BuyMainPage } from '../../pages/pages'
@IonicPage()
@Component({
  selector: 'page-transaction-history-list',
  templateUrl: 'transaction-history-list.html',
})
export class TransactionHistoryListPage {

  public phList = []; //fetch details from provider
  public availableHistory = true; //boolean to check length of array
  public time; //var to store time

  constructor(public translateService: TranslateService, private membershipProvider: MyMembershipProvider,
    public navCtrl: NavController, public navParams: NavParams, public http: HttpClient) {
    this.availableHistory = true; //set boolean to true.
    this.PurchasehistoryList() //method to fetch array
  }

  ionViewWillEnter() {
    this.PurchasehistoryList()
  }

  ionViewDidLoad() {
  }

  //onClick of item - go to next page 
  gotopurchasehistory(PH) {
    this.navCtrl.push(PurchaseHistoryPage, {
      'objectPlist': PH //passing the object through nav params
    });
  }

  public PurchasehistoryList() { //method to fetch array
    this.membershipProvider.getPurchaseDetails().map((response) => response).subscribe((response) => {
      this.phList = response['purchaseHistoryList'] //saving in respone
      for(let i =0;i<this.phList.length;i++)
      {
        this.phList[i].time=moment(this.phList[i].time, "HH:mm:ss").format("hh:mm A");
        console.log(this.time,i,'TIME')
      }
      let len = response['purchaseHistoryList'].length //fetching lenght of array
      if (len >= 1) { //if length is greater than 1, then show items
        this.availableHistory = true;
      }
      else {
        this.availableHistory = false; //else show default UI and redirect to buy membership page
      }
    })

  }



  buyMembership() { //Method from default UI
    this.navCtrl.push(BuyMainPage)
  }
}