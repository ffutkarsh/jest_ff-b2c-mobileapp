import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransactionHistoryListPage } from './transaction-history-list';
import { TranslateModule } from '@ngx-translate/core';
import { MomentModule } from 'angular2-moment';
@NgModule({
  declarations: [
    TransactionHistoryListPage,
  ],
  imports: [
    IonicPageModule.forChild(TransactionHistoryListPage),
    TranslateModule,
    MomentModule
  ],
})
export class TransactionHistoryListPageModule {}
