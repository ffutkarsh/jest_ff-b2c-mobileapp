import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { BookedAppointmentPage } from './booked-appointment';
import { Screenshot } from '@ionic-native/screenshot';
import { SocialSharing } from '@ionic-native/social-sharing';

@NgModule({
  declarations: [
    BookedAppointmentPage
  ],
  imports: [
    IonicPageModule.forChild(BookedAppointmentPage),
    TranslateModule
  ],
  providers: [
    Screenshot,
    SocialSharing
  ]
})
export class BookedAppointmentPageModule { }
