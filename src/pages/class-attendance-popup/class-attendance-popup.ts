import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { DashboardPage } from '../../pages/pages';

@IonicPage()
@Component({
  selector: 'page-class-attendance-popup',
  templateUrl: 'class-attendance-popup.html',
})
export class ClassAttendancePopupPage {

  public classattendance = {
    Guid: "",
    XrefID: "",
    ClassId: "",
    CenterAddress: "",
    UserId: "",
    UserName: "",
    UserImage: "",
    PurposeId: "",
    PurposeName: "",
    PurposeLogo: "",
    Date: "",
    StartTime: "",
    DurationEndtime: ""
  }; //TO STORE THE RESPONSE OF CLASS ATTENDANCE.

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public translateService: TranslateService) {
    this.classattendance = {
      Guid: "1",
      XrefID: "123",
      ClassId: "1",
      CenterAddress: "Marol, Andheri(East), Mumbai, Maharashtra",
      UserId: "1",
      UserName: "Elton David",
      UserImage: "sarah-avatar.png.jpeg",
      PurposeId: "1",
      PurposeName: "Zumba",
      PurposeLogo: "zmb.png",
      Date: "16 Jan 2019",
      StartTime: "03:00 PM",
      DurationEndtime: "1.30 h"
    }; //TO STORE THE RESPONSE OF CLASS ATTENDANCE.

  }

  ionViewDidLoad() { }

  //NAVIGATING TO DASHBOARD PAGE.
  gotodash() {
    this.navCtrl.setRoot(DashboardPage);
  }

}
