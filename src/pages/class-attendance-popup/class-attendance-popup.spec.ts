// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { ClassAttendancePopupPage } from './class-attendance-popup';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// // The describe function is for grouping related specs
// describe('Class Attendance Page UI Testing', () => {
//     let comp: ClassAttendancePopupPage;
//     let fixture: ComponentFixture<ClassAttendancePopupPage>;
//     let de: DebugElement;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 ClassAttendancePopupPage
//             ],
//             imports: [
//                 IonicModule.forRoot(ClassAttendancePopupPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore
//             ]

//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(ClassAttendancePopupPage); /////TESTBED CREATES THE OBJECT OF THE CLASSDETAILS  PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance;   //////CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//     });

//     ///// Test Case for content part
//     it("content for class attendance page should be checked", async () => {
//         fixture = TestBed.createComponent(ClassAttendancePopupPage);
//         de = fixture.debugElement.query(By.css(".contentpage")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it('grid for content of class attendance page should be checked', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.contentgrid')).nativeElement;
//             expect(inputfield.className).toMatch('contentgrid');
//         });
//     });

//     it("grid for logo image on page should be checked", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.gridimage')).nativeElement;
//             expect(inputfield.className).toMatch('gridimage');
//         });
//     });

//     it("row for logo image on page should be checked", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.rowimage')).nativeElement;
//             expect(inputfield.className).toMatch('rowimage');
//         });
//     });

//     it("column for logo image on page should be checked", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.colimage')).nativeElement;
//             expect(inputfield.className).toMatch('colimage');
//         });
//     });

//     it("FitnessForce Logo source should match.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(ClassAttendancePopupPage);
//             const imagefield: HTMLInputElement = fixture.debugElement.query(By.css('.logoimage')).nativeElement;
//             expect(imagefield.getAttribute('src')).toMatch('assets/img/img2.jpg');
//         });
//     });

//     it("FitnessForce Logo image to be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.logoimage')).nativeElement;
//             expect(inputfield.className).toMatch('logoimage');
//         });
//     });

//     it("grid for description should be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.classgrid')).nativeElement;
//             expect(inputfield.className).toMatch('classgrid');
//         });
//     });

//     it("row for description should be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.rowclass')).nativeElement;
//             expect(inputfield.className).toMatch('rowclass');
//         });
//     });

//     it("column for description should be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.labelcol')).nativeElement;
//             expect(inputfield.className).toMatch('labelcol');
//         });
//     });

//     it("label to be present for class description", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.label1')).nativeElement;
//             expect(inputfield.className).toMatch('label1');
//         });
//     });

//     it("label to be present for class description", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.label2')).nativeElement;
//             expect(inputfield.className).toMatch('label2');
//         });
//     });

//     it("avatar tag to be present for trainers image", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.avatarimage')).nativeElement;
//             expect(inputfield.className).toMatch('avatarimage');
//         });
//     });

//     it("image to be present forr trainers image", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.trainericon')).nativeElement;
//             expect(inputfield.className).toMatch('trainericon');
//         });
//     });

//     it("grid for location info to be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.gridlocation')).nativeElement;
//             expect(inputfield.className).toMatch('gridlocation');
//         });
//     });

//     it("column for location info to be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.collocation')).nativeElement;
//             expect(inputfield.className).toMatch('collocation');
//         });
//     });

//     it("row for location info to be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.rowlocation')).nativeElement;
//             expect(inputfield.className).toMatch('rowlocation');
//         });
//     });

//     it("image for location icon to be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.blueloc')).nativeElement;
//             expect(inputfield.className).toMatch('blueloc');
//         });
//     });

//     it("Image path for location should be match", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(ClassAttendancePopupPage);
//             const imagefield: HTMLInputElement = fixture.debugElement.query(By.css('.blueloc')).nativeElement;
//             expect(imagefield.getAttribute('src')).toMatch('assets/icon/locationblue.svg');
//         });
//     });

//     it("column for location info to be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.collabel')).nativeElement;
//             expect(inputfield.className).toMatch('collabel');
//         });
//     });

//     it("row for location info to be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.rowCapacity')).nativeElement;
//             expect(inputfield.className).toMatch('rowCapacity');
//         });
//     });

//     it("label for location info should be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.labelLocation')).nativeElement;
//             expect(inputfield.className).toMatch('labelLocation');
//         });
//     });

//     it("grid for time info to be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.gridtimer')).nativeElement;
//             expect(inputfield.className).toMatch('gridtimer');
//         });
//     });

//     it("row for time info to be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.rowtimer')).nativeElement;
//             expect(inputfield.className).toMatch('rowtimer');
//         });
//     });

//     it("col for time info to be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.coltimer')).nativeElement;
//             expect(inputfield.className).toMatch('coltimer');
//         });
//     });

//     it("icon for time info to be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.icontimer')).nativeElement;
//             expect(inputfield.className).toMatch('icontimer');
//         });
//     });

//     it("label for time info to be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.lblAvailable')).nativeElement;
//             expect(inputfield.className).toMatch('lblAvailable');
//         });
//     });

//     it("grid for button to be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.gridbutton')).nativeElement;
//             expect(inputfield.className).toMatch('gridbutton');
//         });
//     });

//     it("row for button to be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.rowButton')).nativeElement;
//             expect(inputfield.className).toMatch('rowButton');
//         });
//     });

//     it("col for no button should be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.colCancel')).nativeElement;
//             expect(inputfield.className).toMatch('colCancel');
//         });
//     });

//     it("row for trainers image should be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.rowtrainericon')).nativeElement;
//             expect(inputfield.className).toMatch('rowtrainericon');
//         });
//     });

//     it("button should be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.noButton')).nativeElement;
//             expect(inputfield.className).toMatch('noButton');
//         });
//     });

//     it("col for yes button should be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.colConfirm')).nativeElement;
//             expect(inputfield.className).toMatch('colConfirm');
//         });
//     });

//     it("button should be present", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.yesButton')).nativeElement;
//             expect(inputfield.className).toMatch('yesButton');
//         });
//     });

// });