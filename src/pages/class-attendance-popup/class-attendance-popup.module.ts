import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ClassAttendancePopupPage } from './class-attendance-popup';

@NgModule({
  declarations: [
    ClassAttendancePopupPage
  ],
  imports: [
    IonicPageModule.forChild(ClassAttendancePopupPage),
    TranslateModule
  ]
})
export class ClassAttendancePopupPageModule { }
