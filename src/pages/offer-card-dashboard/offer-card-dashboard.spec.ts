// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { OfferCardDashboardPage } from './offer-card-dashboard';
// import { CheckPermissionsProvider } from '../../providers/check-permissions/check-permissions';
// import { DashboardFlowApiProvider } from '../../providers/dashboard-flow-api/dashboard-flow-api';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// //The describe function is for grouping related specs
// describe('---------Offer Card Dashboard Page PAGE UI----------', () => {
//     let de: DebugElement;
//     let comp: OfferCardDashboardPage;
//     let fixture: ComponentFixture<OfferCardDashboardPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 OfferCardDashboardPage
//             ],
//             imports: [
//                 IonicModule.forRoot(OfferCardDashboardPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 CheckPermissionsProvider,
//                 DashboardFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(OfferCardDashboardPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //Creating the main Component
//     it('should create component', () => expect(comp).toBeDefined());

//     //Checking the slide tag using class name
//     it('checking the Main Slides using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.mainSlides')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('mainSlides');
//     });

//     //Checking the slide tag using class name
//     it('checking the Inner Slides using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.slide1')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('slide1');
//         });
//     });

//     //Checking the card tag using class name
//     it('checking the Inner Card using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.card1')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('card1');
//         });
//     });

//     //Checking the image tag using class name
//     it('checking the Image using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.imageClass')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('imageClass');
//         });
//     });

//     //Checking the Title tag using class name
//     it('checking the Title using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.title1')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('title1');
//         });
//     });

//     //Checking the Subtitle tag using class name
//     it('checking the Subtitle using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.title2')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('title2');
//         });
//     });

//     //Checking the Description tag using class name
//     it('checking the Description using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.title3')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('title3');
//         });
//     });

//     //Checking the Validity tag using class name
//     it('checking the Validity using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.title4')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('title4');
//         });
//     });

//     //Checking the row tag using class name
//     it('checking the Row using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row1')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('row1');
//         });
//     });

//     //Checking the button tag using class name
//     it('checking the button using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.button1')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('button1');
//         });
//     });
// });

// describe('----------DashboardFlowApiProvider PAGE METHODS----------', () => {
//     let de: DebugElement;
//     let comp: OfferCardDashboardPage;
//     let fixture: ComponentFixture<OfferCardDashboardPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 OfferCardDashboardPage
//             ],
//             imports: [
//                 IonicModule.forRoot(OfferCardDashboardPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 CheckPermissionsProvider,
//                 DashboardFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(OfferCardDashboardPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //checking json method
//     it('checking json method ', () => {
//         expect(comp.getOfferDetails).toBeTruthy();
//     });

//     //checking goToOfferDetails method
//     it('checking goToOfferDetails method ', () => {
//         expect(comp.goToOfferDetails).toBeTruthy();
//     });
// });