import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { OfferDetailPage } from '../../pages/pages';
import { CheckPermissionsProvider } from '../../providers/check-permissions/check-permissions';
import { DashboardFlowApiProvider } from '../../providers/dashboard-flow-api/dashboard-flow-api';
import { Storage } from '@ionic/storage';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-offer-card-dashboard',
  templateUrl: 'offer-card-dashboard.html',
})
export class OfferCardDashboardPage {

  @ViewChild(Slides) slides: Slides; //THIS IS USED FOR CREATING SLIDES.

  public fetchDialCode; //FROM DASHBOARD PAGE.
  public fetchPhoneNumber; //FROM DASHBOARD PAGE.
  public selectedCenterObject; //FROM DASHBOARD PAGE.
  public otpResponse; //FROM DASHBOARD PAGE.
  public otpVerified; //FROM DASHBOARD PAGE.

  public verificationFlag; //FROM DASHBOARD PAGE.
  public memberType; //FROM DASHBOARD PAGE.
  public permissions; //FROM DASHBOARD PAGE.
  public checkAccess; //FOR CHECKING ACCESS PERMISSIONS.

  public offersList = []; //STORING API RESPONSE OF OFFERS.

  public offerArray = []; //TO STORE THE ARRAY OF OFFERS. 
  public day = []; //FOR STORING DAY IN GIVEN FORMAT.
  public date = []; //FOR STORING DATE IN GIVEN FORMAT.

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public translateService: TranslateService,
    public checkPermissions: CheckPermissionsProvider,
    private dashboardFlowApi: DashboardFlowApiProvider,
    private storage: Storage) {

    this.fetchDialCode = navParams.get("defaultDialcode");
    this.fetchPhoneNumber = navParams.get("phoneNumber");
    this.selectedCenterObject = this.navParams.get('memberDetails');
    this.otpResponse = this.navParams.get('otpResponse');
    this.otpVerified = this.navParams.get('otpVerified');

    this.verificationFlag = navParams.get('verificationFlag');
    this.memberType = navParams.get('memberType');
    this.permissions = navParams.get('permissions');

    this.getOfferDetails();

  }

  ionViewDidLoad() { }

  //API RESPONSE FOR OFFER LIST.
  getOfferDetails() {
    this.dashboardFlowApi.getOfferDetails().map((response) => response).subscribe((response) => {
      this.offersList = response;
      this.offerArray = this.offersList['Offers'];
      for (let i = 0; i < this.offerArray.length; i++) {
        this.day[i] = moment(this.offerArray[i].OfferValidTill).format('ddd');
        this.date[i] = moment(this.offerArray[i].OfferValidTill).format('DD MMM YYYY');
      }
    });
  }

  //NAVIGATING TO OFFERS PAGE.
  goToOfferDetails(i) {
    this.checkAccess = this.checkPermissions.checkAccessibility("OfferPage");
    if (this.checkAccess) {
      this.navCtrl.push(OfferDetailPage, {
        'defaultDialcode': this.fetchDialCode,
        'phoneNumber': this.fetchPhoneNumber,
        'memberDetails': this.selectedCenterObject,
        'otpResponse': this.otpResponse,
        'otpVerified': this.otpVerified,
        'verificationFlag': this.verificationFlag,
        'memberType': this.memberType,
        'permissions': this.permissions,
        'claimedOffer': this.offersList['Offers'][i]
      });
    }
  }

}
