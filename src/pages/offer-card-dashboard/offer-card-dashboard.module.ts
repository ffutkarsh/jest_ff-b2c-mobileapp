import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { OfferCardDashboardPage } from './offer-card-dashboard';

@NgModule({
  declarations: [
    OfferCardDashboardPage
  ],
  imports: [
    IonicPageModule.forChild(OfferCardDashboardPage),
    TranslateModule
  ],
})
export class OfferCardDashboardPageModule { }
