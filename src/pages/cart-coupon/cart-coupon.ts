import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { CartCouponsProvider, BillingProvider } from '../../providers/providers';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import * as moment from 'moment';
import { ApplyCouponModalPage } from '../../pages/pages';

@IonicPage()
@Component({
  selector: 'page-cart-coupon',
  templateUrl: 'cart-coupon.html',
})

export class CartCouponPage {
  couponApplied=[];
  public couponDetails; //Array of objects to store json file.
  public CouponArray = []; //to store coupon offers in an array from response
  public colorarray = []; //Array to store random colors
  public index; //to store index
  public couponObject; //var to store coupon object
  public couponTitle; //var to store coupon title
  public empOffer; // var to check employee offers
  searchTerm: string = ''; //input of search
  searchControl: FormControl;
  searching: any = false; //setting searching false
  validityDate;
  lenghtOf;
  in;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    public translateService: TranslateService,
    private modalController: ModalController,
    public billingProvider: BillingProvider,
    public couponService: CartCouponsProvider) {
    this.fetchCouponDetails() //fetching detailds from json.
    this.searchControl = new FormControl(); //Search controls
console.log(this.billingProvider.receivedTotal,"THIS IS RECEIVED IN COUPON")
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartCouponPage');
    this.setFilteredItems(); //checking searching inputs and flitering
    this.searchControl.valueChanges.debounceTime(700).subscribe(search => { //spinner
      this.searching = false; //set search false when page enters again
      this.setFilteredItems();
    });

  }

  //from json using its provider
  fetchCouponDetails() {
    this.couponService.getAllCouponsDetails().map((response) => response).subscribe((response) => {
      this.couponDetails = response;
      console.log(this.couponDetails, "COUPONSS")
      this.CouponArray = this.couponDetails.Offers;
      //creating  random colors as border-left
      for (this.index = 0; this.index < this.CouponArray.length; this.index++) {
        this.colorarray.push(this.getColor(this.index));
      }
    });

  }

  //creating random color 
  getColor(i) {
    return "rgb(" + 360 * Math.random() + ',' +
      (25 + 70 * Math.random()) + '%,' +
      (85 + 10 * Math.random()) + '%)';
  }

  //from html ngStyle
  setMyStyles1(i) {
    this.index = i;
    let style = {
      'color': this.colorarray[this.index],
    };
    return style; // returning color of specific index
  }

  //from html ngStyle
  setMyStyles2(i) {
    this.index = i;
    let style = {
      'background-color': this.colorarray[this.index],
    };
    return style; // returning color of specific index
  }

  //storing title of coupon on coupon click.
  couponCliked(i, item) {
    this.couponApplied.push(item)
    this.index = i;
    if (this.index == i) {
      this.couponTitle = this.CouponArray[this.index].OfferShortName //fetching detailed title from json

      var dis = this.CouponArray[this.index].OfferDiscount;
      this.billingProvider.discount(dis) //sending to provider
      this.validityDate = moment(this.validityDate).format('Do MMM YYYY');

      this.couponObject = { //object to store data locally
        'coupon': this.couponTitle,
        'booleanTitle': false
      }
      this.storelocally() //method call to store poject locally

      //generating modal on click.
      let contactModal = this.modalController.create(ApplyCouponModalPage, {
        'indexOfCoupon': this.index,
        'itemreceived': item,
        'appliedCoupon' : this.couponApplied
      });
      contactModal.present();
    }
    console.log(this.billingProvider.receivedTotal,"THIS IS RECEIVED IN COUPON")
  }


  storelocally() { //storing the object locally
    this.storage.set('coupondetail', this.couponObject).then((val) => {
      this.storage.get('coupondetail').then((val) => {
      });
    });
  } // Save Object on local storage 



  //serach logic
  filterItems(searchTerm) {
    return this.CouponArray.filter((item) => {
      return item.code.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }
  //setting boolean true , allowing to filter
  onSearchInput() {
    this.searching = true;
  }

  //filtering entire array based on any keyword in title.
  setFilteredItems() {
    this.CouponArray = this.filterItems(this.searchTerm);
  }

  //HANDLES THE BACKPRESSED OR DELETE AND DISPLAYS THE LIST ACCORDING TO CURRENT SEARCH TEXT.
  onBackPress(event) {
    if (event.keyCode == 8 || event.keyCode == 46) {
      this.fetchCouponDetails();
    }
  }

}


