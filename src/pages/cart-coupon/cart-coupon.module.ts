import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CartCouponPage } from './cart-coupon';
import { TranslateModule } from '@ngx-translate/core';
import { MomentModule } from 'angular2-moment';


@NgModule({
  declarations: [
    CartCouponPage,
  ],
  imports: [
    IonicPageModule.forChild(CartCouponPage),
    TranslateModule,
    MomentModule
  ],
})
export class CartCouponPageModule {}
