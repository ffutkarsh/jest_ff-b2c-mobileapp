// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, style } from '@angular/core';
// import { CartCouponPage } from './cart-coupon';
// import { IonicModule, Platform, NavController, NavParams, Item } from 'ionic-angular/index';
// import { HttpModule } from '@angular/http';
// import { CartItemsProvider } from '../../providers/cart-items/cart-items';
// import { CartCouponsProvider,BillingProvider } from '../../providers/providers';

// import { RelatedItemsProvider } from '../../providers/related-items/related-items';
// import { IonicStorageModule, Storage } from '@ionic/storage';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { MomentModule } from 'angular2-moment';
// import {  HttpClientModule } from '@angular/common/http';
// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }
// //The describe function is for grouping related specs
// describe('---------CART COUPON PAGE PAGE UI----------', () => {
//     let de: DebugElement;
//     let comp: CartCouponPage;
//     let fixture: ComponentFixture<CartCouponPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [CartCouponPage],
//             imports: [
//                 HttpModule,
//                 HttpClientModule,
//                 IonicStorageModule.forRoot(),
//                 IonicModule.forRoot(CartCouponPage),
//                 TranslateModule.forChild(),
//                 MomentModule,
//             ],
//             providers: [
//                 CartItemsProvider,
//                 CartCouponsProvider,
//                 RelatedItemsProvider,
//                 TranslateService,
//                 TranslateStore,
              
//                 BillingProvider,
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(CartCouponPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });
//     //Creating the main Component
//     it('should create component', () => expect(comp).toBeDefined());

 


//     //checking the title
//     it('checking the title using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.title')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('title');
//     });
//     //checking the content
//     it('checking the content using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.contentClass')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('contentClass');
//     });


//     //checking the search div
//     it('checking the serach div using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.searchDiv')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('searchDiv');
//     });


//     //checking the content
//     it('checking the List using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.listClass')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('listClass');
//     });

//     //Checking the item tag using class name
//     it('checking the item of list using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.list1')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('list1');
//         });
//     });


//     //Checking the card tag using class name
//     it('checking the card using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.cardItem')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('cardItem');
//         });
//     });

//     //Checking the row tag using class name
//     it('checking the row using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('row');
//         });
//     });

//     //Checking the col tag using class name
//     it('checking the column 1 which consists of avatar using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.col1')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('col1');
//         });
//     });




//     //checking the avatar tag of item using class name
//     it('checking the avatar tag of item using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.avatarClass')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('avatarClass');
//         });
//     });

//     //checking the avatar tag of item using class name
//     it('checking the image tag of item using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.imageClass')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('imageClass');
//         });
//     });


//     //Checking the col tag using class name
//     it('checking the column 2 which consists of coupon title and description using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.col2')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('col2');
//         });
//     });


//     //Checking the col tag using class name
//     it('checking the column 2 coupon title', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.couponTitle')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('couponTitle');
//         });
//     });

//     //Checking the col tag using class name
//     it('checking the column 2 coupon decription', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.couponDesc')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('couponDesc');
//         });
//     });



//     //Checking the col tag using class name
//     it('checking the column 3 which consists of apply button using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.col3')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('col3');
//         });
//     });

//     //Checking the div tag using class name
//     it('checking the div which consists of validity ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.cardSubItem')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('cardSubItem');
//         });
//     });

//     //Checking the div tag using class name
//     it('checking the validity ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.validity')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('validity');
//         });
//     });

// });

// //The describe function is for grouping related specs
// describe('---------CART COUPON PAGE PAGE LOGIC - METHODS----------', () => {
//     let de: DebugElement;
//     let comp: CartCouponPage;
//     let fixture: ComponentFixture<CartCouponPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [CartCouponPage],
//             imports: [
//                 HttpModule,
//                 HttpClientModule,
//                 IonicStorageModule.forRoot(),
//                 IonicModule.forRoot(CartCouponPage),
//                 TranslateModule.forChild(),
//                 MomentModule,
//             ],
//             providers: [
//                 CartItemsProvider,
//                 CartCouponsProvider,
//                 RelatedItemsProvider,
//                 TranslateService,
             
//                 TranslateStore,
//                 BillingProvider,
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(CartCouponPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //from json using its provider
//     it('checking the method from json using its provider', () => {
//         expect(comp.fetchCouponDetails).toBeTruthy();
//     });
//     //checking the method of setMyStyles for setting style as background color
//     it('checking the method of setMyStyles for setting style as background color ', () => {
//         expect(comp.setMyStyles1).toBeTruthy();
//     });
//     //checking the method of setMyStyles for setting style as color 
//     it('checking the method of setMyStyles for setting style as color ', () => {
//         expect(comp.setMyStyles2).toBeTruthy();
//     });

//     //checking the method which generates random color 
//     it('checking the method which generates random color ', () => {
//         expect(comp.getColor).toBeTruthy();
//     });

//     //storing title of coupon on coupon click.
//     it('checking the method which checks click method', () => {
//         expect(comp.couponCliked).toBeTruthy();
//     });


//  //checking the method of couponCliked by passing arguments 
//  it('checking the method of couponCliked by passing arguments ', () => {
//     fixture.whenStable().then(() => {
//         // after something in the component changes, you should detect changes
//         fixture.detectChanges();
//         // everything else in the beforeEach needs to be done here.
//         var i,c;
//         expect(comp.couponCliked.apply(this, [i,c]));
//     });
// });


//     //storing the object locally
//     it('checking the method which stores the object locally', () => {
//         expect(comp.storelocally).toBeTruthy();
//     });

//     //serach logic
//     it('checking the method which contains search logic', () => {
//         expect(comp.filterItems).toBeTruthy();
//     });



//     //checking the method of filterItems by passing arguments 
//     it('checking the method of filterItems by passing arguments ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             var i;
//             expect(comp.filterItems.apply(this, [i]));
//         });
//     });


//     //checking the method of filterItems to be called
//     it('checking the method of filterItems to be called ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             var i;
          
//             expect(comp.filterItems).toHaveBeenCalled();
//         });
//     });


    

//     //checking the method of filterItems to be called
//     it('checking the method of filterItems to be searched ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             var i;
  
//             expect(comp.filterItems(i)).toBe(i);
//         });
//     });


//     //setting boolean true , allowing to filter
//     it('checking the method which sets boolean true , allowing to filter', () => {
//         expect(comp.onSearchInput).toBeTruthy();
//     });
//     //filtering entire array based on any keyword in title.
//     it('checking the method which filters entire array based on any keyword in title.', () => {
//         expect(comp.setFilteredItems).toBeTruthy();
//     });
//     //HANDLES THE BACKPRESSED OR DELETE AND DISPLAYS THE LIST ACCORDING TO CURRENT SEARCH TEXT.
//     it('checking the method which handles backpressed events', () => {
//         expect(comp.onBackPress).toBeTruthy();
//     });
// });

// //The describe function is for grouping related specs
// describe('---------CART COUPON PAGE PAGE LOGIC - VARIABLES ----------', () => {
//     let de: DebugElement;
//     let comp: CartCouponPage;
//     let fixture: ComponentFixture<CartCouponPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [CartCouponPage],
//             imports: [
           
//                 HttpModule,
//                 HttpClientModule,
//                 IonicStorageModule.forRoot(),
//                 IonicModule.forRoot(CartCouponPage),
//                 TranslateModule.forChild(),
//                 MomentModule,
//             ],
//             providers: [
//                 CartItemsProvider,
//                 CartCouponsProvider,
//                 RelatedItemsProvider,
//                 TranslateService,
//                 TranslateStore,
              
//                 BillingProvider,
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(CartCouponPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });
//     //checking Array of coupon list to store json file.
//     it('checking Array of coupon list to store json file. ', () => {
//         expect(comp.couponDetails).toBeUndefined();
//     });
//     //Storing randomly generate color
//     it('checking Array OF randomly generate color ', () => {
//         expect(comp.colorarray).toBeTruthy();
//     });

//     //to store index 
//     it('checking the variable to store index  ', () => {
//         expect(comp.index).toBeUndefined();
//     });

//     //var to store coupon object
//     it('var to store coupon object  ', () => {
//         expect(comp.couponObject).toBeUndefined();
//     });

//     //var to store coupon title
//     it('var to store coupon title  ', () => {
//         expect(comp.couponTitle).toBeUndefined();
//     });
//     //var to check employee offers
//     it('var to check employee offers  ', () => {
//         expect(comp.empOffer).toBeUndefined();
//     });
//     //input of search
//     it('input of search  ', () => {
//         expect(comp.searchTerm).toMatch('');
//     });

//     //setting searching false
//     it('setting searching false ', () => {
//         expect(comp.searching).toBeFalsy();
//     });
// });