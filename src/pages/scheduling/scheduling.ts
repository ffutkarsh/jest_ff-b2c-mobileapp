import { Component, ViewChild } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  FabContainer,
  ViewController,
  MenuController,
  AlertController,
  ToastController,
  App,
  Platform,
  Nav,
  Navbar
} from 'ionic-angular';
import * as moment from 'moment';
import {
  NativeGeocoder,
  NativeGeocoderOptions
} from '@ionic-native/native-geocoder';
import { TranslateService } from '@ngx-translate/core';
import {
  NotificationlistPage,
  AppointmentModalPage,
  ClassesPage,
  SelectFacilityModalPage,
  ClassDetailsPage,
  BookedAppointmentPage,
  FacilityBookedDetailsPage
} from '../../pages/pages';
import { DashboardFlowApiProvider } from '../../providers/dashboard-flow-api/dashboard-flow-api';
import { Geolocation } from '@ionic-native/geolocation';
import { Storage } from '@ionic/storage';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import {IpgeofetchProvider} from "../../providers/ipgeofetch/ipgeofetch";


@IonicPage()
@Component({
  selector: 'page-scheduling',
  templateUrl: 'scheduling.html',
})
export class SchedulingPage {

  @ViewChild(Nav) nav: Nav;
  @ViewChild(Navbar) navBar: Navbar;

  public scheduleList = []; //STORING API RESPONSE OF SCHEDULES.
  public combinedArray = []; //STORING CLASSLIST ARRAY, APPOINTMENTLIST ARRAY AND FACILITY ARRAY RECEIVED FROM RESPONSE.
  public classList = []; //STORING API RESPONSE OF CLASSES.
  public purposeList = []; //STORING API RESPONSE OF PURPOSES.
  public facilityList = []; //STORING API RESPONSE OF FACILITIES.

  public classArray = []; //TO STORE THE ARRAY OF CLASSES. 
  public date = []; //FOR STORING DATE IN GIVEN FORMAT.
  public timeToGo = []; //FOR STORING HOURS TO GO FOR THE SESSION.

  public toggle: boolean = false; //TO TOGGLE THE BACKGROUND ON FAB CLOSE-OPEN.

  public withdraw: boolean = false; //TO SHOW TOAST MESSAGE IF CAME FROM WITHDRAW.
  public join: boolean = false; //TO SHOW TOAST MESSAGE IF CAME FROM JOIN.

  public scheduleKey: boolean = false;

  public latnow = 19.119860; //LATITUDE OF CURRENT LOCATION. [HARDCODED]
  public longnow = 72.883461; //LONGITUDE OF CURRENT LOCATION. [HARDCODED]

  public locationMethodFlag = false; // Variable used to switch location fetching method
                                     // Use FALSE for IP Based Location
                                     // Use TRUE for GPS Based Location (Not working on higher Android API Level)

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalctrl: ModalController,
    public fab: FabContainer,
    public viewCtrl: ViewController,
    public menuCtrl: MenuController,
    public alertCtrl: AlertController,
    private toastCtrl: ToastController,
    public app: App,
    public platform: Platform,
    private androidPermissions: AndroidPermissions,
    public translateService: TranslateService,
    private dashboardFlowApi: DashboardFlowApiProvider,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    public ipgeo:IpgeofetchProvider,
    private storage: Storage) {

    this.menuCtrl.enable(true, 'myMenu'); //ENABLING MENU.

    this.getSchedulelist();
    this.getClassList();
    this.getPurposeList();
    this.getSpecificFacilityDetails();
    this.getCurrentLoc();

    this.withdraw = this.navParams.get('withdraw');
    this.join = this.navParams.get('join');

   
    //TO SHOW TOAST MESSAGE IF CAME FROM WITHDRAW.
    if (this.withdraw == true) {
      let toast = this.toastCtrl.create({
        message: 'Your class booking has been cancelled.',
        duration: 3000,
        position: 'bottom'
      });
      this.menuCtrl.enable(true, 'myMenu');
      //DISMISSING THE TOAST.
      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
        this.menuCtrl.enable(true, 'myMenu'); //ENABLING MENU.
      });
      //DISPLAYING THE TOAST.
      toast.present();
    }

    //TO SHOW TOAST MESSAGE IF CAME FROM JOIN.
    if (this.join == true) {
      let toast = this.toastCtrl.create({
        message: 'You have successfully joined the class',
        duration: 3000,
        position: 'bottom',
        showCloseButton: true,
        closeButtonText: "Undo" //CREATING UNDO BUTTON.
      });
      //DISMISSING THE TOAST.
      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
        this.menuCtrl.enable(true, 'myMenu'); //ENABLING MENU.
      });
      //DISPLAYING THE TOAST.
      toast.present();
    }

  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true, 'myMenu'); //ENABLING MENU.
    this.scheduleKey = this.navParams.get('scheduleKey');
    if (this.scheduleKey) {
      this.viewCtrl.showBackButton(false);
      this.platform.registerBackButtonAction(() => {
        let nav = this.app.getActiveNavs()[0];
        if (nav.canGoBack()) { //Can we go back?
          const alert = this.alertCtrl.create({
            title: 'Do you want to close the app?',
            buttons: [{
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Application exit prevented!');
              }
            }, {
              text: 'Close App',
              handler: () => {
                this.platform.exitApp(); // Close this application
              }
            }]
          });
          alert.present();
        }
      });
    }
  }

  ionViewDidLeave() {
    this.fab.close(); //CLOSE THE FAB ONCE LEFT THE PAGE.
  }

  //API RESPONSE FOR SCHEDULE LIST.
  getSchedulelist() {
    this.dashboardFlowApi.getSchedulelist().map((response) => response).subscribe((response) => {
      this.scheduleList = response;
      this.combinedArray = this.scheduleList['ClassList'].concat(this.scheduleList['Appointmentlist']).concat(this.scheduleList['Facilitylist']);
      for (let i = 0; i < this.combinedArray.length; i++) {
        this.date[i] = moment(this.combinedArray[i].Date).format('DD MMM');
        let combinedDateTime = this.combinedArray[i].Date + " " + this.combinedArray[i].StartTime;
        this.timeToGo[i] = moment(combinedDateTime, 'DD MMM YYYY HH:mm A').fromNow();
      }
      this.addDistance();
    });
  }

  //API RESPONSE FOR CLASS LIST.
  getClassList() {
    this.dashboardFlowApi.getClassList().map((response) => response).subscribe((response) => {
      this.classList = response;
      this.classArray = this.classList['ClassList'];
      for (let i = 0; i < this.classArray.length; i++) {
        this.date[i] = moment(this.classArray[i].Date).format('DD MMM');
        let combinedDateTime = this.classArray[i].Date + " " + this.classArray[i].StartTime;
        this.timeToGo[i] = moment(combinedDateTime, 'DD MMM YYYY HH:mm A').fromNow();
      }
    });
  }

  //API RESPONSE FOR PURPOSE LIST.
  getPurposeList() {
    this.dashboardFlowApi.getPurposeList().map((response) => response).subscribe((response) => {
      this.purposeList = response['Purposes'];
    });
  }

  //API RESPONSE FOR FACILITY LIST.
  getSpecificFacilityDetails() {
    this.dashboardFlowApi.getSpecificFacilityDetails().map((response) => response).subscribe((response) => {
      this.facilityList = response['selectFacilityApi'];
    });
  }

  //Fetching City name from Co-Ordinates (Currently Hardcoded) - Error
  addDistance() {
    // for (let i = 0; i < this.combinedArray.length; i++) {
    //   let rand = this.templocs[Math.floor(Math.random() * this.templocs.length)];
    //   this.combinedArray[i].dist = rand.dist;
    //   this.combinedArray[i].classlocation = rand.name;
    // }
    // for (let i = 0; i < this.scheduleList['Appointmentlist'].length; i++) {
    //   let rand = this.templocs[Math.floor(Math.random() * this.templocs.length)];
    //   this.combinedArray[i].dist = rand.dist;
    //   this.combinedArray[i].classlocation = rand.name;
    // }
    // for (let i = 0; i < this.scheduleList['Facilitylist'].length; i++) {
    //   let rand = this.templocs[Math.floor(Math.random() * this.templocs.length)];
    //   this.combinedArray[i].dist = rand.dist;
    //   this.combinedArray[i].classlocation = rand.name;
    // }
    let latlong = [];
    for (let i = 0; i < this.combinedArray.length; i++) {
      let coordinates = this.combinedArray[i].CentreLocation;
     
    
       latlong = coordinates.split(",");
      // console.log(latlong);
       
       this.combinedArray[i].dist = this.distBetweenPlaces(latlong[0],latlong[1], this.latnow,this.longnow);
       console.log( this.combinedArray[i].dist);
     
    if(!this.locationMethodFlag)
    {
      this.ipgeo.reverseGeo(latlong[0], latlong[1]).subscribe(data => {
        console.log(data);
        if(data.address.city)
        {
          this.combinedArray[i].classlocation = data.address.city;
        }
        else
        if(data.address.village)
        {
          this.combinedArray[i].classlocation = data.address.village;
        }
        else
        {
          this.combinedArray[i].classlocation = data.address.town;
        }
        
      });
    }
    else
    {
     // let rand = this.templocs[Math.floor(Math.random() * this.templocs.length)];
     // this.classList[i].dist = rand.dist;
       let options: NativeGeocoderOptions = {
         useLocale: true,
         maxResults: 5
     };
    
     this.nativeGeocoder.reverseGeocode(latlong[0], latlong[1], options)
       .then((result) =>{ console.log(JSON.stringify(result[0]));
        this.classList[i].classlocation = result[0].locality;
      })
       .catch((error: any) => console.log(error));
      
    }
    
    }
  }

  //CLOSE THE FAB ACCORDING TO THE BACKGROUND.
  closeFab(fab: FabContainer) {
    this.toggleBackground()
    fab.close();
  }

  //TOGGLE THE BACKGROUND.
  toggleBackground() {
    this.toggle = !this.toggle;
  }

  //NAVIGATING TO NOTIFICATION LIST PAGE.
  goToNotification() {
    this.navCtrl.push(NotificationlistPage);
  }

  //NAVIGATING TO APPOINTMENT MODAL PAGE.
  goToBookAppoint(fab: FabContainer) {
    let appointModal = this.modalctrl.create(AppointmentModalPage, {
      "purposeApiResponse": this.purposeList
    });
    appointModal.present();
    fab.close();
    this.toggleBackground();
  }

  //NAVIGATING TO SEARCH CLASSES PAGE.
  goToSearchClassesbyFab(fab: FabContainer) {
    this.navCtrl.setRoot(ClassesPage);
    fab.close();
    this.toggleBackground();
  }

  //NAVIGATING TO FACILITY MODAL PAGE.
  goToBookFacility(fab: FabContainer) {
    let facilityModal = this.modalctrl.create(SelectFacilityModalPage, {
      "facilityApiResponse": this.facilityList
    });
    facilityModal.present();
    fab.close();
    this.toggleBackground();
  }

  //NAVIGATING TO RESPECTIVE PAGES.
  goToMySchedules(schedule) {
    if (schedule['ClassId']) {
      this.navCtrl.push(ClassDetailsPage, {
        'objectOfC': schedule
      });
    }
    else if (!schedule['ClassId'] && !schedule['FacilityId']) {
      this.navCtrl.push(BookedAppointmentPage, {
        'objectOfD': schedule
      });
    }
    else if (schedule['FacilityId']) {
      this.navCtrl.push(FacilityBookedDetailsPage, {
        'objectOfF': schedule
      });
    }
  }

  //TO CALCULATE THE RATING.
  starRating(rating: number) {
    let full;
    if (rating <= 5) {
      full = Math.floor(rating);
    }
    else {
      full = 5;
    }
    let status = [];
    for (let i = 0; i < full; i++) {
      status.push("fulle.svg"); // saves Full Star svg in array
    }
    if (rating - full == 0.5 && !(rating >= 5)) {
      status.push("halfe.svg"); // saves Half Star svg in array
    }
    if (status.length < 5) {
      let emp = 5 - status.length;
      for (let i = 0; i < emp; i++) {
        status.push("fullgrey.svg");// saves Empty Star svg in array
      }
    }
    return status; // Returns array so that it can be rendered using *ngFor
  }

  distBetweenPlaces(lat1,long1,lat2,long2)
  {
    let radlat1 = Math.PI * Number(lat1)/180;
    let radlat2 = Math.PI * Number(lat2)/180;
    let radlon1 = Math.PI * Number(long1)/180;
    let radlon2 = Math.PI * Number(long2)/180;
    let theta = long1-long2;
    let radtheta = Math.PI * theta/180
    let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180/Math.PI
    dist = dist * 60 * 1.1515 * 1.609344;
    return  Math.round(dist * 100) / 100;
     
  }

    //Method to get current Location
    getCurrentLoc() {

      if(!this.locationMethodFlag)
      {
        this.ipgeo.getLocfromIP().subscribe(data => {
          let latlong = data.loc.split(",");
          //console.log("latlong",latlong);
          this.latnow = latlong[0];
          this.longnow = latlong[1];
         
          });
      }
      else
      {
      // Asking Permission For Android Location access
       this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
        success => console.log('Permission granted'),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
      );
      this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION]);
      let options: NativeGeocoderOptions = {
        useLocale: true,
        maxResults: 5
      };
      this.geolocation.getCurrentPosition().then((resp) => {
        this.latnow = resp.coords.latitude;
        this.longnow = resp.coords.longitude;
      }).catch((error) => {
        console.log('Error getting location', error);
      });
   
  
      }
      
  
  
    }

}
