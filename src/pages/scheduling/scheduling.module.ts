import { NgModule } from '@angular/core';
import { IonicPageModule, FabContainer } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Geolocation } from '@ionic-native/geolocation';
import { SchedulingPage } from './scheduling';

@NgModule({
  declarations: [ 
    SchedulingPage
  ],
  imports: [
    IonicPageModule.forChild(SchedulingPage),
    TranslateModule
  ],
  providers: [
    FabContainer,
    AndroidPermissions,
    Geolocation
  ]
})
export class SchedulingPageModule { }
