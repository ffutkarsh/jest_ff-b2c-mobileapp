// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams, FabContainer, ViewController } from 'ionic-angular/index';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { SchedulingPage } from './scheduling';
// import { DashboardFlowApiProvider } from '../../providers/dashboard-flow-api/dashboard-flow-api';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';

// export class ViewControllerMock {
//     readReady = {
//         subscribe() { }
//     };
//     writeReady = {
//         subscribe() { }
//     };
//     dismiss() { }
//     _setHeader() { }
//     _setNavbar() { }
//     _setIONContent() { }
//     _setIONContentRef() { }
// }

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// describe('Schedule Page UI Testing', () => {
//     let de: DebugElement;
//     let comp: SchedulingPage;
//     let fixture: ComponentFixture<SchedulingPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 SchedulingPage
//             ],
//             imports: [
//                 IonicModule.forRoot(SchedulingPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 FabContainer,
//                 { provide: ViewController, useClass: ViewControllerMock },
//                 TranslateService,
//                 TranslateStore,
//                 DashboardFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(SchedulingPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Page component should be created.", () => expect(comp).toBeDefined());

//     it("Header should be present.", async () => {
//         fixture = TestBed.createComponent(SchedulingPage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".Head")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Header should contain navbar.", async () => {
//         fixture = TestBed.createComponent(SchedulingPage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".NavigationBar")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Navbar should contain menutoggle button.", async () => {
//         fixture = TestBed.createComponent(SchedulingPage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".MenutoggleButton")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Menutoggle button should contain menutoggle icon.", async () => {
//         fixture = TestBed.createComponent(SchedulingPage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".MenutoggleIcon")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Ion title should contain My Schedules", async () => {
//         fixture = TestBed.createComponent(SchedulingPage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".Title")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Navbar should contain notification icon", async () => {
//         fixture = TestBed.createComponent(SchedulingPage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".notification")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Ion grid should contain My Schedules cardgrid", async () => {
//         fixture = TestBed.createComponent(SchedulingPage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".cardgrid")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Ion grid should contain  ion-card", async () => {
//         fixture = TestBed.createComponent(SchedulingPage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".card1")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Ion CARD should contain  ion-row", async () => {
//         fixture = TestBed.createComponent(SchedulingPage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".statusgrid")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Ion CARD should contain  row1", async () => {
//         fixture = TestBed.createComponent(SchedulingPage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".row1")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Ion CARD should contain  row2", async () => {
//         fixture = TestBed.createComponent(SchedulingPage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".row2")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Ion CARD should contain  timea duration col", async () => {
//         fixture = TestBed.createComponent(SchedulingPage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".timeandduration")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Ion grid should contain  ion-card", async () => {
//         fixture = TestBed.createComponent(SchedulingPage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".card2")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

// });

// describe('---------Fab Button----------', () => {
//     let de: DebugElement;
//     let comp: SchedulingPage;
//     let fixture: ComponentFixture<SchedulingPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 SchedulingPage
//             ],
//             imports: [
//                 IonicModule.forRoot(SchedulingPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 FabContainer,
//                 { provide: ViewController, useClass: ViewControllerMock },
//                 TranslateService,
//                 TranslateStore,
//                 DashboardFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(SchedulingPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //FAB Button - Test Case 1
//     it('Fab button should be present.', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.FabButton')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('FabButton');
//     });

//     //FAB Button - Test Case 2
//     it('Main Favicon button should be present.', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.MainFavicon')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('MainFavicon');
//     });

//     //FAB Button - Test Case 3
//     it('Fab top button should be present.', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.TopFab')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('TopFab');
//     });

//     //FAB Button - Test Case 4
//     it('Fab middle button should be present.', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.MiddleFab')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('MiddleFab');
//     });

//     //FAB Button - Test Case 5
//     it('Fab bottom button should be present.', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.BottomFab')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('BottomFab');
//     });

// });

// describe('Schedule Page functional Testing', () => {
//     let de: DebugElement;
//     let comp: SchedulingPage;
//     let fixture: ComponentFixture<SchedulingPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 SchedulingPage
//             ],
//             imports: [
//                 IonicModule.forRoot(SchedulingPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 FabContainer,
//                 { provide: ViewController, useClass: ViewControllerMock },
//                 TranslateService,
//                 TranslateStore,
//                 DashboardFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(SchedulingPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it('Rating star function as per  ', () => {
//         expect(comp.starRating).toBeTruthy();
//     });

// });




