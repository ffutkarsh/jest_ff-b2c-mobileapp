import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpgradeHomeActionModalPage } from './upgrade-home-action-modal';
import { FetcherProvider } from '../../providers/fetcher/fetcher';

@NgModule({
  declarations: [
    UpgradeHomeActionModalPage,
  ],
  imports: [
    IonicPageModule.forChild(UpgradeHomeActionModalPage),

  ],
  providers:[   
    FetcherProvider   
  ]
})
export class UpgradeHomeActionModalPageModule {}
