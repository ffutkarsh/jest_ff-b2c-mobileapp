import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FetcherProvider } from '../../providers/fetcher/fetcher';
import { Storage } from '@ionic/storage';
@IonicPage()
@Component({
  selector: 'page-upgrade-home-action-modal',
  templateUrl: 'upgrade-home-action-modal.html',
})
export class UpgradeHomeActionModalPage {

  public upgradeMembership: any;
  total: number;
  public cost = {};
  newcost: any;
  oldcost: any;
  month;
  cities = [];
  city: any;

  upgradeData;
  setofferLocally;


  constructor(public navCtrl: NavController, public navParams: NavParams, public fetcher: FetcherProvider, public viewCtrl: ViewController, private storage: Storage) {
    this.upgradeMembership = navParams.get('upgrade');
    console.log(this.upgradeMembership);
    this.city = [];
    this.getcity();
    this.totalcost();
    this.newcost = 8000;
    this.oldcost = 5000;
    this.month = 3,

      this.cost = [
        {
          "month": "3",
          "newcost": 8000,
          "oldcost": 5000,
        }
      ];

    this.upgradeMembership = navParams.get("upgrade");
    this.upgradeData = this.navParams.get("upgradeJsonData");
    this.upgradeData = this.navParams.get("upgradeJsonData")[0];
    console.log(" this.upgradeData", this.upgradeData)
  }

  ionVieWillEnter() {
    this.upgradeMembership = this.navParams.get('upgrade');
    console.log(this.upgradeMembership);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpgradeHomeActionModalPage');
  }

  totalcost() {
    this.total = Number(this.upgradeMembership.homeRate) - Number(this.oldcost);
    return this.total;
  }

  getcity() {
    this.fetcher.getCities().map((response) => response.json()).subscribe((response) => {
      this.city = response;
      console.log("citylist------", this.city);
    });
  }

  closeModal() {
    // this.view.pop()
    this.viewCtrl.dismiss();
  }

  goToCart(cost) {
    let upData;
    if (this.upgradeData.frozenstatus) {

      upData = {

        expiry: this.upgradeData.expiry,
        frozenicon: this.upgradeData.frozenicon,
        frozenstatus: this.upgradeData.frozenstatus,
        location: this.upgradeData.location,
        purposedescription: this.upgradeData.purposedescription,
        totalCost: cost,
        totalSessions: this.upgradeData.totalSessions,
        usedSessions: this.upgradeData.usedSessions,
        quantity: 1,
        type: "upgradeFrozen"
      }

    } else {

      upData = {
        purposedescription: this.upgradeData.purposedescription,
        contractName: this.upgradeData.contractName,
        contractStatus: this.upgradeData.contractStatus,
        endDate: this.upgradeData["end-date"],
        location: this.upgradeData.location,
        package: this.upgradeData.package,
        purpose: this.upgradeData.purpose,
        purposelogo: this.upgradeData.purposelogo,
        startDate: this.upgradeData["start-date"],
        upgradeAmount: cost,
        quantity: 1,
        type: "upgradeContract"
      }

    }

    console.log("upData", upData);
    this.storage.get('CARTITEM').then((val) => {
      this.setofferLocally = val;
    }).then(
      () => {
        if ((this.setofferLocally) || (this.setofferLocally = [])) {

          this.setofferLocally.push(upData);
          this.storage.set('CARTITEM', this.setofferLocally);
        }
      });

    this.navCtrl.push("CartItemsPage",
      {
        'upgradeFlow': true, "upgrdeTotalcost": this.totalcost(), 'upgradeCartData': this.navParams.get("upgradeJsonData")
      });
  }


  // goToCart() {
  //   this.navCtrl.push("CartItemsPage",
  //     {
  //       'upgradeFlow': true,"upgrdeTotalcost":this.totalcost(),'upgradeCartData':this.navParams.get("upgradeJsonData")
  //     });
  // }
}
