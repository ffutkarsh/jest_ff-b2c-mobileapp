import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { WithdrawModalPage } from './withdraw-modal';

@NgModule({
  declarations: [
    WithdrawModalPage
  ],
  imports: [
    IonicPageModule.forChild(WithdrawModalPage),
    TranslateModule
  ],
})
export class WithdrawModalPageModule { }
