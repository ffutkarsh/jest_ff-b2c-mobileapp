import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { SchedulingPage } from '../../pages/pages'

@IonicPage()
@Component({
  selector: 'page-withdraw-modal',
  templateUrl: 'withdraw-modal.html',
})
export class WithdrawModalPage {

  constructor(
    private app: App,
    public navCtrl: NavController,
    public navParams: NavParams,
    public translateService: TranslateService) { }

  ionViewDidLoad() { }

  //NAVIGATING TO SCHEDULING PAGE ON WITHDRAW TRUE.
  continue() {
    this.closeModal();
    this.app.getRootNav().setRoot(SchedulingPage, {
      'withdraw': true
    });
  }

  //CLOSING MODAL ON WITHDRAW FALSE.
  closeModal() {
    this.navCtrl.pop();
  }

}
