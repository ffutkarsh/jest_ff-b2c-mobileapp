// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { WithdrawModalPage } from './withdraw-modal';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// // The describe function is for grouping related specs
// describe('Withdraw Modal Page UI Testing', () => {
//     let comp: WithdrawModalPage;
//     let fixture: ComponentFixture<WithdrawModalPage>;
//     let de: DebugElement;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 WithdrawModalPage
//             ],
//             imports: [
//                 IonicModule.forRoot(WithdrawModalPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(WithdrawModalPage); /////TESTBED CREATES THE OBJECT OF THE CLASSDETAILS  PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance;   //////CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//     });

//     ///// Test Case for main content to be present
//     it("Main Content for modal should be checked", async () => {
//         fixture = TestBed.createComponent(WithdrawModalPage);
//         de = fixture.debugElement.query(By.css(".mainContent")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     ///// Test Case for content to be present
//     it("Content for modal should be checked", async () => {
//         fixture = TestBed.createComponent(WithdrawModalPage);
//         de = fixture.debugElement.query(By.css(".withdrawContent")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("row for class withdraw text to be present", async () => {
//         fixture = TestBed.createComponent(WithdrawModalPage);
//         de = fixture.debugElement.query(By.css(".rowWithdraw")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("column for class withdraw text to be present", async () => {
//         fixture = TestBed.createComponent(WithdrawModalPage);
//         de = fixture.debugElement.query(By.css(".colWithdraw")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("label for class withdraw text to be present", async () => {
//         fixture = TestBed.createComponent(WithdrawModalPage);
//         de = fixture.debugElement.query(By.css(".labelWithdraw")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("icon for title thumbs down to be present", async () => {
//         fixture = TestBed.createComponent(WithdrawModalPage);
//         de = fixture.debugElement.query(By.css(".thumbsdown")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("icon for title thumbs down to be present", async () => {
//         fixture = TestBed.createComponent(WithdrawModalPage);
//         de = fixture.debugElement.query(By.css(".colText")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("column for cancel button to be present", async () => {
//         fixture = TestBed.createComponent(WithdrawModalPage);
//         de = fixture.debugElement.query(By.css(".colCancel")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("column for Continue button to be present", async () => {
//         fixture = TestBed.createComponent(WithdrawModalPage);
//         de = fixture.debugElement.query(By.css(".colConfirm")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("cancel button to be present", async () => {
//         fixture = TestBed.createComponent(WithdrawModalPage);
//         de = fixture.debugElement.query(By.css(".cancelButton")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("row for button to be present", async () => {
//         fixture = TestBed.createComponent(WithdrawModalPage);
//         de = fixture.debugElement.query(By.css(".rowButton")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("cancel button to be present", async () => {
//         fixture = TestBed.createComponent(WithdrawModalPage);
//         de = fixture.debugElement.query(By.css(".confirmButton")).nativeElement;
//         expect(de).toBeDefined();
//     });

// });