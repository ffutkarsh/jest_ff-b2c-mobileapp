// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, style } from '@angular/core';
// import { OopsPage } from './oops';
// import { IonicModule, Platform, NavController, NavParams, Item,ToastController } from 'ionic-angular/index';
// import { HttpModule } from '@angular/http';
// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// //The describe function is for grouping related specs
// describe('---------OOPS Page PAGE UI----------', () => {
//     let de: DebugElement;
//     let comp: OopsPage;
//     let fixture: ComponentFixture<OopsPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [OopsPage],
//             imports: [
//                 HttpModule,
//                 IonicModule.forRoot(OopsPage),
//             ],
//             providers: [
//                 ToastController,
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(OopsPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //Creating the main Component
//     it('should create component', () => expect(comp).toBeDefined());

//       //Creating the image class
//     it('checking the image class name', () => {
//         const inputfield: HTMLElement = fixture.debugElement.query(By.css('.imgClass')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('imgClass');
//     });

//      //Creating the image src
//      it('checking the image source', () => {
//         const inputfield: HTMLElement = fixture.debugElement.query(By.css('.imgClass')).nativeElement;
//         expect(inputfield.getAttribute('src')).toMatch('assets/img/oops1.jpg');
//     });

//     //checking toast method
//     it('should create toast method', () => expect(comp.presentToast).toBeDefined());
// });