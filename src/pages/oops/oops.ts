import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';




@IonicPage()
@Component({
  selector: 'page-oops',
  templateUrl: 'oops.html',
})
export class OopsPage {
  constructor( private toastCtrl: ToastController,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OopsPage');
    this.presentToast(); //toast to display relaunching of app
  }
  
 
  //TOAST TO PRESENT ON START OF VIEW FOR 20 SECS
  presentToast() { 
    let toast = this.toastCtrl.create({
      message: "Please relaunch the app",
      duration: 20000,
     //creating RE START button
    });
    toast.present();
  }


}
