// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, style } from '@angular/core';
// import { MembershipPage } from './membership';
// import { IonicModule, Platform, NavController, NavParams, Item } from 'ionic-angular/index';
// import { HttpModule } from '@angular/http';
// import { MembershipProvider } from '../../providers/membership/membership';
// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// //The describe function is for grouping related specs
// describe('---------MEMBERSHIP PAGE UI----------', () => {
//     let de: DebugElement;
//     let comp: MembershipPage;
//     let fixture: ComponentFixture<MembershipPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [MembershipPage],
//             imports: [
//                 HttpModule,

//                 IonicModule.forRoot(MembershipPage),
//             ],
//             providers: [
//                 MembershipProvider,
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(MembershipPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //Creating the main Component
//     it('should create component', () => expect(comp).toBeDefined());


//     it('----------HEADER----------', () => {

//     });
//     //Checking header 
//     it('checking the header using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.NavigationBar')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('NavigationBar');
//     });

//     //header icon using class name
//     it('checking the header ICON using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.MenuToggleButton')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('MenuToggleButton');
//     });

//     //title tag using class name
//     it('checking the title using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.Title')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('Title');
//     });

//     it('----------CONTENT----------', () => {

//     });

//     //Checking subtitle class 
//     it('checking the row using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.subtitle')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('subtitle');
//     });

//     //title tag text content using class name
//     it('checking the title using component ', () => {
//         let title = "Your Memberships"
//         expect(comp.subTitle).toMatch(title);
//     });

//     //Checking list class 
//     it('checking the List using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.listClass')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('listClass');
//     });

//  //Checking the item list tag using class name
//  it('checking the item list using class name', () => {
//     fixture.whenStable().then(() => {
//         // after something in the component changes, you should detect changes
//         fixture.detectChanges();
//         // everything else in the beforeEach needs to be done here.
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.list1')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('list1');
//     });
// });

//  //Checking the main row tag using class name
//  it('checking the main row using class name', () => {
//     fixture.whenStable().then(() => {
//         // after something in the component changes, you should detect changes
//         fixture.detectChanges();
//         // everything else in the beforeEach needs to be done here.
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.mainRow')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('mainRow');
//     });
// });


//  //Checking the avatar col  tag using class name
//  it('checking the avatar column using class name', () => {
//     fixture.whenStable().then(() => {
//         // after something in the component changes, you should detect changes
//         fixture.detectChanges();
//         // everything else in the beforeEach needs to be done here.
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.avatarCol')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('avatarCol');
//     });
// });

//  //Checking the avatar tag using class name
//  it('checking the avatar using class name', () => {
//     fixture.whenStable().then(() => {
//         // after something in the component changes, you should detect changes
//         fixture.detectChanges();
//         // everything else in the beforeEach needs to be done here.
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.avatarC')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('avatarC');
//     });
// });


//  //Checking the image using class name
//  it('checking the image using class name', () => {
//     fixture.whenStable().then(() => {
//         // after something in the component changes, you should detect changes
//         fixture.detectChanges();
//         // everything else in the beforeEach needs to be done here.
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.img')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('img');
//     });
// });

//  //Checking the div tag  using class name
//  it('checking the div tag coming from jsonusing class name', () => {
//     fixture.whenStable().then(() => {
//         // after something in the component changes, you should detect changes
//         fixture.detectChanges();
//         // everything else in the beforeEach needs to be done here.
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.item1')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('item1');
//     });
// });

//  //Checking the title  using class name
//  it('checking the title using class name', () => {
//     fixture.whenStable().then(() => {
//         // after something in the component changes, you should detect changes
//         fixture.detectChanges();
//         // everything else in the beforeEach needs to be done here.
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.membertitle')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('membertitle');
//     });
// });

//  //Checking the expiry  using class name
//  it('checking the expiry using class name', () => {
//     fixture.whenStable().then(() => {
//         // after something in the component changes, you should detect changes
//         fixture.detectChanges();
//         // everything else in the beforeEach needs to be done here.
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.expiredDate')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('expiredDate');
//     });
// });

//  //Checking the expiry of label  using class name
//  it('checking the label- expiry using class name', () => {
//     fixture.whenStable().then(() => {
//         // after something in the component changes, you should detect changes
//         fixture.detectChanges();
//         // everything else in the beforeEach needs to be done here.
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.expiryRed')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('expiryRed');
//     });
// });


//  //Checking the slots  using class name
//  it('checking the slots', () => {
//     fixture.whenStable().then(() => {
//         // after something in the component changes, you should detect changes
//         fixture.detectChanges();
//         // everything else in the beforeEach needs to be done here.
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.slotClass')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('slotClass');
//     });
// });

// });





// describe('----------MEMBERSHIP PAGE DISABLED PART----------', () => {
//     let de: DebugElement;
//     let comp: MembershipPage;
//     let fixture: ComponentFixture<MembershipPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [MembershipPage],
//             imports: [
//                 HttpModule,
//                 IonicModule.forRoot(MembershipPage),
//             ],
//             providers: [
//                 MembershipProvider,
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(MembershipPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//    //Checking the ROW   using class name
//  it('checking the description row using class name', () => {
//     fixture.whenStable().then(() => {
//         // after something in the component changes, you should detect changes
//         fixture.detectChanges();
//         // everything else in the beforeEach needs to be done here.
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.description')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('description');
//     });
// });



//    //Checking the item of row   using class name
//    it('checking the item of row using class name', () => {
//     fixture.whenStable().then(() => {
//         // after something in the component changes, you should detect changes
//         fixture.detectChanges();
//         // everything else in the beforeEach needs to be done here.
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.item2')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('item2');
//     });
// });




//    //Checking the activation date   using class name
//    it('checking the activation date   of row using class name', () => {
//     fixture.whenStable().then(() => {
//         // after something in the component changes, you should detect changes
//         fixture.detectChanges();
//         // everything else in the beforeEach needs to be done here.
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.activationDate')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('activationDate');
//     });
// });




//    //Checking the total cost of  using class name
//    it('checking the total cost of row using class name', () => {
//     fixture.whenStable().then(() => {
//         // after something in the component changes, you should detect changes
//         fixture.detectChanges();
//         // everything else in the beforeEach needs to be done here.
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.totalCost')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('totalCost');
//     });
// });



//    //Checking the total session  using class name
//    it('checking the total session using class name', () => {
//     fixture.whenStable().then(() => {
//         // after something in the component changes, you should detect changes
//         fixture.detectChanges();
//         // everything else in the beforeEach needs to be done here.
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.totalSessions')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('totalSessions');
//     });
// });


//    //Checking the balance session cost of  using class name
//    it('checking the balance session using class name', () => {
//     fixture.whenStable().then(() => {
//         // after something in the component changes, you should detect changes
//         fixture.detectChanges();
//         // everything else in the beforeEach needs to be done here.
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.balanceSessions')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('balanceSessions');
//     });
// });



// });

//     describe('----------MEMBERSHIP PAGE METHODS----------', () => {
//         let de: DebugElement;
//         let comp: MembershipPage;
//         let fixture: ComponentFixture<MembershipPage>;

//         beforeEach(async(() => {
//             TestBed.configureTestingModule({
//                 declarations: [MembershipPage],
//                 imports: [
//                     HttpModule,
//                     IonicModule.forRoot(MembershipPage),
//                 ],
//                 providers: [
//                     MembershipProvider,
//                     NavController, { provide: NavParams, useClass: NavParamsMock },]
//             });
//         }));

//         beforeEach(() => {
//             // create component and test fixture
//             fixture = TestBed.createComponent(MembershipPage);
//             // get test component from the fixture
//             comp = fixture.componentInstance;
//         });

//         //checking json method
//         it('checking json method ', () => {
//             expect(comp.getMembership).toBeTruthy();
//         });

//         //checking Expand method
//         it('checking push method 2', () => {
//             expect(comp.expandDescription).toBeTruthy();
//         });



// });