import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MembershipProvider } from '../../providers/membership/membership';

@IonicPage()
@Component({
  selector: 'page-membership',
  templateUrl: 'membership.html',
})
export class MembershipPage {

  //HEADER
  title; //var to store title

  //CONTENT
  subTitle; //var to store sub title
  description; //var o save description boolean
  index; //var to store index
  membership ;
  public allPurposes = []; //Array of objects to store json file.
  constructor(private membershipProvider: MembershipProvider,public navCtrl: NavController, public navParams: NavParams) {
    //Calling json
    this.getMembership() ;

    //HEADER
    this.title = "My Membership"; //title

    //CONTENT
    this.subTitle = "Your Memberships"; //subtitle
    this.description =false; //toggle variable

    this.membership=true ;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MembershipPage');
  
  }

  //from json using its provider
  getMembership() {
    this.membershipProvider.getMembershipDetails().map((response) => response.json()).subscribe((response) => {
      this.allPurposes = response;
    });
  }

  //toggle function
  expandDescription(i)
  {
    this.index = i;
    console.log(this.index)
    this.description =! this.description;
  }
}
