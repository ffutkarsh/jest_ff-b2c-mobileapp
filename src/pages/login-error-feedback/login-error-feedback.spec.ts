// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from "@angular/http";
// import { Http, ConnectionBackend } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';
// import { LoginErrorFeedbackPage } from './login-error-feedback';
// import { GeogetterProvider } from '../../providers/geogetter/geogetter';
// import { Geolocation } from '@ionic-native/geolocation';
// import { NativeGeocoder } from '@ionic-native/native-geocoder';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { AndroidPermissions } from '@ionic-native/android-permissions';
// import { OpenNativeSettings } from '@ionic-native/open-native-settings';
// import { NetworkProvider } from '../../providers/network/network';
// import { Network } from '@ionic-native/network';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// describe('Login error feedback Page - UI Part', () => {
//     let de: DebugElement;
//     let comp: LoginErrorFeedbackPage;
//     let fixture: ComponentFixture<LoginErrorFeedbackPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [LoginErrorFeedbackPage],
//             imports: [
//                 IonicModule.forRoot(LoginErrorFeedbackPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 GeogetterProvider,
//                 Geolocation,
//                 NativeGeocoder,
//                 TranslateService,
//                 TranslateStore,
//                 AndroidPermissions,
//                 OpenNativeSettings,
//                 NetworkProvider,
//                 Network
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Page component should be created.", () => expect(comp).toBeDefined());

//     it("Ion content should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".MainContent")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section grid should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".HeadingGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".HeadingGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".HeadingGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section grid should contain the “Welcome” text.", async () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".WelcomeText")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Feedback Message grid should be present.'", () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".FeedbackMessageGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Feedback Message grid's row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".FeedbackMessageGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Feedback Message grid's column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".FeedbackMessageGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Feedback Message row should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".FeedbackRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Feedback Message should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".Feedback")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Close Button Grid should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".CloseButtonGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Close Button Grid's row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".CloseButtonGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Close Button Grid's column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".CloseButtonGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Close Button should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".CloseButton")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Footer Logo Grid should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".FooterLogoGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Footer Logo Grid's row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".FooterLogoGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Footer Logo Grid's column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".FooterLogoGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Footer Logo should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage);
//         de = fixture.debugElement.query(By.css(".FooterImage")).nativeElement;
//         expect(de).toBeDefined();
//     });

// });

// describe('Login error feedback Page - Methods and Logic Part', () => {
//     let de: DebugElement;
//     let comp: LoginErrorFeedbackPage;
//     let fixture: ComponentFixture<LoginErrorFeedbackPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [LoginErrorFeedbackPage],
//             imports: [
//                 IonicModule.forRoot(LoginErrorFeedbackPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 GeogetterProvider,
//                 Geolocation,
//                 NativeGeocoder,
//                 TranslateService,
//                 TranslateStore,
//                 AndroidPermissions,
//                 OpenNativeSettings,
//                 NetworkProvider,
//                 Network
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(LoginErrorFeedbackPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("ionViewDidLoad Method should be present.", async () => {
//         expect(comp.ionViewDidLoad).toBeDefined();
//     });

// });