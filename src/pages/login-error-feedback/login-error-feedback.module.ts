import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginErrorFeedbackPage } from './login-error-feedback';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    LoginErrorFeedbackPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginErrorFeedbackPage),
    TranslateModule
  ],
})
export class LoginErrorFeedbackPageModule { }
