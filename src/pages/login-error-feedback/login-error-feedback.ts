import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { MainPage } from '../../pages/pages';

@IonicPage()
@Component({
  selector: 'page-login-error-feedback',
  templateUrl: 'login-error-feedback.html',
})
export class LoginErrorFeedbackPage {

  public tenantName; //FROM LOGIN ERROR PAGE.
  public name; //FROM LOGIN ERROR PAGE.
  public emailAddress; //FROM LOGIN ERROR PAGE.
  public tenantlocation; //FROM LOGIN ERROR PAGE.

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public translateService: TranslateService,
    private storage: Storage) {
    this.tenantName = this.navParams.get('tenantName');
    this.name = this.navParams.get('name');
    this.emailAddress = this.navParams.get('emailAddress');
    this.tenantlocation = this.navParams.get('tenantlocation');
    console.log(this.tenantName)
    this.storelocally();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginErrorFeedbackPage');
  }

  //SAVES LOGIN ERROR FEEDBACK DETAILS TO LOCAL DATABASE.
  storelocally() {
    this.storage.set('LoginErrorFeedbackDetails', {
      'tenantName': this.tenantName,
      'name': this.name,
      'emailAddress': this.emailAddress,
      'tenantlocation': this.tenantlocation
    });
  }

  //ON CLOSING LOGIN ERROR FEEDBACK PAGE.
  close() {
    this.navCtrl.setRoot(MainPage);
  }
  
}
