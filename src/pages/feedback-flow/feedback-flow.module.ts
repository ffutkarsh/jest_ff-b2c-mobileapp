import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedbackFlowPage } from './feedback-flow';
import { TranslateModule } from '@ngx-translate/core';
import { Crop } from '@ionic-native/crop';
import { Base64 } from '@ionic-native/base64';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';



@NgModule({
  declarations: [
    FeedbackFlowPage,
  ],
  imports: [
    IonicPageModule.forChild(FeedbackFlowPage),
    TranslateModule

],
providers:
[
  Crop,Base64,FileChooser,FilePath,
]
})
export class FeedbackFlowPageModule {}
