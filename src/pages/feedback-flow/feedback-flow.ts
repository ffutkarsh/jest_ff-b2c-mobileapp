import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { ValidPhoneProvider } from '../../providers/valid-phone/valid-phone';
import 'rxjs/add/operator/map';
import { CountryModalPage } from "../country-modal/country-modal";
import { GeogetterProvider } from '../../providers/geogetter/geogetter';
import { Geolocation } from '@ionic-native/geolocation';
import { AsYouType } from 'libphonenumber-js';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
import { Base64 } from '@ionic-native/base64';
import { DomSanitizer } from '@angular/platform-browser';
import {IpgeofetchProvider} from "../../providers/ipgeofetch/ipgeofetch"

@IonicPage()
@Component({
  selector: 'page-feedback-flow',
  templateUrl: 'feedback-flow.html',
})
export class FeedbackFlowPage {

  public emailid: string; //USER INPUT USED FOR BINDING.
  public regex: any; //REGEX EXPRESSION FOR EMAIL.
  public phoneNumber; //USER INPUT USED FOR BINDING.

  //COUNTRY MODAL PAGE.
  public allCountries = []; //OBJECT THAT STORE THE DATA OF ALL COUNTRIES.
  public CountrySelected: string; //STORES THE DATA OF SELECTED COUNTRY.
  public CountrySelectedState: {}; //OBJECT THAT STORE THE OBJECT OF SELECTED COUNTRY.
  public defaultFlag: any; //STORE THE DEFAULT FLAG AS PER GEO LOCATION.
  public defaultCode //STORE THE DEFAULT COUNTRY CODE.
  public defaultDialcode: any; //STORE THE DEFAULT DIAL CODE AS PER GEO LOCATION.
  public defaultMax: any; //STORE THE MAX LENGTH OF PHONE NO.
  public defaultMin: any; //STORE THE MIN LENGTH OF PHONE NO.
  public defaultLength: string; //STORE THE ACTUAL FORMAT OF PHONE NO. LENGTH.
  public replacedDefaultLength: string; //STORING THE FORMAT OF PHONE NO. AFTER REPLACING TO AND COMMA IN ACTUAL PHONE NO. FORMAT BY SPACE.
  public replaceDefaultLengthArray: any; //STORING THE ACTUAL PHONE NO. LENGTH INTO DIGITS AFTER SPLITING THEM WITH THE HELP OF SPACE.
  public lengthType: string; //VARIABLE TO CHECK WHETHER THE PHONE NO. LENGTH FORMAT CONTAINS TO OR COMMA.
  public flag: boolean = false; //IF FLAG IS 1 MODAL WILL BE DISPLAYED AND IF FLAG IS 0 MODAL WILL NOT BE DISPLAYED.
  public buttonColor;

  //DEFAULT COUNTRY SELECTION BASED ON GEO LOCATION.
  public lat; //LATITUDE.
  public long; //LONGITUDE.
  public loc; //LOCATION.

  public receivedFeedbackType; //VARIABLE TO RECEIVE FEEDBACKTYPE.

  public feedbackText; //USER INPUT FEEDBACK USED FOR BINDING.

  public feedbackdetail; // USED TO SAVE VALUES IN LOCAL STORAGE.

  //public options: ImagePickerOptions; //IMAGE PICKER OPTION OBJECT VARIABLE.
  public fileData: any;
  public path: string;  //storing string path

  public smallImg = null;
  public smallSize = '0';
  public uploadImage: any;
  public imgFlag = false;
  public saveImage = [];
  public toDisplay = [];

  public userDetails1;
  public userDetails2;

  constructor(private validphone: ValidPhoneProvider,
    public alertCtrl: AlertController,
    public http: Http,
    public navCtrl: NavController,
    public navParams: NavParams,
    private modalController: ModalController,
    public geo: GeogetterProvider,
    private geolocation: Geolocation,
    private storage: Storage,
    public translateService: TranslateService,
    public camera: Camera,
    public crop: Crop,
    private fileChooser: FileChooser,
    private filePath: FilePath,
    private base64: Base64,
    public ipgeo:IpgeofetchProvider,
    private sanitizer: DomSanitizer
  ) {

    //INPUT FIELD ON LOGIN PAGE.
    this.phoneNumber = ""; //INITIALLY SETTING IT TO DEFAULT VALUE.
    this.emailid = ""; //INITIALLY SETTING IT TO DEFAULT VALUE.

    this.receivedFeedbackType = navParams.get("feedbackType"); //RECEIVING FEEDBACK TYPE.

    this.feedbackText = ""; //INITIALLY SETTING IT TO NULL.

  }

  ionViewDidLoad() {
   
    this.returnDialcode("IN"); //DEFAULT DIALCODE.
    this.getLatLong(); //GETTING COUNTRY AND COUNTRY CODE.
    this.storage.get('USERDETAILS').then((user1) => {
      this.userDetails1 = user1;
      this.defaultDialcode = this.userDetails1.dialCode;
      this.phoneNumber = this.userDetails1.phoneNumber;
    });
    this.storage.get('User_Details_From_Selected_Center').then((user2) => {
      this.userDetails2 = user2;
      this.emailid = this.userDetails2.emailAddress;
    });
  }

  //FOR DISPLAYING COUNTRY MODAL PAGE.
  CountryModal() {

    let CountrySelectedStateFromMainPage = {
      CountrySelected: this.CountrySelected,
    };

    let CountryModal = this.modalController.create(CountryModalPage, CountrySelectedStateFromMainPage);

    //ON DISMISS OF COUNTRY MODEL PAGE THE SELECTED COUNTRY'S FLAG AND DIAL CODE GETS DISPLAYED ON LOGIN PAGE.
    CountryModal.onDidDismiss((CountrySelectedState) => {
      this.CountrySelectedState = CountrySelectedState;
      this.defaultFlag = "assets/img/Flag-Svg/" + CountrySelectedState.CountrySelected.img; //flag svg of selected country
      
      this.defaultDialcode = CountrySelectedState.CountrySelected.Dialcode; // country code of selected country
      this.defaultMax = CountrySelectedState.CountrySelected.maxlength; // max phone number length of selected country
      this.defaultMin = CountrySelectedState.CountrySelected.minlength; // min phone number length of selected country
      this.defaultLength = CountrySelectedState.CountrySelected.length; // in case only one length available
      this.defaultCode = CountrySelectedState.CountrySelected.code; // Country code of selected country
      this.lengthFormatChecking(this.defaultLength);
    });

    CountryModal.present(); //TO DISPLAY THE COUNTRY MODAL.

   
  }

  //IF THE DIGIT IS ENTERED ACCEPT IT IN AND FORMAT THE SEQUENCE.
  numberChange() {
    this.phoneNumber = this.asYouType(this.defaultCode, this.phoneNumber);
  }

  //KEEP ON FORMATTING THE PHONE NUMBER AS THE USER ENTERS THE DIGIT.
  asYouType(COUNTRY_CODE, NO) {
    return new AsYouType(COUNTRY_CODE).input(NO)
  }

  //WHEN PHONE NO. LENGTH IS GIVEN IN FORM OF SINGLE DIGIT.
  getPhoneLengthWhenInteger(defaultLength) {
    this.defaultLength = defaultLength;
    this.defaultMax = Number(this.defaultLength);
    this.defaultMin = Number(this.defaultLength);
  }

  //WHEN PHONE NO. LENGTH IS GIVEN IN FORM OF RANGE.
  getPhoneLengthWhenTo(defaultLength) {
    this.defaultLength = defaultLength;
    this.replacedDefaultLength = this.defaultLength.replace("to", "");
    this.replaceDefaultLengthArray = this.replacedDefaultLength.split('  ');
    let n1 = Number(this.replaceDefaultLengthArray[0]);
    let n2 = Number(this.replaceDefaultLengthArray[1]);
    this.defaultMax = n2;
    this.defaultMin = n1;
  }

  //WHEN PHONE NO. LENGTH IS GIVEN IN FORM OF COMMA SEPARATED DIGITS.
  getPhoneLengthWhenComma(defaultLength) {
    this.defaultLength = defaultLength;
    this.replacedDefaultLength = this.defaultLength.replace(/,/g, " ");
    this.replaceDefaultLengthArray = this.replacedDefaultLength.split('  ');
    for (let i = 0; i < this.replaceDefaultLengthArray.length; i++) {     
    }
  }

  //WHEN PHONE NO. LENGTH IS GIVEN IN FORM OF BRACKETS CONTAINING PREFIXES.
  getPhoneLengthWhenBracket(defaultLength) {
    this.defaultLength = defaultLength;
    this.replacedDefaultLength = this.defaultLength.replace(/[()]/g, "");
   
    //CONVERTING INTO ARRAY.
    this.replaceDefaultLengthArray = this.replacedDefaultLength.split('+');
    //FOR CHECKING WHETHER THE PREFIX AND LENGTH GOT SEPARATED. E.G.[ (684)+7 ].
    for (let i = 0; i < this.replaceDefaultLengthArray.length; i++) {
      
    }
    //SETTING THE MAX LENTH AND MIN LENGTH ACCORDING TO NEW PHONE NUMBER LENGTH.
    this.getPhoneLengthWhenInteger(Number(this.replaceDefaultLengthArray[1]));
  }

  //CHECKING WHETHER PHONE NO. LENGTH IS GIVEN IN FORM OF RANGE OR COMMA SEPARATED DIGITS.
  lengthFormatChecking(defaultLength) {
    this.defaultLength = defaultLength;
    if (this.checkifNo(this.defaultLength)) {
      this.getPhoneLengthWhenInteger(this.defaultLength);
    }
    else if (this.defaultLength.includes("to")) {
      this.lengthType = "to";
      this.getPhoneLengthWhenTo(this.defaultLength);
    }
    else if (this.defaultLength.includes(",")) {
      this.lengthType = "comma";
      this.getPhoneLengthWhenComma(this.defaultLength);
    }
    else {
      this.getPhoneLengthWhenBracket(this.defaultLength);
    }
  }

  //TO TRIM THE EXTRA SPACES IN PHONE NUMBER AFTER FORMATTING.
  TrimSpace(no) {
    return no.replace(/\s/g, '')
  }

  //VALIDATING PHONE NO. WHOSE LENGTH IS GIVEN IN SINGLE DIGIT THAT IS GOT FROM COMMA SEPARATED DIGITS.
  validatePhone(n) {
    if (this.TrimSpace(this.phoneNumber).length == n)
      return true;
    return false;
  }

  //CHECK IF THE ENTERED CHARACTER IS A DIGIT.
  checkifNo(phoneNumber) {
    let reg = new RegExp('^\\d+$');
    return reg.test(phoneNumber);
  }

  //WILL SLICE THE INPUT IF IT IS CHARACTER.
  dynamic() {
    let a = this.phoneNumber.slice(-1);
    if (!this.checkifNo(a)) {
      this.phoneNumber = this.phoneNumber.substring(0, this.phoneNumber.length - 1);
    }
  }

  //VALIDATING EMAIL IN CORRECT FORMAT abc@xyz.com
  validateEmail(emailid) {
    this.regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  
    if (this.regex.test(emailid) == false) {
   
      return false;
    }
    else {
     
      return true;
    }
  }

  //DISPLAY ALERTS TO THE USER BASED ON THE ENTERED PHONE NO.
  showAlert() {

    if(this.feedbackText.length <= 4)
    {
      const alert = this.alertCtrl.create({
        subTitle: 'Feedback Should be atleast 5 characters long',
        buttons: ['OK']
      });
      alert.present();
      
      return false;
    }
    else

    if (this.validateEmail(this.emailid)) {
   
    }
   
   

    else {
      const alert = this.alertCtrl.create({
        subTitle: 'Invalid Email.',
        buttons: ['OK']
      });
      alert.present();
     
      this.phoneNumber = "";
      return false;
    }

    //TO CHECK WHETHER PHONE NUMBER IS VALID OR NOT AND ACCORDINGLY SHOW ALERT TO THE USER. 
    if (this.phoneNumber.length == 0) {
      const alert = this.alertCtrl.create({
        subTitle: 'Invalid Phone Number.',
        buttons: ['OK']
      });
      alert.present();
     
      this.phoneNumber = "";
      return false;
    } //END OF 1ST IF OF SHOWALERT.

    //WHEN PHONE NO. LENGTH IS GIVEN IN FORM OF RANGE.
    else if (this.TrimSpace(this.phoneNumber).length < this.defaultMin || this.TrimSpace(this.phoneNumber).length > this.defaultMax) {
      let sub = (this.defaultMin == this.defaultMax) ? 'Phone No. should be of ' + this.defaultMax : 'Phone No. should be between ' + this.defaultMin + ' and ' + this.defaultMax;
      const alert = this.alertCtrl.create({
        subTitle: 'Invalid Phone Number.',
        buttons: ['OK']
      });
      alert.present();
     
      this.phoneNumber = "";
      return false;
    } //END OF 2ND IF OF SHOWALERT.

    //WHEN PHONE NO. LENGTH IS GIVEN IN FORM OF COMMA.
    else if (this.lengthType == "comma" && this.replaceDefaultLengthArray.length != 0) {
      for (let i = 0; i < this.replaceDefaultLengthArray.length; i++) {
        let n = Number(this.replaceDefaultLengthArray[i]);
        //MAX LENGTH OF PHONE NO. IN GIVEN ARRAY SHOULD NOT EXCEED 20 DIGITS.
        if (n > 20) {
          continue;
        }
        //IF THE LENGTH OF ENTERED PHONE NO. IS VALID.
        if (this.validatePhone(n)) {
          return;
        }
        //IF THE LENGTH OF ENTERED PHONE NO. IS NOT VALID.
        else {
          this.flag = false; //MAKE THE FLAG FALSE.
        }
      } //END OF FOR LOOP.

      //IF FLAG IS FALSE DISPLAY THE ALERT.
      if (this.flag == false) {
        const alert = this.alertCtrl.create({
          subTitle: 'Invalid Phone Number.',
          buttons: ['OK']
        });
        alert.present();
      
        this.phoneNumber = ""; //RESET THE PHONE NO. FIELD.
      }
    }//END OF 3RD IF OF SHOWALERT.

    //IF USER ENTERS A CHARACTER INSTEAD OF A DIGIT
    else if (!(this.checkifNo(this.TrimSpace(this.phoneNumber)))) {
      const alert = this.alertCtrl.create({
        subTitle: 'Invalid Phone Number.',
        buttons: ['OK']
      });
      alert.present();
      this.phoneNumber = "";
      return false;
    } //END OF 4TH IF OF SHOWALERT.

    else {
     
      this.storelocally();
      this.navCtrl.popToRoot();
      this.navCtrl.setRoot("DashboardPage");
     
      return true;
    } //END OF IFS.

  } //END OF SHOWALERT.

  // Method fetches the country code based on geolocation after receiving reverse geocoded address from google location  
  returnDialcode(CountryCode) {
    this.validphone.getCountries().map((response) => response.json()).subscribe((response) => {
      this.allCountries = response;
      let c = this.searchCC(CountryCode);
      
      this.defaultDialcode = c[0].Dialcode;
    });
  }

  // Helps to fetch the country code
  searchCC(CountryCode) {
    return this.allCountries.filter(it => {
      return it.code.toUpperCase().includes(CountryCode);
    });
  }

  //DEFAULT COUNTRY SELECTION BASED ON GEO LOCATION.
  getLatLong() {
    this.ipgeo.getLocfromIP().subscribe(data => {
      this.defaultFlag = "assets/img/Flag-Svg/" + (data.country).toLowerCase() + ".svg";
      this.returnDialcode(data.country);
      });;
  }

  //SAVES DATA TO LOCAL DATABASE.
  storelocally() {
    this.feedbackdetail = {
      'feedBackType': this.receivedFeedbackType,
      'textFeedback': this.feedbackText,
      'phoneNumber': this.phoneNumber,
      'emailId': this.emailid,
      'fImage': this.fileData
    }
    this.storage.set('feedbackdetails', this.feedbackdetail).then((val) => {
      this.storage.get('feedbackdetails').then((val) => {
      });

    });

  } //SAVE OBJECT ON LOCAL STORAGE , FEEDBACK DETAILS: FEEDBACK TYPE, FEEDBACK TEXT, PHONENUMBER, EMAILID.

  /* method to open camera and crop image */
  cropDOC() {
    let option: CameraOptions = { //setting quality, height,width
      quality: 100,
      targetWidth: 1000,
      targetHeight: 1000,
      destinationType: this.camera.DestinationType.NATIVE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(option).then(url => { //open camera
      this.path = url;
      this.crop.crop(this.path, option).then(newImageUrl => { //crop image
        this.path = newImageUrl;
        if (this.path) {
          // convert your file in base64 format
          this.base64.encodeFile(this.path)
            .then((base64File: string) => {
              this.fileData = base64File;
             
              this.storeImagelocally();
              alert('File uploaded successfully');  ////store in variable

            }, (err) => {
              alert('err' + JSON.stringify(err));
            });
        }
      }, err => {
        // alert("Error" + err);
      });
      // alert("file uploaded")
    }, err => {
      //  alert("Error" + err);
    });
  }
  /* **************************END****************************** */

  /* method to open images via file */
  fileChoose() {
    // choose your file from the device
    this.fileChooser.open().then(uri => {
      // alert('uri'+JSON.stringify(uri));
      // get file path
      this.filePath.resolveNativePath(uri)
        .then(file => {
          // alert('file'+JSON.stringify(file));
          let filePath: string = file;
          if (filePath) {
            // convert your file in base64 format
            this.base64.encodeFile(filePath)
              .then((base64File: string) => {
                this.fileData = base64File;   ////store in variable
               ;
                this.storeImagelocally();
                alert('File uploaded successfully');
              }, (err) => {
                alert('err' + JSON.stringify(err));
              });
          }
        })
        .catch(err => console.log(err));
    })
      .catch(e => alert('uri' + JSON.stringify(e)));
  }
  /* *****************END********************/

  /* alert modal to choose image and capture image */
  showPrompt() {
    const prompt = this.alertCtrl.create({
      title: 'Upload Image',
      message: "choose one of the option to upload image",
      buttons: [
        {
          text: 'choose',
          handler: data => {
           ;
            this.fileChoose();
          }
        },
        {
          text: 'click image',
          handler: data => {
          
            this.cropDOC();
          }
        }]
    });
    prompt.present();
  }
  /******************END**************/

  //SAVES THE IMAGE TO IONIC LOCAL STORAGE.///
  storeImagelocally() {
    let picture = this.fileData
    this.saveImage.push({
      'image': picture
    });
    this.storage.set('cameraImage', this.saveImage).then((val) => {
   
      if (val) {
        this.getImage();
      }
    });
  }
  /********************END******************/

  /* GET IMGE FROM IONIC LOCAL STORAGE */
  getImage() {
    this.storage.get('cameraImage').then((val) => {
    
      if (val) {
       
        this.uploadImage = val;
        for (let i = 0; i < this.uploadImage.length; i++) {
          this.toDisplay[i] = this.uploadImage[i]['image'];
          this.imgFlag = true;
        }
       
      }
    });
  }

  /*************END*****************/

  /* Remove Image based on index value */
  removeimage(i) {
    this.toDisplay.splice(i, 1);
    this.saveImage.splice(i, 1);
  }

  /* ***********************END*********/

}
