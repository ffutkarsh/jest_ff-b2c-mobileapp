// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { FeedbackFlowPage } from './feedback-flow';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from "@angular/http";
// import { Http, ConnectionBackend } from '@angular/http';
// import { ValidPhoneProvider } from '../../providers/valid-phone/valid-phone';
// import { GeogetterProvider } from '../../providers/geogetter/geogetter';
// import { Geolocation } from '@ionic-native/geolocation';
// import { NativeGeocoder } from '@ionic-native/native-geocoder';
// import { TranslateModule, TranslateService } from '@ngx-translate/core';
// import { TranslateStore } from '@ngx-translate/core/src/translate.store';
// import { Camera } from '@ionic-native/camera';
// import { Crop } from '@ionic-native/crop';
// import { Base64 } from '@ionic-native/base64';
// import { FileChooser } from '@ionic-native/file-chooser';
// import { FilePath } from '@ionic-native/file-path';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// export function provideStorage() { return new Storage(); }

// describe('Feedback Flow Page 1 - UI Part', () => {
//     let de: DebugElement;
//     let comp: FeedbackFlowPage;
//     let fixture: ComponentFixture<FeedbackFlowPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [FeedbackFlowPage],
//             imports: [
//                 IonicModule.forRoot(FeedbackFlowPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 ValidPhoneProvider,
//                 GeogetterProvider,
//                 Geolocation,
//                 NativeGeocoder,
//                 TranslateService,
//                 TranslateStore,
//                 Camera,
//                 Crop, Base64, FileChooser, FilePath,
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(FeedbackFlowPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Header to be present.", async () => {
//         fixture = TestBed.createComponent(FeedbackFlowPage);
//         de = fixture.debugElement.query(By.css(".feedbackFlowHeader")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Title of the header to present.", async () => {
//         const title: HTMLElement = fixture.debugElement.query(By.css('.headerTitle')).nativeElement;
//         expect(de).toBeDefined();
//         // expect(title.textContent).toContain("Feedback");
//     });

//     it("Message option heading div should be present.", async () => {
//         fixture = TestBed.createComponent(FeedbackFlowPage);
//         de = fixture.debugElement.query(By.css(".messageOptionHeaderDiv")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading of message option should match", async () => {
//         const title: HTMLElement = fixture.debugElement.query(By.css('.messageOptionHeader')).nativeElement;
//         expect(title.textContent).toContain("Type your message in the box provided below...");
//     });

//     it("Typing message option grid to be present.", async () => {
//         fixture = TestBed.createComponent(FeedbackFlowPage);
//         de = fixture.debugElement.query(By.css(".messageOptionGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Typing message option grid should have message option text box div.", async () => {
//         fixture = TestBed.createComponent(FeedbackFlowPage);
//         de = fixture.debugElement.query(By.css(".messageOptionTextBoxDiv")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Text Box of message option to be present.", async () => {
//         fixture = TestBed.createComponent(FeedbackFlowPage);
//         de = fixture.debugElement.query(By.css(".messageOptionTextBox")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Add picture option heading div should be present.", async () => {
//         fixture = TestBed.createComponent(FeedbackFlowPage);
//         de = fixture.debugElement.query(By.css(".addPictureHeaderDiv")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it(" ion row should contain Heading 'Add picture for reference (optional)' ", async () => {
//         const title: HTMLElement = fixture.debugElement.query(By.css('.addPictureHeader')).nativeElement;
//         expect(de).toBeDefined();
//         // expect(title.textContent).toContain("Add picture for reference (optional)");
//     });

//     // it("Add picture option empty div should be present.", async () => {
//     //     fixture = TestBed.createComponent(FeedbackFlowPage);
//     //     de = fixture.debugElement.query(By.css(".addPictureEmptyDiv")).nativeElement;
//     //     expect(de).toBeDefined();
//     // });

//     // it("Add picture option empty space ion row should be present.", async () => {
//     //     fixture = TestBed.createComponent(FeedbackFlowPage);
//     //     de = fixture.debugElement.query(By.css(".addPictureEmptySpace")).nativeElement;
//     //     expect(de).toBeDefined();
//     // });

//     it("Add picture option grid to be present.", async () => {
//         fixture = TestBed.createComponent(FeedbackFlowPage);
//         de = fixture.debugElement.query(By.css(".addPictureGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Add picture option grid should have picture option div.", async () => {
//         fixture = TestBed.createComponent(FeedbackFlowPage);
//         de = fixture.debugElement.query(By.css(".addPictureDiv")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     // it("Picture option div should have mobile icon.", async () => {
//     //     fixture = TestBed.createComponent(FeedbackFlowPage);
//     //     de = fixture.debugElement.query(By.css(".addPictureIcon")).nativeElement;
//     //     expect(de).toBeDefined();
//     // });
//     // it("Picture option div should have x icon.", async () => {
//     //     fixture = TestBed.createComponent(FeedbackFlowPage);
//     //     de = fixture.debugElement.query(By.css(".removePictureIcon")).nativeElement;
//     //     expect(de).toBeDefined();
//     // });


//     it("Get Back Option heading div to be present.", async () => {
//         fixture = TestBed.createComponent(FeedbackFlowPage);
//         de = fixture.debugElement.query(By.css(".getBackOptionHeaderDiv")).nativeElement;
//         expect(de).toBeDefined();
//     });



//     it("Get Back Option heading should match.", async () => {
//         const title: HTMLElement = fixture.debugElement.query(By.css('.getBackOptionHeader')).nativeElement;
//         expect(title.textContent).toContain("How can we get back to you?");
//     });


//     it("A separate grid for Get Back Option.", async () => {
//         fixture = TestBed.createComponent(FeedbackFlowPage);
//         de = fixture.debugElement.query(By.css(".getBackOptionGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });



//     it("Get Back Option Grid should contain div for Email field.", async () => {
//         fixture = TestBed.createComponent(FeedbackFlowPage);
//         de = fixture.debugElement.query(By.css(".getBackOptionEmailDiv")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Email field should have heading.", async () => {
//         const title: HTMLElement = fixture.debugElement.query(By.css('.getBackOptionEmailHeader')).nativeElement;
//         expect(title.textContent).toContain("Email");
//     });

//     it("Input field for email present.", async () => {
//         fixture = TestBed.createComponent(FeedbackFlowPage);
//         de = fixture.debugElement.query(By.css(".emailInput")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Get Back Option Grid should contain div for Phone Number field.", async () => {
//         fixture = TestBed.createComponent(FeedbackFlowPage);
//         de = fixture.debugElement.query(By.css(".getBackOptionPhoneNumberDiv")).nativeElement;
//         expect(de).toBeDefined();
//     });



//     it("Phone Number field should have heading.", async () => {
//         const title: HTMLElement = fixture.debugElement.query(By.css('.getBackOptionPhoneNumberHeader')).nativeElement;
//         expect(title.textContent).toContain("Phone Number");
//     });



//     it("Input field for dial code", async () => {
//         fixture = TestBed.createComponent(FeedbackFlowPage);
//         de = fixture.debugElement.query(By.css(".dialCode")).nativeElement;
//         expect(de).toBeDefined();
//     });


//     it("Input field for phone number", async () => {
//         fixture = TestBed.createComponent(FeedbackFlowPage);
//         de = fixture.debugElement.query(By.css(".phoneInput")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Get Back Option Grid should contain div for Continue Button.", async () => {
//         fixture = TestBed.createComponent(FeedbackFlowPage);
//         de = fixture.debugElement.query(By.css(".getBackOptionContinueDiv")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Continue Button should be present.", async () => {
//         fixture = TestBed.createComponent(FeedbackFlowPage);
//         de = fixture.debugElement.query(By.css(".getBackOptionContinueButton")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Continue Button text should match.", async () => {
//         const title: HTMLElement = fixture.debugElement.query(By.css('.getBackOptionContinueText')).nativeElement;
//         expect(title.textContent).toContain("Continue");
//     });

// });


// describe('Feedback Flow functional testing - Validating Email and Phone Number', () => {
//     let de: DebugElement;
//     let comp: FeedbackFlowPage;
//     let fixture: ComponentFixture<FeedbackFlowPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [FeedbackFlowPage],
//             imports: [
//                 IonicModule.forRoot(FeedbackFlowPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 ValidPhoneProvider,
//                 GeogetterProvider,
//                 Geolocation,
//                 NativeGeocoder,
//                 TranslateService,
//                 TranslateStore,
//                 Camera,
//                 Crop, Base64, FileChooser, FilePath,
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(FeedbackFlowPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it('Validating Email Id - validateEmail() - Valid Email ', () => {
//         comp.emailid = "shivani@gmail.com";
//         expect(comp.validateEmail(comp.emailid)).toBe(true);
//     });

//     it('Validating Email Id - validateEmail() - Invalid Email ', () => {
//         comp.emailid = "123shivgmail@.com";
//         expect(comp.validateEmail(comp.emailid)).toBe(false);
//     });

//     it('Validating Phone Number - dynamic() - Trimming Character', () => {
//         comp.phoneNumber = "123456a";
//         comp.dynamic();
//         expect(comp.phoneNumber).toMatch("123456");
//     });

//     it('Validating Phone Number - dynamic() - Trimming Space ', () => {
//         comp.phoneNumber = "123 4";
//         comp.dynamic();
//         expect(comp.phoneNumber).toMatch("123");
//     });

//     it('Validating Phone Number - checkifNo() - Valid Number ', () => {
//         comp.phoneNumber = "123456";
//         expect(comp.checkifNo(comp.phoneNumber)).toBe(true);
//     });

//     it('Validating Phone Number - checkifNo() - Invalid Number', () => {
//         comp.phoneNumber = "123456a$";
//         expect(comp.checkifNo(comp.phoneNumber)).toBe(false);
//     });

//     it('Phone Number Length Format (Single Integer)', () => {
//         comp.getPhoneLengthWhenInteger(10);
//         expect(comp.defaultMin == 10 && comp.defaultMax == 10).toBe(true);
//     });

//     it('Phone Number Length Format (to)', () => {
//         comp.getPhoneLengthWhenTo("5 to 10");
//         expect(comp.defaultMin == 5 && comp.defaultMax == 10).toBe(true);
//     });

//     it('Phone Number Length Format (comma)', () => {
//         comp.getPhoneLengthWhenComma("6, 8, 2009");
//         expect(comp.replaceDefaultLengthArray[0] == 6 && comp.replaceDefaultLengthArray[1] == 8 && comp.replaceDefaultLengthArray[2] == 2009).toBe(true);
//     });

//     it('Phone Number Length Format ()', () => {
//         comp.getPhoneLengthWhenBracket("(684)+7");
//         expect(comp.replaceDefaultLengthArray[0] == 684 && comp.replaceDefaultLengthArray[1] == 7).toBe(true);
//     });

//     it('Entered Number Length matches the actual length', () => {
//         expect(comp.phoneNumber == 1234567890).toBe(false);
//     });

//     it('Space in the phone number should be trimmed', () => {
//         comp.phoneNumber = '123 456';
//         expect(comp.TrimSpace(comp.phoneNumber)).toMatch('123456');
//     });


//     it('method to open camera and crop image', () => {
//         expect(comp.cropDOC).toBeTruthy();
//     });

//     it('method to open images via file', () => {
//         expect(comp.fileChoose).toBeTruthy();
//     });

//     it('showPrompt() method to open an alert modal to choose image and capture image on click image icon', () => {
//         expect(comp.showPrompt).toBeTruthy();
//     });

//     it('getImage() GET IMGE FROM IONIC LOCAL STORAGE', () => {
//         expect(comp.getImage).toBeTruthy();
//     });

//     it('removeimage() Remove Image based on index value', () => {
//         expect(comp.removeimage).toBeTruthy();
//     });

//     it('removeimage() Remove Image based on index value', () => {
//         comp.toDisplay=['1','3','5','6'];
//         comp.removeimage(0)
       
//         expect(comp.toDisplay).not.toContain(['1','3','5','6']);
//     });


// });