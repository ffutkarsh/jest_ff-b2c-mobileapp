import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CountryModalPage } from './country-modal';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CountryModalPage
  ],
  imports: [
    IonicPageModule.forChild(CountryModalPage),
    TranslateModule
  ]
})
export class CountryModalPageModule { }
