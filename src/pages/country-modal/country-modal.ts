import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { ValidPhoneProvider } from '../../providers/valid-phone/valid-phone';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-country-modal',
  templateUrl: 'country-modal.html',
})
export class CountryModalPage {

  public allCountries = []; //ARRAY OF OBJECTS TO STORE THE DATA FROM JSON FILE.
  public myInput: any; //VARIABLE TO BIND THE STRING WRITTEN IN SEARCH BAR TO NG-MODEL.
  public CountrySelected: string; //STORES THE DATA OF SELECTED COUNTRY.

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private viewController: ViewController,
    public loadingCtrl: LoadingController,
    private validphone: ValidPhoneProvider,
    public translateService: TranslateService) {

    this.CountrySelected = this.navParams.get("CountrySelected"); //TO GET THE DATA FROM NAV PARAMS.

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CountryModalPage');
    this.Getcountries();
  }
 // Method to close the modal

  //PRESENT LOADING WHILE ENTERING.
  ionViewWillEnter() {
    this.presentLoading();
  }

  //GETTING THE COUNTRIES FROM JSON IN VALID PHONE PROVIDER.
  Getcountries() {
    this.validphone.getCountries().map((response) => response.json()).subscribe((response) => {
      this.allCountries = response;
    });
  }

  //TAKES THE INPUT AND BASED ON IT FILTERS THE LIST OF COUNTRIES.
  onInput($event) {
    this.setFilteredItems(this.myInput)
  }

  //FILTERS THE LIST OF COUNTRIES BY MODIFYING THE LIST OF COUNTRIES AS PER INPUT.
  setFilteredItems(term) {
    this.allCountries = this.filterItemsSearch(term);
  }

  //MODIFIES THE COUNTRY LIST FROM THE SEARCH TEXT.
  filterItemsSearch(searchText) {

    if (!this.allCountries) return [];

    if (!searchText) return this.allCountries;

    searchText = searchText.toLowerCase();

    return this.allCountries.filter(it => {
      return it.name.toLowerCase().includes(searchText);
    });

  }

  //HANDLES THE BACKPRESSED OR DELETE AND DISPLAYS THE LIST ACCORDING TO CURRENT SEARCH TEXT.
  onBackPress(event) {
    if (event.keyCode == 8 || event.keyCode == 46) {
      console.log("back");
      this.Getcountries();
    }
  }
 // Method to close the modal
 closeModal1(e) {
   console.log((e.path[0].className))
  if((e.path[0].className) != 'searchbar-input')
  {
    this.navCtrl.pop()
  }
 
//  this.navCtrl.pop();
}
  //TO CLOSE THE MODAL IF THE COUNTRY IS SELECTED.
  closeModal(selected) {
    let CountrySelectedState = {
      CountrySelected: selected
    };
    this.viewController.dismiss(CountrySelectedState);
  }

  //LOADER.
  presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 500
    });
    loader.present();
  }

}