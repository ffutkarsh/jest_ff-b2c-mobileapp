// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from "@angular/http";
// import { Http, ConnectionBackend } from '@angular/http';
// import { CountryModalPage } from '../country-modal/country-modal';
// import { ValidPhoneProvider } from '../../providers/valid-phone/valid-phone';
// import { App, Config, IonicModule, Platform, ViewController, DomController, Keyboard, GestureController, Form, NavController, NavParams } from "ionic-angular";
// import { mockApp, mockConfig, mockPlatform, mockView } from "ionic-angular/util/mock-providers";
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";

// class ViewControllerMock {
//     public _setHeader(): any {
//         return {}
//     };
//     public _setIONContent(): any {
//         return {}
//     };
//     public _setIONContentRef(): any {
//         return {}
//     };
// }

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// describe('Country Modal Page - UI Part', () => {
//     let comp: CountryModalPage;
//     let fixture: ComponentFixture<CountryModalPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [CountryModalPage],
//             imports: [
//                 IonicModule.forRoot(CountryModalPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: ViewController, useValue: mockView() },
//                 { provide: Config, useValue: mockConfig() },
//                 { provide: Platform, useValue: mockPlatform() },
//                 { provide: App, useValue: mockApp() },
//                 { provide: NavParams, useClass: NavParamsMock },
//                 DomController,
//                 Keyboard,
//                 GestureController,
//                 Form,
//                 ValidPhoneProvider,
//                 TranslateService,
//                 TranslateStore
//             ]
//         }).compileComponents();;
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(CountryModalPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//     });

//     it('Page component should be created', () => expect(comp).toBeDefined());

//     it("SearchBar should be present.", async () => {
//         let fixture = TestBed.createComponent(CountryModalPage);
//         let element2 = fixture.debugElement.nativeElement.querySelector("ion-searchbar");
//         expect(element2).toBeDefined();
//     });

//     it("Should have content", async () => {
//         let fixture = TestBed.createComponent(CountryModalPage);
//         let element1 = fixture.debugElement.nativeElement.querySelector("ion-content");
//         expect(element1).toBeDefined();
//     });

//     it("Column contain icon", async () => {
//         let fixture = TestBed.createComponent(CountryModalPage);
//         const button: HTMLButtonElement = fixture.debugElement.query(By.css('ion-icon')).nativeElement;
//         expect(button).toBeDefined();
//     });
// });

// describe('Country Select - Methods and Logic Part', () => {
//     let comp: CountryModalPage;
//     let fixture: ComponentFixture<CountryModalPage>;
//     let validphone: ValidPhoneProvider;
//     let myInput = "India";

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [CountryModalPage],
//             imports: [
//                 IonicModule.forRoot(CountryModalPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: ViewController, useValue: mockView() },
//                 { provide: Config, useValue: mockConfig() },
//                 { provide: Platform, useValue: mockPlatform() },
//                 { provide: App, useValue: mockApp() },
//                 { provide: NavParams, useClass: NavParamsMock },
//                 DomController,
//                 Keyboard,
//                 GestureController,
//                 Form,
//                 ValidPhoneProvider,
//                 TranslateService,
//                 TranslateStore
//             ]
//         }).compileComponents();
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(CountryModalPage);
//         comp = fixture.componentInstance;
//         validphone = fixture.debugElement.injector.get(ValidPhoneProvider);
//     });

//     afterEach(() => {
//         validphone = null;
//     });

//     it('Instance of Valid Phone Provided created', () => {
//         expect(validphone instanceof ValidPhoneProvider).toBe(true);
//     });

//     it('Ionviewdidload Method should be present.', () => {
//         expect(comp.ionViewDidLoad).toBeDefined(true);
//     });

//     it('ionViewWillEnter Method should be present.', () => {
//         expect(comp.ionViewWillEnter).toBeDefined(true);
//     });

//     it('Getcountries Method should be present.', () => {
//         expect(comp.Getcountries).toBeDefined(true);
//     });

//     it('On input', () => {
//         comp.myInput = 'a'
//         comp.onInput(event);
//         expect(comp.myInput).toBeDefined();
//     });

//     it('Filter Search', () => {
//         comp.myInput = ''
//         comp.filterItemsSearch(myInput);
//         expect(comp.allCountries).toEqual([]);
//     });

//     it('Page Should have a Loader', () => expect(comp.presentLoading).toBeDefined());

// });