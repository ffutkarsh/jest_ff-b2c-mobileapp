import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Component } from '@angular/core';
import  { VideoPlayingPage } from '../../pages/pages';
import { CategoryListProvider } from '../../providers/providers';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-video-list',
  templateUrl: 'video-list.html',
})
export class VideoListPage {
  videoList = [];  //STORING API RESPONSE OF VIDEO LISTS.
  searchTerm: string = '';   //VARIABLE TO BIND THE STRING WRITTEN IN SEARCH BAR TO NG-MODEL.
  items: any; 
  time; //var to store json time in moment format
  constructor(public navCtrl: NavController, public video: CategoryListProvider) {
    this.fetchvideoList();
    this.setFilteredItems()
  }

  ionViewDidLoad() {
  }

  //API RESPONSE OF VIDEO LISTS.
  fetchvideoList() {
    this.video.getVideoDetails().map(response => response).subscribe((response) => {
      this.videoList = response['VideoList'];
      for(let i=0;i<this.videoList.length;i++)
      {
        this.videoList[i].Videolength = moment(this.videoList[i].Videolength, "hh:mm").format("hh:mm");
        console.log( this.videoList[i].Videolength,i,'TIME')
      }
    })
  }


//FILTERS THE LIST OF VIDEO CATEGORIES BY MODIFYING THE LIST OF VIDEO AS PER INPUT.
  filterItems(searchTerm) {

    return this.videoList.filter((item) => {
      return item.VideoTitle.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }

   //FILTERS THE LIST OF VIDEO CATEGORIES BY MODIFYING THE LIST OF VIDEO CATEGORIES AS PER INPUT.
  setFilteredItems() {

    this.items = this.filterItems(this.searchTerm);

  }


  ionViewDidEnter() {
    this.setFilteredItems();
  }

  //going to video playing page 
  goToPlayer(item) {
    this.navCtrl.push(VideoPlayingPage,
      {
        'item': item
      })

      console.log(this.videoList,'LIST1')
  }

}
