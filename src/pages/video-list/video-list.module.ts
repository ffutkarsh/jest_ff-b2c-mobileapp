import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoListPage } from './video-list';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    VideoListPage,
  ],
  imports: [
    IonicPageModule.forChild(VideoListPage),
    TranslateModule
  ],
})
export class VideoListPageModule {}
