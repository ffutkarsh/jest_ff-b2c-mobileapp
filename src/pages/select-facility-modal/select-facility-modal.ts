import { Component } from '@angular/core';
import * as moment from 'moment';
import { Slides } from 'ionic-angular';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ViewChild } from '@angular/core';

@IonicPage()
@Component({
  selector: 'page-select-facility-modal',
  templateUrl: 'select-facility-modal.html',
})
export class SelectFacilityModalPage {

  @ViewChild(Slides) slides: Slides;

  public facilityList = []; //TO RECEIVE LIST OF FACILITY FROM DASHBOARD.
  public selectedFacility = {}; //TO STORE THE OBJECT OF SELECTED FACILTIY.

  public TotalSeriesArray = []; //TO STORE THE SERIES OF DURATION IN HOURS.

  public val1: boolean; //SET TO TRUE WHEN FACILITY IS SELECTED.
  public val2: boolean; //SET TO TRUE WHEN DURATION IS SELECTED.

  public ButtonClass: boolean; //TO APPLY CSS CLASS TO CONFIRM BUTTON.

  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
  ) {

    this.facilityList = this.navParams.get("facilityApiResponse");

    this.TotalSeriesArray = [];
    this.generateSeries();

    this.val1 = false;
    this.val2 = false;

    this.ButtonClass = false;

  }

  ionViewDidLoad() { }

  ionViewWillEnter() {
    this.presentLoading();
  }

  //TO GENERATE SERIES OF DURATION WITH THE INTERVAL OF HALF AN HOUR.
  generateSeries() {
    let currentCount = moment.duration({ 'minutes': 30 });
    for (let i = 0; i < 4; i++) {
      let seriesarray = []
      let Displayseriesarray = [];
      for (let j = 0; j < 5; j++) {
        let duration = currentCount.clone();
        seriesarray.push(duration)
        duration.add(30, 'minutes');
        currentCount = duration.clone();
      }
      for (let j = 0; j < 5; j++) {
        let n = moment.utc(seriesarray[j].as('milliseconds')).format('HH:mm')
        let tempdDur = {
          'duration': n,
          'isColor': "transparent",
          'textColor': 'black',
          'StrDuration': ''
        }
        Displayseriesarray.push(tempdDur)
      }
      this.TotalSeriesArray.push(Displayseriesarray);
    }
  }

  //TO PRESENT LOADING AFTER THE CONFIRM BUTTON IS PRESSED.
  presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 500
    });
    loader.present();
  }

  //TO CHANGE THE STATUS OF FACILITY ON SELECTION.
  SelectFacility(selected) {
    for (let i = 0; i < this.facilityList.length; i++) {
      if (i == selected) {
        this.facilityList[i].status = true;
        this.selectedFacility = this.facilityList[i];
      }
      else
        this.facilityList[i].status = false;
    }
    this.val1 = true;
    this.selectBoth(this.val1, this.val2);
  }

  //TO GO TO PREVIOUS DURATIONS.
  slidePrev() {
    this.slides.slidePrev();
  }

  //TO GO TO NEXT DURATIONS.
  slideNext() {
    this.slides.slideNext();
  }

  //TO APPLY CSS CLASS TO SELECTED DURATION.
  SelectDuration(item) {
    for (let i = 0; i < 4; i++) {
      for (let j = 0; j < 5; j++) {
        if (this.TotalSeriesArray[i][j].duration == item.duration) {
          this.TotalSeriesArray[i][j].isColor = '#99DDE8';
          this.TotalSeriesArray[i][j].textColor = '#8F6CB7';
        } else {
          this.TotalSeriesArray[i][j].isColor = 'transparent';
          this.TotalSeriesArray[i][j].textColor = 'black';
        }
      }
    }
    this.val2 = true;
    this.selectBoth(this.val1, this.val2);
  }

  displayHalf(dur, item) {
    let d;
    if (String(dur).includes(':30')) {
      if (String(dur).startsWith('0')) {
        d = String(dur).substr(1, 1);
      } else {
        d = String(dur).substr(0, 2);
      }
      d = d + '.5';
    } else {
      if (String(dur).startsWith('0')) {
        d = String(dur).substr(1, 1);
      } else {
        d = String(dur).substr(0, 2);
      }
    }
    item.StrDuration = d;
    return d
  }

  //TO CHECK WHETHER BOTH THE FACILITY AND THE DURATION ARE BEEN SELECTED OR NOT.
  selectBoth(val1, val2) {
    if (val1 && val2) {
      this.ButtonClass = true;
    }
  }

  //TO CLOSE THE MODAL.
  closeModal() {
    for (let i = 0; i < this.facilityList.length; i++) {
      this.facilityList[i].status = false;
    }
    this.navCtrl.pop();
  }

  //TO MOVE TO FACILITY DATE AND TIME PAGE.
  goToFacilityDateTime() {
    if (this.ButtonClass) {
      this.navCtrl.push("FacilityDateTimePage", {
        'selectedFacility': this.selectedFacility
      });
    }
  }

}
