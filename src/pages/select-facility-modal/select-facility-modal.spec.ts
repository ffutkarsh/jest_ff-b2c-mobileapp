// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { SelectFacilityModalPage } from './select-facility-modal';
// import { HttpModule } from "@angular/http";
// import { Http, ConnectionBackend } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';
// import { Component } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular';


// class NavParamsMock {
//         static returnParam = null;
//         public get(key): any {
//             if (NavParamsMock.returnParam) {
//                 return NavParamsMock.returnParam
//             }
//             return 'default';
//         }
//         static setParams(value) {
//             NavParamsMock.returnParam = value;
//         }
//     }



// describe('Select facility modal Page UI Testing', () => {
//     let de: DebugElement;
//     let comp: SelectFacilityModalPage;
//     let fixture: ComponentFixture<SelectFacilityModalPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [SelectFacilityModalPage],
//             imports: [
//                 HttpModule,
//                 IonicModule.forRoot(SelectFacilityModalPage),
//                 HttpModule,
//                  HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//              Http,
            
//                 { provide: NavParams, useClass: NavParamsMock },
//               ConnectionBackend,
//                 TranslateService,
//                 TranslateStore
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(SelectFacilityModalPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
   
//     });

//      it("Page component should be created.", () => expect(comp).toBeDefined());



//     it("Ion-content main should be present.", async () => {
//         de = fixture.debugElement.query(By.css(".main-content")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Modal content should be present.", async () => {
//         de = fixture.debugElement.query(By.css(".Modal")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Navigation bar should be present.", async () => {
//         de = fixture.debugElement.query(By.css(".NavigationBar")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Title should be present.", async () => {
//             de = fixture.debugElement.query(By.css(".Title")).nativeElement;
//             expect(de).toBeDefined();
//         });

//     it("scroll terms content should be present", async () => {

//             de = fixture.debugElement.query(By.css(".termsContent")).nativeElement;
//             expect(de).toBeDefined();
//         });



//     it("Card content should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             de = fixture.debugElement.query(By.css(".cardContent")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });


//     it("Facility image should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             de = fixture.debugElement.query(By.css(".facilityImage")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

    
//     it("Facility name should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             de = fixture.debugElement.query(By.css(".facilityName")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Select hours be present.", async () => {
       
//             de = fixture.debugElement.query(By.css(".selecttxt")).nativeElement;
//             expect(de).toBeDefined();
       
//     });

//      it("Hour selection left arrow button should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".arrows arrowsLeft")).nativeElement;
//             expect(de).toBeDefined();
//         });
//         });


//     it("Hour selection right arrow icon should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".iconArrowRight")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });


//         it("Hour selection right arrow button should be present.", async () => {
//        fixture.whenStable().then(() => {
//            // after something in the component changes, you should detect changes
//            fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".arrows arrowsRight")).nativeElement;
//             expect(de).toBeDefined();
//         });
//          });


//     it("Hour selection left arrow icon should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".iconArrowLeft")).nativeElement;
//             expect(de).toBeDefined();
//         });
//         });


    
//     it("Width between slide should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".width-slide")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Time duration text should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".durationtxt")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Circle around selected time should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".circlearound")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Button row should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".btnrow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Close modal Button should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".btncol1")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("cancel should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".cartItemsProceed")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Proceed should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             de = fixture.debugElement.query(By.css(".proceed")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });
//       });

//       describe('Select facility modal Page - Methods and Logic Part', () => {
//     let de: DebugElement;
//     let comp: SelectFacilityModalPage;
//     let fixture: ComponentFixture<SelectFacilityModalPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [SelectFacilityModalPage],
//             imports: [
//                 IonicModule.forRoot(SelectFacilityModalPage),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(SelectFacilityModalPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.

//     });

//     it('Ionviewdidload Method should be present.', () => {
//         expect(comp.ionViewDidLoad).toBeDefined();
//     });

//     it('IonviewWillEnter Method should be present.', () => {
//         expect(comp.ionViewWillEnter).toBeDefined();
//     });

//     it('val1 initial value should be false.', () => {
//         expect(comp.val1).toBeFalsy();
//     });

//     it('val2 initial value should be false.', () => {
//         expect(comp.val2).toBeFalsy();
//     });

    
//     it('Button class initial value should be false.', () => {
//         expect(comp.ButtonClass).toBeFalsy();
//     });
    
//     it('Method go to facility date and time page should be defined', () => {
//         expect(comp.goToFacilityDateTime).toBeDefined();
//     });

//     it('Close modal method should be present', () => {
//         expect(comp.closeModal).toBeDefined();
//     });

//     it('Button class initial value should be false.', () => {
//         expect(comp.ButtonClass).toBeFalsy();
//     });

//     it('Generate series method should be present', () => {
//         expect(comp.generateSeries).toBeDefined();

//     });

//         it("Button class should be true on selecting the selectBoth method", () => {
//         comp.selectBoth(comp.val1=true,comp.val2=true); // On Timer Expire Action
//         expect(comp.ButtonClass).toBeTruthy();
//     }); // Checking is button Enabled



//     it('Present loading should be present', () => {
//         expect(comp.presentLoading).toBeDefined();

//     });

//     it('Display half should be present', () => {
//         expect(comp.displayHalf).toBeDefined();

//     });

//     it('Select duration should be present', () => {
//         let item ={duration: "02:30", isColor: "transparent", textColor: "black", StrDuration: "2.5"};
//         comp.SelectDuration(item);
//         expect(comp.val2).toBe(true);

//     });

//     it('Display half should be present', () => {
//         expect(comp.SelectDuration).toBeDefined();

//     });


//     it('Display half should be present', () => {
//         expect(comp.SelectFacility).toBeDefined();

//     });
    
// });




 






