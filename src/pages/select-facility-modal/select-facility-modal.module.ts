import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectFacilityModalPage } from './select-facility-modal';

@NgModule({
  declarations: [
    SelectFacilityModalPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectFacilityModalPage),
  ],
})
export class SelectFacilityModalPageModule {}
