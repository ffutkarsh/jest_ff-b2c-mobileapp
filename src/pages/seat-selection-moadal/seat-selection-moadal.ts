import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
    selector: 'page-seat-selection-moadal',
    templateUrl: 'seat-selection-moadal.html',
})
export class SeatSelectionMoadalPage {

    public PurposeSessions; //TO STORE ENTIRE PURPOSE SESSIONS OBJECT.
    public purposeSessionsArray; //TO STORE THE SELECTED PURPOSE SESSIONS.
    public purposeSelected; //TO STORE THE PURPOSE.
    public PurposeName; //TO BIND THE PURPOSE NAME AS CLASS NAME INTO HTML.

    public ButtonClass: boolean; //TO APPLY CSS TO CONFIRM BUTTON.

    public Rows: number; //TO STORE THE SEATS ROW
    public Columns: number; //TO STORE THE SEATS COLUMN.

    public size: any; //TO CALULATE THE SEATS.
    public pos: any; //TO STORE THE POSITIONS OF SEAT.
    public seatHeader: any; //TO STORE SEAT HEADINGS.

    public selectedSeats: any; //SELECTED SEATS.
    public Counter: any; //CALCULATE THE NUMBER OF SEATS.
    public buttonValue; //CHECK WHETHER JOIN OR BUY&JOIN.

    public SeatsAlloted: any; //SEAT ALLOTED.

    public seatPurposeModalShow: boolean; //DEPENDING ON BUTTON VALUE DECIDE WHETHER TO DISPLAY PURPOSE SELECTION MODAL OR NOT.
    setofferLocally;
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public translateService: TranslateService,
        private storage: Storage) {

        this.PurposeSessions = this.navParams.get('PurposeSessions');
        this.purposeSessionsArray = this.PurposeSessions['purposeSessions'];
        this.PurposeName = this.navParams.get('PurposeName');
        this.navParams.get('PurposeLogo');

        this.ButtonClass = false;

        this.Rows = 9;
        this.Columns = 9;

        this.size = ((92 - (Number(this.Columns) * 1.2)) / (Number(this.Columns) + 1));
        this.size = this.size + '%';
        this.pos = [];
        this.seatHeader = [];

        this.selectedSeats = [];
        this.Counter = 0;
        this.buttonValue = this.navParams.get('buttonValue');

        this.SeatsAlloted = [
            {
                "Row": "A",
                "Col": "0",
                "Status": "A", "selected": false
            },
            {
                "Row": "A",
                "Col": "1",
                "Status": "A", "selected": false
            },
            {
                "Row": "A",
                "Col": "2",
                "Status": "A", "selected": false
            },
            {
                "Row": "A",
                "Col": "3",
                "Status": "B", "selected": false
            },
            {
                "Row": "A",
                "Col": "4",
                "Status": "B", "selected": false
            },
            {
                "Row": "A",
                "Col": "5",
                "Status": "A", "selected": false
            },
            {
                "Row": "A",
                "Col": "6",
                "Status": "A", "selected": false
            },
            {
                "Row": "A",
                "Col": "7",
                "Status": "UA", "selected": false
            },
            {
                "Row": "A",
                "Col": "8",
                "Status": "A", "selected": false
            },
            {
                "Row": "A",
                "Col": "9",
                "Status": "A", "selected": false
            },
            {
                "Row": "B",
                "Col": "0",
                "Status": "B", "selected": false
            },
            {
                "Row": "B",
                "Col": "1",
                "Status": "B", "selected": false
            },
            {
                "Row": "B",
                "Col": "2",
                "Status": "A", "selected": false
            },
            {
                "Row": "B",
                "Col": "3",
                "Status": "UA", "selected": false
            },
            {
                "Row": "B",
                "Col": "4",
                "Status": "B", "selected": false
            },
            {
                "Row": "B",
                "Col": "5",
                "Status": "UA", "selected": false
            },
            {
                "Row": "B",
                "Col": "6",
                "Status": "UA", "selected": false
            },
            {
                "Row": "B",
                "Col": "7",
                "Status": "A", "selected": false
            },
            {
                "Row": "B",
                "Col": "8",
                "Status": "A", "selected": false
            },
            {
                "Row": "B",
                "Col": "9",
                "Status": "B", "selected": false
            },
            {
                "Row": "C",
                "Col": "0",
                "Status": "A", "selected": false
            },
            {
                "Row": "C",
                "Col": "1",
                "Status": "A", "selected": false
            },
            {
                "Row": "C",
                "Col": "2",
                "Status": "A", "selected": false
            },
            {
                "Row": "C",
                "Col": "3",
                "Status": "A", "selected": false
            },
            {
                "Row": "C",
                "Col": "4",
                "Status": "B", "selected": false
            },
            {
                "Row": "C",
                "Col": "5",
                "Status": "B", "selected": false
            },
            {
                "Row": "C",
                "Col": "6",
                "Status": "A", "selected": false
            },
            {
                "Row": "C",
                "Col": "7",
                "Status": "UA", "selected": false
            },
            {
                "Row": "C",
                "Col": "8",
                "Status": "A", "selected": false
            },
            {
                "Row": "C",
                "Col": "9",
                "Status": "UA", "selected": false
            },
            {
                "Row": "D",
                "Col": "0",
                "Status": "B", "selected": false
            },
            {
                "Row": "D",
                "Col": "1",
                "Status": "B", "selected": false
            },
            {
                "Row": "D",
                "Col": "2",
                "Status": "A", "selected": false
            },
            {
                "Row": "D",
                "Col": "3",
                "Status": "A", "selected": false
            },
            {
                "Row": "D",
                "Col": "4",
                "Status": "A", "selected": false
            },
            {
                "Row": "D",
                "Col": "5",
                "Status": "UA", "selected": false
            },
            {
                "Row": "D",
                "Col": "6",
                "Status": "B", "selected": false
            },
            {
                "Row": "D",
                "Col": "7",
                "Status": "A", "selected": false
            },
            {
                "Row": "D",
                "Col": "8",
                "Status": "UA", "selected": false
            },
            {
                "Row": "D",
                "Col": "9",
                "Status": "UA", "selected": false
            },
            {
                "Row": "E",
                "Col": "0",
                "Status": "B", "selected": false
            },
            {
                "Row": "E",
                "Col": "1",
                "Status": "UA", "selected": false
            },
            {
                "Row": "E",
                "Col": "2",
                "Status": "UA", "selected": false
            },
            {
                "Row": "E",
                "Col": "3",
                "Status": "UA", "selected": false
            },
            {
                "Row": "E",
                "Col": "4",
                "Status": "A", "selected": false
            },
            {
                "Row": "E",
                "Col": "5",
                "Status": "A", "selected": false
            },
            {
                "Row": "E",
                "Col": "6",
                "Status": "UA", "selected": false
            },
            {
                "Row": "E",
                "Col": "7",
                "Status": "B", "selected": false
            },
            {
                "Row": "E",
                "Col": "8",
                "Status": "B", "selected": false
            },
            {
                "Row": "E",
                "Col": "9",
                "Status": "UA", "selected": false
            },
            {
                "Row": "F",
                "Col": "0",
                "Status": "B", "selected": false
            },
            {
                "Row": "F",
                "Col": "1",
                "Status": "UA", "selected": false
            },
            {
                "Row": "F",
                "Col": "2",
                "Status": "B", "selected": false
            },
            {
                "Row": "F",
                "Col": "3",
                "Status": "A", "selected": false
            },
            {
                "Row": "F",
                "Col": "4",
                "Status": "A", "selected": false
            },
            {
                "Row": "F",
                "Col": "5",
                "Status": "B", "selected": false
            },
            {
                "Row": "F",
                "Col": "6",
                "Status": "B", "selected": false
            },
            {
                "Row": "F",
                "Col": "7",
                "Status": "A", "selected": false
            },
            {
                "Row": "F",
                "Col": "8",
                "Status": "UA", "selected": false
            },
            {
                "Row": "F",
                "Col": "9",
                "Status": "UA", "selected": false
            },
            {
                "Row": "G",
                "Col": "0",
                "Status": "B", "selected": false
            },
            {
                "Row": "G",
                "Col": "1",
                "Status": "A", "selected": false
            },
            {
                "Row": "G",
                "Col": "2",
                "Status": "UA", "selected": false
            },
            {
                "Row": "G",
                "Col": "3",
                "Status": "A", "selected": false
            },
            {
                "Row": "G",
                "Col": "4",
                "Status": "B", "selected": false
            },
            {
                "Row": "G",
                "Col": "5",
                "Status": "UA", "selected": false
            },
            {
                "Row": "G",
                "Col": "6",
                "Status": "A", "selected": false
            },
            {
                "Row": "G",
                "Col": "7",
                "Status": "B", "selected": false
            },
            {
                "Row": "G",
                "Col": "8",
                "Status": "A", "selected": false
            },
            {
                "Row": "G",
                "Col": "9",
                "Status": "A", "selected": false
            },
            {
                "Row": "H",
                "Col": "0",
                "Status": "A", "selected": false
            },
            {
                "Row": "H",
                "Col": "1",
                "Status": "B", "selected": false
            },
            {
                "Row": "H",
                "Col": "2",
                "Status": "UA", "selected": false
            },
            {
                "Row": "H",
                "Col": "3",
                "Status": "B", "selected": false
            },
            {
                "Row": "H",
                "Col": "4",
                "Status": "A", "selected": false
            },
            {
                "Row": "H",
                "Col": "5",
                "Status": "UA", "selected": false
            },
            {
                "Row": "H",
                "Col": "6",
                "Status": "UA", "selected": false
            },
            {
                "Row": "H",
                "Col": "7",
                "Status": "B", "selected": false
            },
            {
                "Row": "H",
                "Col": "8",
                "Status": "A", "selected": false
            },
            {
                "Row": "H",
                "Col": "9",
                "Status": "UA", "selected": false
            },
            {
                "Row": "I",
                "Col": "0",
                "Status": "A", "selected": false
            },
            {
                "Row": "I",
                "Col": "1",
                "Status": "B", "selected": false
            },
            {
                "Row": "I",
                "Col": "2",
                "Status": "A", "selected": false
            },
            {
                "Row": "I",
                "Col": "3",
                "Status": "A", "selected": false
            },
            {
                "Row": "I",
                "Col": "4",
                "Status": "A", "selected": false
            },
            {
                "Row": "I",
                "Col": "5",
                "Status": "B", "selected": false
            },
            {
                "Row": "I",
                "Col": "6",
                "Status": "B", "selected": false
            },
            {
                "Row": "I",
                "Col": "7",
                "Status": "UA", "selected": false
            },
            {
                "Row": "I",
                "Col": "8",
                "Status": "A", "selected": false
            },
            {
                "Row": "I",
                "Col": "9",
                "Status": "UA", "selected": false
            },
            {
                "Row": "J",
                "Col": "0",
                "Status": "A", "selected": false
            },
            {
                "Row": "J",
                "Col": "1",
                "Status": "B", "selected": false
            },
            {
                "Row": "J",
                "Col": "2",
                "Status": "A", "selected": false
            },
            {
                "Row": "J",
                "Col": "3",
                "Status": "B", "selected": false
            },
            {
                "Row": "J",
                "Col": "4",
                "Status": "B", "selected": false
            },
            {
                "Row": "J",
                "Col": "5",
                "Status": "UA", "selected": false
            },
            {
                "Row": "J",
                "Col": "6",
                "Status": "UA", "selected": false
            },
            {
                "Row": "J",
                "Col": "7",
                "Status": "B", "selected": false
            },
            {
                "Row": "J",
                "Col": "8",
                "Status": "A", "selected": false
            },
            {
                "Row": "J",
                "Col": "9",
                "Status": "UA", "selected": false
            }
        ]

        for (let i = 0; i < this.Columns; i++) {
            this.pos.push(i);
        }

        let c = 65;
        let seatPad: any = '';

        for (let j = 0; j <= this.Rows; j++) {
            if (j % 26 === 0) {
                c = 65;
                if (j < 25) {
                    seatPad = '';
                } else {
                    seatPad = Number(seatPad) + 1;
                }
            }
            let seat = String.fromCharCode(c) + String(seatPad);
            this.seatHeader.push(seat);
            c = c + 1;
        }
    }

    ionViewDidLoad() { }

    //CLOSE THE MODAL.
    closeModal() {
        this.navCtrl.pop();
    }

    //TO SHOW THE STATUS OF SEATS.
    showSeatStatus(r, c) {
        let objR;
        let objC = [{ 'Status': 'UA' }];
        objR = this.SeatsAlloted.filter(obj => (obj.Row === r));
        objC = objR.filter(obj => (obj.Col == String(c)));
        if (objC.length == 0) {
            objC = [{ 'Status': 'UASpace' }]
        } else {

        }
        return objC[0].Status;
    }

    //TO SELECT A SEAT.
    SelectSeat(r, c) {
        let i = r.charCodeAt(0)
        i = i - 65;
        let incr = this.Columns * i + c
        this.SeatsAlloted[i + incr].selected = !this.SeatsAlloted[i + incr].selected;
        if (this.SeatsAlloted[i + incr].selected == true) {
            this.selectedSeats.push(this.SeatsAlloted[i + incr])
            this.Counter = this.Counter + 1;
        } else {
            this.selectedSeats.pop();
        }
        if (this.selectedSeats.length > 0) {
            this.ButtonClass = true;
        }
    }

    //TO SHOW THE STATUS OF SELECTED SEATS.
    showSeatSelectedStatus(r, c) {
        let i = r.charCodeAt(0)
        i = i - 65;
        let incr = this.Columns * i + c
        return this.SeatsAlloted[i + incr].selected
    }

    //AFTER CLICKING CONFIRM ONCE SEAT SELECTED.
    confirm() {
        if (this.buttonValue == "Join") {
            this.seatPurposeModalShow = false;
            this.closeModal();
            this.navCtrl.setRoot("SchedulingPage", { //IF JOIN
                'join': true
            });
        }
        if (this.buttonValue == "Buy&Join") { //IF BUY AND JOIN
            this.ButtonClass = false;
            this.seatPurposeModalShow = true;
        }
    }

    //NAVIGATING TO CART PAGE.
    goToCart() {
        let obj = {
            purposeSessionCost: this.purposeSelected.purposeSessionCost,
            PurposeName: this.navParams.get('PurposeName'),
            purposeSessionsNumber: this.purposeSelected.purposeSessionsNumber,
            startduratin: "",
            purposeLogo: this.navParams.get('PurposeLogo'),
            type: "buyclass",
        };
        console.log(obj)

        this.storage.get('CARTITEM').then((val) => {
            this.setofferLocally = val;
        }).then(
            () => {
                if ((this.setofferLocally) || (this.setofferLocally = [])) {

                    this.setofferLocally.push(obj);
                    this.storage.set('CARTITEM', this.setofferLocally);
                }
            });

        this.closeModal();
        this.navCtrl.push('CartItemsPage', { 'classFlow': true, 'buyClass': this.purposeSelected });
    }

    //IF JOIN THEN PURPOSE SEAT SELECTION MODAL WILL NOT BE SHOWN.
    gotoSeatModal() {
        this.seatPurposeModalShow = false;
    }

    //MAKE THE STATUS OF SELECTED PURPOSE TO TRUE.
    PurposeSessionData(i) {
        this.purposeSessionsArray[i].status = !this.purposeSessionsArray[i].status;
        if (this.purposeSessionsArray[i].status == true) {
            this.ButtonClass = true;
            this.purposeSelected = this.purposeSessionsArray[i];
            this.storage.set('PURPOSESELECTED--BUY JOIN', this.purposeSessionsArray[i])
        } else {
            this.ButtonClass = false;
            this.storage.remove('PURPOSESELECTED--BUY JOIN');
        }
        this.clearallExcept(i);
    }

    //CLEAR THE REST OF THE SELECTIONS.
    clearallExcept(ex) {
        for (let i = 0; i < this.purposeSessionsArray.length; i++) {
            if (i !== ex) {
                this.purposeSessionsArray[i].status = false;
            }
        }
    }
}
