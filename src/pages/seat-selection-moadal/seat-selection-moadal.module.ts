import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { SeatSelectionMoadalPage } from './seat-selection-moadal';

@NgModule({
  declarations: [
    SeatSelectionMoadalPage
  ],
  imports: [
    IonicPageModule.forChild(SeatSelectionMoadalPage),
    TranslateModule
  ],
})
export class SeatSelectionMoadalPageModule { }
