// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { SeatSelectionMoadalPage } from './seat-selection-moadal';
// import { ClassesFlowApiProvider } from '../../providers/classes-flow-api/classes-flow-api';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// // The describe function is for grouping related specs
// describe('SeatSelectionMoadalPage UI Testing - Seats', () => {
//     let comp: SeatSelectionMoadalPage;
//     let fixture: ComponentFixture<SeatSelectionMoadalPage>;
//     let de: DebugElement;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 SeatSelectionMoadalPage
//             ],
//             imports: [
//                 IonicModule.forRoot(SeatSelectionMoadalPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 ClassesFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(SeatSelectionMoadalPage); /////TESTBED CREATES THE OBJECT OF THE CLASSDETAILS  PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance;   //////CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//     });

//     it("Transparent Div should be present..", async () => {
//         fixture = TestBed.createComponent(SeatSelectionMoadalPage);
//         de = fixture.debugElement.query(By.css(".back")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Seat Content should be present.", async () => {
//         fixture = TestBed.createComponent(SeatSelectionMoadalPage);
//         de = fixture.debugElement.query(By.css(".seat")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Header should be present.", async () => {
//         fixture = TestBed.createComponent(SeatSelectionMoadalPage);
//         de = fixture.debugElement.query(By.css(".Header")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Today Grid should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.TodayGrid')).nativeElement;
//             expect(inputfield.className).toMatch('TodayGrid');
//         });
//     });

//     it("Today Label should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.TodalLabel')).nativeElement;
//             expect(inputfield.className).toMatch('TodalLabel');
//         });
//     });

//     it("Calendar Icon Should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.CalendarIcon')).nativeElement;
//             expect(inputfield.className).toMatch('CalendarIcon');
//         });
//     });

//     it("Seat status should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.SeatStatusRow')).nativeElement;
//             expect(inputfield.className).toMatch('SeatStatusRow');
//         });
//     });

//     it("Available seat status should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.Available')).nativeElement;
//             expect(inputfield.className).toMatch('Available');
//         });
//     });

//     it("Booked seat status should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.Booked')).nativeElement;
//             expect(inputfield.className).toMatch('Booked');
//         });
//     });

//     it("Unavailable seat status should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.UnAvailable')).nativeElement;
//             expect(inputfield.className).toMatch('UnAvailable');
//         });
//     });

//     it("Trainer Pod Row should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.TrainerPodRow')).nativeElement;
//             expect(inputfield.className).toMatch('TrainerPodRow');
//         });
//     });

//     it("Seat Headers should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.SeatHeader')).nativeElement;
//             expect(inputfield.className).toMatch('SeatHeader');
//         });
//     });

//     it("Unavailable space should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.UnAvailableSpace')).nativeElement;
//             expect(inputfield.className).toMatch('UnAvailableSpace');
//         });
//     });

//     it('Avaiable space should be present.', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.AvaiableSeatSpace')).nativeElement;
//             expect(inputfield.className).toMatch('AvaiableSeatSpace');
//         });
//     });

//     it("Seat Label span should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.SeatLabelSpan')).nativeElement;
//             expect(inputfield.className).toMatch('SeatLabelSpan');
//         });
//     });

//     it("Class Image should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.classimage')).nativeElement;
//             expect(inputfield.className).toMatch('classimage');
//         });
//     });

//     it("Class name should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.classname')).nativeElement;
//             expect(inputfield.className).toMatch('classname');
//         });
//     });

//     it("Seat Label should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.labelseat')).nativeElement;
//             expect(inputfield.className).toMatch('labelseat');
//         });
//     });

//     it("Seat Image should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.seatImage')).nativeElement;
//             expect(inputfield.className).toMatch('seatImage');
//         });
//     });

//     it("Seat Number should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.seatnumber')).nativeElement;
//             expect(inputfield.className).toMatch('seatnumber');
//         });
//     });

//     it("Change Seat Option should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.ChangeSeatCol')).nativeElement;
//             expect(inputfield.className).toMatch('ChangeSeatCol');
//         });
//     });

// });

// // The describe function is for grouping related specs
// describe('SeatSelectionMoadalPage UI Testing - Purpose', () => {
//     let comp: SeatSelectionMoadalPage;
//     let fixture: ComponentFixture<SeatSelectionMoadalPage>;
//     let de: DebugElement;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 SeatSelectionMoadalPage
//             ],
//             imports: [
//                 IonicModule.forRoot(SeatSelectionMoadalPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 ClassesFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(SeatSelectionMoadalPage); /////TESTBED CREATES THE OBJECT OF THE CLASSDETAILS  PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance;   //////CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//     });

//     it("Transparent Div should be present..", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.partitionRow')).nativeElement;
//             expect(inputfield.className).toMatch('partitionRow');
//         });
//     });

//     it("Seat Content should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.label1')).nativeElement;
//             expect(inputfield.className).toMatch('label1');
//         });
//     });

//     it("Header should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.package')).nativeElement;
//             expect(inputfield.className).toMatch('package');
//         });
//     });

//     it("Today Grid should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.combinedprices')).nativeElement;
//             expect(inputfield.className).toMatch('combinedprices');
//         });
//     });

//     it("Today Label should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.price')).nativeElement;
//             expect(inputfield.className).toMatch('price');
//         });
//     });

// });

// // The describe function is for grouping related specs
// describe('SeatSelectionMoadalPage UI Testing - Footer', () => {
//     let comp: SeatSelectionMoadalPage;
//     let fixture: ComponentFixture<SeatSelectionMoadalPage>;
//     let de: DebugElement;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 SeatSelectionMoadalPage
//             ],
//             imports: [
//                 IonicModule.forRoot(SeatSelectionMoadalPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 ClassesFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(SeatSelectionMoadalPage); /////TESTBED CREATES THE OBJECT OF THE CLASSDETAILS  PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance;   //////CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//     });

//     it("Footer Buttons should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.footerButtons')).nativeElement;
//             expect(inputfield.className).toMatch('footerButtons');
//         });
//     });

//     it("Footer Buttons row should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.footerRow')).nativeElement;
//             expect(inputfield.className).toMatch('footerRow');
//         });
//     });

//     it("Cancel Button should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.cancel2')).nativeElement;
//             expect(inputfield.className).toMatch('cancel2');
//         });
//     });

//     it("Confirm Button should be present.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.confirm')).nativeElement;
//             expect(inputfield.className).toMatch('confirm');
//         });
//     });

// });

// // The describe function is for grouping related specs
// describe('SeatSelectionMoadalPage UI Testing - Methods', () => {
//     let comp: SeatSelectionMoadalPage;
//     let fixture: ComponentFixture<SeatSelectionMoadalPage>;
//     let de: DebugElement;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 SeatSelectionMoadalPage
//             ],
//             imports: [
//                 IonicModule.forRoot(SeatSelectionMoadalPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 ClassesFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(SeatSelectionMoadalPage); /////TESTBED CREATES THE OBJECT OF THE CLASSDETAILS  PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance;   //////CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//     });

//     it('Close Modal Method should be present.', () => {
//         expect(comp.closeModal).toBeDefined();
//     });

//     it('showSeatStatus Method should be present.', () => {
//         expect(comp.showSeatStatus).toBeDefined();
//     });

//     it('SelectSeat Method should be present.', () => {
//         expect(comp.SelectSeat).toBeDefined();
//     });

//     it('showSeatSelectedStatus Method should be present.', () => {
//         expect(comp.showSeatSelectedStatus).toBeDefined();
//     });

//     it('confirm Method should be present.', () => {
//         expect(comp.confirm).toBeDefined();
//     });

//     it('goToCart Method should be present.', () => {
//         expect(comp.goToCart).toBeDefined();
//     });

//     it('gotoSeatModal Method should be present.', () => {
//         expect(comp.gotoSeatModal).toBeDefined();
//     });

//     it('PurposeSessionData Method should be present.', () => {
//         expect(comp.PurposeSessionData).toBeDefined();
//     });

//     it('clearallExcept Method should be present.', () => {
//         expect(comp.clearallExcept).toBeDefined();
//     });

// });