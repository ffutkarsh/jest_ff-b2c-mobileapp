import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CancelFacilityModalPage } from './cancel-facility-modal';

@NgModule({
  declarations: [
    CancelFacilityModalPage,
  ],
  imports: [
    IonicPageModule.forChild(CancelFacilityModalPage),
  ],
})
export class CancelFacilityModalPageModule {}
