import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, App } from 'ionic-angular';
import { DashboardPage } from '../../pages/pages';

@IonicPage()
@Component({
  selector: 'page-cancel-facility-modal',
  templateUrl: 'cancel-facility-modal.html',
})
export class CancelFacilityModalPage {

  public selectreason = ['Going for Medical Emergency', 'Other'];
  public selectednow = 'Going for Medical Emergency';
  public comment;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewController: ViewController,
    private app: App) { }

  ionViewDidLoad() { }

  onSubmit() {
    let reasonandcomment = {
      reason: this.selectednow,
      comment: this.comment
    };
    this.viewController.dismiss(reasonandcomment);
    this.app.getRootNav().setRoot(DashboardPage);
  }

}
