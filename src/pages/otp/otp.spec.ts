// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { OtpPage } from './otp';
// import { Events, IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { OtpHelperProvider } from '../../providers/otp-helper/otp-helper';
// import { Http, ConnectionBackend } from '@angular/http';
// import { IonicStorageModule } from '@ionic/storage';
// import { AndroidPermissions } from '@ionic-native/android-permissions';
// import { HttpModule } from '@angular/http';
// import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
// import { DashboardFlowAuthProvider } from '../../providers/dashboard-flow-auth/dashboard-flow-auth';
// import { CenterSelectAuthProvider } from '../../providers/center-select-auth/center-select-auth';
// import { HttpClientModule } from '@angular/common/http';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { OpenNativeSettings } from '@ionic-native/open-native-settings';
// import { NetworkProvider } from '../../providers/network/network';
// import { Network } from '@ionic-native/network';
// import { LoginFlowApiProvider } from '../../providers/login-flow-api/login-flow-api';

// class NavParamsMock {
//     public get(key): any {
//         if (key === 'combinedata') {
//             return {
//                 combinedata: {
//                     'domain': '',
//                     'phone': '',
//                     'otp': ''
//                 }
//             }
//         }
//     }
// }

// describe('OTP Page - UI Part', () => {
//     let de: DebugElement;
//     let comp: OtpPage;
//     let fixture: ComponentFixture<OtpPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [OtpPage],
//             imports: [
//                 IonicModule.forRoot(OtpPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 AndroidPermissions,
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 Events,
//                 OtpHelperProvider,
//                 CenterSelectAuthProvider,
//                 AuthServiceProvider,
//                 DashboardFlowAuthProvider,
//                 TranslateService,
//                 TranslateStore,
//                 OpenNativeSettings,
//                 NetworkProvider,
//                 Network,
//                 LoginFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(OtpPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Page component should be created.", () => expect(comp).toBeDefined());

//     it("Ion content should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".MainContent")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section grid should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".HeadingGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section row should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".HeadingGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section column should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".HeadingGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section grid should contain the row for FitnessForce Logo.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".FFLogoRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("FitnessForce Logo should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".FFLogo")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("FitnessForce Logo source should match.", async () => {
//         const imagefield: HTMLInputElement = fixture.debugElement.query(By.css('.FFLogo')).nativeElement;
//         expect(imagefield.getAttribute('src')).toMatch('assets/img/SignupLogo.png');
//     });

//     it("Heading Section grid should contain the “Welcome” text.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".WelcomeText")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Progress Indicator grid should be present.'", () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".ProgressGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Progress Indicator row should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".ProgressGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Progress Indicator column should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".ProgressGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Progress Indicator should be present.", () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".ProgressIndicator")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Details grid should be present.'", () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".ClientDetailsGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Details row should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".ClientDetailsGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Photo column should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".ClientPhotoColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Photo row should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".ClientPhotoRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Profile Photo avatar should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".ClientProfilePhotoAvatar")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Profile Photo should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".ClientProfilePhoto")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Name and Contact column should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".ClientNameContactColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Name should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".ClientName")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Contact should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".ClientContact")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("OTP grid should be present.'", () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".OTPGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("OTP row should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".OTPGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("OTP column should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".OTPGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("OTP label 1 row should be present.", () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".OTPLabel1Row")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("OTP label 1 para should be present.", () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".para")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("OTP label 2 row should be present.", () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".OTPLabel2Row")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("OTP label 2 para should be present.", () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".para")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("OTP row should be present.", () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".OTPRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("OTP col1 should be present.", () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".OTPCol1")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("OTP col2 should be present.", () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".OTPCol2")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("OTP col3 should be present.", () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".OTPCol3")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("OTP col4 should be present.", () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".OTPCol4")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("OTP col5 should be present.", () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".OTPCol5")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("OTP col6 should be present.", () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".OTPCol6")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it('Checking OTP input classname', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.otplabel1')).nativeElement;
//         console.log(inputfield.className);
//         expect(inputfield.className).toMatch('otplabel1');
//     });

//     it('checking input field placeholder', () => {
//         const inputfieldplaceholder: HTMLInputElement = fixture.debugElement.query(By.css('.otplabel1')).nativeElement;
//         console.log(inputfieldplaceholder.getAttribute('placeholder'));
//         expect(inputfieldplaceholder.getAttribute('placeholder')).toMatch('');
//     });

//     it("Max Length for OTP input 1", async () => {
//         let fixture = TestBed.createComponent(OtpPage);
//         let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel1");
//         expect(element1.getAttribute("maxlength")).toContain("1");
//     });

//     it("Max Length for OTP input 2", async () => {
//         let fixture = TestBed.createComponent(OtpPage);
//         let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel2");
//         expect(element1.getAttribute("maxlength")).toContain("1");
//     });

//     it("Max Length for OTP input 3", async () => {
//         let fixture = TestBed.createComponent(OtpPage);
//         let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel3");
//         expect(element1.getAttribute("maxlength")).toContain("1");
//     });

//     it("Max Length for OTP input 4", async () => {
//         let fixture = TestBed.createComponent(OtpPage);
//         let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel4");
//         expect(element1.getAttribute("maxlength")).toContain("1");
//     });

//     it("Max Length for OTP input 5", async () => {
//         let fixture = TestBed.createComponent(OtpPage);
//         let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel5");
//         expect(element1.getAttribute("maxlength")).toContain("1");
//     });

//     it("Max Length for OTP input 6", async () => {
//         let fixture = TestBed.createComponent(OtpPage);
//         let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel6");
//         expect(element1.getAttribute("maxlength")).toContain("1");
//     });

//     it("Continue Button grid should be present.", () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".ContinueGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Continue Button row should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".ContinueGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Continue Button column should be present.", async () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".ContinueGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Continue Button should be present.", () => {
//         fixture = TestBed.createComponent(OtpPage);
//         de = fixture.debugElement.query(By.css(".ContinueButton")).nativeElement;
//         expect(de).toBeDefined();
//     });
// });

// describe('OTP Page - Methods and Logic Part', () => {
//     let component: OtpPage;
//     let fixture: ComponentFixture<OtpPage>;
//     let otpService: OtpHelperProvider;
//     let timerCallback: any;
//     let setFocus: any;
//     let authService: AuthServiceProvider;
//     let loginFlowApi: LoginFlowApiProvider;

//     beforeEach(() => {
//         this.receivedPhoneNo = "2134323";
//         TestBed.configureTestingModule({
//             declarations: [OtpPage],
//             imports: [
//                 IonicModule.forRoot(OtpPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 AndroidPermissions,
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 Events,
//                 OtpHelperProvider,
//                 CenterSelectAuthProvider,
//                 AuthServiceProvider,
//                 DashboardFlowAuthProvider,
//                 TranslateService,
//                 TranslateStore,
//                 OpenNativeSettings,
//                 NetworkProvider,
//                 Network,
//                 LoginFlowApiProvider
//             ]
//         });
//     });

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(OtpPage);

//         // get test component from the fixture
//         component = fixture.componentInstance;

//         // UserService provided to the TestBed
//         // authService = TestBed.get(OtpHelperProvider);
//         otpService = fixture.debugElement.injector.get(OtpHelperProvider);
//         authService = fixture.debugElement.injector.get(AuthServiceProvider);
//         loginFlowApi = fixture.debugElement.injector.get(LoginFlowApiProvider); //INJECTING PROVIDER.
//     });

//     afterEach(() => {
//         otpService = null;
//     });

//     it('Instance of LoginFlowApiProvider created', () => {
//         expect(loginFlowApi instanceof LoginFlowApiProvider).toBe(true);
//     });

//     it('OTP Help Provider should be created', () => {
//         expect(otpService instanceof OtpHelperProvider).toBe(true);
//     });

//     it('Otp should Concat ', () => {
//         component.otp11 = 1; component.otp12 = 2; component.otp13 = 3; component.otp14 = 4; component.otp15 = 5; component.otp16 = 6; // Mimicking User input
//         expect(component.otpconcat()).toContain('123456');
//     });

//     it("Focus on a field test", () => {
//         component.saveFocus(0);
//         expect(component.focused == 0).toBe(true);
//     });

//     // edited by sayli 
//     it("set Focus on", () => {
//         let component = fixture.componentInstance;
//         expect(document.activeElement).toBeTruthy();
//         console.log("testing" + document.activeElement)
//     });

//     //try to check focused element
//     it('focuses the element on mount', () => {
//         const elem: HTMLElement = fixture.debugElement.query(By.css('.otplabel1')).nativeElement;
//         expect(elem.focus).toBeTruthy();
//     }); //It fails on toBeFalsy

//     it("Focus Forward on input", () => {
//         component.saveFocus(3);
//         console.log(component.focused);
//         expect(component.focused).toMatch("3");
//     });

//     it("Focus Backward on backspace or Delete", () => {
//         component.saveFocus(3);
//         console.log(component.focused);
//         component.saveFocus(2);
//         console.log(component.focused);
//         expect(component.focused).toMatch("2");
//     });

//     it("Enable Button on all four OTP Fields Filled", () => {
//         component.otp1 = 5; component.otp1 = 5; component.otp1 = 5; component.otp1 = 5; // Mimicking User input
//         expect(component.isButtondisabled == 0).toBe(false);
//     });

//     it("Check if input is number only", () => {
//         component.otp11 = 5; component.otp11 = 5; component.otp11 = 5; component.otp11 = 5; // Mimicking User input
//         expect(component.checkifNo()).toBe(true);
//     });

//     it("OTP Resend Button Disabled by Default", () => {
//         expect(component.resendState).toBe(true);
//     }); // Checking is button Disabled

//     it("OTP Resend Button Enabled after timeout", () => {
//         component.turnOnResend(); // On Timer Expire Action
//         expect(component.resendState).toBe(false);
//     }); // Checking is button Enabled

//     it("OTP ReadListSMS", () => {
//         expect(component.ReadListSMS).toBeTruthy();
//     });

//     it("Make OTP Field empty if non number input", () => {
//         component.otp1 = 'a'// Mimicking User input
//         let a = component.otp1.slice(-1);
//         if (!component.checkNo(a)) {
//             component.otp1 = "";
//         }
//         expect(component.otp1).toBe("");
//     });

//     it("Check if auth service provider is present", () => {
//         expect(authService instanceof AuthServiceProvider).toBe(true);
//     });

// });



