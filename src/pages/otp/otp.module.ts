import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OtpPage } from './otp';
import { TranslateModule } from '@ngx-translate/core';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    OtpPage
  ],
  imports: [
    IonicPageModule.forChild(OtpPage),
    TranslateModule
  ],
  providers: [
    AndroidPermissions,
    HttpModule
  ]
})
export class OtpPageModule { }
