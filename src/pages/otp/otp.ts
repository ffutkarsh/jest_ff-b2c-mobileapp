import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Platform, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { OtpHelperProvider } from '../../providers/otp-helper/otp-helper';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { DashboardFlowAuthProvider } from '../../providers/dashboard-flow-auth/dashboard-flow-auth';
import { CenterSelectAuthProvider } from '../../providers/center-select-auth/center-select-auth';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import rootReducer from '../../reducers/LoginFlow';
import * as a from '../../actions/LoginAction';
import { MenuController } from 'ionic-angular';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { MethodCall } from '@angular/compiler';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { DashboardPage } from '../../pages/pages';
import { NetworkProvider } from '../../providers/network/network';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { Subject } from 'rxjs/Subject';
import { LoginFlowApiProvider } from '../../providers/login-flow-api/login-flow-api';

declare var SMS: any;

@IonicPage()
@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html',
})
export class OtpPage {

  public fetchDialCode //FROM SIGNUPTERMSEULA PAGE.
  public fetchPhoneNumber = "121"; //FROM SIGNUPTERMSEULA PAGE.
  public selectedCenterObject; //FROM SIGNUPTERMSEULA PAGE.
  public otpResponse; //FROM SIGNUPTERMSEULA PAGE.
  public otpVerified; //STORING API RESPONSE OF OTP VERIFIED.

  public memberType; //SET TO REGISTERED MEMBER IF OTP IS VERIFIED.
  public otp; //OTP ENTERED.
  public verificationFlag: boolean; //SET TRUE IF OTP IS VERIFIED.
  public permissions = []; //GIVE PERMISSIONS ACCORDING TO MEMBERTYPE.

  OTPRes; //sms

  public digits: number;// describe no of digits
  private tick: number; //TIMER
  public resendState = true; // Manages the button disable state for
  public timer; // Used to create reverse countdown timer using TimerObservable
  public n; // used to limit cast (OTP - string to number) length
  public otparry; // Used to change type of input for password hide effect
  public phoneLastDigits; // Saves last 4 digits of phone number
  public focused: number; // Used to save index of currently focussed OTP field
  public setFocus: MethodCall; // Used to change focus on OTP input field
  public index; // Stores currently foucus OTP field index
  public isButtondisabled; // Used to save state (Boolean) of the Submit Button Enable

  //creating ViewChild to help with changing focus of inputs
  @ViewChild('op1') myInput1;
  @ViewChild('op2') myInput2;
  @ViewChild('op3') myInput3;
  @ViewChild('op4') myInput4;

  //creating ViewChild to help with changing focus of inputs
  @ViewChild('op11') myInput11;
  @ViewChild('op12') myInput12;
  @ViewChild('op13') myInput13;
  @ViewChild('op14') myInput14;
  @ViewChild('op15') myInput15;
  @ViewChild('op16') myInput16;

  // Used to store 4 digit OTP
  public otp1;
  public otp2;
  public otp3;
  public otp4;

  // Used to store 6 digit OTP
  public otp11;
  public otp12;
  public otp13;
  public otp14;
  public otp15;
  public otp16;

  public alertTitle; //i18n VALUE FOR ALERT TITLE.
  public alertSubTitle; //i18n VALUE FOR ALERT SUB TITLE.
  public alertButton; //i18n VALUE FOR ALERT BUTTON.

  //STRUCTURE OF INITIAL STATE OF USER OBJECT.
  public currentUserState = {
    dialCode: "",
    phoneNumber: "",
    centerName: "",
    subCenterName: "",
    enquiryType: "",
    memberType: "",
    userName: "",
    userPhoto: "",
    otp: "",
    verificationFlag: "",
    permissions: []
  };

  //CHECKING NETWORK ABSENCE.
  public myObservable = new Subject<boolean>();
  public networkAvailable;
  public networkAlert;

  constructor(public navCtrl: NavController,
  
    public dashboardFlowAuthService: DashboardFlowAuthProvider,
    private toastCtrl: ToastController,
    public authService: AuthServiceProvider,
    public centerAuthService: CenterSelectAuthProvider,
    private otpHelper: OtpHelperProvider,
    public platform: Platform,
    public androidPermissions: AndroidPermissions,
    public http: Http,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    private storage: Storage,
    public translateService: TranslateService,
    private openNativeSettings: OpenNativeSettings,
    public events: Events,
    public networkProvider: NetworkProvider,
    private loginFlowApi: LoginFlowApiProvider) {

    this.menuCtrl.enable(false, 'myMenu'); //DISABLING MENU ON OTP PAGE.


    // this.platform.registerBackButtonAction(() => {
    //   if (this.navCtrl.canGoBack()) { // CHECK IF THE USER IS IN THE ROOT PAGE.
    //     this.navCtrl.pop(); // IF IT'S NOT THE ROOT, POP A PAGE.
    //   } else {
    //     this.platform.exitApp(); // IF IT'S THE ROOT, EXIT THE APP.
    //   }
    // });


    //sms
    this.ReadListSMS(); // Method used to read incoming OTP
    this.index = 0; // Initially setting 1st OTP field focussed
    this.otp11 = ""; this.otp12 = ""; this.otp13 = ""; this.otp14 = ""; this.otp15 = ""; this.otp16 = "";
    this.otp1 = ""; this.otp2 = ""; this.otp3 = ""; this.otp4 = ""; //Init all fields to be empty
    this.sendd();

    // Used to save input type of OTP fields
    this.otparry = ["tel", "tel", "tel", "tel", "tel", "tel"];
    //setting focus on 1st field in both otp4 and otp6
    this.setFocus = this.myInput1;
    this.setFocus = this.myInput11;

    this.isButtondisabled = true; // Disabling Submit Button

    this.fetchDialCode = navParams.get("defaultDialcode"); //FROM SIGN UP TERMS EULA PAGE.
    this.fetchPhoneNumber = navParams.get("phoneNumber"); //FROM SIGN UP TERMS EULA PAGE.
    this.selectedCenterObject = this.navParams.get('memberDetails'); //FROM SIGN UP TERMS EULA PAGE.
    console.log("Member Details on OTP Page");
    console.log(this.selectedCenterObject);
    this.otpResponse = this.navParams.get('otpResponse'); //FROM SIGN UP TERMS EULA PAGE.
    console.log("Otp Response on OTP Page");
    console.log(this.otpResponse);

    //CHECKING NETWORK ABSENCE.
    this.networkProvider.initializeNetworkEvents();//check network condition
    this.networkChangesSubscribe();
    this.networkAvailable = (this.networkProvider.isConnected() ? true : false);
    this.myObservable.next(this.networkAvailable);
    this.myObservable.subscribe(val => {
      if (!val) {
        this.showNetworkAlert();
      }
    });

  }

  ionViewDidLoad() {

    this.storage.get('USERDETAILS').then((USERDETAILS) => { //FETCHING THE CURRENT USER STATE.
      this.currentUserState = USERDETAILS;
    });

    this.phoneLastDigits = this.fetchPhoneNumber.substr(this.fetchPhoneNumber.length - 4);
    console.log("TrimNumber");
    console.log(this.phoneLastDigits);

    this.resendState = true; //By default resend button disabled
    this.startTimer(10); // Setting 10 sec reverse count down
    this.setFocusOn(this.myInput1); // First field focussed initially
    console.log("myInput1" + this.myInput1.values);

    //this is hardcoded code.
    //to test for 4 change this.dgit to 4
    //edit this number for otp 4 and otp 6
    this.digits = 6;
    if (this.digits == 6) {
      this.setFocusOn(this.myInput11);
      // focus on 1st OTP Field on view load
    }
    else if (this.digits == 4) {
      this.setFocusOn4(this.myInput1); // focus on 1st OTP Field on view load
    }

  }

  //Method used to unsubscribe to listeners before changing page
  ionViewDidLeave() {
    this.networkChangesUnsubscribe(); //UNSUBSCRIBE THE CHANGES.
    if (SMS) SMS.stopWatch(() => {
      console.log('watching stopped');
    }, Error => {
      console.log('failed to stop watching');
    });
  }

  //Changing State of Resend Button
  turnOnResend() {
    this.resendState = false;
  }

  //Changing State of Resend Button
  resendOTP() {
    this.resendState = true;
    this.sendd();
  }

  //Method to start reverse timer for 'n' seconds
  startTimer(timeoutt) {
    this.timer = TimerObservable.create(100, 1000).subscribe(t => {
      this.tick = timeoutt - t;
      if (this.tick < 0) {
        this.timer.unsubscribe();
        this.tick = null;
        this.turnOnResend();
      }
    });
  }

  otpconcat() {
    return ("" + this.otp11 + this.otp12 + this.otp13 + this.otp14 + this.otp15 + this.otp16);
  } // Returns the concatinated OTP as String

  //for otp6
  // code to display one grid at a time.
  //if its otp 4  then disable otp 6 and vice versa
  setOtpFour() {
    if (this.digits == 4) {
      this.setOtpSix();
    }
    if (this.digits == 6) {
      let style = {
        'display': 'none'
      };
      return style;
    }
  }

  //if its otp 4  then disable otp 6 and vice versa
  setOtpSix() {
    if (this.digits == 4) {
      let style = {
        'display': 'none'
      };
      return style;
    }
  }

  //for otp6
  // Used to automatically move to next field (Focus Shift)
  switchType(event, id) {
    this.checkSubEnable();
   
    if (event.keyCode == 8 || event.keyCode == 46) {
      this.backwards(id);
    } else {
      if (id == 0) {
        if (this.otp11.length != 0) {
          let a = this.otp11.slice(-1);
          if (!this.checkNo(a)) {
            this.otp11 = "";
          }
          else {
            this.setFocusOn(1);
          }
        }
      }

      if (id == 1) {
        if (this.otp12.length != 0) {
          let a = this.otp12.slice(-1);
          if (!this.checkNo(a)) {
            this.otp12 = "";
          }
          else {
            this.setFocusOn(2);
          }
        }
        if (this.otp11.length == 0) {
          this.otp12 = "";
          this.setFocusOn(0);
        }
      }

      if (id == 2) {
        if (this.otp13.length != 0) {
          let a = this.otp13.slice(-1);
          if (!this.checkNo(a)) {
            this.otp13 = "";
          }
          else {
            this.setFocusOn(3);
          }
        }
        if (this.otp12.length == 0) {
          this.otp13 = "";
          this.setFocusOn(1);
        }
      }

      if (id == 3) {
        if (this.otp14.length != 0) {
          let a = this.otp14.slice(-1);
          if (!this.checkNo(a)) {
            this.otp14 = "";
          }
          else {
            this.setFocusOn(4);
          }
        }
        if (this.otp13.length == 0) {
          this.otp14 = "";
          this.setFocusOn(2);
        }
      }

      if (id == 4) {
        if (this.otp15.length != 0) {
          let a = this.otp15.slice(-1);
          if (!this.checkNo(a)) {
            this.otp15 = "";
          }
          else {
            this.setFocusOn(5);
          }
        }
        if (this.otp14.length == 0) {
          this.otp15 = "";
          this.setFocusOn(3);
        }
      }

      if (id == 5) {
        if (this.otp16.length != 0) {
          let a = this.otp16.slice(-1);
          if (!this.checkNo(a)) {
            this.otp16 = "";
            this.checkSubEnable();
          }
        }
        if (this.otp15.length == 0) {
          this.otp16 = "";
          this.setFocusOn(4);
        }
      }
    }
  } // Used to automatically switch focus on next input while entering OTP

  //otp-4 Used to automatically switch focus on next input while entering OTP
  switchType4(event, id) {
    this.checkSubEnable4();
    if (event.keyCode == 8 || event.keyCode == 46) {
      this.backwards4(id);
    } else {
      if (id == 0) {
        if (this.otp1.length != 0) {
          let a = this.otp1.slice(-1);
          if (!this.checkNo(a)) {
            this.otp1 = "";
          }
          else {
            this.setFocusOn4(1);
          }
        }
      }

      if (id == 1) {
        if (this.otp2.length != 0) {
          let a = this.otp2.slice(-1);
          if (!this.checkNo(a)) {
            this.otp2 = "";
          }
          else {
            this.setFocusOn4(2);
          }
        }
      }

      if (id == 2) {
        if (this.otp3.length != 0) {
          let a = this.otp3.slice(-1);
          if (!this.checkNo(a)) {
            this.otp3 = "";
          }
          else {
            this.setFocusOn4(3);
          }
        }
      }

      if (id == 3) {
        if (this.otp4.length != 0) {
          let a = this.otp4.slice(-1);
          if (!this.checkNo(a)) {
            this.otp4 = "";
            this.checkSubEnable();
          }
        }
      }
    }
  } // Used to automatically switch focus on next input while entering OTP

  // Method that sets focus based on index (6 digits)
  setFocusOn(i) {
    if (i == 0) {
      this.myInput11.setFocus();
    }
    if (i == 1) {
      this.myInput12.setFocus();
    }
    if (i == 2) {
      this.myInput13.setFocus();
    }
    if (i == 3) {
      this.myInput14.setFocus();
    }
    if (i == 4) {
      this.myInput15.setFocus();
    }
    if (i == 5) {
      this.myInput16.setFocus();
    }
  }

  // Method that sets focus based on index (4 digits)
  setFocusOn4(i) {
    if (i == 0) {
      this.myInput1.setFocus();
    }
    if (i == 1) {
      this.myInput2.setFocus();
    }
    if (i == 2) {
      this.myInput3.setFocus();
    }
    if (i == 3) {
      this.myInput4.setFocus();
    }
  }

  //for otp 6
  // Check input and enable Submit Button
  checkSubEnable() {
    console.log(this.otpconcat());
    if (this.otpconcat().length == 6) {
      this.isButtondisabled = false;
      console.log("length " + this.otpconcat().length)
    }
    else {
      this.isButtondisabled = true;
    }
  }

  //un-comment this for otp 4 :
  checkSubEnable4() {
    console.log(this.otpconcat());
    if (this.otpconcat().length == 4) {
      this.isButtondisabled = false;
    }
    else {
      this.isButtondisabled = true;
    }
  } // Enable the Submit Button if All OTP Fields are filled

  checkifNo() {
    let reg = new RegExp('^\\d+$');
    //return reg.test(""+this.otpconcat);
    if (!reg.test(this.otpconcat())) {
      this.showInvalidAlert();
      return false;
    }
    return true;
  } // Checks if invalid input entered in OTP Fields

  // Method to check if Number only
  checkNo(phoneNumber) {
    let reg = new RegExp('^\\d+$');
    return reg.test(phoneNumber);
  }

  // Alert fot Invalid OTP
  showInvalidAlert() {
    //TRANSLATE SERVICE FOR TITLE.
    this.translateService.get('SORRY').subscribe(
      value => {
        this.alertTitle = value;
      }
    )
    //TRANSLATE SERVICE FOR SUB TITLE.
    this.translateService.get('INVALIDOTP').subscribe(
      value => {
        this.alertSubTitle = value;
      }
    )
    //TRANSLATE SERVICE FOR OK BUTTON.
    this.translateService.get('OK').subscribe(
      value => {
        this.alertButton = value;
      }
    )
    const alert = this.alertCtrl.create({
      title: this.alertTitle,
      subTitle: this.alertSubTitle,
      buttons: [this.alertButton]
    });

    //for otp6
    if (this.digits == 6) {
      this.otp11 = ""; this.otp12 = ""; this.otp13 = ""; this.otp14 = ""; this.otp15 = ""; this.otp16 = "";
    }
    else if (this.digits == 4) {
      this.otp1 = ""; this.otp2 = ""; this.otp3 = ""; this.otp4 = "";
    }

    alert.present();
  }   // Shows Alert 

  public saveFocus(i) {
    this.focused = i;
  } // Used to save Focus 

  // Loader to display while verifying OTP
  presentLoadingText() {
    let loading = this.loadingCtrl.create({
      content: 'Verifying Please Wait...'
    });

    loading.present();

    setTimeout(() => {
      loading.dismiss();
      this.getOtpVerified(this.selectedCenterObject.memberID, this.otpconcat()); //USER LOGIN EVENT.
    }, 2000);
  }  // Shows loader while contacting to server 

  // Used to automatically shift focus backwards in case of invalid of ahead of vacant input field
  backwards(id) {
    if (id == 1) {
      if (this.otp12.length == 0) {
        this.setFocusOn(0);
      }
    }

    if (id == 2) {
      if (this.otp13.length == 0) {
        this.setFocusOn(1);
      }
    }

    if (id == 3) {
      if (this.otp14.length == 0) {
        this.setFocusOn(2);
      }
    }

    if (id == 4) {
      if (this.otp15.length == 0) {
        this.setFocusOn(3);
      }
    }

    if (id == 5) {
      if (this.otp16.length == 0) {
        this.setFocusOn(4);
      }
    }
  }

  //on click of back button erase the otp digit one by one
  backwards4(id) {
    if (id == 1) {
      if (this.otp2.length == 0) {
        this.setFocusOn4(0);
      }
    }

    if (id == 2) {
      if (this.otp3.length == 0) {
        this.setFocusOn4(1);
      }
    }

    if (id == 3) {
      if (this.otp4.length == 0) {
        this.setFocusOn4(2);
      }
    }
  }

  //Sends SMS (Currently through cleint's phone number for Testing)
  sendd() {
    this.platform.ready().then((readySource) => {
      let otp = Math.floor(Math.random() * 999999) + 1;
      if (SMS) SMS.sendSMS("+919869081205", "Hello, Testing your OTP is " + otp + ".This will expire soon.", () => {
        console.log("Sent");
      }, Error => {
        console.log("Error occurs")
      });
    });
  }

  // Method that listens for new SMS and Finds OTP from it.
  ReadListSMS() {
    this.platform.ready().then((readySource) => {
      if (SMS) SMS.startWatch(() => {
        console.log('watching started');
      }, Error => {
        console.log('failed to start watching');
      });
      document.addEventListener('onSMSArrive', (e: any) => {
        var sms = e.data;
        //console.log(sms);

        // let o:string = sms.body+"".match(this.findotp);
        // let found = o.replace("OTP is ","")
        let num = sms.body.replace(/[^0-9]/g, '');
        this.OTPRes = num;

        //alert('OTP is ==> ' + num); // Alert when OTP received
        this.otpToast(num);
        this.autoFillOTP(num);
      });
    });
  }

  // Method used to get permissions
  ionViewWillEnter() {
    // Asking Permission For Android SMS access
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(
      success => console.log('Permission granted'),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS)
    );
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);
  }

  // Show user a a toast on receiving OTP 
  otpToast(num) {
    let toast = this.toastCtrl.create({
      message: 'OTP received successfully --> ' + num,
      duration: 2000,
      position: 'middle'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present(); // Presenting the Toast
  }

  // Auto fill OTP inside OTP fields
  autoFillOTP(num) {
    let a = num.split("", 6);  // Spliting OTP into 6 parts
    // Filling OTP digits on by one
    this.otp11 = a[0];
    this.otp12 = a[1];
    this.otp13 = a[2];
    this.otp14 = a[3];
    this.otp15 = a[4];
    this.otp16 = a[5];
    this.checkSubEnable(); // Enabling Submit button
  }

  //ADDING OTP TO CURRENT USER STATE.
  addOTP() {
    this.currentUserState = rootReducer(this.currentUserState, a.addOTP(this.otpconcat()));
    console.log("Add otp: ");
    console.log(this.currentUserState);
    this.storelocally();
  }

  //ADDING VERIFICATION FLAG TO CURRENT USER STATE.
  addVerificationFlag() {
    this.currentUserState = rootReducer(this.currentUserState, a.addVerificationFlag(this.verificationFlag));
    console.log("Add Verification Flag: ");
    console.log(this.currentUserState);
    this.storelocally();
  }

  //ADDING MEMBERTYPE TO CURRENT USER STATE.
  addMemberType() {
    this.currentUserState = rootReducer(this.currentUserState, a.addMemberType(this.memberType));
    console.log("Add Member Type: ");
    console.log(this.currentUserState);
    this.storelocally();
  }

  //ADDING PERMISSIONS TO CURRENT USER STATE.
  addPermissions() {
    this.currentUserState = rootReducer(this.currentUserState, a.addPermissions(this.permissions));
    console.log("Add Permissions: ");
    console.log(this.currentUserState);
    this.storelocally();
  }

  //SAVES DATA TO LOCAL DATABASE.
  storelocally() {
    this.storage.set('USERDETAILS', this.currentUserState).then((val) => {
      this.storage.get('USERDETAILS').then((val) => {
        console.log(val);
      });
    });
  }

  //SUBSCRIBE TO NETWORK CHANGES.
  networkChangesSubscribe() {
    // Offline event
    this.events.subscribe('network:offline', () => {  //method to check internet
      this.myObservable.next(false);
      this.networkAvailable = false;
    });

    // Online event
    this.events.subscribe('network:online', () => {
      this.myObservable.next(true);
      this.networkAvailable = true;
    });
  }

  //UNSUBSCRIBE TO NETWORK CHANGES.
  networkChangesUnsubscribe() {
    this.events.unsubscribe('network:offline');
    this.events.unsubscribe('network:online');
  }

  //SHOWING ALERT ON NETWORK ABSENCE.
  showNetworkAlert() {
    this.networkAlert = this.alertCtrl.create({
      title: 'No Internet Connection',
      message: 'Please check your internet connection.',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Retry',
          handler: () => {
            if (this.networkAvailable == false) {
              this.showNetworkAlert();
            }
          }
        },
        {
          text: 'Open Settings',
          handler: () => {
            this.networkAlert.dismiss().then(() => {
              this.showSettings();
              if (this.networkAvailable == false) {
                this.showNetworkAlert();
              }
            });
          }
        }
      ]
    });
    this.networkAlert.present();
  }

  //MOVING TO SETTINGS ON CLICKING SHOW SETTINGS.
  showSettings() {
    if ((<any>window).cordova && (<any>window).cordova.plugins.settings) {
      console.log('openNativeSettingsTest is active');
      (<any>window).cordova.plugins.settings.open("settings", function () {
        console.log('opened settings');
      },
        function () {
          console.log('failed to open settings');
        }
      );
    } else {
      console.log('openNativeSettingsTest is not active!');
    }
  }

  //FETCHING OTP VERIFIED FROM API.
  getOtpVerified(memberID, otp) {
    if (memberID == "11223344" && otp == 123456) {
      this.loginFlowApi.getOtpVerified().map((response) => response).subscribe((response) => {
        this.otpVerified = response;
        console.log("Fetched OTP Verified From Api: ");
        console.log(this.otpVerified);
        this.events.publish('User Verified and Logged In', Date.now());
        this.verificationFlag = true; //OTP VERIFIED.
        this.memberType = "Registered Member"; //USER IS A REGISTERED MEMBER.
        //ACCESSIBLE PAGES.
        this.permissions = [
          "DashboardPage",
          "OfferCardDashboardPage",
          "OfferPage",
          "OfferDetailPage",
          "ScheduleCardDashboardPage",
          "ClassesPage",
          "LocationModalPage",
          "ClassDetailsPage",
          "WithdrawModalPage",
          "ClassRatingPage",
          "ClassAttendancePopupPage",
          "SeatSelectionMoadalPage",
          "SchedulingPage",
          "BookedAppointmentPage",
          "MembershipPage",
          "NewMyMembershipsPage",
          "FreezePage",
          "UpgradePage",
          "UpgradeHomeActionModalPage",
          "UpgradeActionModalPage",
          "RenewPage",
          "RenewHomeActionPage",
          "RenewActionModalPage",
          "BuyMainPage",
          "AppointmentModalPage",
          "SpecificorAnyTrainerPage",
          "AnyTrainerPage",
          "Datetime",
          "Repeatbooking",
          "AppointmentBookedPage",
          "CancelAppointmentModalPage",
          "AppointmentAttendancePopupPage",
          "SelectFacilityModalPage",
          "FacilityDateTimePage",
          "FacilityBookedDetailsPage",
          "ProfilePage",
          "ProfileNewPage",
          "TransactionHistoryListPage",
          "PurchaseHistoryPage",
          "MyrefferalPage",
          "MyWalletPage",
          "LogoutconfirmPage",
          "PaymentOptionsPage",
          "PaymentSuccessfulPurchasePage",
          "PaymentFailedPage",
          "CartItemsPage",
          "ApplyCouponModalPage",
          "CartCouponPage",
          "CartWaiverActionModalPage",
          "CartWaiverEsignPage",
          "CartWaiverOtpPage",
          "WaiverSuccessPage",
          "AboutPage",
          "FeedbackPage",
          "FeedbackFlowPage"
        ]
        this.storage.set('loggedIn', true); //USER LOGGED IN.
        this.addOTP(); //ADDING OTP TO CURRENT USER STATE.
        this.addVerificationFlag(); //ADDING VERIFICATION FLAG TO CURRENT USER STATE.
        this.addMemberType(); //ADDING MEMBER TYPE TO CURRENT USER STATE.
        this.addPermissions(); //ADDING PERMISSIONS TO CURRENT USER STATE.
        this.navCtrl.setRoot(DashboardPage, {
          'defaultDialcode': this.fetchDialCode,
          'phoneNumber': this.fetchPhoneNumber,
          'memberDetails': this.selectedCenterObject,
          'otpResponse': this.otpResponse,
          'otpVerified': this.otpVerified,
          'verificationFlag': this.verificationFlag,
          'memberType': this.memberType,
          'permissions': this.permissions
        })
      });
    }
  }

}
