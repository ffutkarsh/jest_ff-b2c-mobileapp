// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { ClassRatingPage } from './class-rating';
// import { FetcherProvider } from "../../providers/fetcher/fetcher"
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// let dummy = {
//     "bad": ["Loreum Ipsum bad", "neuro bad", "Ipsum bad", "porto bad", "Dolar sit bad", "Ipsum bad", "porto bad", "Dolar sit bad"],
//     "poor": ["Loreum poor", "neuro poor", "porto poor", "Dolar poor", "sit poor", "porto poor", "Dolar poor", "sit poor"],
//     "okay": ["Loreum okay", "neuro okay", "porto okay", "Dolar okay", "sit okay", "porto okay", "Dolar okay", "sit okay"],
//     "good": ["Loreum good", "neuro good", "porto good", "Dolar good", "sit good", "porto good", "Dolar good", "sit good"],
//     "great": ["Loreum great", "neuro great", "porto great", "Dolar great", "sit great", "porto great", "Dolar great", "sit great"]
// }

// // The describe function is for grouping related specs
// describe('Class rating Page UI Testing', () => {
//     let comp: ClassRatingPage;
//     let fixture: ComponentFixture<ClassRatingPage>;
//     let de: DebugElement;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 ClassRatingPage
//             ],
//             imports: [
//                 IonicModule.forRoot(ClassRatingPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 FetcherProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(ClassRatingPage); /////TESTBED CREATES THE OBJECT OF THE CLASSDETAILS  PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance;   //////CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//     });

//     it("Content for class rating page should be checked", async () => {
//         fixture = TestBed.createComponent(ClassRatingPage);
//         de = fixture.debugElement.query(By.css(".content")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("grid to be present fortrainers information", async () => {
//         fixture = TestBed.createComponent(ClassRatingPage);
//         de = fixture.debugElement.query(By.css(".gridTrainer")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("column to be present for image of trainer", async () => {
//         fixture = TestBed.createComponent(ClassRatingPage);
//         de = fixture.debugElement.query(By.css(".colimage")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("image to be present for image of trainer", async () => {
//         fixture = TestBed.createComponent(ClassRatingPage);
//         de = fixture.debugElement.query(By.css(".imageTrainer")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("column to be present for trainers information", async () => {
//         fixture = TestBed.createComponent(ClassRatingPage);
//         de = fixture.debugElement.query(By.css(".colinfo")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("label to be present for trainers name", async () => {
//         fixture = TestBed.createComponent(ClassRatingPage);
//         de = fixture.debugElement.query(By.css(".trainerName")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("label to be present for date and time", async () => {
//         fixture = TestBed.createComponent(ClassRatingPage);
//         de = fixture.debugElement.query(By.css(".dateTime")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("avatar to be present for class image", async () => {
//         fixture = TestBed.createComponent(ClassRatingPage);
//         de = fixture.debugElement.query(By.css(".classicon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("label to be present for class Name", async () => {
//         fixture = TestBed.createComponent(ClassRatingPage);
//         de = fixture.debugElement.query(By.css(".className")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("label to be present for class Name", async () => {
//         fixture = TestBed.createComponent(ClassRatingPage);
//         de = fixture.debugElement.query(By.css(".labelrate")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("row to be present for star rating", async () => {
//         fixture = TestBed.createComponent(ClassRatingPage);
//         de = fixture.debugElement.query(By.css(".rowStar")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("column to be present for star rating", async () => {
//         fixture = TestBed.createComponent(ClassRatingPage);
//         de = fixture.debugElement.query(By.css(".colstar")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("row to be present for partition line", async () => {
//         fixture = TestBed.createComponent(ClassRatingPage);
//         de = fixture.debugElement.query(By.css(".partitionRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("grid to be present for Button", async () => {
//         fixture = TestBed.createComponent(ClassRatingPage);
//         de = fixture.debugElement.query(By.css(".className")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("row to be present for Button", async () => {
//         fixture = TestBed.createComponent(ClassRatingPage);
//         de = fixture.debugElement.query(By.css(".rowButton")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("col to be present for Button", async () => {
//         fixture = TestBed.createComponent(ClassRatingPage);
//         de = fixture.debugElement.query(By.css(".colbutton")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("button should be checked", async () => {
//         fixture = TestBed.createComponent(ClassRatingPage);
//         de = fixture.debugElement.query(By.css(".continueButton")).nativeElement;
//         expect(de).toBeDefined();
//     });

// });

// describe('Class rating Page Logic Testing', () => {
//     let comp: ClassRatingPage;
//     let fixture: ComponentFixture<ClassRatingPage>;
//     let de: DebugElement;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 ClassRatingPage
//             ],
//             imports: [
//                 IonicModule.forRoot(ClassRatingPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 FetcherProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(ClassRatingPage); /////TESTBED CREATES THE OBJECT OF THE CLASSDETAILS  PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance;   //////CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//     });

//     it('Star Rating Render Function - 1', () => {
//         expect(comp.starRating(2)).toContain('fulle.svg');
//     });

//     it('Star Rating Render Function - 2', () => {
//         expect(comp.starRating(3.5)).toContain('halfe.svg');
//     });

//     it('Star Rating Render Function - 3', () => {
//         expect(comp.starRating(5)).not.toContain('halfe.svg');
//     });

//     it('Star Rating Render Function - 4', () => {
//         expect(comp.starRating(100)).not.toContain('fullgrey.svg');
//     });

//     it('Star Rating Click Event', () => {
//         expect(comp.rate).toBeDefined();
//     });

//     it('Method to fetch tags', () => {
//         expect(comp.getTags).toBeDefined();
//     });

//     it('Method to that returns tags as per rating', () => {
//         comp.allTags = dummy; // Setting Dummy json
//         expect(comp.tagSelect(3.5).length != 0).toBe(true);
//     });

//     it('Array Shuffling Method', () => {
//         let r = comp.shuffleArray(dummy.bad);
//     });

//     it('Method to create a field to bind ngclass (button status)', () => {
//         let r = comp.selectableButtons(dummy);
//         expect(r[0].clicked).toBe(false);
//     });

//     it('Method to track the button clicked', () => {
//         comp.tagsToShow = [{ name: "Loreum Ipsum bad", clicked: false }, { name: "neuro bad", clicked: false }];
//         comp.ClickTag(1);
//         expect(comp.tagsToShow[1].clicked).toBe(true);
//     });

//     it('Method to Submit rating and tags', () => {
//         comp.tagsToShow = [{ name: "Loreum Ipsum bad", clicked: false }, { name: "neuro bad", clicked: false }];
//         comp.ClickTag(1);
//         let res = comp.submit();
//         expect(res.tags.length).not.toBe(0);
//     });

// });