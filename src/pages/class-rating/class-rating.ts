import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { DashboardPage } from '../../pages/pages';
import { FetcherProvider } from '../../providers/fetcher/fetcher';

@IonicPage()
@Component({
  selector: 'page-class-rating',
  templateUrl: 'class-rating.html',
})
export class ClassRatingPage {

  public classrating = {
    Guid: "",
    XrefID: "",
    ClassId: "",
    CenterAddress: "",
    UserId: "",
    UserName: "",
    UserImage: "",
    PurposeId: "",
    PurposeName: "",
    PurposeLogo: "",
    Date: "",
    StartTime: "",
    DurationEndtime: ""
  }; //TO STORE THE RESPONSE OF CLASS RATING.

  public RATING; //CLASS RATING
  public tagsToShow; //TAGS TO SHOW AS PER RATING
  public allTags; //TAGS API RESPONSE
  public tagname; //TAG NAME
  public trainer; //TRAINER NAME
  public class; //CLASS NAME

  constructor(
    public translateService: TranslateService,
    private modalController: ModalController,
    public fetcher: FetcherProvider,
    public navCtrl: NavController,
    public navParams: NavParams) {

    this.classrating = {
      Guid: "1",
      XrefID: "123",
      ClassId: "1",
      CenterAddress: "Marol, Andheri(East), Mumbai, Maharashtra",
      UserId: "1",
      UserName: "Elton David",
      UserImage: "sarah-avatar.png.jpeg",
      PurposeId: "1",
      PurposeName: "Zumba",
      PurposeLogo: "zmb.png",
      Date: "16 Jan 2019",
      StartTime: "03:00 PM",
      DurationEndtime: "1.30 h"
    }; //TO STORE THE RESPONSE OF CLASS ATTENDANCE.

    this.RATING = 2; // Will be passed from the notification bar component
    this.getTags(); // Fetches for all tags

  }

  // Draws star rating as per the number (rating) passed
  starRating(rating: number) {
    let full;
    if (rating <= 5) {
      full = Math.floor(rating);
    }
    else {
      full = 5;
    }
    let status = [];
    for (let i = 0; i < full; i++) {
      status.push("fulle.svg"); // saves Full Star svg in array
    }
    if (rating - full == 0.5 && !(rating >= 5)) {
      status.push("halfe.svg"); // saves Half Star svg in array
    }
    if (status.length < 5) {
      let emp = 5 - status.length;
      for (let i = 0; i < emp; i++) {
        status.push("fullgrey.svg");// saves Empty Star svg in array
      }
    }
    return status; // Returns array so that it can be rendered using *ngFor
  }

  // Method used to change rating bar value , Takes inpur click event
  rate(e) {
    // Uses offsetX to check if click if on first or second half of the star
    if (Number(e.offsetX) > 22.5) {
      this.RATING = Number(e.srcElement.id) + 1; // Sets last Star as Full star
      this.tagsToShow = this.selectableButtons(this.tagSelect(this.RATING)); // Saving tags with additional atribute to track if button is selected
      this.shuffleArray(this.tagsToShow); // Shuffles the tags
    }
    else {
      this.RATING = Number(e.srcElement.id) + 0.5; // Sets last Star as Half star
      this.tagsToShow = this.selectableButtons(this.tagSelect(this.RATING));// Saving tags with additional atribute to track if button is selected
      this.shuffleArray(this.tagsToShow); // Shuffles the tags
    }
  }

  // Method yo fetch all tags
  getTags() {
    this.fetcher.getRatingTags().map((response) => response.json()).subscribe((response) => {
      this.allTags = response;
      this.tagsToShow = this.selectableButtons(this.tagSelect(this.RATING)); // Saving tags with additional atribute to track if button is selected
      this.shuffleArray(this.tagsToShow); // Shuffles the tags
    });
  }

  // Takes input rating and returns sets Type and returns list of tags
  tagSelect(rating) {
    if (rating > 0 && rating <= 1) {
      this.tagname = "BAD";
      return this.allTags.bad;
    }
    else
      if (rating > 1 && rating <= 2) {
        this.tagname = "POOR";
        return this.allTags.poor;
      }
      else
        if (rating > 2 && rating <= 3) {
          this.tagname = "OKAY";
          return this.allTags.okay;
        }
        else
          if (rating > 3 && rating <= 4) {
            this.tagname = "GOOD";
            return this.allTags.good;
          }
          else
            if (rating > 4) {
              this.tagname = "GREAT";
              return this.allTags.great;
            }
  }

  // Shuffles the tags , takes input array and returns toggled array
  shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
  }

  // Saving tags with additional atribute to track if button is selected
  selectableButtons(array) {
    let arrayWithStatus = [];
    for (let a in array) {
      arrayWithStatus.push({ name: array[a], clicked: false }); // Setting click state to false by default
    }
    return arrayWithStatus;
  }

  // Takes input index of the tags, use to toggle style of buttons
  ClickTag(i) {
    this.tagsToShow[i].clicked = !this.tagsToShow[i].clicked;
  }

  // Submit button returns an array of selected tags
  submit() {
    let selectedTags = [];
    this.tagsToShow.map(t => {
      if (t.clicked == true)
        selectedTags.push(t.name);
    });
    this.navCtrl.setRoot(DashboardPage);
    return {
      rating: this.RATING,
      tags: selectedTags
    }
  }

}
