import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ClassRatingPage } from './class-rating';
import { FetcherProvider } from "../../providers/fetcher/fetcher"
import { MomentModule } from 'angular2-moment';

@NgModule({
  declarations: [
    ClassRatingPage
  ],
  imports: [
    IonicPageModule.forChild(ClassRatingPage),
    TranslateModule,
    MomentModule,
  ],
  providers: [
    FetcherProvider
  ]
})
export class ClassRatingPageModule { }
