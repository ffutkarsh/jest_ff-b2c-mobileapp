import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as moment from 'moment';
import { NewProfileDetailsProvider } from '../../providers/new-profile-details/new-profile-details';
import { NewProfListProvider } from '../../providers/new-prof-list/new-prof-list';

/** 
 * Generated class for the NewProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-profile',
  templateUrl: 'new-profile.html',
})
export class NewProfilePage {
//binding from html
dummyResponse =
{
  
  "userImg" : "", 
  "userName" : "", 
  "userStatus" : "", 
  "active" : "",
  "Birthdate" : "",
  "amount":"",
"wallet": ""
};

allItems = [];
clientname: any; //var to store name
now: any; //var for finding current date time
fulldate: any;
userImg; //var to store userImage
payIcon; //pay icon 
checkIn; //checkin icon

  constructor(private itemList: NewProfListProvider,private newProfProvider: NewProfileDetailsProvider,public navCtrl: NavController, public navParams: NavParams) {

     //BINDING-- variables to static values
     this.now = moment(); //finding current date and time
     this.clientname = "Julian Marcos";
     this.fulldate = moment(this.now).format('03 12 1996');
     this.userImg = "assets/img/pro-pic.png"
     this.payIcon = "assets/img/moneybag.svg";
     this.checkIn = "assets/img/wallet.svg";

    this.getNewProfileDetails() ;
    this.getItemList();
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewProfilePage');
    this.getNewProfileDetails() ;
    this.getItemList();
  }


 //from json using its provider
 getNewProfileDetails() {
  this.newProfProvider.getNewProfileDetails().map((response) => response.json()).subscribe((response) => {
    this.dummyResponse = response;

    console.log(this.dummyResponse)
  });
}

 //from json using its provider
 getItemList() {
  this.itemList.getProfList().map((response) => response.json()).subscribe((response) => {
    this.allItems = response;

    console.log(this.allItems)
  });
}



}
