import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatWindowPage } from './chat-window';
import { MomentModule } from 'angular2-moment';
@NgModule({
  declarations: [
    ChatWindowPage,
  ],
  imports: [
    IonicPageModule.forChild(ChatWindowPage),MomentModule
  ],
})
export class ChatWindowPageModule {}
