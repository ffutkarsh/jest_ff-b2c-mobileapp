import { Component ,ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams,Events, Content  } from 'ionic-angular';
import { Socket } from 'ng-socket-io';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';



let sentmsgs;

/**
 * Generated class for the ChatWindowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat-window',
  templateUrl: 'chat-window.html',
})
export class ChatWindowPage {
  toUser ;
  userId;
  msgnlist;
  msgList;
  user;
  editorMsg;
  mydetails =
  {"userId":"140000198202211138",
  "userName":"utkarsh"};

  @ViewChild(Content) content: Content;
  constructor(public navCtrl: NavController, public navParams: NavParams,private socket: Socket) {
    sentmsgs = [];
   
    this.socket.connect();  // Establishing a socket connection
    this.socket.emit('setusername', this.mydetails);
    //this.msgs.push("Msgs appear below : ");

  // Method to listen received socket msg
  this.getMessages().subscribe(message => {
    // Update 
     // this.msgs.push(message.content);
     console.log("frm dsfds edf sdf  sfr",JSON.stringify(message));
     this.recMsg(message);
    // this.recMsg(message);
this.sendMsg()
    
    });



this.msgnlist = this.navParams.get('msgList');
this.editorMsg='';
console.log(this.msgnlist,"this is the one and only")
    this.toUser = {
      id: "210000198410281948",
      name: this.msgnlist[0].toUserName,
     
    };
    this.userId = {
      id:  "140000198202211138", //Self User ID
      name: this.msgnlist[0].userName,
    }
  
  
  }


  fromNow( msg ){
 console.log( moment(msg))
    return moment(msg).fromNow();
      }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatWindowPage');
  }

  pushNewMsg(msg) {
    const userId = this.userId.id,
      toUserId = this.toUser.id;
    // Verify user relationships
    if (msg.userId === userId && msg.toUserId === toUserId) {
      this.msgnlist.push(msg);
     // this.msgList.push(msg);
    } else if (msg.toUserId === userId && msg.userId === toUserId) {
      this.msgnlist.push(msg);
     // this.msgList.push(msg);
    }
    this.scrollToBottom();
  }



  onFocus() {

    this.content.resize();
    this.scrollToBottom();
  }


  scrollToBottom() {
    setTimeout(() => {
      if (this.content.scrollToBottom) {
        this.content.scrollToBottom();
      }
    }, 400)

}




recMsg(msg) {
 // if (!msg.message) return;
console.log('kjgdkasd')
 console.log(msg);



 let newMsg = {

  messageId:msg.messageId,
  userId: msg.userId,
  userName: msg.userName,
  toUserId:msg.toUserId,
  time: msg.time,
  message:msg.message,
  status: msg.Status

};
  
  this.pushNewMsg(newMsg);


  this.content.resize();
  this.scrollToBottom();

}


sendMsg() {
  if (!this.editorMsg.trim()) return;


  const id = Date.now().toString();
  let newMsg = {
    messageId: Date.now().toString(),
    userId: this.userId.id,
    userName: this.msgnlist[0].userName,
    toUserId: this.toUser.id,
    time: Date.now(),
    message: this.editorMsg,
    status: 'pending'
  };

  this.pushNewMsg(newMsg);
  this.editorMsg = '';



this.sendmsggg( this.toUser.id,"nikhil", newMsg.message);


}

ionViewDidLeave()
{
  this.socket.disconnect(); // Disconnection socket connection on leaving the page 
}


// Method to listen for socket's 'add-message' message
// Continously listen for new messages
getMessages() {
  let observable = new Observable(observer => {
    this.socket.on('add-message', (data) => {
     // console.log(data);
     
      observer.next(data);
    });

  })
  return observable;
}



sendmsggg(toUserID,toUserName,msgString)
{

 
  let mid = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15); // Creating Message ID using random no.s

  sentmsgs.push({username : toUserID , msg : msgString ,msgid : mid , ack :false});
 // Emit new private msg via socket
 this.socket.emit('private-message', { 
  messageId: Date.now().toString(),
 "toUserId": toUserID,
 "toUserName" : toUserName,
 "message" : msgString ,
 "displayName" : toUserName,
 "msgid"   : mid,
 time: Date.now(),
 userId : this.mydetails.userId,
 userName : this.mydetails.userName

 },

//  messageId: Date.now().toString(),
//  userId: this.userId.id,
//  userName: this.msgnlist[0].userName,
//  toUserId: this.toUser.id,
//  time: Date.now(),
//  message: this.editorMsg,
//  status: 'pending'



 function(responseData){
   console.log('Callback called with msgid :'+ responseData);
  //Call Back method to receive acknowledgement
  // Changing received flag once we received
   for(let i = 0; i< sentmsgs.length ; i++)
   {
     if(sentmsgs[i].msgid == responseData)
     {
       sentmsgs[i].ack = true; // Changing message 
     }
   }

   }
);


//console.log("for ackkk",sentmsgs);

}

}
