import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { SelectTrainerProvider } from '../../providers/select-trainer/select-trainer-api';
import { ScheduleAuthProvider } from '../../providers/schedule-auth/schedule-auth';
import { TranslateService } from '@ngx-translate/core';
import { ChooseTrainerAuthProvider } from '../../providers/choose-trainer-auth/choose-trainer-auth';

@IonicPage()
@Component({
  selector: 'page-specificor-any-trainer',
  templateUrl: 'specificor-any-trainer.html',
})
export class SpecificorAnyTrainerPage {

  public appointFull; //FROM APPOINTMENT MODAL PAGE.
  public purpose; //FROM APPOINTMENT MODAL PAGE.
  public purposeTotalSessions; //FROM APPOINTMENT MODAL PAGE.
  public icon; //FROM APPOINTMENT MODAL PAGE.
  public cartOrbooked; //FROM APPOINTMENT MODAL PAGE.

  public trainerList = []; //TO STORE TRAINER LIST.
  public trainerType; //FOR GETTING THE TYPE OF TRAINER "ANY TRAINER" OR "CHOOSE SPECIFIC".
  public index; //TO STORE THE INDEX OF SELECTED TRAINER.
  public trainername; //TO STORE SELECTED TRAINER NAME.

  public sessionOwner: boolean; //BOOLEAN
  public calluiboolean; //BOOLEAN

  constructor(
    public translateService: TranslateService,
    public chooseTrainerAuthService: ChooseTrainerAuthProvider,
    public scheduleAuthService: ScheduleAuthProvider,
    public loadingCtrl: LoadingController,
    private trainer: SelectTrainerProvider,
    public navCtrl: NavController,
    public navParams: NavParams) {

    this.appointFull = this.navParams.get('appointFull');
    this.purpose = this.navParams.get('purpose');
    this.purposeTotalSessions = this.navParams.get('purposeTotalSessions');
    this.icon = this.navParams.get('icon');
    this.cartOrbooked = this.navParams.get('cartOrbooked');
    console.log(this.cartOrbooked,"cart or booking flag");
    this.fetchScheduleDetails();

    this.calluiboolean = true; //flag to set specific trainer or anytrainer view

  }

  ionViewDidLoad() { }

  //from json using its provider
  fetchScheduleDetails() {
    this.trainer.getSpecificTrainerDetails().map(response => response).subscribe((response) => {
      this.trainerList = response['selectTrainer'];
      console.log("TRAINER API RESPONSE ON CHOOSE TRAINER PAGE");
      console.log(this.trainerList);
      this.checkEmptyData();
      if (this.trainerList.length == 0) {
        this.callUI();
      }
      else if (this.trainerList.length == 1) {
        this.sessionOwner == true;
        this.gotoDateTimeSession();
      }
      else if (this.trainerList.length > 1) {
        this.sessionOwner == false;
      }
    });
  }

  // Method used to set placeholders in case server does not send data
  checkEmptyData() {
    for (let i = 0; i < this.trainerList.length; i++) { // Checking if trainer icon is empty
      if (this.trainerList[i].icon == "") {
        this.trainerList[i].icon = "pro-pic.png"; // Setting Default Placeholder i.e default image of trainer
      }
    }
  }

  //setting the value to false
  callUI() {
    console.log("SESSION OWNER");
    this.calluiboolean = false;
  }

  gotoDateTimeSession() {
    this.presentLoadingDefault();
    this.navCtrl.push('Datetime', //pushing it to new page
      {
        'appointFull': this.appointFull,
        'purpose': this.purpose,
        'purposeTotalSessions': this.purposeTotalSessions,
        'icon': this.icon,
        'cartOrbooked': this.cartOrbooked,
        'trainername': this.trainerList[this.index].TrainerName,
        'trainerType': this.trainerType
      }
    );
  }

  presentLoadingDefault() { // displaying loading
    const loading = this.loadingCtrl.create({
      content: 'Loading...',
      spinner: 'crescent'
    });
    loading.present();
    setTimeout(() => {
      loading.dismiss();
    }, 1000);
  }

  // method to move to next page - DateandTime Page
  gotoDateTime() {
    this.trainerType = "Any Trainer" //IF ANY TRAINER IS SELECTED.
    //Passing details via nav params
    this.navCtrl.push('Datetime', {
      'appointFull': this.appointFull,
      'purpose': this.purpose,
      'purposeTotalSessions': this.purposeTotalSessions,
      'icon': this.icon,
      'cartOrbooked': this.cartOrbooked,
      'trainername': "Any Trainer",
      'trainerType': this.trainerType,
      'trainerIcon': 'anytrainer.png',
      'mastertrainerlist': this.trainerList,
    }
    );
  }

  //method to fetch name using index
  doToDateSpecific(i) {
    this.trainerType = "Choose Specific" //IF TRAINER IS SELECTED FROM CHOOSE SPECIFIC LIST.
    this.index = i; //fetch index of trainer name array
    this.navCtrl.push('Datetime', //pushing it to new page
      {
        'appointFull': this.appointFull,
        'purpose': this.purpose,
        'purposeTotalSessions': this.purposeTotalSessions,
        'icon': this.icon,
        'cartOrbooked': this.cartOrbooked,
        'trainername': this.trainerList[this.index].TrainerName,
        'trainerType': this.trainerType,
        'trainerIcon': this.trainerList[this.index].TrainerPicture,
        'trainerrating': this.trainerList[this.index].TrainerRating,
        'ratingsurveycount': this.trainerList[this.index].RatingSurveyCount,
        'mastertrainerlist': this.trainerList,
      }
    );
  }

  //Star ratings logic 
  starRating(rating: number) {
    let full;
    if (rating <= 5) {
      full = Math.floor(rating);
    }
    else {
      full = 5;
    }
    let status = [];
    for (let i = 0; i < full; i++) {
      status.push("fulle.svg"); // saves Full Star svg in array
    }
    if (rating - full == 0.5 && !(rating >= 5)) {
      status.push("halfe.svg"); // saves Half Star svg in array
    }
    if (status.length < 5) {
      let emp = 5 - status.length;
      for (let i = 0; i < emp; i++) {
        status.push("fullgrey.svg");// saves Empty Star svg in array
      }
    }
    return status; // Returns array so that it can be rendered using *ngFor
  }

}
