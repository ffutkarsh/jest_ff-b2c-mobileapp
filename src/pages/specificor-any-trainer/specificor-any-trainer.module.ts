import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpecificorAnyTrainerPage } from './specificor-any-trainer';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    SpecificorAnyTrainerPage,
   
  ],
  imports: [
    IonicPageModule.forChild(SpecificorAnyTrainerPage),
    TranslateModule
  ],
}) 
export class SpecificorAnyTrainerPageModule {}
