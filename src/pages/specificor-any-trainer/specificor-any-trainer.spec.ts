// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, ViewChild, Component } from '@angular/core';
// import { SpecificorAnyTrainerPage } from './specificor-any-trainer';
// import { IonicModule, Platform, NavController, NavParams } from 'ionic-angular/index';
// import { StatusBar } from '@ionic-native/status-bar';
// import { SplashScreen } from '@ionic-native/splash-screen';
// import * as moment from 'moment';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { HttpClientModule } from '@angular/common/http';
// import { HttpModule, ConnectionBackend } from "@angular/http";
// import { SelectTrainerProvider } from '../../providers/select-trainer/select-trainer-api';
// import { ChooseTrainerAuthProvider } from '../../providers/choose-trainer-auth/choose-trainer-auth';
// import { ScheduleAuthProvider } from '../../providers/schedule-auth/schedule-auth';

// class NavParamsMock {
//     static returnParam = {
//         'domain': '',
//         'phone': '',
//         'otp': ''
//     };
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// describe('Specific Testing', () => {

//     let comp: SpecificorAnyTrainerPage;
//     let fixture: ComponentFixture<SpecificorAnyTrainerPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [SpecificorAnyTrainerPage],
//             imports: [
//                 HttpModule,
//                 HttpClientModule,
//                 IonicModule.forRoot(SpecificorAnyTrainerPage),
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 ConnectionBackend,
//                 SelectTrainerProvider,
//                 TranslateService,
//                 TranslateStore,
//                 ChooseTrainerAuthProvider,
//                 ScheduleAuthProvider,
//                 { provide: NavParams, useClass: NavParamsMock }
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(SpecificorAnyTrainerPage);
//         comp = fixture.componentInstance;
//     });

//     it('Header classname', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.Header')).nativeElement;
//         expect(inputfield.className).toMatch('Header');
//     });


//     // it(" Header Text Match ", async () => {
//     //     let fixture = TestBed.createComponent(SpecificorAnyTrainerPage);
//     //     let element2 = fixture.debugElement.nativeElement.querySelector(".Header");
//     //     expect(element2.textContent.trim()).toContain("Choose Trainer");
//     // });


//     it("Checking if Grid Present ", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.mygrid')).nativeElement;
//             expect(inputfield.getAttribute('class')).toContain("mygrid");
//         });
//     });

//     it("Checking if Any Trainer list-header present ", async () => {
//         let fixture = TestBed.createComponent(SpecificorAnyTrainerPage);
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.anytrainers')).nativeElement;
//             expect(inputfield.getAttribute('class')).toContain("anytrainers");
//         });
//     });

//     it("Checking if Specific Trainer list-header present ", async () => {
//         let fixture = TestBed.createComponent(SpecificorAnyTrainerPage);
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.specifictrainers')).nativeElement;
//             expect(inputfield.getAttribute('class')).toContain("specifictrainers");
//         });
//     });

//     it("Checking if Any Trainer Appointment Type present ", async () => {
//         let fixture = TestBed.createComponent(SpecificorAnyTrainerPage);
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             let element2 = fixture.debugElement.nativeElement.querySelector(".belowheader");
//             expect(element2.getAttribute('class')).toContain("belowheader");
//         });
//     });

//     it("Checking if Any Trainer Avatar present ", async () => {
//         let fixture = TestBed.createComponent(SpecificorAnyTrainerPage);
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             let element2 = fixture.debugElement.nativeElement.querySelector(".anytavatar1");
//             expect(element2.getAttribute('class')).toContain("anytavatar1");
//         });
//     });

//     it("Should contain Any Trainer Avatar image ", () => {
//         let fixture = TestBed.createComponent(SpecificorAnyTrainerPage);
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             const img: HTMLImageElement = fixture.debugElement.query(By.css('img')).nativeElement;
//             console.log(img);
//             expect(img.src).toContain('anytrainer.png');
//         });
//     });

//     it("Checking if Specific Trainer Grid present ", async () => {
//         let fixture = TestBed.createComponent(SpecificorAnyTrainerPage);
//         let element2 = fixture.debugElement.nativeElement.querySelector(".specificgrid");
//         expect(element2.className).toContain("specificgrid");
//     });
    
//     it('Star Rating Render Function - 1', () => {
//                     expect(comp.starRating(2)).toContain('fulle.svg');
//                 });
        
//                 it('Star Rating Render Function - 2', () => {
//                     expect(comp.starRating(3.5)).toContain('halfe.svg');
//                 });
        
//                 it('Star Rating Render Function - 3', () => {
//                     expect(comp.starRating(5)).not.toContain('halfe.svg');
//                 });
        
//                 it('Star Rating Render Function - 4', () => {
//                     expect(comp.starRating(100)).not.toContain('fullgrey.svg');
//                 });
      

//     // new test cases mab 2
//     //to check json method
//     it("Checking if json is present or no ", async () => {
//         expect(comp.fetchScheduleDetails).toBeTruthy();
//     });
 
//     //to check method to fetch name of trainer method
//     it("Checking if method to fetch name of trainer is present or no ", async () => {
//         expect(comp.fetchScheduleDetails).toBeTruthy();
//     });

//     //to check new variable made to save specifi trainer 
//     it("check new variable made to save specific trainer ", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             expect(comp.trainerName).toBeUndefined();
//         });
//     });


//     //to check new variable index to save specific trainer 
//     it("check new variable index made to save specific trainer ", async () => {
//         expect(comp.index).toBeUndefined();
//     });

//     //to check method to fetch call ui 
//     it("Checking if method to call UI of call button ", async () => {
//         expect(comp.callUI).toBeTruthy();
//     });

//     //to check  method to go directly to date and time page
//     it("Checking if method to go directly to date and time page ", async () => {
//         expect(comp.gotoDateTimeSession).toBeTruthy();
//     });

//     //to check method to present loading
//     it("Checking if method to present loading ", async () => {
//         expect(comp.presentLoadingDefault).toBeTruthy();
//     });

//     //to check new variable calluiboolean 
//     it("check new variable calluiboolean ", async () => {
//         expect(comp.calluiboolean).toBeTruthy();
//     });

//     //to check new variable cartOrbooked 
//     it("check new variable cartOrbooked ", async () => {
//         expect(comp.cartOrbooked).toBeDefined();
//     });


//     //to check new variable sessionOwner 
//     it("check new variable sessionOwner ", async () => {
//         expect(comp.sessionOwner).toBeUndefined();
//     });

//     //check if call button is present or no
//     it("Checking if call button is present or no ", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             const inputfield: HTMLElement = fixture.debugElement.query(By.css('.callButton')).nativeElement;
//             expect(inputfield.className).toMatch('callButton');
//         });
//     });

//     //check if call icon is present or no
//     it("Checking if call icon is present or no ", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             const inputfield: HTMLElement = fixture.debugElement.query(By.css('.callIcon')).nativeElement;
//             expect(inputfield.className).toMatch('callIcon');
//         });
//     });
    
//     it('Check if rtings is defined', async () => {
//         comp.starRating(3);
//         expect(comp.starRating(3)).toBeDefined();
//     });

  

//     it('Check if received any empty value from server and use placeholder instead', () => {
//                     expect(comp.checkEmptyData).toBeDefined();
//        });
//     });
             
