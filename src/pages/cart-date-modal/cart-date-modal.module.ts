import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CartDateModalPage } from './cart-date-modal';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    CartDateModalPage,
  ],
  imports: [
    IonicPageModule.forChild(CartDateModalPage),
    TranslateModule,
  
    
  ],
})
export class CartDateModalPageModule {}
