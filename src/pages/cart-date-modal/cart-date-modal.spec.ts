// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, style } from '@angular/core';
// import { CartDateModalPage } from './cart-date-modal';
// import { IonicModule, Platform, NavController, NavParams, Item } from 'ionic-angular/index';
// import { HttpModule } from '@angular/http';
// import * as moment from 'moment';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { MomentModule } from 'angular2-moment';
// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// //The describe function is for grouping related specs
// describe('---------CART DATE MODAL PAGE UI----------', () => {
//     let de: DebugElement;
//     let comp: CartDateModalPage;
//     let fixture: ComponentFixture<CartDateModalPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [CartDateModalPage],
//             imports: [
//                 HttpModule,
//                 TranslateModule.forChild(),
//                 MomentModule,
//                 IonicModule.forRoot(CartDateModalPage),
//             ],
//             providers: [

//                 TranslateService,
//                 TranslateStore,
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(CartDateModalPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });
//     //Creating the main Component
//     it('should create component', () => expect(comp).toBeDefined());
//     it('checking the main content using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.mainContent')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('mainContent');
//     });
//     //checking the sub content
//     it('checking the sub content using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.modal')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('modal');
//     });

//     //checking the sub content
//     it('checking the dib containing attach document header using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.Header')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('Header');
//     });

//     //checking the ion row 
//     it('checking the ion row white holds the customised date', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.dateContent')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('dateContent');
//     });

//     //checking the ion col 
//     it('checking the ion col which holds the the transalated string starte date', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.startDateText')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('startDateText');
//     });
//     //checking the ion button 
//     it('checking the ion col which holds the button to decreament date', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.incrementDate')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('incrementDate');
//     });

//     //checking the ion button 
//     it('checking the ion col which holds the button to increment date', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.incrementDate')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('incrementDate');
//     });

//     //checking the ion input 
//     it('checking the ion input which holds the {{date}} by moment js', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.incrementDate')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('incrementDate');
//     });

//     //checking the img 
//     it('checking the img which holds the calendar svg', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.calendar')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('calendar');
//     });

//     //checking the footer 
//     it('checking the footer which holds 2 buttons', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.footerButtons')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('footerButtons');
//     });

//     //checking the footer row
//     it('checking the row in footer by class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.footerRow')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('footerRow');
//     });

//     //checking the footer button 1 
//     it('checking the col in footer which holds cancel button', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.cancel')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('cancel');
//     });

//     //checking the footer button2
//     it('checking the col in footer which holds save button', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.save')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('save');
//     });
// });


// //The describe function is for grouping related specs
// describe('---------CART DATE MODAL PAGE METHODS----------', () => {
//     let de: DebugElement;
//     let comp: CartDateModalPage;
//     let fixture: ComponentFixture<CartDateModalPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [CartDateModalPage],
//             imports: [
//                 HttpModule,
//                 TranslateModule.forChild(),
//                 MomentModule,
//                 IonicModule.forRoot(CartDateModalPage),
//             ],
//             providers: [

//                 TranslateService,
//                 TranslateStore,
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(CartDateModalPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //add date
//     it('checking the add to calender function', () => {
//         expect(comp.addDateCalendar).toBeTruthy();
//     });

//     //add date
//     it('checking the add calender function - logically proven', () => {
//         this.addDate = moment(this.date); //today's date
//         this.addDate = this.addDate.add(1, "day"); //add by 1 day
//         this.addDate = this.addDate.format("DD MMM YYYY"); // apply format
//         this.date = this.addDate
//         expect(this.date).toMatch(this.addDate);
//     });


//     //sub date
//     it('checking the sub from calender function', () => {
//         expect(comp.subDateCalendar).toBeTruthy();
//     });

//         //add date
//         it('checking the sub method calender function - logically proven', () => {
//             this.subDate = moment(this.date); //today's date
//             this.subDate = this.subDate.subtract(1, "days"); //sub by 1 day
//             this.subDate = this.subDate.format("DD MMM YYYY"); // apply format
//             this.date = this.subDate
//             expect(this.date).toMatch(this.subDate);
//         });
//     //close modal 
//     it('checking the close modal function', () => {
//         expect(comp.closeModal).toBeTruthy();
//     });
// });