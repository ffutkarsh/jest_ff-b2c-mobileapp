import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-cart-date-modal',
  templateUrl: 'cart-date-modal.html',
})


export class CartDateModalPage {
  date; // variable to save current date
  addDate; //var to save added date by 1 day
  subDate; //var to save subs date by 1 day
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.date = moment().format('DD MMM YYYY'); //finding out today's date in the format 19 Nov 2018
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartDateModalPage');
  }

  //add date by 1 day
  addDateCalendar() {
    this.addDate = moment(this.date); //today's date
    this.addDate = this.addDate.add(1, "day"); //add by 1 day
    this.addDate = this.addDate.format("DD MMM YYYY"); // apply format
    this.date = this.addDate; // set added date in date var
  }

  //sub date by 1 day
  subDateCalendar() {
    this.subDate = moment(this.date); //today's date
    this.subDate = this.subDate.subtract(1, "days"); //sub by 1 day
    this.subDate = this.subDate.format("DD MMM YYYY"); // apply format
    this.date = this.subDate;// set added date in date var
  }

  //method to close modal
  closeModal() {
    this.navCtrl.pop();
  }

}
