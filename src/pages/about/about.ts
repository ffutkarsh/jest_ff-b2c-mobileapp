import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { AboutContentProvider } from '../../providers/about-content/about-content';

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})

export class AboutPage {

  public versionNumber: any; //APPLICATION VERSION NUMBER
  public aboutHTML; //VARIABLE TO BIND JSON CONTENT INTO HTML.

  constructor(public navCtrl: NavController,  private aboutcontent: AboutContentProvider) {
    this.versionNumber = "1.5.1"; //INITIALISING APPLICATION VERSION NUMBER.
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');

    // //STRINGIFYING HTML CONTENT TO CONVERT IT INTO JSON FORMAT.
    // this.aboutHTML = JSON.stringify(`<p>
    //     <p align="middle"><img height="50px" width="180px" src="assets/img/fflogo.png"></p>

    //     <h3 align="left">What is FitnessForce ?</h3>

    //     <p style="line-height: 30px;">FitnessForce is an<b>&nbsp;easy, efficient and economical web-based software
    //         </b>&nbsp;that helps you manage all your Fitness & Wellness business activities from anywhere. It is a CRM software,
    //         with Sales, Marketing, Billing, Scheduling , Dashboard and many such business oriented features.
    //     </p>

    //     <p style="font-size: 15px; color: #000000; margin-top: 1px; line-height: 30px;">
    //         <img height="12px" width="12px" src="assets/icon/play-icon.png">We provide unlimited free online trainings even if
    //         it is retraining all over again.
    //     </p>

    //     <p style="font-size: 15px; color: #000000; margin-top: 1px; line-height: 30px;">
    //         <img height="12px" width="12px" src="assets/icon/play-icon.png">We are renowned to provide an impeccable support
    //         from our qualified team of 30+ professionals.
    //     </p>

    //     <p style="font-size: 15px; color: #000000; margin-top: 1px; line-height: 30px;">
    //         <img height="12px" width="12px" src="assets/icon/play-icon.png">Our development team has something cooking for you
    //         every single month
    //     </p>

    // </p>`);
    // console.log(this.aboutHTML);

    this.GetAboutContent(); //CALLING TO GET FETCHED DATA FROM JSON.
  }


  abc(){
    console.log("hfxhfxhx");
  }
  
  //FETCHING STRINGIFIED DATA FROM ABOUTCONTENT JSON WITH THE HELP OF ABOUTCONTENT PROVIDER.
  GetAboutContent() {
    this.aboutcontent.getAboutContent().map((response) => response.json()).subscribe((response) => {
      console.log(response.aboutHtml);
      this.aboutHTML = response.aboutHTML;
    });
  }

}
