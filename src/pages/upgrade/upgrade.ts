import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-upgrade',
  templateUrl: 'upgrade.html',
})
export class UpgradePage {

  description1; //var o save description boolean
  description2; //var o save description boolean
  home;
  passport;
  index;
 
  public selectedUpgradeMembership = {};
  public upgradeMembership = [];
  public upgradeData: any = [];
  public upgradeFlag: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController) {

    this.description1 = false; //toggle variable
    this.description2 = false; //toggle variable
    this.home = "Home Membership gives you access to the branch for which you purchased the membership at a more affordable rate. Best suited for people who regularly use facility at only 1 location";
    this.passport = "Passport Membership gives you access to the branch for which you purchased the membership at a more affordable rate. Best suited for people who use facility at multiple location";

  }

  ionViewWillEnter() {

    this.upgradeFlag = (this.navParams.get("upgradeflag"));
    this.upgradeData.push(this.navParams.get("upgrade"));
    console.log(this.upgradeData[0].contractName);
    if (this.upgradeData[0].contractName == 'Contract Mansoon') {
      this.upgradeFlag = false;
    }




    this.upgradeMembership = [
      {
        "monthDuration": "3",
        "homeRate": "8000",
        "passportRate": "11000",
        "statushome": false,
        "statuspass": false
      },
      {
        "monthDuration": "6",
        "homeRate": "14000",
        "passportRate": "19000",
        "statushome": false,
        "statuspass": false
      },
      {
        "monthDuration": "9",
        "homeRate": "20000",
        "passportRate": "25000",
        "statushome": false,
        "statuspass": false
      }
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpgradePage');
  }

  expandDescription1() {
    this.description1 = !this.description1;
  }

  expandDescription2() {
    this.description2 = !this.description2;
  }

  openHome(U,selected) {
    for (let i = 0; i < this.upgradeMembership.length; i++) {
      if (this.upgradeMembership[i].statuspass == true) {
        this.upgradeMembership[i].statuspass = false
      }
    }
    for (let i = 0; i < this.upgradeMembership.length; i++) {
      if (i == selected) {
        this.upgradeMembership[i].statushome = true;
        this.selectedUpgradeMembership = this.upgradeMembership[i];
        let renewModal = this.modalCtrl.create("UpgradeHomeActionModalPage", { "upgrade": U ,'upgradeJsonData':this.upgradeData});
        renewModal.present();
      }
      else
        this.upgradeMembership[i].statushome = false;
    }
  }



  openPassport(U,selected) {
    for (let i = 0; i < this.upgradeMembership.length; i++) {
      if (this.upgradeMembership[i].statushome == true) {
        this.upgradeMembership[i].statushome = false;
      }
    }
    for (let i = 0; i < this.upgradeMembership.length; i++) {
      if (i == selected) {
        this.upgradeMembership[i].statuspass = true;
        this.selectedUpgradeMembership = this.upgradeMembership[i];
        let renewModal = this.modalCtrl.create("UpgradeActionModalPage", { "upgrade": U,'upgradeJsonData':this.upgradeData });
        renewModal.present();
      }
      else
        this.upgradeMembership[i].statuspass = false;
    }
  }

}
