import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { EmailComposer } from '@ionic-native/email-composer';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { TranslateService } from '@ngx-translate/core';
import  { VirtualTourPage } from '../../pages/pages';
@IonicPage()
@Component({
  selector: 'page-club-details',
  templateUrl: 'club-details.html',
})
export class ClubDetailsPage {
  public sendEmail: any;//var to store receipent

  public getClubDetails = { //storing default object with key
    "TenantID": 0,
    "TenantName": "",
    "GymName": "",
    "CompleteAddress": "",
    "ShortPostalAddress": "",
    "FIELD6": 0,
    "PhoneNumber": "",
    "Email": "",
    "Latitude": 0,
    "longitude": 0,
    "IndoorMapsURL": "",
    "State": "",
    "City": "",
    "FIELD14": 0,
    "offers": "", 
    "geo": null,
    "img": ""
  }
  constructor(private launchNavigator: LaunchNavigator,public translateService: TranslateService, public navCtrl: NavController, private app: InAppBrowser, private callNumber: CallNumber, private emailComposer: EmailComposer, public navParams: NavParams) {
    this.getClubDetails = this.navParams.get("objectOfClub"); //RECEIVING PURPOSE.   

  }

 ionViewDidLoad() {   
  }
  goToVirtualTour() { //onClick of virtual tour go to virtual tour page
    this.navCtrl.push("VirtualTourPage", {
      'objectOfClub': this.getClubDetails
    })
  } 

  //Calling Option
  callOption() {   
    this.callNumber.callNumber("8898188951", true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
    //this.navCtrl.push('FeedbackFlowPage', { 'feedbackType': 'call' }); //SENDING FEEDBACK TYPE THROUGH NAV PARAMS TO FEEDBACKFLOWPAGE.
  }


  //USE THIS WHILE RUNNING ON BROWSER
  //opnes gmail in another browser
  emailTesting() {
    //this is working -
    this.app.create('https://mail.google.com/mail/u/0/#inbox', "_blank", "https://mail.google.com/mail/u/0/#inbox");
  }


  //USE THIS WHILE RUNNING ON APP
  //this method is for APP
  sendEmailTo() {
    this.emailComposer.isAvailable().then((available: boolean) => {
      if (available) {
        this.sendEmail = { //receipent
          to: 'max@mustermann.de',
          cc: 'erika@mustermann.de',
          bcc: ['john@doe.com', 'jane@doe.com'],
          attachments: [
            'file://img/logo.png',
            'res://icon.png',
            'base64:icon.png//iVBORw0KGgoAAAANSUhEUg...',
            'file://README.pdf'
          ],
          subject: 'Cordova Icons',
          body: 'How are you? Nice greetings from Leipzig',
          isHtml: true
        };
      }
    });
    this.emailComposer.open(this.sendEmail);
  }

 //pictures of gym
  pictures(IndoorMapsURL) {
    var imgUrl = IndoorMapsURL
    //this is working -
    this.app.create(imgUrl);
  }

  //fetch lang and lat to open map
  goToMaps() {
    let options: LaunchNavigatorOptions = {
      start: [this.getClubDetails.Latitude, this.getClubDetails.longitude]
   // app: LaunchNavigator.APPS.GOOGLE_MAPS
    };
    this.launchNavigator.navigate('Toronto, ON', options)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }
}
