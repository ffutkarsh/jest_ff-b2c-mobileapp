import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClubDetailsPage } from './club-details';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    ClubDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ClubDetailsPage),
    TranslateModule
  ],
})
export class ClubDetailsPageModule {}
