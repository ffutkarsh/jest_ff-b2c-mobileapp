import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentAttendancePopupPage } from './appointment-attendance-popup';
import { ClassesFlowApiProvider } from '../../providers/classes-flow-api/classes-flow-api';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AppointmentAttendancePopupPage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentAttendancePopupPage),
    TranslateModule
  ],
  providers: [
    ClassesFlowApiProvider
  ]
})
export class AppointmentAttendancePopupPageModule { }
