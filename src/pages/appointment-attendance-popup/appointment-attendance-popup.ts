import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ClassesFlowApiProvider } from '../../providers/classes-flow-api/classes-flow-api';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-appointment-attendance-popup',
  templateUrl: 'appointment-attendance-popup.html',
})
export class AppointmentAttendancePopupPage {

  public apptattendance = [];
  constructor(
    public navCtrl: NavController,
    public classesFlowApi: ClassesFlowApiProvider,
    public navParams: NavParams,
    public translateService: TranslateService, ) {
    this.getAttendance();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppointmentAttendancePopupPage');
  }

  // Fetches data for Appointment attendance
  getAttendance() {
    this.classesFlowApi.getClassAttendance().map((response) => response).subscribe((response) => {
      this.apptattendance = response;
      console.log("my appointment attendance=============", response);
      console.log(this.apptattendance);
    })
  }

  gotodash()
{
  this.navCtrl.push("DashboardPage");
}

}
