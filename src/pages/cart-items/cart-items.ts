import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams, ModalController } from 'ionic-angular';
import { CartItemsProvider, CartCouponsProvider, RelatedItemsProvider, BillingProvider } from '../../providers/providers';
import { TranslateService } from '@ngx-translate/core';
import { TimerObservable } from 'rxjs/observable/TimerObservable';

import { CartCouponPage, EmployeeModalPage, WaiverSuccessPage, CartDateModalPage } from '../../pages/pages'
import { isDefined } from 'ionic-angular/util/util';
import { DashboardPage } from '../dashboard/dashboard';
import { Storage } from '@ionic/storage';
import { CartBillingProvider } from '../../providers/cart-billing/cart-billing';


@IonicPage()
@Component({
    selector: 'page-cart-items',
    templateUrl: 'cart-items.html',
})
export class CartItemsPage {

    public timer; //timer
    public cartItems = []; //to fetch CART ITEMS
    public lastIteminCart = {
        'purpose': "",
        'purposeIcon': "",
        'purposeTotalSessions': 0,
        'date': "",
        'trainer': "",
        'slots': "",
        'price': 0,
        'ratingsurveycount': "this.ratingsurveycount",
        'trainerrating': "",
        'trainerType': "",
        'availableSlot': "",
        'apptype': "",
        'repeattime': "",
        'mastertrainerlist': "",
        'status': "",
        'fromCart': ""
    }
    public cartStorage = [];
    public offerFlowCart = false;
    public membershipFlowCart = false;

    public freezeFlowCart = false;
    public updgradeFlowCart = false;
    public renewFlowCart = false;

    public facilityFlowCart = false;
    public classFLowCart = false;
    public appFlowCart = false;

    public myArray = [];
    public flowArray = [];

    public classFlowData = [];
    public setofferLocally = [];


    public offernameFromOfferDetails;
    public countdownNumberEl;
    public relatedItems; //to fetch IETMS YOU MAY LIKE
    public couponDetails; //Array of objects to store json file.
    public discount; //var to store discount
    public index; //to store index
    public index1; //to save index
    public index2; //to save index
    public indexOfItem: number;
    public noOfItems;
    itemNumber; itemNumber1 = [];
    numeral: any //currency
    public RelArray = []; //to store recommended products in an array frpm response
    public CouponArray = []; //to store coupon offers in an array from response
    //BILLING
    singleItemPrice; singleItemPriceD; //storing a single items price for subtraction/addition
    totalBill = 0; //initialising bills to 0
    gstAmount; // gst amount
    substractedAmount = 0; //initialising bills to 0
    addedAmount = 0;//initialising bills to 0
    discountAmount = 0;//initialising bills to 0
    relatedItemRegiPrice = 0;//initialising bills to 0
    storeBillingObject;//initialising bills to 0
    toPayAmount = 0;//initialising bills to 0
    relatedItemObjects; //object to store related item on click
    public addRelatedBoolean; //boolean to allow permission to add related items

    public localRelatedArray = []; //storing related items in local array

    // FROM COUPON PAGE:-
    public indexFromCoupon; //storing index from coupon page
    public booleanTitle; //boolean to display if else
    public booleanEMPcoupons;//boolean to check json flag.
    public itemReceived; //var to store discount from coupon list json
    couponDetailedTitle; //var to store default title.
    //FROM COUPON MODAL
    public oneCouponRecord; //storing single coupon details

    public colorarray = []; //strong rgb color
    public colorarrayLight = []; //storing light colors
    public newColorList = [];
    lenghtOf;
    in;
    lenghtOfCartObj;
    cartObj = [];
    arrayOfCouponsApplied = [];
    purpose; //VARIABLE TO RECEIVE PURPOSE THROUGH NAV PARAMS.
    icon; //VARIABLE TO RECEIVE PURPOSE ICON THROUGH NAV PARAMS.
    purposeTotalSessions; //VARIABLE TO RECEIVE PURPOSE'S TOTAL NUMBER OF SESSIONS THROUGH NAV PARAMS.
    date; //VARIABLE TO RECEIVE DATE THROUGH NAV PARAMS.

    /* new variable */
    public cartAddedItem: any = [];
    public UpgradeData = [];
    public renewData = [];
    public upgradePassportAmount: any;

    // public arrQuantity = [];
    public Quantity = [];
    public itemCout: number = 0;
    // public freezefromDate;
    // public freezepurposeName;
    // public freezetoDate;
    // public freezedurationinDays;
    public freezeDetails = {};



    constructor(
        public CBP: CartBillingProvider,
        public navCtrl: NavController,
        private storage: Storage,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        public relatedItemService: RelatedItemsProvider,
        public translateService: TranslateService,
        private modalController: ModalController,
        public cartItemsService: CartItemsProvider,
        public billingProvider: BillingProvider,
        public couponService: CartCouponsProvider,
    ) {
        this.getCartCount();

        this.newColorList = ['#e6bfb5', ' #b9ddaf', ' #c1c8cd', '#e6bfb5', ' #b9ddaf', ' #c1c8cd', '#e6bfb5', ' #b9ddaf'];


        this.offerFlowCart = this.navParams.get("offerFlowCart");
        this.offernameFromOfferDetails = this.navParams.get("offername");

        this.membershipFlowCart = this.navParams.get("membershipFlowCart");

        this.freezeFlowCart = this.navParams.get("freezeFlow");

        this.freezeDetails = this.navParams.get("freezeDetails");
        console.log("frezzzzzz", this.freezeDetails)
        this.updgradeFlowCart = this.navParams.get("upgradeFlow");

        this.renewFlowCart = this.navParams.get("renewFlowCart");

        this.classFLowCart = this.navParams.get('classFlow');

        this.facilityFlowCart = this.navParams.get("facilityFlow");

        this.appFlowCart = this.navParams.get("appFlow"); //from app flow.

        this.upgradePassportAmount = (this.navParams.get('upgrdeTotalcost'));
        console.log(this.upgradePassportAmount);

        this.offernameFromOfferDetails = this.navParams.get("offername");


        this.itemNumber = 0;
        this.itemNumber1[0] = 1;

        this.cartObj = this.cartItemsService.tempItem
        this.lastIteminCart = this.cartItemsService.tempObj
        console.log("fed up last item in cart", this.lastIteminCart)
        if (this.cartItemsService.tempObj == isDefined) { this.totalBill = this.lastIteminCart.price }

        this.gstCalculator()
        this.fetchCartItems(this.cartObj) //calling cart items provider method
        this.fetchCouponDetails() //coupon list
        this.fetchRelatedItems() //items you may like

        this.booleanTitle = true; //default value true
        this.booleanEMPcoupons = true; //default value true
        this.addRelatedBoolean = false;   //boolean to allow permission to add related items

        //from repeat booking 
        this.purpose = this.navParams.get("purpose"); //RECEIVING PURPOSE.
        this.icon = this.navParams.get("purposeIcon"); //RECEIVING PURPOSE ICON.
        this.purposeTotalSessions = this.navParams.get("purposeTotalSessions"); //RECEIVING TOTAL NUMBER OF SESSIONS.
        this.date = this.navParams.get("date"); //RECEIVING DATE.



        this.upgradePassportAmount = (this.navParams.get('upgrdeTotalcost'));
        console.log(this.upgradePassportAmount);
        this.timerFun();

        this.renewData = [{ "Ramount": this.navParams.get('renewTotalcost'), "Rdata": this.navParams.get('renewCartData') }];
        this.navParams.get('upgrdeTotalcost');
        this.navParams.get('upgradeCartData');
        this.classFlowData.push(this.navParams.get('buyClass'));

       console.log(this.CBP.res); this.CBP.res;
        // this.Quantity.push(Number(1));
        // this.myArray.push(Number(1));
    }

    ionViewDidLeave() {
        this.cartAddedItem = [];
        this.Quantity = [];
        this.myArray = [];
        this.timer.unsubscribe();
    }

    ionViewWillEnter() {
        this.Quantity.push(Number(1));
        this.myArray.push(Number(1));

        // upgrade //
        this.UpgradeData = [{
            "Uamount": this.navParams.get('upgrdeTotalcost'),
            "UIdata": this.navParams.get('upgradeCartData')
        }];

        console.log(this.UpgradeData);

        /* <<<renew flow>>>> */
        this.renewData = [{
            "Ramount": this.navParams.get('renewTotalcost'),
            "Rdata": this.navParams.get('renewCartData')
        }]

        console.log(this.renewData);

        this.upgradePassportAmount = (this.navParams.get('upgrdeTotalcost'));
        console.log(this.upgradePassportAmount);
        this.getlocaStorageOfferlist();
        this.appFlowCart = true
        // this.updgradeFlowCart == true
        if (this.cartItemsService.tempObj == undefined) {
            this.totalBill = 0

        }
        // if (this.freezeFlowCart == true) {
        //     //  this.lastIteminCart.price = 0
        //     // this.lastIteminCart.purposeIcon = 'a'
        //     this.appFlowCart = false
        //     this.updgradeFlowCart == false
        // }


        // if (this.facilityFlowCart == true) {
        //     this.appFlowCart = false
        //     this.updgradeFlowCart == false
        //     //  this.lastIteminCart.price = 0
        //     // this.lastIteminCart.purposeIcon = 'a'
        // }

        // if (this.updgradeFlowCart == true) {
        //     this.appFlowCart = false

        //     //  this.lastIteminCart.price = 0
        //     // this.lastIteminCart.purposeIcon = 'a'
        // }
        // if (this.renewFlowCart == true) {
        //     this.appFlowCart = false

        //     //  this.lastIteminCart.price = 0
        //     // this.lastIteminCart.purposeIcon = 'a'
        // }

        // if (this.membershipFlowCart == true) {
        //     this.appFlowCart = false

        //     //  this.lastIteminCart.price = 0
        //     // this.lastIteminCart.purposeIcon = 'a'
        // }

        if (this.appFlowCart == true || this.updgradeFlowCart == true) {
            this.appFlowCart = this.navParams.get("appFlow"); //from app flow.
            this.facilityFlowCart = this.navParams.get("facilityFlow");
            this.freezeFlowCart = this.navParams.get("freezeFlow");
            this.freezeDetails = this.navParams.get("freezeDetails");
            //  numeral(this.lastIteminCart.price).format('0,0')
            this.oneCouponRecord = this.navParams.get('arrayOfCoupon'); //fetching from coupon list
            this.arrayOfCouponsApplied.push(this.oneCouponRecord)
            console.log(this.arrayOfCouponsApplied, "COUPON APPLI4ED")
            this.booleanTitle = this.navParams.get('boolean'); //fetching from coupon list
            this.itemReceived = this.navParams.get('itemreceived'); //fetching from coupon list
            this.addRelatedBoolean = this.relatedItemService.booleanRelatedItem
            this.localRelatedArray = this.relatedItemService.storeRelatedItems(item)
            console.log(this.addRelatedBoolean)
            console.log(this.totalBill, this.addRelatedBoolean, "-=BILL=-")
            // IF TOTAL bill is  0 then get from provider
            if (this.totalBill == 0 || this.relatedItemService.booleanRelatedItem) {
                this.totalBill = this.billingProvider.receivedTotal + this.lastIteminCart.price
                this.addRelatedBoolean = this.relatedItemService.booleanRelatedItem
                var item;
                this.localRelatedArray = this.relatedItemService.storeRelatedItems(item)

                //initialise values from provider
                this.totalBill = this.billingProvider.receivedTotal
                this.discountAmount = this.billingProvider.receivedDiscount
                this.relatedItemRegiPrice = this.billingProvider.receivedRegi
                this.toPayAmount = this.billingProvider.receivedTopay - this.discountAmount
                this.gstCalculator()

            }




            if (this.booleanTitle == undefined || this.booleanEMPcoupons == undefined) { //when loaded first time show apply coupons
                this.couponDetailedTitle = "Apply Coupon";
                this.translateService.get('APPLY_COUPON_TS').subscribe(
                    value => {
                        this.couponDetailedTitle = value;
                    }
                )
            }

            else if (this.booleanTitle == false) { //else - show respective titles
                this.couponDetailedTitle = this.oneCouponRecord.OfferShortName.toUpperCase();




                this.lenghtOf = this.oneCouponRecord.OfferDocumentsRequired.length
                console.log(this.lenghtOf, "LENGHT OF AVAILABLE COUPONS")
                for (this.in = 0; this.in < this.lenghtOf; this.in++) {
                    if (this.oneCouponRecord.OfferDocumentsRequired[this.in].Available == true) {
                        this.booleanEMPcoupons = true
                    }
                    else {
                        this.booleanEMPcoupons = false
                    }

                }
                // this.booleanEMPcoupons = this.oneCouponRecord.OfferDocumentsRequired[0].Available;
                this.discountCalculator()
                if (this.booleanEMPcoupons == true) {
                    this.booleanEMPcoupons = false;
                }

                else if (this.booleanEMPcoupons == false) {
                    this.booleanEMPcoupons = true;
                }
            }
        }

        console.log(this.appFlowCart, "OMGGGGGG")

        this.offerFlowCart = this.navParams.get("offerFlowCart");
        this.offernameFromOfferDetails = this.navParams.get("offername");
        this.membershipFlowCart = this.navParams.get("membershipFlowCart");
        this.freezeFlowCart = this.navParams.get("freezeFlow");
        this.freezeDetails = this.navParams.get("freezeDetails");
        this.updgradeFlowCart = this.navParams.get("upgradeFlow");
        this.renewFlowCart = this.navParams.get("renewFlowCart");
        this.classFLowCart = this.navParams.get('classFlow');
        this.facilityFlowCart = this.navParams.get("facilityFlow");
        this.appFlowCart = this.navParams.get("appFlow"); //from app flow.

        this.upgradePassportAmount = (this.navParams.get('upgrdeTotalcost'));
        console.log(this.upgradePassportAmount);


    }

    ionViewDidLoad() {

        this.storeCartItem();
        this.storage.get('CARTITEM').then((val) => { this.cartAddedItem = val; });

        this.navParams.get('renewTotalcost'),
            this.navParams.get('renewCartData')

        this.navParams.get('upgrdeTotalcost');
        this.navParams.get('upgradeCartData');

        console.log(this.classFlowData);

        console.log('ionViewDidLoad CartItemsPage');
    }


    /* <<newcode>> getting offer list from local storage added to cart */
    getlocaStorageOfferlist() {
        this.storage.get('CARTITEM').then((val) => {
            console.log(val);

            this.cartAddedItem = val;
            console.log(this.cartAddedItem);
            // this.cartAddedItem.forEach(element => {
            //     this.arrQuantity.push(element.quantity)
            // console.log(this.arrQuantity);
            // });
        });
    }

    /* <<<<<<<<<<<Add item quantity new code>>>>>>>>>> */
    minusbutton1(i) {

        // if (this.Quantity[i] > 1) {
        //     this.Quantity[i] = Number(this.Quantity[i]) - 1;
        //     console.log("MINUS BUTTON", this.Quantity[i])
        // }
        // else { this.Quantity[i] = 1; }
        if (this.cartAddedItem[i].quantity > 1) {
            this.cartAddedItem[i].quantity = Number(this.cartAddedItem[i].quantity) - 1;
            this.storage.get('CARTITEM').then((val) => {
                val[i].quantity = Number(val[i].quantity) - 1;
                this.cartAddedItem = val;
                this.storage.set('CARTITEM', this.cartAddedItem);
            });

        }


    }

    plusButton1(i) {

        this.cartAddedItem[i] = Number(this.cartAddedItem[i]) + 1;
        this.storage.get('CARTITEM').then((val) => {
            val[i].quantity = Number(val[i].quantity) + 1;
            this.cartAddedItem = val;
            this.storage.set('CARTITEM', this.cartAddedItem);
        });
    }




    // minusbutton(i) {

    //     if (this.arrQuantity[i] > 1) {
    //         this.arrQuantity[i] = Number(this.arrQuantity[i]) - 1;
    //         console.log("MINUS BUTTON", this.arrQuantity[i])
    //     }
    //     else {
    //         this.arrQuantity[i] = 1;
    //     }
    // }

    // plusButton(i) {

    //     this.arrQuantity[i] = Number(this.arrQuantity[i]) + 1;
    //     console.log(this.arrQuantity[i])
    // }


    /* <<<<<<<<<<<Add item quantity new code end>>>>>>>>>> */


    deleteItem(i) {
        // switch (true) {
        //     case this.offerFlowCart:
        //         this.cartAddedItem.splice(i, 1);
        //         this.storage.set('CARTITEM', this.cartAddedItem).then((res) => {
        //             console.log("In storrage", res, "removed..." + i, "Locallly", this.cartAddedItem);
        //         });
        //         console.log(this.offerFlowCart);
        //         break;

        //     case this.membershipFlowCart:
        //         console.log(this.membershipFlowCart);
        //         break;

        //     case this.freezeFlowCart:
        //         console.log(this.freezeFlowCart);
        //         break;

        //     case this.updgradeFlowCart:
        //         this.UpgradeData.splice(i, 1)
        //         console.log(this.updgradeFlowCart);
        //         break;

        //     case this.renewFlowCart:
        //         this.renewData.splice(i, 1)
        //         console.log(this.renewFlowCart);
        //         break;

        //     case this.classFLowCart:
        //         this.classFlowData.splice(i, 1)
        //         console.log(this.classFLowCart);
        //         break;

        //     case this.appFlowCart:
        //         console.log(this.appFlowCart);
        //         break;
        // }
        console.log(i)
        this.cartAddedItem.splice(i, 1);
        this.storage.get('CARTITEM').then((val) => {
            val.splice(i, 1);
            this.cartAddedItem = val;
            this.storage.set('CARTITEM', this.cartAddedItem);
        });

    }



    //from json using its provider -- CART ITEMS
    fetchCartItems(cartObj) {
        console.log(cartObj, "CART ITEMS...")
        console.log("fed up", this.lastIteminCart)
        for (var index = 0; index < cartObj.length; index++) {
            this.cartItems.push(cartObj)
            console.log(this.cartItems, "INSIDE FETCH CART ITEMS")
        }
        var length = this.cartItems.length - 1;
        // this.lastIteminCart =

    }

    //from json using its provider -- ITEMS YOU MAY LIKE
    fetchRelatedItems() {
        this.relatedItemService.getAllRelatedItems().map((response) => response).subscribe((response) => {
            this.relatedItems = response;
            this.RelArray = this.relatedItems.RecomendedProducts;
            console.log("RELATED ITEMS", this.relatedItems.RecomendedProducts[0])

            //related items
            for (this.index2 = 0; this.index2 < this.RelArray.length; this.index2++) {
                this.colorarrayLight.push(this.getColorLight(this.index2));
            }
        });
    }


    //from json using its provider - COUPON LIST
    fetchCouponDetails() {
        this.couponService.getAllCouponsDetails().map((response) => response).subscribe((response) => {
            this.couponDetails = response;
            this.CouponArray = this.couponDetails.Offers;
            console.log("COUPONS", this.CouponArray)
            //creating  random colors as border-left
            for (this.index1 = 0; this.index1 < this.CouponArray.length; this.index1++) {
                this.colorarray.push(this.getColor(this.index1));
                console.log(this.colorarray.push(this.getColor(this.index1)))
            }
        });
    }

    //creating random color  -RGB
    getColor(i) {
        return "rgb(" + 360 * Math.random() + ',' +
            (25 + 70 * Math.random()) + '%,' +
            (85 + 10 * Math.random()) + '%)';
    }


    //creating random color  - LIGHT
    getColorLight(i) {
        return "hsl(" + 360 * Math.random() + ',' +
            (25 + 70 * Math.random()) + '%,' +
            (85 + 10 * Math.random()) + '%)';

    }

    //from html ngStyle
    setMyStyles2() {
        // this.index = i;
        let style = {
            ' border-inline-start': 'solid 2',
            'border-left-width': 10,
            'color': this.colorarray[0],
            'border-left-color': this.colorarray[0]
        };
        return style; // returning color of specific index
    }


    setMyStylesFont() {
        let style = {
            'font-size': '14px',
            'color': this.colorarray[0]
            //storing random color
        };
        return style; // returning color of specific index
    }

    //from html ngStyle
    setMyStyles1() {
        let style = {
            'color': this.colorarray,
            'font-weight': 600
            //storing random color
        };
        return style; // returning color of specific index
    }

    //from html ngStyle
    setMyStylesSlides(i) {

        this.index = i;
        let style = {
            //'background-color': this.colorarrayLight[this.index],
            'background-color': this.newColorList[this.index],
            //storing random color
        };
        return style; // returning color of specific index
    }

    //on click of coupon option- go to coupon page
    goToCouponPage() {
        this.billingProvider.total(this.totalBill)
        this.billingProvider.registration(this.relatedItemRegiPrice)
        this.billingProvider.toPay(this.toPayAmount)

        if (this.billingProvider.toPay(this.toPayAmount) != 0) {
            this.navCtrl.push(CartCouponPage, {
                'localPriceObject': this.storeBillingObject
            })

        }
        else {
            const alert = this.alertCtrl.create({
                subTitle: "Select atleast 1 item",
                buttons: ['ok']
            });
            alert.present();

        }
    }

    //renew, membership, upgrade

    goToCouponPage1() {
        this.navCtrl.push(CartCouponPage)
    }


    //deleting offer on cross clcik
    deleteOffer() {
        this.booleanTitle = undefined; //de - initialising variables.
        this.booleanEMPcoupons = undefined;
        this.toPayAmount = this.billingProvider.receivedTopay
        var dis = 0
        this.discountAmount = this.billingProvider.discount(dis)
        this.gstCalculator()
        this.translateService.get('APPLY_COUPON_TS').subscribe(
            value => {
                this.couponDetailedTitle = value;
            }
        )
    }

    //Opening employee modal
    openEmpModal() {
        let EmployeeModal = this.modalController.create(EmployeeModalPage);
        EmployeeModal.present();
    }


    //openign waiver modal
    showWaiver() {
        //  
        let counter = 1 + this.localRelatedArray.length;
        this.cartItemsService.proceedToCheckoutResponses(this.localRelatedArray, this.lastIteminCart)
        this.cartItemsService.proceedToCheckout(counter, this.toPayAmount)

        this.storage.get('WaiverSignedIn').then((WaiverSignedIn) => {
            if (!WaiverSignedIn) {
                let waiverModal = this.modalController.create('CartWaiverActionModalPage', {
                    'cartProducts': this.lastIteminCart,
                    'additionalProducts': this.localRelatedArray,
                    'totalProducts': counter,
                    'totalPrice': this.toPayAmount,
                    'discountAmount': this.discountAmount,
                    'appFlow': this.appFlowCart
                });
                waiverModal.present();
            } else {
                this.navCtrl.push('PaymentOptionsPage', {
                    'cartProducts': this.lastIteminCart,
                    'additionalProducts': this.localRelatedArray,
                    'totalProducts': counter,
                    'totalPrice': this.toPayAmount,
                    'discountAmount': this.discountAmount,
                    'appFlow': this.appFlowCart
                });
            }
        });

    }

    //adding items you may like 
    addItemsYouLike(i, item) {
        console.log(item);
        this.myArray.push(1);
        item.type = "RelatedItem";
        this.index = i;
        this.addRelatedBoolean = this.relatedItemService.showRelatedItem();

        this.singleItemPrice = parseInt(this.RelArray[this.index].ProductPrice)
        let regiPrice = parseInt(this.RelArray[this.index].ProductTax[0].ProductRegistration)
        //related
        this.relatedItemObjects = this.relatedItemService.storeRelatedItems(item);

        this.storage.get('CARTITEM').then((val) => {
            this.setofferLocally = val;
        }).then(
            () => {

                this.setofferLocally.push(item)
                this.storage.set('CARTITEM', this.setofferLocally);



            });

        //this.relatedItemService.obectOfItem.push(item)

        if (this.totalBill == 0) {
            console.log("TOTALBILL == 0")
            //total
            this.totalBill = 0;
            this.addedAmount = 0;
            this.addedAmount = this.addedAmount + this.singleItemPrice;
            this.totalBill = this.addedAmount;
            //regi
            this.relatedItemRegiPrice = 0;
            this.relatedItemRegiPrice = this.relatedItemRegiPrice + regiPrice
            this.billingProvider.receivedRegi = this.relatedItemRegiPrice
            console.log("REGISTRATION PRICE 0 ", this.relatedItemRegiPrice)
            //gst
            this.gstCalculator()
            // to pay
            this.toPayAmount = ((this.totalBill + this.relatedItemRegiPrice) - this.discountAmount) + this.gstAmount;

        }
        else {
            console.log("TOTALBILL ELSE")

            console.log("RECIEVED TOTAL", this.totalBill)
            this.addedAmount = this.totalBill + this.singleItemPrice;
            this.totalBill = this.addedAmount;
            this.billingProvider.total(this.totalBill);
            this.totalBill = this.billingProvider.total(this.totalBill)

            console.log("RECEIVED TOTAL AFTER ADDITION", this.totalBill)
            //regi
            var receiveRegi = this.relatedItemRegiPrice + regiPrice
            this.relatedItemRegiPrice = receiveRegi
            console.log("REGISTRATION PRICE else ", this.relatedItemRegiPrice)
            //gst
            this.gstCalculator()
            //to pay
            this.toPayAmount = ((this.totalBill + this.relatedItemRegiPrice) - this.discountAmount) + this.gstAmount;
        }

        this.localRelatedArray.push(this.RelArray[this.index]);
        console.log("LOCAL ARRAY", this.localRelatedArray)
    }

    //REmoving related items from array
    removeRelatedItems(items, i) {
        this.index = i;
        //when total turns to 0, initialise all values to 0 or else bill will consists of negative value
        if (this.totalBill == 0) {
            console.log()
            this.totalBill = 0;
            this.relatedItemRegiPrice = 0;
            var dis = 0
            this.discountAmount = this.billingProvider.discount(dis)
            this.substractedAmount = items.ProductPrice;
            var sub = this.totalBill - this.substractedAmount
            this.totalBill = sub;
            //registration charges
            var removeRegiPrice = items.ProductTax[0].ProductRegistration;
            var subRegiPrice = this.relatedItemRegiPrice - removeRegiPrice;
            this.relatedItemRegiPrice = subRegiPrice
            console.log("REMOVE RELATED 0", this.relatedItemRegiPrice)

        }
        else {
            this.substractedAmount = items.ProductPrice;
            var sub = this.totalBill - this.substractedAmount
            this.totalBill = sub;
            //registration charges
            var removeRegiPrice = items.ProductTax[0].ProductRegistration;
            var subRegiPrice = this.relatedItemRegiPrice - removeRegiPrice;
            this.relatedItemRegiPrice = subRegiPrice
            if (this.totalBill == 0) {
                console.log("INSIDE 000")
                this.discountAmount = 0;
                this.gstAmount = 0;
                this.booleanTitle = undefined; //de - initialising variables.
                this.booleanEMPcoupons = undefined;
                this.translateService.get('APPLY_COUPON_TS').subscribe(
                    value => {
                        this.couponDetailedTitle = value;
                    }
                )
            }
        }

        this.gstCalculator()
        this.localRelatedArray.splice(this.index, 1); //removing item
    }

    //start date label click - customisable date
    startsOn() {
        let DateModal = this.modalController.create(CartDateModalPage);
        DateModal.present();
    }

    //calculate gst 5%;
    gstCalculator() {
        let gst = ((this.totalBill + this.relatedItemRegiPrice) - this.discountAmount) * 0.05;
        this.gstAmount = gst;
        this.toPayAmount = ((this.totalBill + this.relatedItemRegiPrice) - this.discountAmount) + this.gstAmount;
    }

    //applying coupon discountss
    discountCalculator() {
        this.discountAmount = this.itemReceived.OfferDiscount
    }



    minusITEM(Rindex) {
        if (this.myArray[Rindex] > 1) {
            let temp = this.myArray[Rindex];
            temp--;
            this.myArray[Rindex] = temp;
            console.log("minusssssss", this.myArray[Rindex])
        }
        else {
            this.myArray[Rindex] = 1;
        }
    }
    addITEM(Aindex) {
        let temp = this.myArray[Aindex];
        temp++;
        this.myArray[Aindex] = temp;
        console.log("plussssssss", this.myArray[Aindex])

    }


    // timerFun() {
    //     let renewModal = this.modalController.create("TimeUpModalPage");
    //     let countdown = 100;
    //     this.countdownNumberEl = countdown;
    //     setInterval(() => {
    //         //countdown = --countdown <= 0 ? 100 : countdown;
    //         countdown = --countdown
    //         this.countdownNumberEl = countdown;
    //         if(countdown == 0)
    //         { 
    //             renewModal.present();
    //             setInterval(() => {
    //                 this.navCtrl.setRoot("DashboardPage")
    //         }, 5000);
    //         }
    //     }, 1000);
    // }
    timerFun() {
        if (this.appFlowCart || this.facilityFlowCart) {
            this.startTimer(100);
        }
    }

    startTimer(timeoutt) {

        // if(this.appFlowCart|| this.facilityFlowCart)
        // {
        let renewModal = this.modalController.create("TimeUpModalPage");
        this.timer = TimerObservable.create(100, 1000).subscribe(t => {
            // let countdown = 100;
            // let timeoutt = 100;
            if (timeoutt == 0) {
                this.timer.unsubscribe();
            }
            else {
                this.countdownNumberEl = timeoutt;
                timeoutt = --timeoutt
            }


            /*  
              if (timeoutt == 0) {
                  renewModal.present();
                  setTimeout(() => {
                      this.navCtrl.setRoot("DashboardPage")
                  }, 5000);
                  this.timer.unsubscribe();
              }*/

        });
        // }
    }


    getCartCount() {
        this.CBP.getBill().then(val => {
            console.log((val));
        });
    }




    /* storing cart itm into a local storage into an array */
    storeCartItem() {

        switch (true) {
            case this.offerFlowCart:
                this.cartStorage = this.cartAddedItem;
                console.log(this.cartStorage);
                console.log(this.offerFlowCart);
                break;

            case this.membershipFlowCart:
                console.log(this.membershipFlowCart);
                break;

            case this.freezeFlowCart:
                console.log(this.freezeFlowCart);
                break;

            case this.updgradeFlowCart:

                console.log(this.updgradeFlowCart);
                break;

            case this.renewFlowCart:

                console.log(this.renewFlowCart);
                break;

            case this.classFLowCart:

                console.log(this.classFLowCart);
                break;

            case this.appFlowCart:
                console.log(this.appFlowCart);
                break;
        }

    }




    // if (timeoutt <= 0) {
    //     renewModal.present();

    //     this.navCtrl.push(DashboardPage);
    //     console.log(this.billingProvider.receivedTotal, "THIS IS RECEIVED IN apply")
    // }

    // else if (this.cancelBoolean == true && this.tick >= 1) { //if cancel button is pressed then stay on coupon list page
    //     this.timer.unsubscribe();
    //     this.dis = 0;
    //     this.billingProvider.discount(this.dis)
    //     this.navCtrl.pop()
    // }

    // });
}