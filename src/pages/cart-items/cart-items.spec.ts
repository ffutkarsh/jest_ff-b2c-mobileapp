// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, style } from '@angular/core';
// import { CartItemsPage } from './cart-items';
// import { IonicModule, Platform, NavController, NavParams, Item } from 'ionic-angular/index';
// import { HttpModule } from '@angular/http';
// import { CartItemsProvider, CartCouponsProvider, RelatedItemsProvider, BillingProvider } from '../../providers/providers';
// import {  HttpClientModule } from '@angular/common/http';

// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { MomentModule } from 'angular2-moment';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// //The describe function is for grouping related specs
// describe('---------CART PAGE PAGE UI----------', () => {
//     let de: DebugElement;
//     let comp: CartItemsPage;
//     let fixture: ComponentFixture<CartItemsPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [CartItemsPage],
//             imports: [
//                  HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild(),
//                 MomentModule,
//                 IonicModule.forRoot(CartItemsPage),
//             ],
//             providers: [
//                 CartItemsProvider,
//                 CartCouponsProvider,
//                 RelatedItemsProvider,
//                 TranslateService,
//                 TranslateStore,
//                 BillingProvider,
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(CartItemsPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });
//     //Creating the main Component
//     it('should create component', () => expect(comp).toBeDefined());

//     //checking the content
//     it('checking the content using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.contentClass')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('contentClass');
//     });



//     //ITEMS DETAILS ----------

// });





// //ITEMS DETAILS ----------
// //The describe function is for grouping related specs
// describe('---------CART ITEM UI----------', () => {
//     let de: DebugElement;
//     let comp: CartItemsPage;
//     let fixture: ComponentFixture<CartItemsPage>;
//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [CartItemsPage],
//             imports: [
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild(),
//                 MomentModule,
//                 IonicModule.forRoot(CartItemsPage),
//             ],
//             providers: [
//                 CartItemsProvider,
//                 CartCouponsProvider,
//                 RelatedItemsProvider,
//                 TranslateService,
//                 TranslateStore,
//                 BillingProvider,
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(CartItemsPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //Creating the main Component
//     // it('should create component', () => expect(comp).toBeDefined());

//     //ITEMS DETAILS ----------
//     //checking the content
//     it('checking the List using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.listClass')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('listClass');
//     });

//     //Checking the item tag using class name
//     it('checking the item of list using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.list1')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('list1');
//         });
//     });

//     //Checking the grid tag using class name
//     it('checking the grid of item using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.ItemGrid')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('ItemGrid');
//         });
//     });


//     //Checking the row tag using class name
//     it('checking the row of item using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.mainRow')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('mainRow');
//         });
//     });

//     //Checking the column containing avatar tag of item using class name'
//     it('checking the column containing avatar tag of item using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.avatarCol')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('avatarCol');
//         });
//     });
//     //checking the avatar tag of item using class name
//     it('checking the avatar tag of item using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.avatarC')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('avatarC');
//         });
//     });

//     //checking the image tag of item using class name
//     it('checking the image tag of item using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.img')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('img');
//         });
//     });

//     //checking the session column  of item using class name which consists of item title
//     it('checking the session title column tag using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.sessionCol')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('sessionCol');
//         });
//     });

//     //checking the label tag of item using class name
//     it('checking the labrl tag of item using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.labelTitle')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('labelTitle');
//         });
//     });
//     //checking the row tag of item using class name which consists of start training details
//     it('checking the row tag of item using class name which consists of start training details', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.startsOn')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('startsOn');
//         });
//     });

//     //checking the label tag of item using class name which consists of start training details
//     it('checking the label tag of item using class name which consists of start training details', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.startsLabel')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('startsLabel');
//         });
//     });
//     //checking the plus button  of item using class name
//     it('checking the plus button  of item using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.plusButton')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('plusButton');
//         });
//     });
//     //checking the input tag of item using class name
//     it('checking the input tag of item using class name ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.inputbox')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('inputbox');
//         });
//     });
//     //checking the minus button  of item using class name
//     it('checking the minus button  of item using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.minusButton')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('minusButton');
//         });
//     });

//     //checking the price of item  using class name
//     it('checking the price of item  using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.priceCol')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('priceCol');
//         });
//     });

//     //checking the price of item  using class name
//     it('checking the price of item  using label', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.itemPrice')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('itemPrice');
//         });
//     });


//     //checking the close icon  using class name
//     it('checking the close icon  using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.closeIcon')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('closeIcon');
//         });
//     });


//     //checking the close icon  name using name attribute
//     it('checking the close icon  name using name attribute', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.closeIcon')).nativeElement;
//             expect(inputfield.getAttribute('name')).toMatch('close-circle');
//         });
//     });


//     //checking the close icon  color using color attribute
//     it('checking the close icon  color using color attribute', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.closeIcon')).nativeElement;
//             expect(inputfield.getAttribute('color')).toMatch('danger');
//         });
//     });


// });



// //The describe function is for grouping related specs
// describe('---------RELATED ITEMS  UI----------', () => {
//     let de: DebugElement;
//     let comp: CartItemsPage;
//     let fixture: ComponentFixture<CartItemsPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [CartItemsPage],
//             imports: [
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild(),
//                 MomentModule,
//                 IonicModule.forRoot(CartItemsPage),
//             ],
//             providers: [
//                 CartItemsProvider,
//                 CartCouponsProvider,
//                 RelatedItemsProvider,
//                 TranslateService,
//                 TranslateStore,
//                 BillingProvider,
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(CartItemsPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });


//     //checking the DIV Tag
//     it('checking the related items main tag of div using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.backgroundGrey1')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('backgroundGrey1');
//     });


//     //checking the label Tag
//     it('checking the label tag using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.relatedLikes')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('relatedLikes');
//     });

//     //checking the main slide tag
//     it('checking the main slide tag using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.mainSlides')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('mainSlides');
//     });

//     //checking the main slide tag
//     it('checking the main slide tag slide per view using class attribute', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.mainSlides')).nativeElement;
//         expect(contentClass.getAttribute('slidesPerView')).toMatch('4');
//     });

//     //checking the main slide tag
//     it('checking the main slide tag space between using class attribute', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.mainSlides')).nativeElement;
//         expect(contentClass.getAttribute('spaceBetween')).toMatch('20');
//     });

//     //checking the slide 
//     it('checking the slide tag ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.slide')).nativeElement;
//             expect(inputfield.getAttribute('slide')).toMatch('slide');
//         });
//     });


//     //checking the close icon  color using color attribute
//     it('checking the div tag of slide tag ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.slide1')).nativeElement;
//             expect(inputfield.getAttribute('slide1')).toMatch('slide1');
//         });
//     });

//     //checking the add button on slide  
//     it('checking the add button on slide  ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.addRelatedItem')).nativeElement;
//             expect(inputfield.getAttribute('addRelatedItem')).toMatch('addRelatedItem');
//         });
//     });


//     //checking the image icon on slide
//     it('checking the image icon on slide  ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.imgIcons')).nativeElement;
//             expect(inputfield.getAttribute('imgIcons')).toMatch('imgIcons');
//         });
//     });


//     //checking the div which holds item price 
//     it('checking the div which holds item price  ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.relatedPrice')).nativeElement;
//             expect(inputfield.getAttribute('relatedPrice')).toMatch('relatedPrice');
//         });
//     });


//     //checking the div which holds item price 
//     it('checking the label which holds item title  ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.relatedTitles')).nativeElement;
//             expect(inputfield.getAttribute('relatedTitles')).toMatch('relatedTitles');
//         });
//     });



// });



// //The describe function is for grouping related specs
// describe('---------CART PAGE COUPONS UI----------', () => {
//     let de: DebugElement;
//     let comp: CartItemsPage;
//     let fixture: ComponentFixture<CartItemsPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [CartItemsPage],
//             imports: [
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild(),
//                 MomentModule,
//                 IonicModule.forRoot(CartItemsPage),
//             ],
//             providers: [
//                 CartItemsProvider,
//                 CartCouponsProvider,
//                 RelatedItemsProvider,
//                 TranslateService,
//                 TranslateStore,
//                 BillingProvider,
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(CartItemsPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });




//     //checking the ROW Tag
//     it('checking the row tag using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.row4')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('row4');
//     });


//     //checking the COL Tag
//     it('checking the column tag inside the row using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.row4col1')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('row4col1');
//     });

//     //checking the inner ROW Tag
//     it('checking the inner row tag using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.item1')).nativeElement;
//             expect(contentClass.getAttribute('class')).toMatch('item1');
//         });
//     });


//     //checking the column tag using class name for vertical strips
//     it('checking the column tag using class name for vertical strips', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.verticalstrip')).nativeElement;
//             expect(contentClass.getAttribute('class')).toMatch('verticalstrip');
//         });
//     });

//     //checking the icon tag using class name
//     it('checking the icon tag using class name ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.iconItem')).nativeElement;
//             expect(contentClass.getAttribute('class')).toMatch('iconItem');
//         });
//     });

//     //checking the label tag using class name which holds item title
//     it('checking the label tag using class name which holds item title ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.itemlabel1')).nativeElement;
//             expect(contentClass.getAttribute('class')).toMatch('itemlabel1');
//         });
//     });

//     //checking the image tag using class name which holds arrow icon
//     it('checking the image tag using class name which holds arrow icon', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.arrowClass')).nativeElement;
//             expect(contentClass.getAttribute('class')).toMatch('arrowClass');
//         });
//     });

//     //checking the div tag using class name which makes the background grey
//     it('checking the div tag using class name which makes the background grey', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.backgroundGrey')).nativeElement;
//             expect(contentClass.getAttribute('class')).toMatch('backgroundGrey');
//         });
//     });

// });



// //The describe function is for grouping related specs
// describe('---------CART PAGE BILLING UI----------', () => {
//     let de: DebugElement;
//     let comp: CartItemsPage;
//     let fixture: ComponentFixture<CartItemsPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [CartItemsPage],
//             imports: [
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild(),
//                 MomentModule,
//                 IonicModule.forRoot(CartItemsPage),
//             ],
//             providers: [
//                 CartItemsProvider,
//                 CartCouponsProvider,
//                 RelatedItemsProvider,
//                 TranslateService,
//                 TranslateStore,
//                 BillingProvider,
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(CartItemsPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });




//     //cchecking the list header tag using class name
//     it('checking the list header tag using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.billingSummary')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('billingSummary');
//     });


//     // KEEPING THE TEST CASES PENDING- 
//     //I WILL IMPLEMENT BILLING SECTION TEXT CASES ONCE I FINISH BILLING PAGE.



// });


// // ITEMS DETAILS ----------
// // The describe function is for grouping related specs
// describe('---------CART ITEM MAIN PAGE - METHODS ----------', () => {
//     let de: DebugElement;
//     let comp: CartItemsPage;
//     let fixture: ComponentFixture<CartItemsPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [CartItemsPage],
//             imports: [
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild(),
//                 MomentModule,
//                 IonicModule.forRoot(CartItemsPage),
//             ],
//             providers: [
//                 CartItemsProvider,
//                 CartCouponsProvider,
//                 RelatedItemsProvider,
//                 TranslateService,
//                 BillingProvider,
//                 TranslateStore,
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));
//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(CartItemsPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //from json using its provider
//     it('from json using its provider - CART ITEMS', () => {
//         expect(comp.fetchCartItems).toBeTruthy();
//     });

//     //from json using its provider -- ITEMS YOU MAY LIKE
//     it('from json using its provider -- ITEMS YOU MAY LIKE', () => {
//         expect(comp.fetchCartItems).toBeTruthy();
//     });

//     //from json using its provider
//     it('from json using its provider - CART ITEMS', () => {
//         expect(comp.fetchCartItems).toBeTruthy();
//     });

//     //from json using its provider -- COUPON LIST
//     it('from json using its provider -- COUPON LIST', () => {
//         expect(comp.fetchCouponDetails).toBeTruthy();
//     });
//     //creating random color  -RGB
//     it('generating random color  -RGB', () => {
//         expect(comp.getColor).toBeTruthy();
//     });

//     //creating random color  - LIGHT
//     it('generating random color  - LIGHT', () => {
//         expect(comp.getColorLight).toBeTruthy();
//     });

//     //checking the method of setMyStyles for setting style as background color
//     it('checking the method of setMyStyles for setting style as background color ', () => {
//         expect(comp.setMyStyles1).toBeTruthy();
//     });
//     //checking the method of setMyStyles for setting style as color 
//     it('checking the method of setMyStyles for setting style as color ', () => {
//         expect(comp.setMyStyles2).toBeTruthy();
//     });

//     //checking the method of setMyStyles for setting style FOR FONT 
//     it('checking the method of setMyStyles for setting style for font ', () => {
//         expect(comp.setMyStylesFont).toBeTruthy();
//     });

//     //checking the method of setMyStyles for slides 
//     it('checking the method of setMyStyles for slides ', () => {
//         expect(comp.setMyStylesSlides).toBeTruthy();
//     });

//     //checking on click of coupon option- go to coupon page
//     it('checking the method of on click of coupon option- go to coupon page ', () => {
//         expect(comp.goToCouponPage).toBeTruthy();
//     });

//     //checking the method of deleting offer on cross clcik
//     it('checking the method of deleting offer on cross clcik ', () => {
//         expect(comp.deleteOffer).toBeTruthy();
//     });

//     //checking the method of Opening employee modal
//     it('checking the method of Opening employee modal ', () => {
//         expect(comp.openEmpModal).toBeTruthy();
//     });

//     //after billing -- 
//     //checking the method of add items
//     it('checking the method of adding items in cart ', () => {
//         expect(comp.addItemsYouLike).toBeTruthy();
//     });

//     //checking the method of addItemsYouLike to be called
//     it('checking the method of addItemsYouLike to be called ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             var i;
//             var c;
//             expect(comp.addItemsYouLike).toHaveBeenCalled();
//         });
//     });

//     //if offer is deleted then make total bill as undefined.
//     it('checking the method of addItemsYouLike by passing arguments ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             var i;
//             var c;
//             expect(comp.addItemsYouLike.apply(this, [i, c]));
//         });
//     });

//     //checking the method of removing items from cart
//     it('checking the method of removing items from cart ', () => {
//         expect(comp.removeRelatedItems).toBeTruthy();
//     });
//     //checking the method of removeRelatedItems by passing arguments
//     it('checking the method of removeRelatedItems by passing arguments ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             var i;
//             var c;
//             expect(comp.removeRelatedItems.apply(this, [i, c]));
//         });
//     });


//     //checking the method of removeRelatedItems to be called
//     it('checking the method of removeRelatedItems to be called ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             var i;
//             var c;
//             expect(comp.removeRelatedItems).toHaveBeenCalled();
//         });
//     });

//     //checking the method of gstCalculator
//     it('checking the method of gstCalculator ', () => {
//         expect(comp.gstCalculator).toBeTruthy();
//     });

//     //checking the method of discountCalculator
//     it('checking the method of discountCalculator ', () => {
//         expect(comp.discountCalculator).toBeTruthy();
//     });

//     //calculating gst 
//     it('checking the gst logic ', () => {
//         var gst = 0.05
//         var total = 100
//         var afterGst = total * gst
//         expect(afterGst).toBeCloseTo(5);
//     });

//     //calculating gst 
//     it('checking the discount logic ', () => {
//         var discount = 50
//         var total = 100
//         var afterDis = total - discount
//         expect(afterDis).toBeCloseTo(50);
//     });



// });






// //The describe function is for grouping related specs
// describe('---------CART ITEM MAIN PAGE - VARIABLES ----------', () => {
//     let de: DebugElement;
//     let comp: CartItemsPage;
//     let fixture: ComponentFixture<CartItemsPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [CartItemsPage],
//             imports: [
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild(),
//                 MomentModule,
//                 IonicModule.forRoot(CartItemsPage),
//             ],
//             providers: [
//                 CartItemsProvider,
//                 CartCouponsProvider,
//                 RelatedItemsProvider,
//                 TranslateService,
//                 BillingProvider,
//                 TranslateStore,
//                 NavController, { provide: NavParams, useClass: NavParamsMock },]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(CartItemsPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     // //to fetch CART ITEMSl
//     // it('checking the array to fetch CART ITEMS ', () => {
//     //     expect(comp.cartItems).toBeUndefined();
//     // });

//     //to fetch related ITEMSl
//     // it('checking the array to fetch IteMS YOU MAY LIKE', () => {
//     //     expect(comp.relatedItems).toBeTruthy();
//     // });

//       //to fetch related ITEMS IN OTHER ARRAY
//     it('checking the array to fetch IteMS YOU MAY LIKE', () => {
//         expect(comp.RelArray).toBeTruthy();
//     });
//     //to fetch index
//     it('checking the index 1', () => {
//         expect(comp.index1).toBeUndefined();
//     });

//     //to fetch index
//     it('checking the  index 2', () => {
//         expect(comp.index2).toBeUndefined();
//     });

//     //to fetch index
//     it('checking the  index ', () => {
//         expect(comp.index).toBeUndefined();
//     });


//     //tstoring index from coupon page
//     it('storing index from coupon page ', () => {
//         expect(comp.indexFromCoupon).toBeUndefined();
//     });
//     //boolean to display if else
//     it('boolean to display if else ', () => {
//         expect(comp.booleanTitle).toBeTruthy();
//     });
//     //boolean to check json flag
//     it('boolean to check json flag', () => {
//         expect(comp.booleanEMPcoupons).toBeTruthy();
//     });

//     //var to store default title.
//     it('var to store default title.', () => {
//         expect(comp.couponDetailedTitle).toBeUndefined();
//     });
//     //storing single coupon details
//     it('storing single coupon details ', () => {
//         expect(comp.oneCouponRecord).toBeUndefined();
//     });
//     //checking Array of coupon list to store json file.
//     it('checking Array of coupon list to store json file. ', () => {
//         expect(comp.couponDetails).toBeUndefined();
//     });
//     //Storing randomly generate color
//     it('checking Array OF randomly generate color ', () => {
//         expect(comp.colorarray).toBeTruthy();
//     });
//     //Storing randomly generate color
//     it('checking Array OF randomly generate light colors ', () => {
//         expect(comp.colorarrayLight).toBeTruthy();
//     });



//     //var to store single ietms price.
//     it('var to store single ietms price.', () => {
//         expect(comp.singleItemPrice).toBeUndefined();
//     });
//     //storing single coupon details
//     it('storing single coupon price ', () => {
//         expect(comp.singleItemPriceD).toBeUndefined();
//     });
//     //to store total bill in a variable
//     it('to store total bill in a variable. ', () => {
//         expect(comp.totalBill).toBe(0);
//     });
//     //to store gst amount in a variable
//     it('to store gst amount in a variable', () => {
//         expect(comp.gstAmount).toBeUndefined();
//     });
//     //to store substracted amount from total bill in a variable
//     it('to store substracted amount from total bill in a variable', () => {
//         expect(comp.substractedAmount).toBe(0);
//     });


//     //to store addedAmount in total bill in a variable.
//     it('to store addedAmount in total bill in a variable.', () => {
//         expect(comp.addedAmount).toBe(0);
//     });
//     //storing discount amount details 
//     it('storing discount amount details ', () => {
//         expect(comp.discountAmount).toBe(0);
//     });
//     //storeing single item registration price in a variabl
//     it('storeing single item registration price in a variable ', () => {
//         expect(comp.relatedItemRegiPrice).toBe(0);
//     });
//     //store the entire bill object for comparison
//     it('store the entire bill object for comparison', () => {
//         expect(comp.storeBillingObject).toBeUndefined();
//     });

//     //total amount to be paid
//     it('total amount to be paid ', () => {
//         expect(comp.toPayAmount).toBe(0);
//     });

//     //storing boolean to check related array
//     it('storing boolean to check related array ', () => {
//         expect(comp.addRelatedBoolean).toBeFalsy();
//     });
 

// });