import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CartItemsPage } from './cart-items';
import { TranslateModule } from '@ngx-translate/core';
import { MomentModule } from 'angular2-moment';
import { NumeralPipe, NumeralModule } from 'ngx-numeral';

@NgModule({
  declarations: [
   CartItemsPage,
  ],
  imports: [
  IonicPageModule.forChild(CartItemsPage),
    TranslateModule, 
    MomentModule,
  NumeralModule.forRoot()
  
  ],
})
export class CartItemsPageModule {}
