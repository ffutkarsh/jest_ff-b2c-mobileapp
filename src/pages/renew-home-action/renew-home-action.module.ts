import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RenewHomeActionPage } from './renew-home-action';
import { FetcherProvider } from '../../providers/fetcher/fetcher';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    RenewHomeActionPage,
  ],
  imports: [
    IonicPageModule.forChild(RenewHomeActionPage),
    TranslateModule
  ],
  providers:[
    FetcherProvider
  ]
})
export class RenewHomeActionPageModule {}
