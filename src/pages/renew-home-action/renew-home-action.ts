import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FetcherProvider } from '../../providers/fetcher/fetcher';
import { get } from 'selenium-webdriver/http';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-renew-home-action',
  templateUrl: 'renew-home-action.html',
})
export class RenewHomeActionPage {
  renewMembership: any;
  city = [];
  renewPageData: any;
  renewhomeData;
  setofferLocally;
  constructor(public navCtrl: NavController, public navParams: NavParams, public fetcher: FetcherProvider, public viewCtrl: ViewController, private storage: Storage) {
    this.getcity();

    this.renewMembership = navParams.get("renew");
    console.log(this.renewMembership);
    this.renewPageData = navParams.get('renew');
    this.renewhomeData = this.navParams.get("renewJsonData")[0];
    console.log(this.navParams.get("renewJsonData"));
    console.log(this.renewPageData);


  }

  ionViewDidLoad() {
    this.renewPageData = this.navParams.get('renew');
    this.navParams.get("renewJsonData");
    console.log('ionViewDidLoad RenewActionModalPage');
  }


/// Method for fetching details of city list 
  getcity() {
    this.fetcher.getCities().map((response) => response.json()).subscribe((response) => {
      this.city = response;
      console.log("citylist------", this.city);
    });
  }
  
   //  method to navigate to Cart page
  goToCart(amount) {

    let obj = {
      expiry: this.renewhomeData.expiry,
      frozenicon: this.renewhomeData.frozenicon,
      frozenstatus: this.renewhomeData.frozenstatus,
      location: this.renewhomeData.location,
      purposedescription: this.renewhomeData.purposedescription,
      totalCost: amount,
      totalSessions: this.renewhomeData.totalSessions,
      usedSessions: this.renewhomeData.usedSessions,
      quantity: 1,
      type: "renewFrozen"
    };

    this.storage.get('CARTITEM').then((val) => {
      this.setofferLocally = val;
    }).then(
      () => {
        if ((this.setofferLocally) || (this.setofferLocally = [])) {

          this.setofferLocally.push(obj);
          this.storage.set('CARTITEM', this.setofferLocally);
        }
      });

    this.navCtrl.push("CartItemsPage",
      {
        'renewFlowCart': true,
      });
  }


  // Method to close the modal
  closeModal() {
    this.viewCtrl.dismiss();
  }
}
