// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, ViewChild } from '@angular/core';
// import {RatingWidgetModalPage} from './rating-widget-modal';
// import { IonicModule, Platform, NavController, NavParams , ViewController} from 'ionic-angular/index';
// import { StatusBar } from '@ionic-native/status-bar';
// import { SplashScreen } from '@ionic-native/splash-screen';
// import { Http, ConnectionBackend } from '@angular/http';
// import { IonicStorageModule, Storage } from '@ionic/storage';
// import { Grid, Col, Row } from 'ionic-angular/umd';
// import { HttpModule } from '@angular/http';
// import { TranslatePipe } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { TranslateService } from '@ngx-translate/core';
// import {FetcherProvider} from "../../providers/fetcher/fetcher"
// import { mockApp, mockConfig, mockPlatform, mockView } from "ionic-angular/util/mock-providers";
// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// // The describe function is for grouping related specs
// describe('Rating Modal Page UI Testing', () => {
//     let comp: RatingWidgetModalPage;
//     let fixture: ComponentFixture<RatingWidgetModalPage>;
//     let de: DebugElement;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [RatingWidgetModalPage],
//             imports: [
//                 HttpModule,
//                 IonicModule.forRoot(RatingWidgetModalPage),
//                 IonicStorageModule.forRoot(),
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 FetcherProvider,
//                 { provide: ViewController, useValue: mockView() },

//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(RatingWidgetModalPage); /////TESTBED CREATES THE OBJECT OF THE CLASSDETAILS  PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance;   //////CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//     });

//     ///// Test Case for Header Part
//     it("Main content should be checked", async () => {
//         fixture = TestBed.createComponent(RatingWidgetModalPage);
//         de = fixture.debugElement.query(By.css(".maincontent")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Ratingcard should be checked", async () => {
//         fixture = TestBed.createComponent(RatingWidgetModalPage);
//         de = fixture.debugElement.query(By.css(".ratingcard")).nativeElement;
//         expect(de).toBeDefined();
//     });
//     it("Rating text should be present", async () => {
//         fixture = TestBed.createComponent(RatingWidgetModalPage);
//         de = fixture.debugElement.query(By.css(".ratingtxt")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Avatar icon should be present", async () => {
//         fixture = TestBed.createComponent(RatingWidgetModalPage);
//         de = fixture.debugElement.query(By.css(".avtrcol")).nativeElement;
//         expect(de).toBeDefined();
//     });

    
//     it("Image of trainer should be present", async () => {
//         fixture = TestBed.createComponent(RatingWidgetModalPage);
//         de = fixture.debugElement.query(By.css(".TrainerPhoto")).nativeElement;
//         expect(de).toBeDefined();
//     });

    
//     it("Date Time should be present", async () => {
//         fixture = TestBed.createComponent(RatingWidgetModalPage);
//         de = fixture.debugElement.query(By.css(".dateTime")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Stars Row should be present", async () => {
//         fixture = TestBed.createComponent(RatingWidgetModalPage);
//         de = fixture.debugElement.query(By.css(".rowStar")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Stars should be present", async () => {
//         fixture.whenStable().then(() => { 
//             fixture = TestBed.createComponent(RatingWidgetModalPage);
//             de = fixture.debugElement.query(By.css(".stargeneral")).nativeElement;
//             expect(de).toBeDefined();
//                          });
       
//     });

   

// });




// describe('Rating Modal Page Logic Testing', () => {
//     let comp: RatingWidgetModalPage;
//     let fixture: ComponentFixture<RatingWidgetModalPage>;
//     let de: DebugElement;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [RatingWidgetModalPage],
//             imports: [
//                 HttpModule,
//                 IonicModule.forRoot(RatingWidgetModalPage),
//                 IonicStorageModule.forRoot(),
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 FetcherProvider,
//                 { provide: ViewController, useValue: mockView() },

//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(RatingWidgetModalPage); /////TESTBED CREATES THE OBJECT OF THE CLASSDETAILS  PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance;   //////CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//     });

//         it('Star Rating Render Function - 1', () => {
//             expect(comp.starRating(2)).toContain('fulle.svg');
//         });

//         it('Star Rating Render Function - 2', () => {
//             expect(comp.starRating(3.5)).toContain('halfe.svg');
//         });

//         it('Star Rating Render Function - 3', () => {
//             expect(comp.starRating(5)).not.toContain('halfe.svg');
//         });

//         it('Star Rating Render Function - 4', () => {
//             expect(comp.starRating(100)).not.toContain('fullgrey.svg');
//         });

//         it('Star Rating Click Event', () => {
//             expect(comp.rate).toBeDefined();
//         }); 


// });

