import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { LoginFlowApiProvider } from '../../providers/login-flow-api/login-flow-api';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the RatingWidgetModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rating-widget-modal',
  templateUrl: 'rating-widget-modal.html',
})
export class RatingWidgetModalPage {
  public RATING;
  public datetime;
  public trainer;
  public class;
  constructor(public translateService: TranslateService,
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    public LFP: LoginFlowApiProvider
  ) 
    {
    this.RATING = 0; // Setting default value
    this.trainer = "Alton David";
    this.class = "Zumba Class";
    this.datetime = moment("09:00 AM , WED JAN 18 2017", "hh:mm A , ddd MMM DD YYYY").format('hh:mm A , ddd MMM DD YYYY');
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }


// ionViewWillEnter(){

//   // console.log(this.navCtrl.getActive().component.name);
//   // console.log(this.navCtrl.getActive().component.name);
//   // console.log(this.viewCtrl);
//   // console.log(this.navCtrl.getActive().id);
//   // console.log(this.viewCtrl.pageRef());

// }

  // Draws star rating as per the number (rating) passed
  starRating(rating: number) {
    let full;
    if (rating <= 5) {
      full = Math.floor(rating);
    }
    else {
      full = 5;
    }

    let status = [];
    for (let i = 0; i < full; i++) {
      status.push("fulle.svg"); // saves Full Star svg in array
    }
    if (rating - full == 0.5 && !(rating >= 5)) {
      status.push("halfe.svg"); // saves Half Star svg in array
    }
    if (status.length < 5) {
      let emp = 5 - status.length;
      for (let i = 0; i < emp; i++) {
        status.push("fullgrey.svg");// saves Empty Star svg in array
      }
    }

    return status; // Returns array so that it can be rendered using *ngFor
  }



  // Method used to change rating bar value , Takes inpur click event
  rate(e) {
    // Uses offsetX to check if click if on first or second half of the star
    if (Number(e.offsetX) > 22.5) {
      this.RATING = Number(e.srcElement.id) + 1; // Sets last Star as Full star
      this.viewCtrl.dismiss(this.RATING);
    }
    else {
      this.RATING = Number(e.srcElement.id) + 0.5; // Sets last Star as Half star
      this.viewCtrl.dismiss(this.RATING);
    }
    this.setRatingwidget();
    
      }
    
      /* set rating widget to local storage */
      setRatingwidget() {
        this.storage.get('WidgetRating').then((val) => {
          let rate = val;
          rate.splice(rate.length - 1, 1);
          console.log(rate);
          this.storage.set('WidgetRating', rate);
        });
        /********************END******************/
      }
//     this.setRatingwidget();

//   }

//   /* set rating widget to local storage */
//   setRatingwidget() {
//     this.LFP.RatinWidgetData[this.LFP.RatinWidgetData.length-1].ratingStar=this.RATING;
//  console.log(this.LFP.RatinWidgetData);
//  this.storage.set('WidgetRating', this.LFP.RatinWidgetData);



//   }
  /********************END******************/
}


