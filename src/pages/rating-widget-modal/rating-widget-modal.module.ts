import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RatingWidgetModalPage } from './rating-widget-modal';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    RatingWidgetModalPage,
  ],
  imports: [
    IonicPageModule.forChild(RatingWidgetModalPage),
    TranslateModule
  ],
})
export class RatingWidgetModalPageModule {}
