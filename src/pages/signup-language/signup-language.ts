import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { LanguageProvider } from '../../providers/language/language';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { MainPage } from '../../pages/pages';

@IonicPage()
@Component({
  selector: 'page-signup-language',
  templateUrl: 'signup-language.html',
})
export class SignupLanguagePage {

  public allLanguages; //TO STORE THE JSON RESPONSE.

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public language: LanguageProvider,
    public translateService: TranslateService,
    public storage: Storage) {
    this.menuCtrl.enable(false, 'myMenu'); //DISABLING MENU ON SIGNUPLANGUAGE PAGE.
    this.getLang(); //GETTING ALL THE LANGUAGES.
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupLanguagePage');
  }

  //ON LANGUAGE SELECTION MOVING TO LOGIN PAGE.
  gotoSignupMobile(selected) {
    console.log("Language Selected: " +
      this.allLanguages[selected].name1 + ", " +
      this.allLanguages[selected].name2 + ", " +
      this.allLanguages[selected].name3);

    this.translateService.use(this.allLanguages[selected].name3); //CALLING THE TRANSLATE SERVICE WITH THE HELP OF SELECTED LANGUAGE.

    //SETTING THE STATUS OF SELECTED LANGUAGE TO TRUE.
    for (let i = 0; i < this.allLanguages.length; i++) {
      if (i == selected)
        this.allLanguages[i].status = true;

      else
        this.allLanguages[i].status = false;
    }

    this.storage.set('signuplangShown', true); //SETTING KEY TO TRUE, ONCE THE INITIAL ROOT PAGE IS SHOWN.
    this.storage.set('langSelect', this.allLanguages[selected].name3); //SETTING THE KEY FOR SELECTED LANGUAGE TO BE USED THROUGHOUT THE APP.
    this.navCtrl.setRoot(MainPage); //ONCE THE 'SIGNUPLANGPAGE' KEY IS SET TO TRUE, SETTING THE ROOT PAGE AS THE LOGIN PAGE.
  }

  //FETCHING THE DATA FROM JSON.
  getLang() {
    this.language.getLanguages().map((response) => response).subscribe((response) => {
      this.allLanguages = response;
      console.log(this.allLanguages);
    });
  }

}
