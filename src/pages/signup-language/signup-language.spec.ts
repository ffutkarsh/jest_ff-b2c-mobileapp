// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from "@angular/http";
// import { Http, ConnectionBackend } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';
// import { SignupLanguagePage } from './signup-language';
// import { LanguageProvider } from '../../providers/language/language';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// describe('SignupLanguage Page - UI Part', () => {
//     let de: DebugElement;
//     let comp: SignupLanguagePage;
//     let fixture: ComponentFixture<SignupLanguagePage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [SignupLanguagePage],
//             imports: [
//                 IonicModule.forRoot(SignupLanguagePage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 LanguageProvider,
//                 TranslateService,
//                 TranslateStore
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(SignupLanguagePage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Page component should be created.", () => expect(comp).toBeDefined());

//     it("Ion content should be present.", async () => {
//         fixture = TestBed.createComponent(SignupLanguagePage);
//         de = fixture.debugElement.query(By.css(".MainContent")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section grid should be present.", async () => {
//         fixture = TestBed.createComponent(SignupLanguagePage);
//         de = fixture.debugElement.query(By.css(".HeadingGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section row should be present.", async () => {
//         fixture = TestBed.createComponent(SignupLanguagePage);
//         de = fixture.debugElement.query(By.css(".HeadingGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section column should be present.", async () => {
//         fixture = TestBed.createComponent(SignupLanguagePage);
//         de = fixture.debugElement.query(By.css(".HeadingGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section grid should contain the row for FitnessForce Logo.", async () => {
//         fixture = TestBed.createComponent(SignupLanguagePage);
//         de = fixture.debugElement.query(By.css(".FFLogoRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("FitnessForce Logo should be present.", async () => {
//         fixture = TestBed.createComponent(SignupLanguagePage);
//         de = fixture.debugElement.query(By.css(".FFLogo")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("FitnessForce Logo source should match.", async () => {
//         const imagefield: HTMLInputElement = fixture.debugElement.query(By.css('.FFLogo')).nativeElement;
//         expect(imagefield.getAttribute('src')).toMatch('assets/img/SignupLogo.png');
//     });

//     it("Heading Section grid should contain the “Welcome” text.", async () => {
//         fixture = TestBed.createComponent(SignupLanguagePage);
//         de = fixture.debugElement.query(By.css(".WelcomeText")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Progress Indicator grid should be present.'", () => {
//         fixture = TestBed.createComponent(SignupLanguagePage);
//         de = fixture.debugElement.query(By.css(".ProgressGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Progress Indicator row should be present.", async () => {
//         fixture = TestBed.createComponent(SignupLanguagePage);
//         de = fixture.debugElement.query(By.css(".ProgressGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Progress Indicator column should be present.", async () => {
//         fixture = TestBed.createComponent(SignupLanguagePage);
//         de = fixture.debugElement.query(By.css(".ProgressGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Progress Indicator should be present.", () => {
//         fixture = TestBed.createComponent(SignupLanguagePage);
//         de = fixture.debugElement.query(By.css(".ProgressIndicator")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Language List should be present.", async () => {
//         fixture = TestBed.createComponent(SignupLanguagePage);
//         de = fixture.debugElement.query(By.css(".LanguageList")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Language List should contain header.", async () => {
//         fixture = TestBed.createComponent(SignupLanguagePage);
//         de = fixture.debugElement.query(By.css(".LanguageListHeader")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Language List should contain items.", async () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             fixture = TestBed.createComponent(SignupLanguagePage);
//             de = fixture.debugElement.query(By.css(".ListItem")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("List item grid should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(SignupLanguagePage);
//             de = fixture.debugElement.query(By.css(".ListItemGrid")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("List item row should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(SignupLanguagePage);
//             de = fixture.debugElement.query(By.css(".ListItemRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("List items should have labels.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(SignupLanguagePage);
//             de = fixture.debugElement.query(By.css(".ItemLabel")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Check icon column should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(SignupLanguagePage);
//             de = fixture.debugElement.query(By.css(".CheckIconColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Check icon should be present on Language Selection.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(SignupLanguagePage);
//             de = fixture.debugElement.query(By.css(".CheckIcon")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

// });

// describe('SignupLanguage Page - Methods and Logic Part', () => {
//     let de: DebugElement;
//     let comp: SignupLanguagePage;
//     let fixture: ComponentFixture<SignupLanguagePage>;
//     let language: LanguageProvider;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [SignupLanguagePage],
//             imports: [
//                 IonicModule.forRoot(SignupLanguagePage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 LanguageProvider,
//                 TranslateService,
//                 TranslateStore
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(SignupLanguagePage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//         language = fixture.debugElement.injector.get(LanguageProvider); //INJECTING PROVIDER.
//     });

//     it('Instance of LanguageProvider created', () => {
//         expect(language instanceof LanguageProvider).toBe(true);
//     });

//     it('Ionviewdidload Method should be present.', () => {
//         expect(comp.ionViewDidLoad).toBeDefined(true);
//     });

//     it('Language Selection Method should be present.', () => {
//         expect(comp.gotoSignupMobile).toBeDefined(true);
//     });

//     it('Getting Languages from json method should be present.', () => {
//         expect(comp.getLang).toBeDefined(true);
//     });

// });



