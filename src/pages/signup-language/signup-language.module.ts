import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupLanguagePage } from './signup-language';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SignupLanguagePage
  ],
  imports: [
    IonicPageModule.forChild(SignupLanguagePage),
    TranslateModule
  ]
})
export class SignupLanguagePageModule { }
