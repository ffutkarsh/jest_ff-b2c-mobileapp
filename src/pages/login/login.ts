import { Component } from '@angular/core';
import {IonicPage,NavController,NavParams,AlertController,ModalController,MenuController,Events,LoadingController} from 'ionic-angular';
import { ValidPhoneProvider } from '../../providers/valid-phone/valid-phone';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { DashboardFlowAuthProvider } from '../../providers/dashboard-flow-auth/dashboard-flow-auth';
import { GeogetterProvider } from '../../providers/geogetter/geogetter';
import { Geolocation } from '@ionic-native/geolocation';
import { CountryModalPage } from "../country-modal/country-modal";
import { TranslateService } from '@ngx-translate/core';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import rootReducer from '../../reducers/LoginFlow';
import * as a from '../../actions/LoginAction';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { WelcomeScreenPage, DashboardPage, LoginErrorPage } from '../../pages/pages';
import { NetworkProvider } from '../../providers/network/network';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { Subject } from 'rxjs/Subject';
import { LoginFlowApiProvider } from '../../providers/login-flow-api/login-flow-api';
import {IpgeofetchProvider} from "../../providers/ipgeofetch/ipgeofetch"

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public memberDetails = {}; //STORING API RESPONSE OF MEMBER DETAILS.

  //LOGIN PAGE.
  public phoneNumber; //USER INPUT USED FOR BINDING.
  public verificationFlag: boolean; //VERIFICATION FLAG FOR USER. [SET TO TRUE ONCE THE USER IS VERIFIED I.E OTP IS VERIFIED]
  public memberType; //MEMBER TYPE OF USER. [GUEST MEMBER OR REGISTERED MEMBER].
  public permissions = []; //ARRAY OF ACCESSIBLE PAGES.

  //COUNTRY MODAL PAGE.
  public allCountries = []; //OBJECT THAT STORE THE DATA OF ALL COUNTRIES.
  public CountrySelected: string; //STORES THE DATA OF SELECTED COUNTRY.
  public CountrySelectedState: {}; //OBJECT THAT STORE THE OBJECT OF SELECTED COUNTRY.
  public defaultFlag: any; //STORE THE DEFAULT FLAG AS PER GEO LOCATION.
  public defaultCode //STORE THE DEFAULT COUNTRY CODE.
  public defaultDialcode: any; //STORE THE DEFAULT DIAL CODE AS PER GEO LOCATION.
  public defaultMax: any; //STORE THE MAX LENGTH OF PHONE NO.
  public defaultMin: any; //STORE THE MIN LENGTH OF PHONE NO.
  public defaultLength: string; //STORE THE ACTUAL FORMAT OF PHONE NO. LENGTH.
  public replacedDefaultLength: string; //STORING THE FORMAT OF PHONE NO. AFTER REPLACING TO AND COMMA IN ACTUAL PHONE NO. FORMAT BY SPACE.
  public replaceDefaultLengthArray: any; //STORING THE ACTUAL PHONE NO. LENGTH INTO DIGITS AFTER SPLITING THEM WITH THE HELP OF SPACE.
  public lengthType: string; //VARIABLE TO CHECK WHETHER THE PHONE NO. LENGTH FORMAT CONTAINS TO OR COMMA.
  public flag: boolean = false; //IF FLAG IS 1 MODAL WILL BE DISPLAYED AND IF FLAG IS 0 MODAL WILL NOT BE DISPLAYED.

  //DEFAULT COUNTRY SELECTION BASED ON GEO LOCATION.
  public lat; //LATITUDE.
  public long; //LONGITUDE.
  public loc; //LOCATION.

  //STRUCTURE OF INITIAL STATE OF USER OBJECT.
  public currentUserState = {
    dialCode: "",
    phoneNumber: "",
    centerName: "",
    subCenterName: "",
    enquiryType: "",
    memberType: "",
    userName: "",
    userPhoto: "",
    otp: "",
    verificationFlag: "",
    permissions: []
  };

  public alertSubTitle; //i18n VALUE FOR ALERT SUB TITLE.
  public alertButton; //i18n VALUE FOR ALERT BUTTON.

  //CHECKING NETWORK ABSENCE.
  public myObservable = new Subject<boolean>();
  public networkAvailable;
  public networkAlert;

  public loading;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private modalController: ModalController,
    public menuCtrl: MenuController,
    private validphone: ValidPhoneProvider,
    public authService: AuthServiceProvider,
    public dashboardFlowAuthService: DashboardFlowAuthProvider,
    public geo: GeogetterProvider,
    private geolocation: Geolocation,
    public translateService: TranslateService,
    private storage: Storage,
    public http: Http,
    public androidPermissions: AndroidPermissions,
    private openNativeSettings: OpenNativeSettings,
    public events: Events,
    public networkProvider: NetworkProvider,
    private loginFlowApi: LoginFlowApiProvider,
    public ipgeo:IpgeofetchProvider,
    public loadingCtrl: LoadingController) {

    this.menuCtrl.enable(false, 'myMenu'); //DISABLING MENU ON LOGIN PAGE.

    //INPUT FIELD ON LOGIN PAGE.
    this.phoneNumber = ""; //INITIALLY SETTING IT TO NULL.
    this.memberType = ""; //INITIALLY SETTING IT TO NULL.

    //CHECKING NETWORK ABSENCE.
    this.networkProvider.initializeNetworkEvents();//check network condition
    this.networkChangesSubscribe();
    this.networkAvailable = (this.networkProvider.isConnected() ? true : false);
    this.myObservable.next(this.networkAvailable);
    this.myObservable.subscribe(val => {
      if (!val) {
        this.showNetworkAlert();
      }
    });

    // this.platform.registerBackButtonAction(() => {
    //   if (this.navCtrl.canGoBack()) { // CHECK IF THE USER IS IN THE ROOT PAGE.
    //     this.navCtrl.pop(); // IF IT'S NOT THE ROOT, POP A PAGE.
    //   } else {
    //     this.platform.exitApp(); // IF IT'S THE ROOT, EXIT THE APP.
    //   }
    // });

  }

  ionViewDidLoad() {
  //  this.returnDialcode("IN"); //DEFAULT DIALCODE.
    this.getLatLong();
  }

  ionViewDidLeave() {
    this.networkChangesUnsubscribe(); //UNSUBSCRIBE THE CHANGES.
  }

  //FOR DISPLAYING COUNTRY MODAL PAGE.
  CountryModal() {

    let CountrySelectedStateFromMainPage = {
      CountrySelected: this.CountrySelected,
    };

    let CountryModal = this.modalController.create(CountryModalPage, CountrySelectedStateFromMainPage);

    //ON DISMISS OF COUNTRY MODEL PAGE THE SELECTED COUNTRY'S FLAG AND DIAL CODE GETS DISPLAYED ON LOGIN PAGE.
    CountryModal.onDidDismiss((CountrySelectedState) => {
      this.CountrySelectedState = CountrySelectedState;
      this.defaultFlag = "assets/img/Flag-Svg/" + CountrySelectedState.CountrySelected.img; //flag svg of selected country
      this.defaultDialcode = CountrySelectedState.CountrySelected.Dialcode; // country code of selected country
      this.defaultMax = CountrySelectedState.CountrySelected.maxlength; // max phone number length of selected country
      this.defaultMin = CountrySelectedState.CountrySelected.minlength; // min phone number length of selected country
      this.defaultLength = CountrySelectedState.CountrySelected.length; // in case only one length available
      this.defaultCode = CountrySelectedState.CountrySelected.code; // Country code of selected country
      this.lengthFormatChecking(this.defaultLength);
    });

    CountryModal.present(); //TO DISPLAY THE COUNTRY MODAL.

  }

  //WHEN PHONE NO. LENGTH IS GIVEN IN FORM OF SINGLE DIGIT.
  getPhoneLengthWhenInteger(defaultLength) {
    this.defaultLength = defaultLength;
    this.defaultMax = Number(this.defaultLength);
    this.defaultMin = Number(this.defaultLength);
  }

  //WHEN PHONE NO. LENGTH IS GIVEN IN FORM OF RANGE.
  getPhoneLengthWhenTo(defaultLength) {
    this.defaultLength = defaultLength;
    this.replacedDefaultLength = this.defaultLength.replace("to", "");
    this.replaceDefaultLengthArray = this.replacedDefaultLength.split('  ');
    let n1 = Number(this.replaceDefaultLengthArray[0]);
    let n2 = Number(this.replaceDefaultLengthArray[1]);
    this.defaultMax = n2;
    this.defaultMin = n1;
  }

  //WHEN PHONE NO. LENGTH IS GIVEN IN FORM OF COMMA SEPARATED DIGITS.
  getPhoneLengthWhenComma(defaultLength) {
    this.defaultLength = defaultLength;
    this.replacedDefaultLength = this.defaultLength.replace(/,/g, " ");
    this.replaceDefaultLengthArray = this.replacedDefaultLength.split('  ');
    for (let i = 0; i < this.replaceDefaultLengthArray.length; i++) {
      console.log(this.replaceDefaultLengthArray[i]);
    }
  }

  //WHEN PHONE NO. LENGTH IS GIVEN IN FORM OF BRACKETS CONTAINING PREFIXES.
  getPhoneLengthWhenBracket(defaultLength) {
    this.defaultLength = defaultLength;
    this.replacedDefaultLength = this.defaultLength.replace(/[()]/g, "");
    console.log(this.replacedDefaultLength);
    //CONVERTING INTO ARRAY.
    this.replaceDefaultLengthArray = this.replacedDefaultLength.split('+');
    //FOR CHECKING WHETHER THE PREFIX AND LENGTH GOT SEPARATED. E.G.[ (684)+7 ].
    for (let i = 0; i < this.replaceDefaultLengthArray.length; i++) {
      console.log(this.replaceDefaultLengthArray[i]); //FOR i=0 -> 684 & FOR i=1 -> 7.
    }
    //SETTING THE MAX LENTH AND MIN LENGTH ACCORDING TO NEW PHONE NUMBER LENGTH.
    this.getPhoneLengthWhenInteger(Number(this.replaceDefaultLengthArray[1]));
  }

  //CHECKING WHETHER PHONE NO. LENGTH IS GIVEN IN FORM OF RANGE OR COMMA SEPARATED DIGITS.
  lengthFormatChecking(defaultLength) {
    this.defaultLength = defaultLength;
    if (this.checkifNo(this.defaultLength)) {
      this.getPhoneLengthWhenInteger(this.defaultLength);
    }
    else if (this.defaultLength.includes("to")) {
      this.lengthType = "to";
      this.getPhoneLengthWhenTo(this.defaultLength);
    }
    else if (this.defaultLength.includes(",")) {
      this.lengthType = "comma";
      this.getPhoneLengthWhenComma(this.defaultLength);
    }
    else {
      this.getPhoneLengthWhenBracket(this.defaultLength);
    }
  }

  //TO TRIM THE EXTRA SPACES IN PHONE NUMBER AFTER FORMATTING.
  TrimSpace(no) {
    return no.replace(/\s/g, '')
  }

  //VALIDATING PHONE NO. WHOSE LENGTH IS GIVEN IN SINGLE DIGIT THAT IS GOT FROM COMMA SEPARATED DIGITS.
  validatePhone(n) {
    if (this.TrimSpace(this.phoneNumber).length == n)
      return true;
    return false;
  }

  //CHECK IF THE ENTERED CHARACTER IS A DIGIT.
  checkifNo(phoneNumber) {
    let reg = new RegExp('^\\d+$');
    return reg.test(phoneNumber);
  }

  //WILL SLICE THE INPUT IF IT IS CHARACTER.
  dynamic() {
    let a = this.phoneNumber.slice(-1);
    if (!this.checkifNo(a)) {
      this.phoneNumber = this.phoneNumber.substring(0, this.phoneNumber.length - 1);
    }
  }

  //DISPLAY ALERTS TO THE USER BASED ON THE ENTERED PHONE NO.
  showAlert() {
    //TRANSLATE SERVICE FOR SUB TITLE.
    this.translateService.get('INVALIDPHONENUMBER').subscribe(
      value => {
        this.alertSubTitle = value;
      }
    )
    //TRANSLATE SERVICE FOR OK BUTTON.
    this.translateService.get('OK').subscribe(
      value => {
        this.alertButton = value;
      }
    )
    //TO CHECK WHETHER PHONE NUMBER IS VALID OR NOT AND ACCORDINGLY SHOW ALERT TO THE USER. 
    if (this.phoneNumber.length == 0) {
      const alert = this.alertCtrl.create({
        subTitle: this.alertSubTitle,
        buttons: [this.alertButton]
      });
      alert.present();
      this.phoneNumber = "";
      return false;
    } //END OF 1ST IF OF SHOWALERT.
    //WHEN PHONE NO. LENGTH IS GIVEN IN FORM OF RANGE.
    else if (this.TrimSpace(this.phoneNumber).length < this.defaultMin || this.TrimSpace(this.phoneNumber).length > this.defaultMax) {
      let sub = (this.defaultMin == this.defaultMax) ? 'Phone No. should be of ' + this.defaultMax : 'Phone No. should be between ' + this.defaultMin + ' and ' + this.defaultMax;
      const alert = this.alertCtrl.create({
        subTitle: this.alertSubTitle,
        buttons: [this.alertButton]
      });
      alert.present();
      this.phoneNumber = "";
      return false;
    } //END OF 2ND IF OF SHOWALERT.
    //WHEN PHONE NO. LENGTH IS GIVEN IN FORM OF COMMA.
    else if (this.lengthType == "comma" && this.replaceDefaultLengthArray.length != 0) {
      for (let i = 0; i < this.replaceDefaultLengthArray.length; i++) {
        let n = Number(this.replaceDefaultLengthArray[i]);
        //MAX LENGTH OF PHONE NO. IN GIVEN ARRAY SHOULD NOT EXCEED 20 DIGITS.
        if (n > 20) {
          continue;
        }
        //IF THE LENGTH OF ENTERED PHONE NO. IS VALID.
        if (this.validatePhone(n)) {
          return;
        }
        //IF THE LENGTH OF ENTERED PHONE NO. IS NOT VALID.
        else {
          this.flag = false; //MAKE THE FLAG FALSE.
        }
      } //END OF FOR LOOP.
      //IF FLAG IS FALSE DISPLAY THE ALERT.
      if (this.flag == false) {
        const alert = this.alertCtrl.create({
          subTitle: this.alertSubTitle,
          buttons: [this.alertButton]
        });
        alert.present();
        this.phoneNumber = ""; //RESET THE PHONE NO. FIELD.
      }
    }//END OF 3RD IF OF SHOWALERT.
    //IF USER ENTERS A CHARACTER INSTEAD OF A DIGIT
    else if (!(this.checkifNo(this.TrimSpace(this.phoneNumber)))) {
      const alert = this.alertCtrl.create({
        subTitle: this.alertSubTitle,
        buttons: [this.alertButton]
      });
      alert.present();
      this.phoneNumber = "";
      return false;
    } //END OF 4TH IF OF SHOWALERT.
    else {
      this.addDialCode(); //ADDING DIALCODE TO CURRENT USER STATE.
      this.addPhoneNumber(); //ADDING PHONE NUMBER TO CURRENT USER STATE.
      this.getMemberDetails(this.defaultDialcode, this.phoneNumber); //FETCHING MEMBER DETAILS FROM API BASED ON PHONE NUMBER.
      return true;
    } //END OF IFS.
  } //END OF SHOWALERT.

  //ADDING DIALCODE TO CURRENT USER STATE.
  addDialCode() {
    this.currentUserState = rootReducer(this.currentUserState, a.addDialCode(this.defaultDialcode));
    console.log("Add DialCode: ");
    console.log(this.currentUserState);
    this.storelocally();
  }

  //ADDING PHONE NUMBER TO CURRENT USER STATE.
  addPhoneNumber() {
    this.currentUserState = rootReducer(this.currentUserState, a.addPhoneNumber(this.phoneNumber));
    console.log("Add Phone Number: ");
    console.log(this.currentUserState);
    this.storelocally();
  }

  //ADDING VERIFICATION FLAG TO CURRENT USER STATE.
  addVerificationFlag() {
    this.currentUserState = rootReducer(this.currentUserState, a.addVerificationFlag(this.verificationFlag));
    console.log("Add Verification Flag: ");
    console.log(this.currentUserState);
    this.storelocally();
  }

  //ADDING MEMBERTYPE TO CURRENT USER STATE.
  addMemberType() {
    this.currentUserState = rootReducer(this.currentUserState, a.addMemberType(this.memberType));
    console.log("Add Member Type: ");
    console.log(this.currentUserState);
    this.storelocally();
  }

  //ADDING PERMISSIONS TO CURRENT USER STATE.
  addPermissions() {
    this.currentUserState = rootReducer(this.currentUserState, a.addPermissions(this.permissions));
    console.log("Add Permissions: ");
    console.log(this.currentUserState);
    this.storelocally();
  }

  //SAVES DATA TO LOCAL DATABASE.
  storelocally() {
    this.storage.set('USERDETAILS', this.currentUserState).then((val) => {
      this.storage.get('USERDETAILS').then((val) => {
        console.log(val);
      });
    });
  }

  // Method fetches the country code based on geolocation after receiving reverse geocoded address from google location  
  returnDialcode(CountryCode) {
    this.validphone.getCountries().map((response) => response.json()).subscribe((response) => {
      this.allCountries = response;
      let c = this.searchCC(CountryCode);
      this.defaultDialcode = c[0].Dialcode;
    });
  }

  // Helps to fetch the country code
  searchCC(CountryCode) {
    return this.allCountries.filter(it => {
      return it.code.toUpperCase().includes(CountryCode);
    });
  }

  //DEFAULT COUNTRY SELECTION BASED ON GEO LOCATION.
  getLatLong() {

    // this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
    //   success => console.log('Permission granted'),
    //   err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
    // );

    // this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION]);

    // this.geolocation.getCurrentPosition().then((resp) => {
    //   this.lat = resp.coords.latitude;
    //   this.long = resp.coords.longitude;
    //   this.loc = this.geo.getLatLong(this.lat, this.long).map(res => res.json()).subscribe(data => {
    //     this.defaultFlag = "assets/img/Flag-Svg/" + (data.results[0].address_components[7].short_name).toLowerCase() + ".svg";
    //     this.returnDialcode((data.results[0].address_components[8].short_name));
    //   });
    // }).catch((error) => {
    //   console.log('Error getting location', error);
    // });

this.ipgeo.getLocfromIP().subscribe(data => {
     this.defaultFlag = "assets/img/Flag-Svg/" + (data.country).toLowerCase() + ".svg";
     this.returnDialcode(data.country);
     });;


  }

  //IF THE USER IS A GUEST - DIRECT IT TO DASHBOARD.
  gotoDashboard() {
    this.verificationFlag = false; //DIDNT WENT THROUGH LOGIN FLOW, SO OTP NOT VERIFIED.
    this.memberType = "Guest Member"; //ENTERED AS A GUEST.
    //ACCESSIBLE PAGES.
    this.permissions = [
      "DashboardPage",
      "OfferPage",
      "OfferDetailPage",
      "AboutPage",
      "FeedbackPage",
      "FeedbackFlowPage"
    ]
    this.storage.set('loggedIn', false); //USER NOT LOGGED IN.
    this.addVerificationFlag(); //ADDING VERIFICATION FLAG TO CURRENT USER STATE BASED ON MEMBER TYPE.
    this.addMemberType(); //ADDING MEMBER TYPE TO CURRENT USER STATE BASED ON MEMBER TYPE.
    this.addPermissions(); //ADDING PERMISSIONS TO CURRENT USER STATE BASED ON MEMBER TYPE.
    this.navCtrl.push(DashboardPage, {
      'verificationFlag': this.verificationFlag,
      'memberType': this.memberType,
      'permissions': this.permissions
    });
  }

  //SUBSCRIBE TO NETWORK CHANGES.
  networkChangesSubscribe() {
    // Offline event
    this.events.subscribe('network:offline', () => {  //method to check internet
      this.myObservable.next(false);
      this.networkAvailable = false;
    });

    // Online event
    this.events.subscribe('network:online', () => {
      this.myObservable.next(true);
      this.networkAvailable = true;
    });
  }

  //UNSUBSCRIBE TO NETWORK CHANGES.
  networkChangesUnsubscribe() {
    this.events.unsubscribe('network:offline');
    this.events.unsubscribe('network:online');
  }

  //SHOWING ALERT ON NETWORK ABSENCE.
  showNetworkAlert() {
    this.networkAlert = this.alertCtrl.create({
      title: 'No Internet Connection',
      message: 'Please check your internet connection.',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Retry',
          handler: () => {
            if (this.networkAvailable == false) {
              this.showNetworkAlert();
            }
          }
        },
        {
          text: 'Open Settings',
          handler: () => {
            this.networkAlert.dismiss().then(() => {
              this.showSettings();
              if (this.networkAvailable == false) {
                this.showNetworkAlert();
              }
            });
          }
        }
      ]
    });
    this.networkAlert.present();
  }

  //MOVING TO SETTINGS ON CLICKING SHOW SETTINGS.
  showSettings() {
    if ((<any>window).cordova && (<any>window).cordova.plugins.settings) {
      console.log('openNativeSettingsTest is active');
      (<any>window).cordova.plugins.settings.open("settings", function () {
        console.log('opened settings');
      },
        function () {
          console.log('failed to open settings');
        }
      );
    } else {
      console.log('openNativeSettingsTest is not active!');
    }
  }

  //FETCHING MEMBER DETAILS FROM API.
  getMemberDetails(defaultDialcode, phoneNumber) {
    if (defaultDialcode == "+91" && phoneNumber == 1002003000) {
      this.loginFlowApi.getMemberDetails().map((response) => response).subscribe((response) => {
        this.memberDetails = response;
        console.log("Fetched Member Details From Api: ");
        console.log(this.memberDetails);
        this.navCtrl.push(WelcomeScreenPage, {
          'defaultDialcode': this.defaultDialcode,
          'phoneNumber': this.phoneNumber,
          'memberDetails': this.memberDetails
        })
      });
    } else if (defaultDialcode != "+91" || phoneNumber != 1002003000) {
      this.navCtrl.push(LoginErrorPage, {
        'defaultDialcode': this.defaultDialcode,
        'phoneNumber': this.phoneNumber
      })
    }
  }

  goToBecomeMember() {
    console.log("Become a Member");
  }

}