// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from "@angular/http";
// import { Http, ConnectionBackend } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';
// import { LoginPage } from './login';
// import { ValidPhoneProvider } from '../../providers/valid-phone/valid-phone';
// import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
// import { DashboardFlowAuthProvider } from '../../providers/dashboard-flow-auth/dashboard-flow-auth';
// import { GeogetterProvider } from '../../providers/geogetter/geogetter';
// import { Geolocation } from '@ionic-native/geolocation';
// import { NativeGeocoder } from '@ionic-native/native-geocoder';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { AndroidPermissions } from '@ionic-native/android-permissions';
// import { OpenNativeSettings } from '@ionic-native/open-native-settings';
// import { NetworkProvider } from '../../providers/network/network';
// import { Network } from '@ionic-native/network';
// import { LoginFlowApiProvider } from '../../providers/login-flow-api/login-flow-api';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// describe('Login Page - UI Part', () => {
//     let de: DebugElement;
//     let comp: LoginPage;
//     let fixture: ComponentFixture<LoginPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [LoginPage],
//             imports: [
//                 IonicModule.forRoot(LoginPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 ValidPhoneProvider,
//                 AuthServiceProvider,
//                 DashboardFlowAuthProvider,
//                 GeogetterProvider,
//                 Geolocation,
//                 NativeGeocoder,
//                 TranslateService,
//                 TranslateStore,
//                 AndroidPermissions,
//                 OpenNativeSettings,
//                 NetworkProvider,
//                 Network,
//                 LoginFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(LoginPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Page component should be created.", () => expect(comp).toBeDefined());

//     it("Ion content should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".MainContent")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section grid should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".HeadingGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".HeadingGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".HeadingGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section grid should contain the row for FitnessForce Logo.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".FFLogoRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("FitnessForce Logo should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".FFLogo")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("FitnessForce Logo source should match.", async () => {
//         const imagefield: HTMLInputElement = fixture.debugElement.query(By.css('.FFLogo')).nativeElement;
//         expect(imagefield.getAttribute('src')).toMatch('assets/img/SignupLogo.png');
//     });

//     it("Heading Section grid should contain the “Welcome” text.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".WelcomeText")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Progress Indicator grid should be present.'", () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".ProgressGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Progress Indicator row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".ProgressGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Progress Indicator column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".ProgressGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Progress Indicator should be present.", () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".ProgressIndicator")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Sub Heading grid should be present.'", () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".SubHeadingGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Sub Heading row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".SubHeadingGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Sub Heading column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".SubHeadingGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Sub Heading should be present.", () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".SubHeading")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Input grid should be present.'", () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".inputBoxGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Input row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".inputBoxRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("CountrySelectButton column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".CountrySelectButtonColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("CountrySelectButton should be present.", () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".CountrySelectButton")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Country Flag should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(LoginPage);
//             de = fixture.debugElement.query(By.css(".Image")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Country Dialcode should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(LoginPage);
//             de = fixture.debugElement.query(By.css(".Dcodesize")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Drop Down Arrow should be present.", () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(LoginPage);
//             de = fixture.debugElement.query(By.css(".arrow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Phone Number column should be present.", () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".phone")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Ion Item for phone number should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".inputItem")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Input box for phone number should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".inputBox")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Continue button should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".continueButton")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Guest grid should be present.'", () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".GuestGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Guest grid row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".GuestGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Guest grid column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".GuestGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Continue as Guest row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".GuestRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("'Continue as Guest' text should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".SubHeading")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("OR row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".OrRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Become a Member button should be present.", async () => {
//         fixture = TestBed.createComponent(LoginPage);
//         de = fixture.debugElement.query(By.css(".memberButton")).nativeElement;
//         expect(de).toBeDefined();
//     });

// });

// describe('Login Page - Methods and Logic Part', () => {
//     let de: DebugElement;
//     let comp: LoginPage;
//     let fixture: ComponentFixture<LoginPage>;
//     let validphone: ValidPhoneProvider;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [LoginPage],
//             imports: [
//                 IonicModule.forRoot(LoginPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 ValidPhoneProvider,
//                 AuthServiceProvider,
//                 DashboardFlowAuthProvider,
//                 GeogetterProvider,
//                 Geolocation,
//                 NativeGeocoder,
//                 TranslateService,
//                 TranslateStore,
//                 AndroidPermissions,
//                 OpenNativeSettings,
//                 NetworkProvider,
//                 Network,
//                 LoginFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(LoginPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//         validphone = fixture.debugElement.injector.get(ValidPhoneProvider); //INJECTING PROVIDER.
//     });

//     it('Instance of ValidPhoneProvider created', () => {
//         expect(validphone instanceof ValidPhoneProvider).toBe(true);
//     });

//     it('Ionviewdidload Method should be present.', () => {
//         expect(comp.ionViewDidLoad).toBeDefined(true);
//     });

//     it('Language Selection Method should be present.', () => {
//         expect(comp.ionViewDidLeave).toBeDefined(true);
//     });

//     it('Country Fetching Method', () => {
//         expect(comp.CountryModal).toBeDefined(true);
//     });

//     it('Phone Number Range when given as Integer', () => {
//         expect(comp.getPhoneLengthWhenInteger).toBeDefined(true);
//     });

//     it('Phone Number Range when given as "To"', () => {
//         expect(comp.getPhoneLengthWhenTo).toBeDefined(true);
//     });

//     it('Phone Number Range when given as "Comma"', () => {
//         expect(comp.getPhoneLengthWhenComma).toBeDefined(true);
//     });

//     it('Phone Number Range when given as "Bracket"', () => {
//         expect(comp.getPhoneLengthWhenBracket).toBeDefined(true);
//     });

//     it('Phone Number Range checking format.', () => {
//         expect(comp.lengthFormatChecking).toBeDefined(true);
//     });

//     it('Method to trim the space should be present.', () => {
//         expect(comp.TrimSpace).toBeDefined(true);
//     });

//     it('Method to validate phone number should be present.', () => {
//         expect(comp.validatePhone).toBeDefined(true);
//     });

//     it('Method to check if the phone number contains only digits.', () => {
//         expect(comp.checkifNo).toBeDefined(true);
//     });

//     it('Method to slice the character if it is not a digit.', () => {
//         expect(comp.dynamic).toBeDefined(true);
//     });

//     it('Showing alert on incorrect number.', () => {
//         expect(comp.showAlert).toBeDefined(true);
//     });

//     it('Method to return the dialcode.', () => {
//         expect(comp.returnDialcode).toBeDefined(true);
//     });

//     it('Method to search the country.', () => {
//         expect(comp.searchCC).toBeDefined(true);
//     });

//     it('Method to get the Latitude and Longitudes', () => {
//         expect(comp.getLatLong).toBeDefined(true);
//     });

//     it('Validating Phone Number - dynamic() - Trimming Character', () => {
//         comp.phoneNumber = "123456a";
//         comp.dynamic();
//         expect(comp.phoneNumber).toMatch("123456");
//     });

//     it('Validating Phone Number - dynamic() - Trimming Space ', () => {
//         comp.phoneNumber = "123 4";
//         comp.dynamic();
//         expect(comp.phoneNumber).toMatch("123");
//     });

//     it('Validating Phone Number - checkifNo() - Valid Number ', () => {
//         comp.phoneNumber = "123456";
//         expect(comp.checkifNo(comp.phoneNumber)).toBe(true);
//     });

//     it('Validating Phone Number - checkifNo() - Invalid Number', () => {
//         comp.phoneNumber = "123456a$";
//         expect(comp.checkifNo(comp.phoneNumber)).toBe(false);
//     });

//     it('Phone Number Length Format (Single Integer)', () => {
//         comp.getPhoneLengthWhenInteger(10);
//         expect(comp.defaultMin == 10 && comp.defaultMax == 10).toBe(true);
//     });

//     it('Phone Number Length Format (to)', () => {
//         comp.getPhoneLengthWhenTo("5 to 10");
//         expect(comp.defaultMin == 5 && comp.defaultMax == 10).toBe(true);
//     });

//     it('Phone Number Length Format (comma)', () => {
//         comp.getPhoneLengthWhenComma("6, 8, 2009");
//         expect(comp.replaceDefaultLengthArray[0] == 6 && comp.replaceDefaultLengthArray[1] == 8 && comp.replaceDefaultLengthArray[2] == 2009).toBe(true);
//     });

//     it('Phone Number Length Format ()', () => {
//         comp.getPhoneLengthWhenBracket("(684)+7");
//         expect(comp.replaceDefaultLengthArray[0] == 684 && comp.replaceDefaultLengthArray[1] == 7).toBe(true);
//     });

//     it('Entered Number Length matches the actual length', () => {
//         expect(comp.phoneNumber == 1234567890).toBe(false);
//     });

//     it('Space in the phone number should be trimmed', () => {
//         comp.phoneNumber = '123 456';
//         expect(comp.TrimSpace(comp.phoneNumber)).toMatch('123456');
//     });

// });