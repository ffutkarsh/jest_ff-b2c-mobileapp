import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import 'rxjs/add/operator/map';

@IonicPage()
@Component({
  selector: 'page-in-app-browser',
  templateUrl: 'in-app-browser.html',
})
export class InAppBrowserPage {

  receivedlink: string;

  loader = this.loadingCtrl.create({
    content: "Please wait...",
    duration: 3000
  });

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private iab: InAppBrowser,
    private loadingCtrl: LoadingController) {

    console.log(this.navParams)
    this.receivedlink = this.navParams.get('link');
    //  this.load(0); //STARTING THE BROWSER.
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    this.loader.present();
   // this.presentLoading()
    this.load1(); //STARTING THE BROWSER.
 
  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad InAppBrowserPage');
  }

  //METHOD TO SHOW THE LOADER.
  presentLoading() {
    this.loader.present();
  }


  load1() {
    const browser = this.iab.create(this.receivedlink);
    browser.on('loadstart').subscribe((eve) => {
    
      browser.show();
    }, err => {
      this.navCtrl.pop();
    })

    browser.on('exit').subscribe(() => {
      this.navCtrl.pop();
    }, err => {
      this.navCtrl.pop();
    })

 


    







    // this.loader.present();
    // const browser = this.iab.create(this.receivedlink);
    // browser.on('loadstart').subscribe((eve) => {
    //   this.loader.dismiss();
    //  browser.show();
    // }, err => {
    //   browser.close();
    // })



    // browser.on('exit').subscribe(() => {
    //   this.navCtrl.pop();
    // }, err => {
    //   this.navCtrl.pop();
    // })

  }

  // load(close) {

  //   let browser = this.iab.create(this.receivedlink);
  //   browser.show(); //DISPLAYING THE BROWSER.
  //   if (close == 0) {
  //     //START THE LOADER.
  //     browser.on('loadstart').subscribe(event => {
  //       browser.hide(); //HIDING THE BROWSER.

  //    this.presentLoading(); //CALLING THE METHOD TO SHOW THE LOADER.

  //       //STOP THE LOADER.
  //       browser.on('loadstop').subscribe(event => {

  //         //Setting timer for displaying page once its loaded on dismissal of loader.
  //         setTimeout(() => {
  //           this.loader.dismiss(() => {
  //             this.loader.onDidDismiss(() => {
  //               console.log('Dismissed loading');
  //             });
  //           });
  //         }, 1000);

  //         browser.show() //DISPLAYING THE BROWSER AFTER THE LOADER DISMISSED.
  //       });
  //     });
  //   }

  //   //Popping the In-App Browser Page and Moving Back to Login Page.
  //   browser.on('exit').subscribe(event => {
  //     this.navCtrl.pop();
  //   });
  //   if (close == 1) {
  //     browser.close();
  //     // this.navCtrl.pop();
  //   }

  // }
}










    //loadstart: event fires when the InAppBrowser starts to load a URL.
    //loadstop: event fires when the InAppBrowser finishes loading a URL.
    //loaderror: event fires when the InAppBrowser encounters an error when loading a URL.
    //exit: event fires when the InAppBrowser window is closed.