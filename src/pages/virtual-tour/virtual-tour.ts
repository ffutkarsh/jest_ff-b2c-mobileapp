import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

@IonicPage()
@Component({
  selector: 'page-virtual-tour',
  templateUrl: 'virtual-tour.html',
})
export class VirtualTourPage {
  videoUrl: any; //var to store video url
  public getClubDetails = { //default object to get club details
    "TenantID": 0,
    "TenantName": "",
    "GymName": "",
    "CompleteAddress": "",
    "ShortPostalAddress": "",
    "FIELD6": 0,
    "PhoneNumber": "",
    "Email": "",
    "Latitude": 0,
    "longitude": 0,
    "IndoorMapsURL": "",
    "State": "",
    "City": "",
    "FIELD14": 0,
    "offers": "",
    "geo": null,
    "img": ""
  }
  fetchVideo; //var to store fetched video

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public sanitizer: DomSanitizer) {  

    this.getClubDetails = this.navParams.get("objectOfClub"); //RECEIVING PURPOSE.
    this.fetchVideo = this.getClubDetails.IndoorMapsURL //fetchinb video from object

    this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl("https://www.google.com/maps/embed?pb=!1m0!4v1510143549044!6m8!1m7!1sCAoSK0FGMVFpcE42NHRrTHZJcm5MUjlFQkdGYnlEVnVyWFJYczZWcGtYZEhVcVk.!2m2!1d19.05989236508084!2d72.83280020249845!3f325.49!4f0!5f0.7820865974627469");
    // this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.fetchVideo);

  }

}
