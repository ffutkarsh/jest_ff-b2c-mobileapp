import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VirtualTourPage } from './virtual-tour';

@NgModule({
  declarations: [
    VirtualTourPage,
  ],
  imports: [
    IonicPageModule.forChild(VirtualTourPage),
  ],
})
export class VirtualTourPageModule {}
