// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from "@angular/http";
// import { Http, ConnectionBackend } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';
// import { PaymentFailedPage } from './payment-failed';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// describe('Payment Options Page - UI Part', () => {
//     let de: DebugElement;
//     let comp: PaymentFailedPage;
//     let fixture: ComponentFixture<PaymentFailedPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [PaymentFailedPage],
//             imports: [
//                 IonicModule.forRoot(PaymentFailedPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(PaymentFailedPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Page component should be created.", () => expect(comp).toBeDefined());

//     it("Transparent Div should be present above the modal.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".TransparentBack")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Transaction Failed modal should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".TransactionFailedModal")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Grid for Transaction Failed modal's title should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".TransactionFailedTitleGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Title's row should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".TransactionFailedTitleRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Title's icon column should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".TransactionFailedTitleIconColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Title's icon should be present. ", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".TransactionFailedTitleIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Title's column should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".TransactionFailedTitleColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Sub Text grid should be present.", () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".RefundMsgGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Sub Text row should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".RefundMsgRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Sub Text column should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".RefundMsgColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Sub Text should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".RefundMsg")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Pay INR grid should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".PayINRGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Pay INR row should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".PayINRRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Pay INR column should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".PayINRColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Pay INR should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".PayINR")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Pay INR total number of items should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".PayINRSubText")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Buttons Grid should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".ButtonsGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Retry button row should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".RetryButtonRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Retry Button should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".RetryButton")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Try Others button row should be present.", () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".TryOthersButtonRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Try Others button should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentFailedPage);
//         de = fixture.debugElement.query(By.css(".TryOthersButton")).nativeElement;
//         expect(de).toBeDefined();
//     });
// });

// describe('Payment Failed Page - Methods Part', () => {
//     let de: DebugElement;
//     let comp: PaymentFailedPage;
//     let fixture: ComponentFixture<PaymentFailedPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [PaymentFailedPage],
//             imports: [
//                 IonicModule.forRoot(PaymentFailedPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(PaymentFailedPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it('Ionviewdidload Method should be present.', () => {
//         expect(comp.ionViewDidLoad).toBeDefined(true);
//     });

//     it('Close Modal Method should be present.', () => {
//         expect(comp.closeModal).toBeDefined(true);
//     });
// });


