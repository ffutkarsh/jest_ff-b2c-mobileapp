import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';

declare var RazorpayCheckout: any;

@IonicPage()
@Component({
  selector: 'page-payment-failed',
  templateUrl: 'payment-failed.html',
})
export class PaymentFailedPage {

  public cartProducts; //FROM PAYMENT OPTION PAGE.
  public additionalProducts; //FROM PAYMENT OPTION PAGE.
  public totalProducts; //FROM PAYMENT OPTION PAGE.
  public totalPrice; //FROM PAYMENT OPTION PAGE.
  public discountAmount; //FROM PAYMENT OPTION PAGE.
  public error; //FROM PAYMENT OPTION PAGE.
  public appFlowCart;
  public userDetails;
  public phonenumber;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public translateService: TranslateService,
    private storage: Storage,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController) {

    this.error = this.navParams.get('error');
    this.cartProducts = this.navParams.get('cartProducts');
    this.additionalProducts = this.navParams.get('additionalProducts');
    this.totalProducts = this.navParams.get('totalProducts');
    this.totalPrice = this.navParams.get('totalPrice');
    this.discountAmount = this.navParams.get('discountAmount');
    this.appFlowCart = this.navParams.get("appFlow"); //from app flow.
    this.userDetails = this.navParams.get('userDetails');
    this.phonenumber = this.navParams.get('phonenumber');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentFailedPage');
  }

  retry() {
    this.viewCtrl.dismiss();
    this.storage.get('PaymentOption').then((p) => {
      var options = {
        description: 'Payment for Products in Cart',
        image: "https://png.pngtree.com/svg/20151115/pay_86048.png",
        currency: 'INR',
        key: 'rzp_test_z2d4UMo9n09efc',
        amount: this.totalPrice.toFixed() * 100,
        name: this.userDetails.firstName + " " + this.userDetails.middleName + " " + this.userDetails.lastName,
        prefill: {
          email: this.userDetails.emailAddress,
          contact: this.phonenumber[1],
          name: this.userDetails.firstName,
          method: p.razorPayoptionName
        },
        theme: {
          color: '#53d0f2',
          emi_mode: true
        },
        modal: {
          ondismiss: () => {
            alert('dismissed')
          }
        }
      };

      //CALL BACK FUNCTION IN CASE THE TRANSACTION IS DONE SUCCESSFULLY.
      var successCallback = (success) => {
        console.log(success);
        this.navCtrl.setRoot("PaymentSuccessfulPurchasePage", {
          'success': success,
          'cartProducts': this.cartProducts,
          'additionalProducts': this.additionalProducts,
          'totalProducts': this.totalProducts,
          'totalPrice': this.totalPrice,
          'discountAmount': this.discountAmount,
          'appFlow': this.appFlowCart
        })
      };

      //CALL BACK FUNCTION IN CASE THE TRANSACTION IS FAILED.
      var CancelCallback = (error) => {
        console.log(error);
        let paymentFailedModal = this.modalCtrl.create("PaymentFailedPage", {
          'error': error,
          'cartProducts': this.cartProducts,
          'additionalProducts': this.additionalProducts,
          'totalProducts': this.totalProducts,
          'totalPrice': this.totalPrice,
          'discountAmount': this.discountAmount,
          'appFlow': this.appFlowCart
        });
        paymentFailedModal.present();
      };

      //https://api.razorpay.com/v1/payments/:id

      RazorpayCheckout.open(options, successCallback, CancelCallback);

    });
  }

  tryothers() {
    this.storage.remove('PaymentOption');
    this.navCtrl.pop();
  }

  closeModal()
  {
    // this.navCtrl.pop()
    this.viewCtrl.dismiss();
  }
}
