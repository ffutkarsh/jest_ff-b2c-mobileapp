import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentFailedPage } from './payment-failed';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    PaymentFailedPage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentFailedPage),
    TranslateModule
  ],
})
export class PaymentFailedPageModule { }
