import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CancelAppointmentModalPage } from './cancel-appointment-modal';

@NgModule({
  declarations: [
    CancelAppointmentModalPage,
  ],
  imports: [
    IonicPageModule.forChild(CancelAppointmentModalPage),
  ],
})
export class CancelAppointmentModalPageModule {}
