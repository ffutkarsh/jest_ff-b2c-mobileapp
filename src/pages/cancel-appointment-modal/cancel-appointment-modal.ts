import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ViewController ,LoadingController,App} from 'ionic-angular';

import { DashboardPage} from '../../pages/pages'

@IonicPage()
@Component({
  selector: 'page-cancel-appointment-modal',
  templateUrl: 'cancel-appointment-modal.html',
})
export class CancelAppointmentModalPage {
  public selectreason =['Going for Medical Emergency','Other'];
  public selectednow='Going for Medical Emergency';
  public comment;
  constructor(private  app: App,  public navCtrl: NavController, private viewController: ViewController,public navParams: NavParams) {
    this.selectednow='Going for Medical Emergency';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CancelAppointmentModalPage');
  }
  onSubmit(){
    let reasonandcomment = {
      reason: this.selectednow ,
      comment: this.comment
    };

    // console.log(reasonandcomment);
   this.viewController.dismiss(reasonandcomment);
   // this.navCtrl.push("DashboardPage")
   
   this.app.getRootNav().setRoot(DashboardPage);
  }

}
