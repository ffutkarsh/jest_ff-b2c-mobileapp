// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { WelcomeScreenPage } from './welcome-screen';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { HttpModule } from '@angular/http';
// import { Http, ConnectionBackend } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';
// import { IonicStorageModule } from '@ionic/storage';
// import { CenterSelectAuthProvider } from '../../providers/center-select-auth/center-select-auth';
// import { DashboardAuthProvider } from '../../providers/dashboard-auth/dashboard-auth';
// import { MenuController } from 'ionic-angular';
// import { DashboardFlowAuthProvider } from '../../providers/dashboard-flow-auth/dashboard-flow-auth';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { OpenNativeSettings } from '@ionic-native/open-native-settings';
// import { NetworkProvider } from '../../providers/network/network';
// import { Network } from '@ionic-native/network';
// import { LoginFlowApiProvider } from '../../providers/login-flow-api/login-flow-api';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// describe('WelcomeScreen Page - UI Part', () => {
//     let de: DebugElement;
//     let comp: WelcomeScreenPage;
//     let fixture: ComponentFixture<WelcomeScreenPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [WelcomeScreenPage],
//             imports: [
//                 HttpModule,
//                 IonicStorageModule.forRoot(),
//                 IonicModule.forRoot(WelcomeScreenPage),
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 CenterSelectAuthProvider,
//                 DashboardAuthProvider,
//                 NetworkProvider,
//                 MenuController,
//                 Network,
//                 DashboardFlowAuthProvider,
//                 NavController,
//                 TranslateService,
//                 TranslateStore,
//                 OpenNativeSettings,
//                 NetworkProvider,
//                 Network,
//                 LoginFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(WelcomeScreenPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Page component should be created.", () => expect(comp).toBeDefined());

//     it("Ion content should be present.", async () => {
//         fixture = TestBed.createComponent(WelcomeScreenPage);
//         de = fixture.debugElement.query(By.css(".MainContent")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section grid should be present.", async () => {
//         fixture = TestBed.createComponent(WelcomeScreenPage);
//         de = fixture.debugElement.query(By.css(".HeadingGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section row should be present.", async () => {
//         fixture = TestBed.createComponent(WelcomeScreenPage);
//         de = fixture.debugElement.query(By.css(".HeadingGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section column should be present.", async () => {
//         fixture = TestBed.createComponent(WelcomeScreenPage);
//         de = fixture.debugElement.query(By.css(".HeadingGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section grid should contain the “Welcome” text.", async () => {
//         fixture = TestBed.createComponent(WelcomeScreenPage);
//         de = fixture.debugElement.query(By.css(".WelcomeText")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Hello grid should be present.", async () => {
//         fixture = TestBed.createComponent(WelcomeScreenPage);
//         de = fixture.debugElement.query(By.css(".HelloGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Hello row should be present.", async () => {
//         fixture = TestBed.createComponent(WelcomeScreenPage);
//         de = fixture.debugElement.query(By.css(".HelloGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Hello column should be present.", async () => {
//         fixture = TestBed.createComponent(WelcomeScreenPage);
//         de = fixture.debugElement.query(By.css(".HelloGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Hello grid should contain the “Hello,” text.", async () => {
//         fixture = TestBed.createComponent(WelcomeScreenPage);
//         de = fixture.debugElement.query(By.css(".Hello")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("SubHeading grid should be present.", async () => {
//         fixture = TestBed.createComponent(WelcomeScreenPage);
//         de = fixture.debugElement.query(By.css(".SubHeadingGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("SubHeading row should be present.", async () => {
//         fixture = TestBed.createComponent(WelcomeScreenPage);
//         de = fixture.debugElement.query(By.css(".SubHeadingGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("SubHeading column should be present.", async () => {
//         fixture = TestBed.createComponent(WelcomeScreenPage);
//         de = fixture.debugElement.query(By.css(".SubHeadingGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("SubHeading grid should contain the “Select your club to continue” text.", async () => {
//         fixture = TestBed.createComponent(WelcomeScreenPage);
//         de = fixture.debugElement.query(By.css(".SubHeading")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it('Center grid should be present.', () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(WelcomeScreenPage);
//             de = fixture.debugElement.query(By.css(".CenterGrid")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('Center row should be present.', () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(WelcomeScreenPage);
//             de = fixture.debugElement.query(By.css(".CenterRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('Center column should be present.', () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(WelcomeScreenPage);
//             de = fixture.debugElement.query(By.css(".CenterColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('Main center label row should be present.', () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(WelcomeScreenPage);
//             de = fixture.debugElement.query(By.css(".MainCenterLabelRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('Main center label should be present.', () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(WelcomeScreenPage);
//             de = fixture.debugElement.query(By.css(".MainCenterLabel")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('Sub center label row should be present.', () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(WelcomeScreenPage);
//             de = fixture.debugElement.query(By.css(".SubCenterLabelRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('Sub center label should be present.', () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(WelcomeScreenPage);
//             de = fixture.debugElement.query(By.css(".SubCenterLabel")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('Sub center para should be present.', () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(WelcomeScreenPage);
//             de = fixture.debugElement.query(By.css(".SubCenterPara")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('Sub center should be present.', () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(WelcomeScreenPage);
//             de = fixture.debugElement.query(By.css(".SubCenter")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('Enquiry type icon should be present.', () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(WelcomeScreenPage);
//             de = fixture.debugElement.query(By.css(".EnquiryTypeIcon")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('Enquiry type should be present.', () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(WelcomeScreenPage);
//             de = fixture.debugElement.query(By.css(".EnquiryType")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('Arrow column should be present.', () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(WelcomeScreenPage);
//             de = fixture.debugElement.query(By.css(".ArrowColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('Arrow label should be present.', () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(WelcomeScreenPage);
//             de = fixture.debugElement.query(By.css(".ArrowLabel")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('Arrow icon should be present.', () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(WelcomeScreenPage);
//             de = fixture.debugElement.query(By.css(".ArrowIcon")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('Horizontal line after each center should be present.', () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(WelcomeScreenPage);
//             de = fixture.debugElement.query(By.css(".HorizontalLine")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('Empty Div should be present.', () => {
//         fixture = TestBed.createComponent(WelcomeScreenPage);
//         de = fixture.debugElement.query(By.css(".emptydiv")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it('Footer Div should be present.', () => {
//         fixture = TestBed.createComponent(WelcomeScreenPage);
//         de = fixture.debugElement.query(By.css(".foot")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it('Footer Logo should be present.', () => {
//         fixture = TestBed.createComponent(WelcomeScreenPage);
//         de = fixture.debugElement.query(By.css(".FooterImage")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Footer Logo source should match.", async () => {
//         const imagefield: HTMLInputElement = fixture.debugElement.query(By.css('.FooterImage')).nativeElement;
//         expect(imagefield.getAttribute('src')).toMatch('assets/img/ff-purp-logo.png');
//     });

// });

// describe('WelcomeScreen Page - Methods and Logic Part', () => {
//     let de: DebugElement;
//     let comp: WelcomeScreenPage;
//     let fixture: ComponentFixture<WelcomeScreenPage>;
//     let loginFlowApi: LoginFlowApiProvider;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [WelcomeScreenPage],
//             imports: [
//                 HttpModule,
//                 IonicStorageModule.forRoot(),
//                 IonicModule.forRoot(WelcomeScreenPage),
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 CenterSelectAuthProvider,
//                 DashboardAuthProvider,
//                 NetworkProvider,
//                 MenuController,
//                 Network,
//                 DashboardFlowAuthProvider,
//                 NavController,
//                 TranslateService,
//                 TranslateStore,
//                 OpenNativeSettings,
//                 NetworkProvider,
//                 Network,
//                 LoginFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(WelcomeScreenPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//         loginFlowApi = fixture.debugElement.injector.get(LoginFlowApiProvider); //INJECTING PROVIDER.
//     });

//     it('Instance of LoginFlowApiProvider created', () => {
//         expect(loginFlowApi instanceof LoginFlowApiProvider).toBe(true);
//     });

//     it('Ionviewdidload method should be present.', () => {
//         expect(comp.ionViewDidLoad).toBeDefined(true);
//     });

//     it('Center Selection method should be present.', () => {
//         expect(comp.goToSignupTermsEula).toBeDefined(true);
//     });

// });
