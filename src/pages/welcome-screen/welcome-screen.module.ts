import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WelcomeScreenPage } from './welcome-screen';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    WelcomeScreenPage
  ],
  imports: [
    IonicPageModule.forChild(WelcomeScreenPage),
    TranslateModule
  ]
})
export class WelcomeScreenPageModule { }
