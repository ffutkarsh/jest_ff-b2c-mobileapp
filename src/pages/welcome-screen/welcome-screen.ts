import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController, MenuController, ToastController } from 'ionic-angular';
import { CenterSelectAuthProvider } from '../../providers/center-select-auth/center-select-auth';
import { DashboardFlowAuthProvider } from '../../providers/dashboard-flow-auth/dashboard-flow-auth';
import { DashboardAuthProvider } from '../../providers/dashboard-auth/dashboard-auth';
import { Network } from '@ionic-native/network';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import rootReducer from '../../reducers/LoginFlow';
import * as a from '../../actions/LoginAction';
import { SignupTermsEulaPage } from '../../pages/pages';
import { NetworkProvider } from '../../providers/network/network';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { Subject } from 'rxjs/Subject';

@IonicPage()
@Component({
  selector: 'page-welcome-screen',
  templateUrl: 'welcome-screen.html',
})
export class WelcomeScreenPage {

  public fetchDialCode //FROM LOGIN PAGE.
  public fetchPhoneNumber; //FROM LOGIN PAGE.
  public memberDetailsResponse = {
    memberDetails: []
  }; //STORING API RESPONSE OF MEMBER DETAILS.
  public memberDetailsArray = []; //MEMBER DETAILS ARRAY FROM RESPONSE.
  public selectedCenterObject; //STORING SELECTED CENTER'S OBJECT.
  public index; //STORING INDEX OF SELECTED CENTER.

  //STRUCTURE OF INITIAL STATE.
  public currentUserState = {
    dialCode: "",
    phoneNumber: "",
    centerName: "",
    subCenterName: "",
    enquiryType: "",
    memberType: "",
    userName: "",
    userPhoto: "",
    otp: "",
    verificationFlag: "",
    permissions: []
  };

  //CHECKING NETWORK ABSENCE.
  public myObservable = new Subject<boolean>();
  public networkAvailable;
  public networkAlert;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public centerAuthService: CenterSelectAuthProvider,
    public dashboardFlowAuthService: DashboardFlowAuthProvider,
    public dashboardAuthService: DashboardAuthProvider,
    public network: Network,
    public translateService: TranslateService,
    private storage: Storage,
    private openNativeSettings: OpenNativeSettings,
    public events: Events,
    public networkProvider: NetworkProvider) {

    this.menuCtrl.enable(false, 'myMenu'); //DISABLING MENU ON LOGIN MULTI CENTER [WELCOMESCREEN] PAGE.

    this.fetchDialCode = this.navParams.get('defaultDialcode'); //FROM LOGIN PAGE.
    this.fetchPhoneNumber = this.navParams.get('phoneNumber'); //FROM LOGIN PAGE.
    this.memberDetailsResponse = this.navParams.get('memberDetails'); //FROM LOGIN PAGE.
    this.memberDetailsArray = this.memberDetailsResponse.memberDetails; //STORING MEMBER DETAILS ARRAY.
    console.log("Member Details on Welcome Screen Page");
    console.log(this.memberDetailsArray);

    //CHECKING NETWORK ABSENCE.
    this.networkProvider.initializeNetworkEvents();//check network condition
    this.networkChangesSubscribe();
    this.networkAvailable = (this.networkProvider.isConnected() ? true : false);
    this.myObservable.next(this.networkAvailable);
    this.myObservable.subscribe(val => {
      if (!val) {
        this.showNetworkAlert();
      }
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomeScreenPage');
    this.storage.get('USERDETAILS').then((USERDETAILS) => { //FETCHING THE CURRENT USER STATE.
      this.currentUserState = USERDETAILS;
    });
  }

  ionViewDidLeave() {
    this.networkChangesUnsubscribe(); //UNSUBSCRIBE THE CHANGES.
  }

  //GOING TO SIGNUPTERMSEULA PAGE.
  goToSignupTermsEula(i) {

    this.index = i; //INDEX OF THE SELECTED CENTER.
    this.selectedCenterObject = this.memberDetailsArray[i]; //STORING SELECTED CENTER'S OBJECT.
    console.log("Selected Center");
    console.log(this.selectedCenterObject);

    // this.addCenterName(); //ADDING CENTERNAME TO CURRENT USER STATE.
    // this.addSubCenterName(); //ADDING SUBCENTERNAME TO CURRENT USER STATE.
    // this.addEnquiryType(); //ADDING ENQUIRYTYPE TO CURRENT USER STATE.

    this.navCtrl.push(SignupTermsEulaPage, {
      'defaultDialcode': this.fetchDialCode,
      'phoneNumber': this.fetchPhoneNumber,
      'memberDetails': this.selectedCenterObject
    });

    this.storage.set('User_Details_From_Selected_Center', this.selectedCenterObject);
  }

  // //ADDING CENTERNAME TO CURRENT USER STATE.
  // addCenterName() {
  //   this.currentUserState = rootReducer(this.currentUserState, a.addCenterName(this.centerName));
  //   console.log("Add CenterName: ");
  //   console.log(this.currentUserState);
  //   this.storelocally();
  // }

  // //ADDING SUBCENTERNAME TO CURRENT USER STATE.
  // addSubCenterName() {
  //   this.currentUserState = rootReducer(this.currentUserState, a.addSubCenterName(this.subCenterName));
  //   console.log("Add SubCenterName: ");
  //   console.log(this.currentUserState);
  //   this.storelocally();
  // }

  // //ADDING ENQUIRYTYPE TO CURRENT USER STATE.
  // addEnquiryType() {
  //   this.currentUserState = rootReducer(this.currentUserState, a.addEnquiryType(this.enquiryType));
  //   console.log("Add EnquiryType: ");
  //   console.log(this.currentUserState);
  //   this.storelocally();
  // }

  //SAVES DATA TO LOCAL DATABASE.
  storelocally() {
    this.storage.set('USERDETAILS', this.currentUserState).then((val) => {
      this.storage.get('USERDETAILS').then((val) => {
        console.log(val);
      });
    });
  }

  //SUBSCRIBE TO NETWORK CHANGES.
  networkChangesSubscribe() {
    // Offline event
    this.events.subscribe('network:offline', () => {  //method to check internet
      this.myObservable.next(false);
      this.networkAvailable = false;
    });

    // Online event
    this.events.subscribe('network:online', () => {
      this.myObservable.next(true);
      this.networkAvailable = true;
    });
  }

  //UNSUBSCRIBE TO NETWORK CHANGES.
  networkChangesUnsubscribe() {
    this.events.unsubscribe('network:offline');
    this.events.unsubscribe('network:online');
  }

  //SHOWING ALERT ON NETWORK ABSENCE.
  showNetworkAlert() {
    this.networkAlert = this.alertCtrl.create({
      title: 'No Internet Connection',
      message: 'Please check your internet connection.',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Retry',
          handler: () => {
            if (this.networkAvailable == false) {
              this.showNetworkAlert();
            }
          }
        },
        {
          text: 'Open Settings',
          handler: () => {
            this.networkAlert.dismiss().then(() => {
              this.showSettings();
              if (this.networkAvailable == false) {
                this.showNetworkAlert();
              }
            });
          }
        }
      ]
    });
    this.networkAlert.present();
  }

  //MOVING TO SETTINGS ON CLICKING SHOW SETTINGS.
  showSettings() {
    if ((<any>window).cordova && (<any>window).cordova.plugins.settings) {
      console.log('openNativeSettingsTest is active');
      (<any>window).cordova.plugins.settings.open("settings", function () {
        console.log('opened settings');
      },
        function () {
          console.log('failed to open settings');
        }
      );
    } else {
      console.log('openNativeSettingsTest is not active!');
    }
  }

}






