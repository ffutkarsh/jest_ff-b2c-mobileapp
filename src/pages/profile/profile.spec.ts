// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, style } from '@angular/core';
// import { ProfilePage } from './profile';
// import { IonicModule, Platform, NavController, NavParams, Item } from 'ionic-angular/index';
// import { HttpModule } from '@angular/http';
// import { FetcherProvider } from '../../providers/fetcher/fetcher';
// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }


// let dummyResponse =
// {

//     "title" : "My Profile",
//     "userImg" : "assets/img/userimg.png", 
//     "userName" : "Suzanie Lamborus", 
//     "userStatus" : "Member", 
//     "userClientId" : "1129901 (Client ID)",
//     "userSlot" : "FitnessForce Demo", 
//     "itemHeading1" : "Balance Due", 
//     "itemHeading2" : "Last Visit", 
//     "itemHeading3" : "Next Apptmt", 
//     "subItemHeader" : "0", 
//     "contactEmail" : "suzanie@fitnessforce.com",
//     "contactMobile" : "9375934775"
    
//     }

// //The describe function is for grouping related specs
// describe('*****ProfilePage UI*****', () => {
//     let de: DebugElement;
//     let comp: ProfilePage;
//     let fixture: ComponentFixture<ProfilePage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [ProfilePage],
//             imports: [
//                 HttpModule,
//                 IonicModule.forRoot(ProfilePage),
//             ],
//             providers: [
//                 NavController, { provide: NavParams, useClass: NavParamsMock },FetcherProvider]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(ProfilePage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //Creating the main Component
//     it('should create component', () => expect(comp).toBeDefined());


//     //HEADER-----------------
//     it('----------HEADER----------',()=> {

//     });
//     //Checking title bar tag
//     it('checking the title bar using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.titlebar')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('titlebar');
//     });
//     //Checking title bar content
//     it('checking the column text content in 2nd row using class name', () => {
//         expect(dummyResponse.title).toMatch('Profile');
//     });
//     //checking nav bar
//     it('checking the nav bar', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.navbarclass')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('navbarclass');
//     });
//     //checking icon in header
//     it('checking icon in header', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.HeaderIcon')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('HeaderIcon');
//     });
//     //checking icon of menu toggle
//     it('checking icon of menu toggle', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.HeaderIcon')).nativeElement;
//         expect(inputfield.getAttribute('name')).toMatch('menu');
//     });

//     //Content
//     it('----------CONTENT----------',()=> {

//     });
//     //Checking content tag
//     it('checking the content using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.contentClass')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('contentClass');
//     });

//     //Checking grid tag
//     it('checking the grid using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.MainGrid')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('MainGrid');
//     });


//     //ROW 1------------

//     //ROW 1 testing using class name
//     it('checking the ROW 1 using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row1')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row1');
//     });
//     //col 1 testing using class name
//     it('checking the columnn 1 in row 1 using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row1col1')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row1col1');
//     });

//     //image class using class name
//     it('checking the image of columnn 1- row 1 using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.userimg')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('userimg');
//     });

//     //image source
//     it('checking the image source columnn 1- row 1 using class name', () => {
//         expect(dummyResponse.userImg).toMatch('assets/img/userimg.png');
//     });


//     //ROW 2 ----------------------
//     it('--------ROW 2----------', () => {

//     });


//     //checking row using class name
//     it('checking the ROW 2 using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row2')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row2');
//     });
//     //col 2 testing using class name
//     it('checking the columnn 1 in row 2 using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row2col1')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row2col1');
//     });

//     //col 2 TEXT CONTENT testing using class name
//     it('checking the text content of columnn 1 in row 2 using class name', () => {
//         expect(dummyResponse.userName).toMatch('Suzanie Lamborus');
//     });




//     //ROW 3
//     it('--------ROW 3----------', () => {

//     });
//     //checking row using lass name
//     it('checking the ROW 3 using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row3')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row3');
//     });
//     //col 1 testing using class name
//     it('checking the columnn 1 in row 3 using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row3col1')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row3col1');
//     });
//     //Testing img
//     it('checking the img class', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.locateImg')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('locateImg');
//     });




//     //ROW 4
//     it('--------ROW 4----------', () => {

//     });
//     //CHCKING ROW
//     it('checking the ROW 4 using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row4')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row4');
//     });
//     //checking column name
//     it('checking the column 1 of row 4 ', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row4col1')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row4col1');
//     });
//     //checking the text ontent in column
//     it('text content of the column 1 of row 4 ', () => {
//         expect(dummyResponse.itemHeading1).toMatch('Balance Due');
//     });
//        //checking the column name
//     it('checking the column 2 of row 4 ', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row4col2')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row4col2');
//     });
//        //checking the text ontent in column
//     it('text content of column 2 of row 4 ', () => {
//         expect(dummyResponse.itemHeading2).toMatch('Last Visit');
//     });
//     //checking the column name
//     it('checking the column 3 of row 4 ', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row4col3')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row4col3');
//     });
//        //checking the text content in column
//     it('text content of column 3 of row 4 ', () => {
//         expect(dummyResponse.itemHeading3).toMatch('Next Apptmt');
//     });



//     //ROW 5

//     it('--------ROW 5----------', () => {

//     });
//     //cheking the row name
//     it('checking the ROW 5 using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row5')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row5');
//     });
//     //checking the column class name
//     it('checking the column 1 of row 5 ', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row5col1')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row5col1');
//     });
//     //checking text content of column
//     it('text content of the columnn1 of row 5 ', () => {
//         expect(dummyResponse.subItemHeader).toMatch('0');
//     });
//      //checking the column name
//     it('checking the column 2 of row 5 ', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row5col2')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row5col2');
//     });
//      //checking the column name
//     it('checking the column 3 of row 5 ', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row5col3')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row5col3');
//     });


//     //ROW 6---------------------

//     it('--------ROW 6----------', () => {

//     });
//      //cheking the row name
//     it('checking the ROW 6 using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row6')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row6');
//     });
//     //checking the column class name
//     it('checking the column 1 of row 6 ', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row6col1')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row6col1');
//     });
//     //text ontent of column
//     it('text content of the columnn of row 6 ', () => {
//         expect(dummyResponse.contactEmail).toMatch('suzanie@fitnessforce.com');
//     });
//     //checking the input class
//     it('checking the input of row 6 ', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.inputemail')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('inputemail');
//     });
   
//     //chcking the placeholder of input
//     it('checking the inputtype of row 6 ', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.inputemail')).nativeElement;
//         expect(inputfield.getAttribute('type')).toMatch('email');
//     });



//     //ROW 7-------------
//     it('--------ROW 7----------', () => {

//     });
//      //cheking the row name
//     it('checking the ROW 7 using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row7')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row7');
//     });
//     //checking the column class name
//     it('checking the column 1 of row 7 ', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.row7col1')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('row7col1');
//     });
//     //text content of column
//     it('text content of the columnn of row 7 ', () => {
//         expect(dummyResponse.contactMobile).toMatch('9375934775');
//     });
//     //checking the input class
//     it('checking the input of row 7 ', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.inputmobile')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('inputmobile');
//     });
    
//     //chcking the placeholder of input
//     it('checking the inputtype of row 7 ', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.inputmobile')).nativeElement;
//         expect(inputfield.getAttribute('type')).toMatch('number');
//     });

//     it('Checking if JSON getter method is present', () => {
//         expect(comp.getData).toBeDefined(true);
//     });

// });
