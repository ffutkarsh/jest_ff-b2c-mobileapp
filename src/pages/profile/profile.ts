import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FetcherProvider } from '../../providers/fetcher/fetcher';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  itemOne; //var to fetch nav params
  itemTwo; //var to fetch nav params

  //HEADER
  title; //var to store title

  dummyResponse =
  {
    "title" : "",
    "userImg" : "", 
    "userName" : "", 
    "userStatus" : "", 
    "userClientId" : "",
    "userSlot" : "", 
    "itemHeading1" : "", 
    "itemHeading2" : "", 
    "itemHeading3" : "", 
    "subItemHeader" : "", 
    "contactEmail" : "",
    "contactMobile" : ""
  }
  ; // Used to mimick response from json

 


  constructor(public fetcher: FetcherProvider,public navCtrl: NavController, public navParams: NavParams) {

    this.itemOne = navParams.get('itemOne'); //from nav params
    this.itemTwo = navParams.get('itemTwo');  //from nav params

    
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
    this.getData(); // Getting from JSON
  }

  getData()
  {
    this.fetcher.getProfileData().map((response) => response.json()).subscribe((response) => {
      this.dummyResponse = response;

console.log("-----------t ---------------------");
      console.log(this.dummyResponse);
    });
  }
}
