import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimeUpModalPage } from './time-up-modal';

@NgModule({
  declarations: [
    TimeUpModalPage,
  ],
  imports: [
    IonicPageModule.forChild(TimeUpModalPage),
  ],
})
export class TimeUpModalPageModule {}
