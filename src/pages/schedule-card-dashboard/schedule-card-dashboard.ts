import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import {
  ClassDetailsPage,
  BookedAppointmentPage,
  FacilityBookedDetailsPage,
  MainPage
} from '../../pages/pages';
import { CheckPermissionsProvider } from '../../providers/check-permissions/check-permissions';
import { DashboardFlowApiProvider } from '../../providers/dashboard-flow-api/dashboard-flow-api';
import { Storage } from '@ionic/storage';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-schedule-card-dashboard',
  templateUrl: 'schedule-card-dashboard.html',
})
export class ScheduleCardDashboardPage {

  @ViewChild(Slides) slides: Slides; //THIS IS USED FOR CREATING SLIDES.

  public fetchDialCode; //FROM DASHBOARD PAGE.
  public fetchPhoneNumber; //FROM DASHBOARD PAGE.
  public selectedCenterObject; //FROM DASHBOARD PAGE.
  public otpResponse; //FROM DASHBOARD PAGE.
  public otpVerified; //FROM DASHBOARD PAGE.

  public verificationFlag; //FROM DASHBOARD PAGE.
  public memberType; //FROM DASHBOARD PAGE.
  public permissions; //FROM DASHBOARD PAGE.
  public checkAccess; //FOR CHECKING ACCESS PERMISSIONS.

  public scheduleList = []; //STORING API RESPONSE OF SCHEDULES.

  public combinedArray = []; //STORING CLASSLIST ARRAY, APPOINTMENTLIST ARRAY AND FACILITY ARRAY RECEIVED FROM RESPONSE.
  public date = []; //FOR STORING DATE IN GIVEN FORMAT.
  public timeToGo = []; //FOR STORING HOURS TO GO FOR THE SESSION.

  public colorarray = []; //ARRAY TO STORE COLOR.

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public translateService: TranslateService,
    public checkPermissions: CheckPermissionsProvider,
    private dashboardFlowApi: DashboardFlowApiProvider,
    private storage: Storage) {

    this.fetchDialCode = navParams.get("defaultDialcode");
    this.fetchPhoneNumber = navParams.get("phoneNumber");
    this.selectedCenterObject = this.navParams.get('memberDetails');
    this.otpResponse = this.navParams.get('otpResponse');
    this.otpVerified = this.navParams.get('otpVerified');

    this.verificationFlag = navParams.get('verificationFlag');
    this.memberType = navParams.get('memberType');
    this.permissions = navParams.get('permissions');

    this.getSchedulelist();

    this.colorarray = [
      '#BFB996', '#5A983F', '#F1B648',
      '#BFB996', '#5A983F', '#F1B648',
      '#BFB996', '#5A983F', '#F1B648',
      '#BFB996', '#5A983F', '#F1B648',
      '#BFB996', '#5A983F', '#F1B648',
      '#BFB996', '#5A983F', '#F1B648',
      '#BFB996', '#5A983F', '#F1B648',
      '#BFB996', '#5A983F', '#F1B648',
    ];

  }

  ionViewDidLoad() { }

  //API RESPONSE FOR SCHEDULE LIST.
  getSchedulelist() {
    this.dashboardFlowApi.getSchedulelist().map((response) => response).subscribe((response) => {
      this.scheduleList = response;
      this.combinedArray = this.scheduleList['ClassList'].concat(this.scheduleList['Appointmentlist']).concat(this.scheduleList['Facilitylist']);
      for (let i = 0; i < this.combinedArray.length; i++) {
        this.date[i] = moment(this.combinedArray[i].Date).format('DD MMM');
        let combinedDateTime = this.combinedArray[i].Date + " " + this.combinedArray[i].StartTime;
        this.timeToGo[i] = moment(combinedDateTime, 'DD MMM YYYY HH:mm A').fromNow();
      }
    });
  }

  //NAVIGATING TO RESPECTIVE PAGES.
  goToMySchedules(schedule) {
    if (schedule['ClassId']) {
      this.checkAccess = this.checkPermissions.checkAccessibility("ClassDetailsPage");
      if (this.checkAccess) {
        this.navCtrl.push(ClassDetailsPage, { //IF USER IS A REGISTERED MEMBER. [I.E USER HAS LOGGED IN]
          'objectOfC': schedule
        });
      }
      else {
        this.navCtrl.setRoot(MainPage); //IF USER IS A GUEST MEMBER. [I.E USER HAS NOT LOGGED IN]
      }
    }
    else if (!schedule['ClassId'] && !schedule['FacilityId']) {
      this.checkAccess = this.checkPermissions.checkAccessibility("BookedAppointmentPage");
      if (this.checkAccess) {
        this.navCtrl.push(BookedAppointmentPage, {
          'objectOfD': schedule
        }); //IF USER IS A REGISTERED MEMBER. [I.E USER HAS LOGGED IN]
      }
      else {
        this.navCtrl.setRoot(MainPage); //IF USER IS A GUEST MEMBER. [I.E USER HAS NOT LOGGED IN]
      }
    }
    else if (schedule['FacilityId']) {
      this.checkAccess = this.checkPermissions.checkAccessibility("FacilityBookedDetailsPage");
      if (this.checkAccess) {
        this.navCtrl.push(FacilityBookedDetailsPage, {
          'objectOfF': schedule
        }); //IF USER IS A REGISTERED MEMBER. [I.E USER HAS LOGGED IN]
      }
      else {
        this.navCtrl.setRoot(MainPage); //IF USER IS A GUEST MEMBER. [I.E USER HAS NOT LOGGED IN]
      }
    }
  }

}
