// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { ScheduleCardDashboardPage } from './schedule-card-dashboard';
// import { CheckPermissionsProvider } from '../../providers/check-permissions/check-permissions';
// import { DashboardFlowApiProvider } from '../../providers/dashboard-flow-api/dashboard-flow-api';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// //The describe function is for grouping related specs
// describe('---------Schedule Card Dashboard  Page PAGE UI----------', () => {
//     let de: DebugElement;
//     let comp: ScheduleCardDashboardPage;
//     let fixture: ComponentFixture<ScheduleCardDashboardPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 ScheduleCardDashboardPage
//             ],
//             imports: [
//                 IonicModule.forRoot(ScheduleCardDashboardPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 CheckPermissionsProvider,
//                 DashboardFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(ScheduleCardDashboardPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //Creating the main Component
//     it('should create component', () => expect(comp).toBeDefined());

//     //Checking the slide tag using class name
//     it('checking the Main Slides using class name', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.mainSlides')).nativeElement;
//         expect(inputfield.getAttribute('class')).toMatch('mainSlides');
//     });

//     //Checking the slide tag using class name
//     it('checking the Inner Slides using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.slide1')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('slide1');
//         });
//     });

//     //Checking the image tag using class name
//     it('checking the Image using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.userImage1')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('userImage1');
//         });
//     });

//     //Checking the icon tag using class name
//     it('checking the icon using  class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.IconClass')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('IconClass');
//         });
//     });

//     //Checking the  icon 2 tag using class name
//     it('checking the icon 2 class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.userImage')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('userImage');
//         });
//     });

//     //Checking the Description tag using class name
//     it('checking the Description using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.title3')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('title3');
//         });
//     });

//     //Checking the Validity tag using class name
//     it('checking the Validity using class name', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.title2')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('title2');
//         });
//     });

//     //Checking the class name tag 
//     it('checking the class name ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.title1')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('title1');
//         });
//     });
// });

// describe('----------DashboardProvider PAGE METHODS----------', () => {
//     let de: DebugElement;
//     let comp: ScheduleCardDashboardPage;
//     let fixture: ComponentFixture<ScheduleCardDashboardPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 ScheduleCardDashboardPage
//             ],
//             imports: [
//                 IonicModule.forRoot(ScheduleCardDashboardPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 CheckPermissionsProvider,
//                 DashboardFlowApiProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(ScheduleCardDashboardPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });

//     //checking json method
//     it('checking json method ', () => {
//         expect(comp.getSchedulelist).toBeTruthy();
//     });

//     //checking nav push method
//     it('checking goToAppointment method ', () => {
//         expect(comp.goToMySchedules).toBeTruthy();
//     });
// });