import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ScheduleCardDashboardPage } from './schedule-card-dashboard';

@NgModule({
  declarations: [
    ScheduleCardDashboardPage
  ],
  imports: [
    IonicPageModule.forChild(ScheduleCardDashboardPage),
    TranslateModule
  ]
})
export class ScheduleCardDashboardPageModule { }
