import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

/**
 * Generated class for the FacilityModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-facility-modal',
  templateUrl: 'facility-modal.html',
})
export class FacilityModalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalctrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FacilityModalPage');
  }
  
  closemodal() {
    this.navCtrl.pop();
  }
}
