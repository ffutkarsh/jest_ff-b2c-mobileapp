import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FacilityModalPage } from './facility-modal';

@NgModule({
  declarations: [
    FacilityModalPage,
  ],
  imports: [
    IonicPageModule.forChild(FacilityModalPage),
  ],
})
export class FacilityModalPageModule {}
