// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, style } from '@angular/core';
// import { WaiverSuccessPage } from './waiver-success';

// import { IonicModule, Platform, NavController, NavParams, ViewController, Item } from 'ionic-angular/index';
// import { HttpModule } from '@angular/http';
// import { Camera, CameraOptions, DestinationType } from '@ionic-native/camera';
// import { Crop } from '@ionic-native/crop';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { mockApp, mockConfig, mockPlatform, mockView } from "ionic-angular/util/mock-providers";
// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }
// //The describe function is for grouping related specs
// describe('---------WAIVER MODAL PAGE UI----------', () => {
//     let de: DebugElement;
//     let comp: WaiverSuccessPage;
//     let fixture: ComponentFixture<WaiverSuccessPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [WaiverSuccessPage],
//             imports: [
//                 HttpModule,
//                 IonicModule.forRoot(WaiverSuccessPage),
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 {
//                     provide: ViewController, useValue: mockView(),
                                
//                 },
//                 TranslateService,
//                 TranslateStore,
//                 NavController, Camera, Crop, { provide: NavParams, useClass: NavParamsMock },],
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(WaiverSuccessPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });
//     //Creating the main Component
//     it('should create component', () => expect(comp).toBeDefined());
//     //checking the content
//     it('checking the main content using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.main-content')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('main-content');
//     });
//     //checking the sub content - card
//     it('checking the sub content using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.modal')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('modal');
//     });

//     //checking the main grid - card
//     it('checking the main grid using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.mainGrid')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('mainGrid');
//     });


//     //checking therow holding waiver text class name
//     it('checking the row holding waiver text class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.waiverText')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('waiverText');
//     });
//     //checking col holding waiver text name
//     it('checking the col holding waiver text name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.col11')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('col11');
//     });
//     //checking the row holding image class name
//     it('checking the row holding image class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.row2')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('row2');
//     });

//     //checking the row fo redirecting
//     it('checking the row fo redirecting', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.redirecting')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('redirecting');
//     });

//     //checking the spinner tag
//     it('checking the spinner tag', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.spinnerClass')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('spinnerClass');
//     });



// });