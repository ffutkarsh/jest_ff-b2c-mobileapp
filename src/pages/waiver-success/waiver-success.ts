import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { PaymentOptionsPage } from '../../pages/pages'
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-waiver-success',
  templateUrl: 'waiver-success.html',
})
export class WaiverSuccessPage {

  public cartProducts;
  public additionalProducts;
  public totalProducts;
  public totalPrice;
  public discountAmount;
  public appFlowCart;

  constructor(
    public navCtrl: NavController,
    public translateService: TranslateService,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public storage: Storage) {

    this.gotToPayment()

    this.cartProducts = this.navParams.get('cartProducts');
    this.additionalProducts = this.navParams.get('additionalProducts');
    this.totalProducts = this.navParams.get('totalProducts');
    this.totalPrice = this.navParams.get('totalPrice');
    this.discountAmount = this.navParams.get('discountAmount');
    this.appFlowCart = this.navParams.get("appFlow"); //from app flow.

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WaiverSuccessPage');
  }

  ionViewDidLeave() {
    this.viewCtrl.dismiss();
  }

  gotToPayment() {
    setTimeout(() => {
      this.storage.set('WaiverSignedIn', true);
      this.navCtrl.push(PaymentOptionsPage, {
        'cartProducts': this.cartProducts,
        'additionalProducts': this.additionalProducts,
        'totalProducts': this.totalProducts,
        'totalPrice': this.totalPrice,
        'discountAmount': this.discountAmount,
        'appFlow': this.appFlowCart
      });
    }, 3000);
  }

}
