import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WaiverSuccessPage } from './waiver-success';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    WaiverSuccessPage,
  ],
  imports: [
    IonicPageModule.forChild(WaiverSuccessPage),
    TranslateModule
  ],
})
export class WaiverSuccessPageModule {}
