import { Component } from '@angular/core';
import { LoadingController, IonicPage, NavParams, NavController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { SelectTrainerProvider } from '../../providers/select-trainer/select-trainer-api';

@IonicPage()
@Component({
  selector: 'page-any-trainer',
  templateUrl: 'any-trainer.html',
})
export class AnyTrainerPage {

  public memberDetailsResponse = {
    memberDetails: []//array to store server response.
  };

  public trainerList = []; //ARRAY OF OBJECTS TO STORE JSON FILE.

  public appointFull; //FROM DATE AND TIME PAGE.
  public purpose; //FROM DATE AND TIME PAGE.
  public purposeTotalSessions; //FROM DATE AND TIME PAGE.
  public icon; //FROM DATE AND TIME PAGE.
  public cartOrbooked; //FROM DATE AND TIME PAGE.
  public trainername; //FROM DATE AND TIME PAGE.
  public trainerType; //FROM DATE AND TIME PAGE.
  public trainerIcon; //FROM DATE AND TIME PAGE.
  public mastertrainerlist; //FROM DATE AND TIME PAGE.
  public trianerrating; //FROM DATE AND TIME PAGE.
  public ratingsurveycount; //FROM DATE AND TIME PAGE.
  public date; //FROM DATE AND TIME PAGE.
  public availableSlot; //FROM DATE AND TIME PAGE.

  public index; //VARIABLE TO STORE THE INDEX OF TRAINER SELECTED FROM LIST.

  constructor(
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
   public navCtrl: NavController,
    public navParams: NavParams,
    //public translateService: TranslateService,
    private trainer: SelectTrainerProvider) {

    this.appointFull = this.navParams.get('appointFull');
    this.purpose = this.navParams.get("purpose");
    this.purposeTotalSessions = this.navParams.get("purposeTotalSessions");
    this.icon = this.navParams.get("icon");
    this.cartOrbooked = this.navParams.get('cartOrbooked');
    this.trainername = this.navParams.get("trainername");
    this.trainerType = this.navParams.get("trainerType");
    this.trainerIcon = this.navParams.get('trainerIcon');
    this.mastertrainerlist = this.navParams.get('mastertrainerlist');
    this.trianerrating = this.navParams.get('trainerrating');
    this.ratingsurveycount = this.navParams.get('ratingsurveycount');
    this.date = this.navParams.get('date');
    this.availableSlot = this.navParams.get('availableSlot');

  }

  ionViewDidLoad() {
    this.fetchTrainerList(); //FETCHING THE LIST OF TRAINER FROM JSON.
  }

  ionViewWillEnter() {
    this.presentLoading();  //SHOWING LOADER.
  }

  //FETCHING THE LIST OF TRAINER FROM JSON WITH THE HELP OF SELECT TRAINER PROVIDER.
  fetchTrainerList() {
    this.trainer.getSpecificTrainerDetails().map(response => response).subscribe((response) => {
      this.trainerList = response['selectTrainer'];
    });
  }

  //METHOD TO SHOW LOADER.
  presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 500
    });
    loader.present();
  }

  //CLOSING THE MODAL.
  closeModal() {
    this.viewCtrl.dismiss();
  }

  //METHOD TO MOVE TO DATE AND TIME PAGE.
  gotoDateTime(i) {
    this.index = i; //fetch index of trainer name array
    this.navCtrl.push('Repeatbooking', {
      'appointFull': this.appointFull,
      'purpose': this.purpose,
      'purposeTotalSessions': this.purposeTotalSessions,
      'icon': this.icon,
      'trainername': this.trainerList[this.index].TrainerName,
      'trainerType': this.trainerType,
      'trainerIcon': this.trainerList[this.index].TrainerPicture,
      'trainerrating': this.trainerList[this.index].TrainerRating,
      'ratingsurveycount': this.trainerList[this.index].RatingSurveyCount,
      'date': this.date,
      'availableSlot': this.availableSlot
    }); //WILL SEND THE DETAILS THROUGH NAV PARAMS.
  }

  //star rating logic for appropriate ratings
  starRating(rating: number) {
    let full;
    if (rating <= 5) {
      full = Math.floor(rating);
    }
    else {
      full = 5;
    }
    let status = [];
    for (let i = 0; i < full; i++) {
      status.push("fulle.svg"); // saves Full Star svg in array
    }
    if (rating - full == 0.5 && !(rating >= 5)) {
      status.push("halfe.svg"); // saves Half Star svg in array
    }
    if (status.length < 5) {
      let emp = 5 - status.length;
      for (let i = 0; i < emp; i++) {
        status.push("fullgrey.svg");// saves Empty Star svg in array
      }
    }
    return status; // Returns array so that it can be rendered using *ngFor
  }

}
