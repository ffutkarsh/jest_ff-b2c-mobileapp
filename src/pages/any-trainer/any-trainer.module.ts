import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnyTrainerPage } from './any-trainer';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AnyTrainerPage,
  ],
  imports: [
    IonicPageModule.forChild(AnyTrainerPage),
    TranslateModule
  ],
})
export class AnyTrainerPageModule {}
