import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { By } from '@angular/platform-browser';
import { AnyTrainerPage } from './any-trainer';

import { App,NavParams,NavController, Config, IonicModule, Platform, ViewController, DomController, Keyboard, GestureController, Form } from "ionic-angular";
import { mockApp, mockConfig, mockPlatform, mockView } from "ionic-angular/util/mock-providers";
import { ModalController, DeepLinker, LoadingController } from 'ionic-angular';
import { SelectTrainerProvider } from '../../providers/select-trainer/select-trainer-api';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule, ConnectionBackend , Http} from "@angular/http";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { Observable } from "rxjs";

import { ViewControllerMock,LoadingControllerMock,EventsMock ,NavParamsMock} from 'ionic-mocks-jest';


export class TranslateServiceStub{

	public get(key: any): any {
		Observable.of(key);
	}
}
describe('AnyTrainerPage', () => {


    
    let fixture: ComponentFixture<AnyTrainerPage>;
    let instance: AnyTrainerPage;
    let VController: ViewController;
    let lcontroller: LoadingController;
   let nparam: NavParamsMock
  
    beforeEach(() => {
        VController = ViewControllerMock.instance();
        lcontroller = LoadingControllerMock.instance();
        nparam = NavParamsMock.instance()
      TestBed.configureTestingModule({
        schemas: [
          NO_ERRORS_SCHEMA
        ],
        declarations: [
            AnyTrainerPage,
          
        ],
        imports: [
        
                     HttpClientModule,
                          HttpModule,
                          
                          IonicModule
                      ],
        providers: [
          
          SelectTrainerProvider,
          NavController,
          Config,
          Platform,
          DomController,
          Keyboard,
          App,
          {provide:NavParams, useFactory: () =>NavParamsMock.instance()},
           {provide:ViewController, useFactory: () =>ViewControllerMock.instance()},
           {provide:LoadingController, useFactory: () =>LoadingControllerMock.instance()},
         
          ConnectionBackend,
          Http,
        ]
      }).compileComponents();
    });
  
    beforeEach(() => {
      fixture = TestBed.createComponent(AnyTrainerPage);
      instance = fixture.debugElement.componentInstance;
    });
  
    it('should create Any trainer page', () => {
      expect(fixture).toBeTruthy;
    })
  
    
    // it('should create the Any trianer page', () => {
    //   expect(instance).toBeTruthy();
    // });
  
    // it('should have the method ioniviewdidleave', () => {
    //   expect(instance.ionViewDidLoad).toBeDefined();
    // });

    // it('should have ioniviewwillenter', () => {
    //     expect(instance.ionViewWillEnter).toBeDefined();
    //   });

    // //TEST CASE-1 TO CHECK IF IT CONTAINS class modal.
    // it('should contain the card with class name modal ', () => {
    //     const list: HTMLElement = fixture.debugElement.query(By.css('.modal')).nativeElement;
    //     expect(list.getAttribute('class')).toMatch("modal");
    // });


    // //TEST CASE-2 TO CHECK IF IT CONTAINS title.
    // it('should contain title with class name title', () => {
    //     const list: HTMLElement = fixture.debugElement.query(By.css('.title')).nativeElement;
    //     expect(list.getAttribute('class')).toMatch("title");
    // });

    // //TEST CASE-3 TO CHECK IF IT CONTAINS button.
    // it('should contain a close button ', () => {
    //     const list: HTMLButtonElement = fixture.debugElement.query(By.css('.closeButton')).nativeElement;
    //     expect(list.getAttribute('class')).toMatch("closeButton");
    // });

    // //TEST CASE-4 TO CHECK IF IT CONTAINS a icon.
    // it('should contain close icon ', () => {
    //     const list: HTMLElement = fixture.debugElement.query(By.css('.close')).nativeElement;
    //     expect(list.getAttribute('name')).toMatch("close");
    // });

    // //TEST CASE-5 TO CHECK IF IT CONTAINS a row.
    // it('should contain a row ', () => {
    //     fixture.whenStable().then(() => {
    //         fixture.detectChanges();
    //         const list: HTMLElement = fixture.debugElement.query(By.css('.tselection')).nativeElement;
    //         expect(list.getAttribute('class')).toMatch("tselection");
    //     });
    // });

    // //TEST CASE-3 TO CHECK IF IT CONTAINS a avatar.
    // it('should contain a avatar ', () => {
    //     fixture.whenStable().then(() => {
    //         fixture.detectChanges();
    //         const list: HTMLElement = fixture.debugElement.query(By.css('.anytavatar1')).nativeElement;
    //         expect(list.getAttribute('class')).toMatch("anytavatar1");
    //     });
    // });

    // //TEST CASE-3 TO CHECK IF IT CONTAINS a avatar image.
    // it('should contain a avatar image ', () => {
    //     fixture.whenStable().then(() => {
    //         fixture.detectChanges();
    //         const list: HTMLElement = fixture.debugElement.query(By.css('.anytavatarimg')).nativeElement;
    //         expect(list.getAttribute('class')).toMatch("anytavatarimg");
    //     });
    // });

    //     //TO CHECK METHOD presentLoading is  CREATED.
    // it("presentLoading method should be created", () => {
    //     expect(instance.presentLoading).toBeTruthy();
    // });

    // //     //TO CHECK METHOD gotoDateTime is  CREATED.
    // // it("gotoDateTime method should be created", () => {
    // //     expect(instance.gotoDateTime).toBeTruthy();
    // // });

    // //TO CHECK METHOD closeModal is  CREATED.
    // it("closeModal method should be created", () => {
    //     expect(instance.closeModal).toBeTruthy();
    // });







// describe("AnyTrainer Modal - FUNCTIONS", () => {
//     let component: AnyTrainerPage;
//     let fixture: ComponentFixture<AnyTrainerPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             imports: [
//                 IonicModule,
//                 HttpModule,
//                  HttpClientModule,
//                  TranslateModule.forChild()
//             ],
//             providers: [
//                 ModalController,
//                 LoadingController,
//                 TranslateService,
//                 TranslateStore,
//                 { provide: DeepLinker, useClass: DeepLinkerMock },
//                 { provide: ViewController, useValue: mockView() },
//                 { provide: Config, useValue: mockConfig() },
//                 { provide: Platform, useValue: mockPlatform() },
//                 { provide: App, useValue: mockApp() },
//                 { provide: NavParams, useClass: NavParamsMock },
//                 NavController, DomController, Keyboard, GestureController, Form,
//                 SelectTrainerProvider,
                
//             ],
//             declarations: [AnyTrainerPage]
//         }).compileComponents();
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(AnyTrainerPage);
//         component = fixture.componentInstance;
//         fixture.detectChanges();
//     });

//     //TO CHECK METHOD presentLoading is  CREATED.
//     it("presentLoading method should be created", () => {
//         expect(component.presentLoading).toBeTruthy();
//     });

//     //TO CHECK METHOD gotoDateTime is  CREATED.
//     it("gotoDateTime method should be created", () => {
//         expect(component.gotoDateTime).toBeTruthy();
//     });

//     //TO CHECK METHOD closeModal is  CREATED.
//     it("closeModal method should be created", () => {
//         expect(component.closeModal).toBeTruthy();
//     });
// });

// describe("AnyTrainer Modal - Logic Part", () => {
//     let component: AnyTrainerPage;
//     let fixture: ComponentFixture<AnyTrainerPage>;
//     let trainer: SelectTrainerProvider;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             imports: [
//                 IonicModule,
//                // HttpModule
//                HttpModule,
//                  HttpClientModule,
//                  TranslateModule.forChild()
//             ],
//             providers: [
//                 ModalController,
//                 LoadingController,
//                 TranslateService,
//                 TranslateStore,
//                 { provide: DeepLinker, useClass: DeepLinkerMock },
//                 { provide: ViewController, useValue: mockView() },
//                 { provide: Config, useValue: mockConfig() },
//                 { provide: Platform, useValue: mockPlatform() },
//                 { provide: App, useValue: mockApp() },
//                 { provide: NavParams, useClass: NavParamsMock },
//                 NavController, DomController, Keyboard, GestureController, Form,
//                 SelectTrainerProvider,
//                 //Http,
//                // ConnectionBackend
//             ],
//             declarations: [AnyTrainerPage]
//         }).compileComponents();
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(AnyTrainerPage);
//         component = fixture.componentInstance;
//         fixture.detectChanges();
//         trainer = fixture.debugElement.injector.get(SelectTrainerProvider); //INJECTING PROVIDER FOR DEPENDENCY.
//     });

//     //CARD 1 TEST CASE -1.
//     it("Card 1 purpose icon to be present.", async () => {
//         expect(component.purposeIcon).toBeDefined();
//     });

//     //CARD 1 TEST CASE -2.
//     it("Card 1 purpose name to be present.", async () => {
//         expect(component.purpose).toBeDefined();
//     });

//     //DATE AND TIME TEST CASE -1.
//     it("Date of Appoinment to be defined.", async () => {
//         expect(component.date).toBeDefined();
//     });

//     //DATE AND TIME TEST CASE -2.
//     it("Time of Appointment to be defined.", async () => {
//         expect(component.availableSlot).toBeDefined();
//     });

//     it('Star Rating Render Function - 1', () => {
//                             expect(component.starRating(2)).toContain('fulle.svg');
//                         });
                
//                         it('Star Rating Render Function - 2', () => {
//                             expect(component.starRating(3.5)).toContain('halfe.svg');
//                         });
                
//                         it('Star Rating Render Function - 3', () => {
//                             expect(component.starRating(5)).not.toContain('halfe.svg');
//                         });
                
//                         it('Star Rating Render Function - 4', () => {
//                             expect(component.starRating(100)).not.toContain('fullgrey.svg');
//                         });

//     //TOTAL NUMBER OF SESSIONS OF A PARTICULAR PURPOSE TEST CASE -1.
//     it("Number of sessions of a particular purpose to be defined.", async () => {
//         expect(component.purposeTotalSessions).toBeDefined();
//     });

//     //SELECT TRAINER PROVIDER - TEST CASE - 1.
//     it("Provider to be present", async () => {
//         expect(trainer instanceof SelectTrainerProvider).toBe(true);
//     });

// });


});


