// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { FeedbackPage } from './feedback';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from "@angular/http";
// import { Http, ConnectionBackend } from '@angular/http';

// describe('Feedback Page - UI Part', () => {
//     let de: DebugElement;
//     let comp: FeedbackPage;
//     let fixture: ComponentFixture<FeedbackPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [FeedbackPage],
//             imports: [
//                 IonicModule.forRoot(FeedbackPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams },
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(FeedbackPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Menu Bar to be present.", async () => {
//         fixture = TestBed.createComponent(FeedbackPage);
//         de = fixture.debugElement.query(By.css(".feedbackMenuBar")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Title of Menu bar to be present.", async () => {
//         const title: HTMLElement = fixture.debugElement.query(By.css('.menuBarTitle')).nativeElement;
//         expect(title.textContent).toContain("Feedback");
//     });

//     it("Div for the question to be present.", async () => {
//         fixture = TestBed.createComponent(FeedbackPage);
//         de = fixture.debugElement.query(By.css(".questionDiv")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it('Div text should be present.', () => {
//         expect(comp.question).toMatch("Would you like to tell FitnessForce Demo?");
//     });

//     it("List to be present.", async () => {
//         fixture = TestBed.createComponent(FeedbackPage);
//         de = fixture.debugElement.query(By.css(".listOfItems")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it('List Items to be Present.', () => {
//         fixture = TestBed.createComponent(FeedbackPage);
//         de = fixture.debugElement.query(By.css(".itemOfList")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Items1 should have mail svg.", async () => {
//         expect(comp.mail).toMatch("assets/icon/mail.svg");
//     });

//     it("Items1 should have heading.", async () => {
//         expect(comp.mailHeading).toMatch("Write to us.");
//     });

//     it("Items1 should have subcontext.", async () => {
//         expect(comp.mailSubContext).toMatch("Hi! I need help with something else...");
//     });

//     it("Items2 should have call svg.", async () => {
//         expect(comp.call).toMatch("assets/icon/call.svg");
//     });

//     it("Items2 should have heading.", async () => {
//         expect(comp.callHeading).toMatch("Call Customer Service");
//     });

//     it("Items2 should have subcontext.", async () => {
//         expect(comp.callSubContext).toMatch("You will be connected to concerned depar...");
//     });

// });