import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { TranslateService } from '@ngx-translate/core';
import  { FeedbackFlowPage } from '../../pages/pages';

@IonicPage()
@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html',
})
export class FeedbackPage {

  public question; //VARIABLE FOR BINDING QUESTION.

  public mail; //VARIABLE FOR BINDING MAIL SVG.
  public call; //VARIABLE FOR BINDING CALL SVG.

  public mailHeading; //VARIABLE FOR BINDING MAIL HEADING.
  public callHeading; //VARIABLE FOR BINDING CALL HEADING.

  public mailSubContext; //VARIABLE FOR BINDING MAIL SUB CONTEXT.
  public callSubContext; //VARIABLE FOR BINDING CALL SUB CONTEXT.

  constructor(public navCtrl: NavController, public navParams: NavParams, private callNumber: CallNumber,public translateService: TranslateService) {

    // this.question = "Would you like to tell FitnessForce Demo ?"; //QUESTION TO BE DISPLAYED.

    this.mail = "assets/icon/mail.svg"; //PATH OF MAIL SVG.
    this.call = "assets/icon/call.svg"; //PATH OF CALL SVG.

    // this.mailHeading = "Write to us."; //MAIL HEADING TEXT.
    // this.callHeading = "Call Customer Service"; //CALL HEADING TEXT.

    // this.mailSubContext = "Hi! I need help with something else..."; //MAIL SUB CONTEXT TEXT.
    // this.callSubContext = "You will be connected to concerned department"; //CALL SUB CONTEXT TEXT.

  }

  ionViewDidLoad() {
   
  }

  //Mailing Option
  mailOption() {
    
    this.navCtrl.push(FeedbackFlowPage, { 'feedbackType': 'mail' }); //SENDING FEEDBACK TYPE THROUGH NAV PARAMS TO FEEDBACKFLOWPAGE.
  }

  //Calling Option
  callOption() {
    this.callNumber.callNumber("9167361161", true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));    
  }

}
