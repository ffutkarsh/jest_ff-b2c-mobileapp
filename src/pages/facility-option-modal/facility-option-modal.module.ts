import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FacilityOptionModalPage } from './facility-option-modal';

@NgModule({
  declarations: [
    FacilityOptionModalPage,
  ],
  imports: [
    IonicPageModule.forChild(FacilityOptionModalPage),
  ],
})
export class FacilityOptionModalPageModule {}
