import { Component } from '@angular/core';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, LoadingController, NavParams } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { ValidPhoneProvider } from '../../providers/valid-phone/valid-phone';
import { ViewController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { AnyTrainerPage } from '../any-trainer/any-trainer';
import { ChooseTrainerAuthProvider } from '../../providers/choose-trainer-auth/choose-trainer-auth';
import { DateTimeAuthProvider } from '../../providers/date-time-auth/date-time-auth';
import { SelectTrainerProvider } from '../../providers/select-trainer/select-trainer-api';
import { ToastController } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-datetime',
    templateUrl: 'datetime.html',
})
export class Datetime {

    @ViewChild(Slides) slides: Slides;

    public appointFull; //FROM CHOOSE TRAINER PAGE.
    public purpose; //FROM CHOOSE TRAINER PAGE.
    public purposeTotalSessions; //FROM CHOOSE TRAINER PAGE.
    public icon; //FROM CHOOSE TRAINER PAGE.
    public cartOrbooked; //FROM CHOOSE TRAINER PAGE.
    public trainername; //FROM CHOOSE TRAINER PAGE.
    public trainerType; //FROM CHOOSE TRAINER PAGE.
    public trainerIcon; //FROM CHOOSE TRAINER PAGE.
    public mastertrainerlist; //FROM CHOOSE TRAINER PAGE.
    public trianerrating; //FROM CHOOSE TRAINER PAGE.
    public ratingsurveycount; //FROM CHOOSE TRAINER PAGE.

    public trainerList = []; //TO STORE TRAINER LIST.

    YearArray;
    datea;
    beforeDays;
    afterDays;
    DisplayafterDays;
    currentUpcomingDayAfterWeek;
    date; //finding date
    now: any; //variable for current date
    displayDate: any; //displaying current date of slider
    calcD: any //calculating date
    beforethat: any; //var to store day before yesterday's date
    before: any; //var to store yesterday's date
    dates: any; //array to store dates
    availableSlot; //var to store available slots
    datelist: any; //array to store list columns
    indexofSlides: number; //var to store index of slide in slider
    fetchDate; //fetching date from slider

    //initialising 3 tabs
    tab1Title = " ";
    tab2Title = " ";
    tab3Title = " ";

    indexOfSlot; //this is a variable to store index of slot

    dFormat: any; //storing today's date
    dddFormat: any; //storing today's day

    editFromApp;

    public current: any; //STORES CURRENT DATE AND TIME.
    public startTime: any; //STARTNG TIME OF TRANING.
    public slot: any; //TO CHECK WHETHER SLOT IS AVAILABLE OR NOT.
    public slots: any; //OBJECT OF ARRAYS OF OBJECTS IN SLOTS JSON.
    public sObj: any; //STARTING TIME.
    public addHour: any; //TIME AFTER ADDING INTERVAL.
    public slotTime = []; //TO STORE THE SLOTS AND RETURN IT FOR BINDING WITH THE BUTTONS.
    public halfSlots = []; //ARRAY TO DISPLAY HALF SLOTS.
    public onnLoadMore: boolean = true; //ENABLING THE LOAD MORE BUTTON INITIALLY.

    constructor(
        private toastCtrl: ToastController,
        public dateTimeAuthService: DateTimeAuthProvider,
        public chooseTrainerAuthService: ChooseTrainerAuthProvider,
        public modalCtrl: ModalController,
        public viewCtrl: ViewController,
        public popoverCtrl: PopoverController,
        public translateService: TranslateService,
        public loadingCtrl: LoadingController,
        public navCtrl: NavController,
        public navParams: NavParams,
        private validphone: ValidPhoneProvider,
        private trainer: SelectTrainerProvider) {

        this.editFromApp = false;
        this.current = moment(); //GETS CURRENT DATE AND TIME.
        this.datelist = [, , , , , , , , , , , , , , ,] //DEFAULT ARRAY OF SLOTS.
        this.YearArray = [];
        this.beforeDays = [];
        this.afterDays = [];
        this.DisplayafterDays = [];

        this.appointFull = this.navParams.get('appointFull');
        this.purpose = this.navParams.get("purpose");
        this.purposeTotalSessions = this.navParams.get("purposeTotalSessions");
        this.icon = this.navParams.get("icon");
        this.cartOrbooked = this.navParams.get('cartOrbooked');
        this.trainername = this.navParams.get("trainername");
        this.trainerType = this.navParams.get("trainerType");
        this.trainerIcon = this.navParams.get('trainerIcon');
        this.mastertrainerlist = this.navParams.get('mastertrainerlist');
        this.trianerrating = this.navParams.get('trainerrating');
        this.ratingsurveycount = this.navParams.get('ratingsurveycount');

        //3 tabs
        translateService.get(['TAB1_TITLE', 'TAB2_TITLE', 'TAB3_TITLE']).subscribe(values => {
            this.tab1Title = values['TAB1_TITLE'];
            this.tab2Title = values['TAB2_TITLE'];
            this.tab3Title = values['TAB3_TITLE'];
        });

        this.indexofSlides = 0;

        this.now = moment(); //find date
        this.dFormat = moment(this.now).format('D'); //finding day
        this.dddFormat = moment(this.now).format('ddd'); //finding date

        this.dates = new Array; //today's date in array
        this.before = moment(this.now).subtract(1, 'days'); //yesterday
        this.beforethat = moment(this.before).subtract(1, 'days'); //day before yesterday

        let temp = {
            'isColor': 'transparent',
            'textColor': '#E5E5E5',
            'date': this.before, //changing yesterdays color
        }

        let temp2 = {
            'isColor': 'transparent',
            'textColor': '#E5E5E5',
            'date': this.beforethat  //changing day before yesterdays color
        }

        let tempNow = {
            'isColor': '#69C258',
            'textColor': 'white',
            'date': this.now //changing todays color
        }

        this.dates.push(temp2); //pushing day before ysterday in array
        this.dates.push(temp); //pushing yesterday date in array
        this.dates.push(tempNow); //pushing todays date in array

        this.calcD = this.now; //calculating future dates
        let day = moment(this.now).format('dddd');
        this.displayDate = this.now;

        for (let i = 3; i <= 360; i++) {
            this.calcD = moment(this.calcD).add(1, 'days'); //adding +1 as date
            let calc = {
                'isColor': 'transparent',
                'textColor': 'black', //changing current dates color
                'date': this.calcD
            }
            this.dates.push(calc); //pushing to date array
        }

        if (day == 'Monday') {
            this.computePrevious(this.beforeDays, 1)
        }
        else if (day == 'Tuesday') {
            this.computePrevious(this.beforeDays, 2);
        }
        else if (day == 'Wednesday') {
            this.computePrevious(this.beforeDays, 3);
        }
        else if (day == 'Thursday') {
            this.computePrevious(this.beforeDays, 4);
        }
        else if (day == 'Friday') {
            this.computePrevious(this.beforeDays, 5);
        } else if (day == 'Saturday') {
            this.computePrevious(this.beforeDays, 6);
        }
        else {
            console.log('start');
        }

        //reverse to start from sunday
        this.beforeDays = this.beforeDays.reverse();
        this.computeNext(this.afterDays, this.beforeDays.length);
        this.currentUpcomingDayAfterWeek = this.afterDays[this.afterDays.length - 1];

        let beForeLength = this.beforeDays.length
        let FirstWeek = [];
        let afterLength = this.afterDays.length;
        let increment = 0;

        while (beForeLength > 0) {
            let temp = {
                'isColor': 'transparent',
                'textColor': '#E5E5E5',
                'date': this.beforeDays[increment], //changing yesterdays color
            }
            FirstWeek.push(temp);
            increment = increment + 1;
            beForeLength = beForeLength - 1;
        }

        let afterCounter = Math.abs(afterLength - beForeLength);
        let increment2 = 0;
        let tempafter;
        while (afterCounter > 0) {
            if (increment2 == 0) {
                tempafter = {
                    'isColor': '#8FDCE7',
                    'textColor': '#8FDCE7',
                    'date': this.afterDays[increment2], //changing yesterdays color
                }
            } else {
                tempafter = {
                    'isColor': 'transparent',
                    'textColor': 'black',
                    'date': this.afterDays[increment2], //changing yesterdays color
                }
            }

            FirstWeek.push(tempafter);
            increment2 = increment2 + 1;
            afterCounter = afterCounter - 1;
        }

        this.YearArray.push(FirstWeek)
        this.computeYear(this.currentUpcomingDayAfterWeek)
    }

    ionViewDidLoad() {
        this.fetchScheduleDetails();
        this.Getslots(); //CALL FOR GETTING DATA FROM JSON ON VIEW LOAD.
    }

    //from json using its provider
    fetchScheduleDetails() {
        this.trainer.getSpecificTrainerDetails().map(response => response).subscribe((response) => {
            this.trainerList = response['selectTrainer'];
            if (this.trainerList.length == 1) {
                this.navCtrl.remove(2, 1); // This will remove the 'trainer' from stack.
                this.navCtrl.pop(); //will pop to shedule list
            }
        });
    }

    //GET THE SLOTS FROM JSON.
    Getslots() {
        this.validphone.getSlots().map((response) => response.json()).subscribe((response) => {
            this.slots = response;
            if (this.slots.slots0.length == 0) {
                this.presentEmptyToast();
            }
            else {
                this.datelist = this.slots.slots0; //ASSIGNING VALUES OF SLOTS0 ARRAY FROM JSON TO DEFAULT DATELIST ARRAY.
                for (let i = 0; i < this.slots.slots0.length; i++) {

                    //STARTING TIME.
                    this.sObj = moment(this.slots.slots0[i].startTime, 'hh:mm').format('hh:mm');

                    //TIME AFTER ADDING INTERVAL.
                    this.addHour = moment(this.slots.slots0[i].startTime, 'hh:mm').add('hours', 1).format('hh:mm');

                    //MAINTAINING SLOTS ARRAY CONTAINING START TIME AND END TIME OF SESSION.
                    this.slotTime[i] = this.sObj + "-" + this.addHour;

                } //END OF FOR.

                for (let i = 0; i < 22; i++) { //displaying buttons on screen
                    this.halfSlots[i] = this.slotTime[i];
                }
            }
        });
    }

    presentEmptyToast() {
        let toast = this.toastCtrl.create({
            message: 'No Slots Available, Please check again later',
            duration: 3000,
            position: 'bottom'
        });

        toast.onDidDismiss(() => {
            this.navCtrl.setRoot("SchedulingPage");
            this.navCtrl.popAll();
        });

        toast.present();
    }

    computePrevious(arr, count) {
        for (let i = 0; i < count; i++) {
            let addDay = moment(this.now).subtract(i + 1, 'days');
            arr.push(addDay);
        }
    }

    computeNext(arr, count) {
        for (let i = 0; i < 7 - count; i++) {
            let addDay = moment(this.now).add(i, 'days');
            arr.push(addDay);
        }
    }

    computeYear(date) {
        //Logic For First week 
        let TempArray = []
        let Dateholder;
        for (let j = 0; j < 52; j++) {
            for (let i = 1; i <= 7; i++) {
                let DatetoBeAdded = moment(date).add(i, 'days');
                let temp = {
                    'isColor': 'transparent',
                    'textColor': 'black',
                    'date': DatetoBeAdded, //changing yesterdays color
                }
                TempArray.push(temp);
                Dateholder = DatetoBeAdded
            }
            this.YearArray.push(TempArray)
            TempArray = []
            date = Dateholder;
        }
    }

    setMyStyles() {
        if (this.editFromApp == true) {
            let style = {
                'opacity': '0.5',
            };
            return style; // returning color of specific index
        }
    }

    slidePrev() {
        this.slides.slidePrev();
    }

    slideNext() {
        this.slides.slideNext();
    }

    //nikhils
    selectDateNew(item) {
        this.datea = moment(item.date).format('ddd, DD MMM YYYY');
        for (let j = 0; j < 53; j++) {
            if (j == 0) {
                for (let i = this.beforeDays.length; i < 7; i++) {
                    if (this.YearArray[j][i].date == item.date) {
                        this.YearArray[j][i].isColor = '#8FDCE7';
                        this.YearArray[j][i].textColor = '#8FDCE7';
                        var newdate = this.YearArray[j][i]
                    }
                    else {
                        this.YearArray[j][i].isColor = 'transparent';
                        this.YearArray[j][i].textColor = 'black';
                    }
                }
            }
            else {
                for (let i = 0; i < 7; i++) {
                    if (this.YearArray[j][i].date == item.date) {
                        this.YearArray[j][i].isColor = '#8FDCE7';
                        this.YearArray[j][i].textColor = '#8FDCE7';
                        console.log(this.YearArray[j][i], "NIK DATE1234")
                    } else {
                        this.YearArray[j][i].isColor = 'transparent';
                        this.YearArray[j][i].textColor = 'black';
                    }
                }
            }
        }
    }

    chckAvailAbility(slotval) {
        this.slot = false;
        //slotval array present Json Api
        if (slotval) {
            this.slot = true;
        }
        return this.slot
    }

    // nav params to go to new page -  RepeatBooking
    gotoRepeat(i) {
        this.indexOfSlot = i;

        //STARTING TIME.
        this.sObj = moment(this.slots.slots0[this.indexOfSlot].startTime, 'hh:mm').format('hh:mm A');

        //TIME AFTER ADDING INTERVAL.
        this.addHour = moment(this.slots.slots0[this.indexOfSlot].startTime, 'hh:mm').add('hours', 1).format('hh:mm A');

        //MAINTAINING SLOTS ARRAY CONTAINING START TIME AND END TIME OF SESSION.
        let slottosend = this.sObj + " -" + this.addHour;

        if (this.editFromApp == true) {

            if (this.datea === undefined) {

                    this.now = moment().format('ddd, DD MMM YYYY');
                    this.datea = this.now
                    this.navCtrl.push('AppointmentBookedPage', {
                        'appointFull': this.appointFull,
                        'purpose': this.purpose,
                        'purposeTotalSessions': this.purposeTotalSessions,
                        'icon': this.icon,
                        'cartOrbooked': this.cartOrbooked,
                        'trainername': this.trainername,
                        'trainerType': this.trainerType,
                        'trainerIcon': this.trainerIcon,
                        'mastertrainerlist': this.mastertrainerlist,
                        'trainerrating': this.trianerrating,
                        'ratingsurveycount': this.ratingsurveycount,
                        'date': this.datea,
                        'availableSlot': slottosend,
                        'updated': true
                    });
                
            }
            else {

                    this.navCtrl.push('AppointmentBookedPage', {
                        'appointFull': this.appointFull,
                        'purpose': this.purpose,
                        'purposeTotalSessions': this.purposeTotalSessions,
                        'icon': this.icon,
                        'cartOrbooked': this.cartOrbooked,
                        'trainername': this.trainername,
                        'trainerType': this.trainerType,
                        'trainerIcon': this.trainerIcon,
                        'mastertrainerlist': this.mastertrainerlist,
                        'trainerrating': this.trianerrating,
                        'ratingsurveycount': this.ratingsurveycount,
                        'date': this.datea,
                        'availableSlot': slottosend,
                        'updated': true
                    });
                
            }
        }

        //IF TRAINER IS SELECTED FROM "ANY TRAINER".
        else if (this.trainerType == 'Any Trainer') {

            //IF DATE IS UNDEFINED [NOT SELECTED FROM DATE SLIDER].
            if (this.datea === undefined) {
                this.now = moment().format('ddd, DD MMM YYYY');
                this.datea = this.now
                // this.fetchDate = moment(this.now).format('ddd, DD MMM YYYY');
                if (this.indexOfSlot == i) {
                    let myModal = this.modalCtrl.create(AnyTrainerPage, {
                        'appointFull': this.appointFull,
                        'purpose': this.purpose,
                        'icon': this.icon,
                        'purposeTotalSessions': this.purposeTotalSessions,
                        'cartOrbooked': this.cartOrbooked,
                        'trainername': this.trainername,
                        'trainerType': this.trainerType,
                        'trainerIcon': this.trainerIcon,
                        'mastertrainerlist': this.mastertrainerlist,
                        'trainerrating': this.trianerrating,
                        'ratingsurveycount': this.ratingsurveycount,
                        'date': this.datea,
                        'availableSlot': slottosend
                    });
                    myModal.present();
                }
            }
            //IF DATE IS DEFINED [SELECTED FROM DATE SLIDER]..
            else {
                //this.datea.format('ddd, DD MMM YYYY')
                if (this.indexOfSlot == i) {
                    let myModal = this.modalCtrl.create(AnyTrainerPage, {
                        'appointFull': this.appointFull,
                        'purpose': this.purpose,
                        'icon': this.icon,
                        'purposeTotalSessions': this.purposeTotalSessions,
                        'cartOrbooked': this.cartOrbooked,
                        'trainername': this.trainername,
                        'trainerType': this.trainerType,
                        'trainerIcon': this.trainerIcon,
                        'mastertrainerlist': this.mastertrainerlist,
                        'trainerrating': this.trianerrating,
                        'ratingsurveycount': this.ratingsurveycount,
                        'date': this.datea,
                        'availableSlot': slottosend
                    });
                    myModal.present();
                }
            }
        }

        //IF TRAINER IS SELECTED FROM "CHOOSE SPECIFIC".
        else if (this.trainerType == "Choose Specific") {

            if (this.editFromApp == true) {
                this.navCtrl.push('AppointmentBookedPage', {
                    'appointFull': this.appointFull,
                    'purpose': this.purpose,
                    'icon': this.icon,
                    'purposeTotalSessions': this.purposeTotalSessions,
                    'cartOrbooked': this.cartOrbooked,
                    'trainername': this.trainername,
                    'trainerType': this.trainerType,
                    'trainerIcon': this.trainerIcon,
                    'mastertrainerlist': this.mastertrainerlist,
                    'trainerrating': this.trianerrating,
                    'ratingsurveycount': this.ratingsurveycount,
                    'date': this.datea,
                    'availableSlot': slottosend,
                    'updated': true
                });
            }
            //IF DATE IS UNDEFINED [NOT SELECTED FROM DATE SLIDER].
            if (this.datea === undefined) {
                this.now = moment().format('ddd, DD MMM YYYY');
                console.log(this.now, "undefined date")
                this.datea = this.now
                //  this.fetchDate = moment(this.now).format('ddd, DD MMM YYYY');
                if (this.indexOfSlot == i) {
                    this.navCtrl.push('Repeatbooking', {
                        'appointFull': this.appointFull,
                        'purpose': this.purpose,
                        'icon': this.icon,
                        'purposeTotalSessions': this.purposeTotalSessions,
                        'cartOrbooked': this.cartOrbooked,
                        'trainername': this.trainername,
                        'trainerType': this.trainerType,
                        'trainerIcon': this.trainerIcon,
                        'mastertrainerlist': this.mastertrainerlist,
                        'trainerrating': this.trianerrating,
                        'ratingsurveycount': this.ratingsurveycount,
                        'date': this.datea,
                        'availableSlot': slottosend
                    });
                }
            }
            //IF DATE IS DEFINED [SELECTED FROM DATE SLIDER].
            else {
                if (this.indexOfSlot == i) {
                    this.navCtrl.push('Repeatbooking', {
                        'appointFull': this.appointFull,
                        'purpose': this.purpose,
                        'icon': this.icon,
                        'purposeTotalSessions': this.purposeTotalSessions,
                        'cartOrbooked': this.cartOrbooked,
                        'trainername': this.trainername,
                        'trainerType': this.trainerType,
                        'trainerIcon': this.trainerIcon,
                        'mastertrainerlist': this.mastertrainerlist,
                        'trainerrating': this.trianerrating,
                        'ratingsurveycount': this.ratingsurveycount,
                        'date': this.datea,
                        'availableSlot': slottosend
                    });
                }
            }
        }
        this.dateTimeAuthService.fromDateTimepage();
    }

    //DISPLAYING THE SLOTS INTERVAL.
    slotInterval(index) {
        this.halfSlots = this.slotTime;
        return this.halfSlots[index];
    }

    ionViewWillEnter() {
        this.editFromApp = this.navParams.get("booleanFromEdit");
        if (this.editFromApp == true) {
            this.purpose = this.navParams.get("purpose"); //RECEIVING PURPOSE.
            this.icon = this.navParams.get("icon"); //RECEIVING PURPOSE ICON.
            this.trainername = this.navParams.get("trainername"); //RECEIVING TRAINER NAME.
            this.trianerrating = this.navParams.get("trainerrating");
            this.ratingsurveycount = this.navParams.get("ratingsurveycount");
            this.date = this.navParams.get("date"); //RECEIVING DATE.
            this.availableSlot = this.navParams.get("availableSlot"); //RECEIVING AVAILABLE SLOT.
        }
    }

}
