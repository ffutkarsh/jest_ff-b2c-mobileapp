import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Datetime } from './datetime';
import { TranslateModule } from '@ngx-translate/core';

import { MomentModule } from 'angular2-moment';
@NgModule({
  declarations: [
    Datetime,
  ],
  imports: [
    IonicPageModule.forChild(Datetime),
    TranslateModule,
    MomentModule
  ],
})
export class DatetimeModule {}
