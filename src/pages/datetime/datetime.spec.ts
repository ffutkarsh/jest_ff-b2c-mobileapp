// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, ViewChild } from '@angular/core';
// import { Datetime } from './datetime';
// import { IonicModule, Platform, NavController, NavParams } from 'ionic-angular/index';
// import { MomentModule } from 'angular2-moment';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
// import * as moment from 'moment';
// import { ValidPhoneProvider } from '../../providers/valid-phone/valid-phone';
// import { Http, ConnectionBackend } from '@angular/http';
// import { HttpModule } from "@angular/http";
// import { mockApp, mockConfig, mockPlatform, mockView } from "ionic-angular/util/mock-providers";
// import { App, Config, ViewController, DomController, Keyboard, GestureController, Form } from "ionic-angular";
// import { ChooseTrainerAuthProvider } from '../../providers/choose-trainer-auth/choose-trainer-auth';
// import { DateTimeAuthProvider } from '../../providers/date-time-auth/date-time-auth';
// import { ToastController } from 'ionic-angular';
// import { SelectTrainerProvider } from '../../providers/select-trainer/select-trainer';
// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// describe('*****Date and time UI Testing*****', () => {
//     let de: DebugElement;
//     let comp: Datetime;
//     let fixture: ComponentFixture<Datetime>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [Datetime],
//             imports: [
//                 TranslateModule.forRoot(),
//                 IonicModule.forRoot(Datetime),
//                 IonicModule.forRoot(ViewChild),
//                 MomentModule,
//                 HttpModule
//             ],
//             providers: [
//                 { provide: ViewController, useValue: mockView() },
//                 TranslateService,
//                 TranslateModule,
//                 NavController,
//                 ChooseTrainerAuthProvider,DateTimeAuthProvider,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 ValidPhoneProvider,
//                 Http,
//                 ConnectionBackend,
//                 HttpModule,
//                 ToastController,
//                 SelectTrainerProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(Datetime);
//         comp = fixture.componentInstance;
//     });

//     //To check component created.
//     it('should create component', () => expect(comp).toBeDefined());

//     //  display color of background of header as purple
//     it('should display color of background of header as purple', () => {
//         const background: HTMLElement = fixture.debugElement.query(By.css('.primary')).nativeElement;
//         expect(background.getAttribute('color')).toMatch('purple');
//     });

//     //Displaying title Choose time slot
//     it('should display original title', () => {
//         const title: HTMLElement = fixture.debugElement.query(By.css('.title')).nativeElement;
//         expect(title.textContent).toContain("Choose Timing");
//     });

//     //CARD SECTION TEST CASE -1.
//     it("Grid for cards to be present.", async () => {
//         fixture = TestBed.createComponent(Datetime);
//         de = fixture.debugElement.query(By.css(".mygrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //CARD SECTION TEST CASE -2.
//     it("Div for cards to be present.", async () => {
//         fixture = TestBed.createComponent(Datetime);
//         de = fixture.debugElement.query(By.css(".belowheader")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //CARD SECTION TEST CASE -3.
//     it("Row for cards to be present.", async () => {
//         fixture = TestBed.createComponent(Datetime);
//         de = fixture.debugElement.query(By.css(".row")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //CARD SECTION TEST CASE -4.
//     it("Card 1 to be present.", async () => {
//         fixture = TestBed.createComponent(Datetime);
//         de = fixture.debugElement.query(By.css(".card1")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //CARD SECTION TEST CASE -5.
//     it("Card 1 heading to be present.", async () => {
//         fixture = TestBed.createComponent(Datetime);
//         de = fixture.debugElement.query(By.css(".card-header1")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //CARD SECTION TEST CASE -6.
//     it("Card 2 to be present.", async () => {
//         fixture = TestBed.createComponent(Datetime);
//         de = fixture.debugElement.query(By.css(".card2")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //CARD SECTION TEST CASE -7.
//     it("Card 2 heading to be present.", async () => {
//         fixture = TestBed.createComponent(Datetime);
//         de = fixture.debugElement.query(By.css(".card-header2")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     // to check main grid class 
//     it('Checking main grid   ', () => {
//         const row: HTMLElement = fixture.debugElement.query(By.css('.MainGrid')).nativeElement;
//         expect(row.getAttribute('class')).toMatch("MainGrid");
//     });

//     // to check button row class 
//     it('Checking button row ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const row: HTMLElement = fixture.debugElement.query(By.css('.ButtonRow')).nativeElement;
//             expect(row.getAttribute('class')).toMatch("ButtonRow");
//         });
//     });

//     //Displaying  column of buttons
//     it('should display column of buttons', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const title: HTMLElement = fixture.debugElement.query(By.css('.ButtonCol')).nativeElement;
//             expect(title.getAttribute('name')).toMatch('ButtonCol');
//         });
//     });

//     //Displaying title Choose time slot buttons
//     it('should display time buttons ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const title: HTMLElement = fixture.debugElement.query(By.css('.SlotButtons')).nativeElement;
//             expect(title.getAttribute('name')).toMatch('SlotButtons');
//         });
//     });

    
//     //checking go to timer method
//     it('checking timer method ', () => {
//         expect(comp.startTimer1).toBeTruthy();
//     });
//     //checking go to goToSchedule method
//     it('checking goToSchedule method ', () => {
//         expect(comp.goToSchedule).toBeTruthy();
//     });
//     //checking fromChooseTrainerpage method
//     it('checking fromChooseTrainerpage method ', () => {
//         expect(comp.fromChooseTrainerpage).toBeTruthy();
//     });
// });

// describe('*****Date and time Logic Testing*****', () => {
//     let de: DebugElement;
//     let comp: Datetime;
//     let fixture: ComponentFixture<Datetime>;
//     let validphone: ValidPhoneProvider;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [Datetime],
//             imports: [
//                 TranslateModule.forRoot(),
//                 IonicModule.forRoot(Datetime),
//                 IonicModule.forRoot(ViewChild),
//                 MomentModule,
//                 HttpModule
//             ],
//             providers: [
//                 { provide: ViewController, useValue: mockView() },
//                 TranslateService,
//                 ChooseTrainerAuthProvider,DateTimeAuthProvider,
//                 TranslateModule,
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 ValidPhoneProvider,
//                 Http,
//                 ConnectionBackend,
//                 HttpModule,
//                 ToastController,
//                 SelectTrainerProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(Datetime); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//         validphone = fixture.debugElement.injector.get(ValidPhoneProvider);
//     });

//     // checking the button on schedule page
//     it("Sucessfull Booking  Button to schedule  to be shown ", async () => {
//         expect(comp.onnLoadMore).toBe(true);
//     });

//     //checking if object is created or no
//     it('Instance of Valid Phone Provided created', () => {
//         expect(validphone instanceof ValidPhoneProvider).toBe(true);
//     });

//     //checking the slot availability
//     it('Slot Availability', () => {
//         comp.chckAvailAbility(true);
//         expect(comp.slot).toBe(true);
//     });

//     //checking slot unavailability
//     it('Slot Unavailability', () => {
//         comp.chckAvailAbility(false);
//         expect(comp.slot).toBe(false);
//     });

//     //CARD 1 TEST CASE -1.
//     it("Card 1 purpose icon to be present.", async () => {
//         expect(comp.icon).toBeDefined();
//     });

//     //CARD 1 TEST CASE -2.
//     it("Card 1 purpose name to be present.", async () => {
//         expect(comp.purpose).toBeDefined();
//     });

//     //CARD 2 TEST CASE -1.
//     it("Card 2 trainer image to be present.", async () => {
//         expect(comp.trainerImage).toBeDefined();
//     });

//     //CARD 2 TEST CASE -2.
//     it("Card 2 trainer name to be present.", async () => {
//         expect(comp.trainername).toBeDefined();
//     });

//     //TOTAL NUMBER OF SESSIONS OF A PARTICULAR PURPOSE TEST CASE -1.
//     it("Number of sessions of a particular purpose to be defined.", async () => {
//         expect(comp.purposeTotalSessions).toBeDefined();
//     });
// });

// describe('Date and time - Calendar slider', () => {
//     let de: DebugElement;
//     let comp: Datetime;
//     let fixture: ComponentFixture<Datetime>;
//     let validphone: ValidPhoneProvider;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [Datetime],
//             imports: [
//                 TranslateModule.forRoot(),
//                 IonicModule.forRoot(Datetime),
//                 IonicModule.forRoot(ViewChild),
//                 MomentModule,
//                 HttpModule
//             ],
//             providers: [
//                 { provide: ViewController, useValue: mockView() },
//                 TranslateService,
//                 TranslateModule,
//                 NavController,
//                 ChooseTrainerAuthProvider,DateTimeAuthProvider,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 ValidPhoneProvider,
//                 Http,
//                 ConnectionBackend,
//                 HttpModule,
//                 ToastController,
//                 SelectTrainerProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(Datetime);
//         comp = fixture.componentInstance;
//         validphone = TestBed.get(ValidPhoneProvider);
//         validphone = fixture.debugElement.injector.get(ValidPhoneProvider);
//     });

//     //calendar
//     //Displaying left icon
//     it('should display left arrow icon ', () => {
//         const icon: HTMLElement = fixture.debugElement.query(By.css('.iconArrowLeft')).nativeElement;
//         expect(icon.getAttribute('name')).toMatch("md-arrow-dropleft");
//     });

//     //Displaying right icon
//     it('should display right arrow icon ', () => {
//         const icon: HTMLElement = fixture.debugElement.query(By.css('.iconArrowRight')).nativeElement;
//         expect(icon.getAttribute('name')).toMatch("md-arrow-dropright");
//     });

//     //checking slides 
//     it('should display slide ', () => {
//         const icon: HTMLElement = fixture.debugElement.query(By.css('.width-slide')).nativeElement;
//         expect(icon.getAttribute('class')).toMatch("width-slide");
//     });

//     // checking pixels of slides
//     it('space between slides should be 5.8px ', () => {
//         const icon: HTMLElement = fixture.debugElement.query(By.css('.slide1')).nativeElement;
//         expect(icon.getAttribute('spaceBetween')).toMatch("5.8px");
//     });

//     // slides should be in center
//     it('centered Slides should be true ', () => {
//         const icon: HTMLElement = fixture.debugElement.query(By.css('.slide1')).nativeElement;
//         expect(icon.getAttribute('centeredSlides')).toBeTruthy();
//     });

//     //Checking todays date
//     it('checking todays date ', () => {
//         this.now = moment();
//         this.dFormat = moment(this.now).format('D');
//         expect(comp.dFormat).toMatch(this.dFormat);
//     });

//     //Checking todays day
//     it('checking todays day ', () => {
//         this.now = moment();
//         this.dddFormat = moment(this.now).format('ddd');
//         expect(comp.dddFormat).toMatch(this.dddFormat);
//     });

//     //Checking the balance session cost of  using class name
//     it('checking the VIRTUAL LIST ', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLElement = fixture.debugElement.query(By.css('.virtualList')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('virtualList');
//         });
//     });

//     it('checking the Virtual item', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLElement = fixture.debugElement.query(By.css('.virtualItem')).nativeElement;
//             expect(inputfield.getAttribute('class')).toMatch('virtualItem');
//         });
//     });
// });