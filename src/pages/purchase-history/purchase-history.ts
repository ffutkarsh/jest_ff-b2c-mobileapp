import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { File } from '@ionic-native/file';
import * as pdfmake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { EmailComposer } from '@ionic-native/email-composer';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-purchase-history',
  templateUrl: 'purchase-history.html',
})
export class PurchaseHistoryPage {

  public purchaseHistoryobject; //fetch object from previous page
  public bill = []; // array to store bill

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public file: File,
    public translateService: TranslateService,
    public toastCtrl: ToastController,
    private emailComposer: EmailComposer) {
    this.purchaseHistoryobject = this.navParams.get('objectPlist'); //fetch object from previous page
    this.bill.push(this.purchaseHistoryobject); //push object into bill
  }

  ionViewDidLoad() {
  }


  DownloadReceipt() { //function to download a receipt
    let self = this;
    pdfmake.vfs = pdfFonts.pdfMake.vfs;

    //step 1: design page & add content
    var docDefinition = { //content in a page
      content: [
        { text: 'Purchase History \n\n', style: 'header' },
        { text: this.bill[0].billNumber, style: 'header' },
        { text: 'Purchase Date: ' + this.bill[0].purchaseDate, style: 'header' },
        { text: this.bill[0].location + ' ' + moment(this.bill[0].time, "HH:mm:ss").format("hh:mm A"), style: 'header' },
        '\n\n',
        {
          columns: [
            {
              text: 'Total Bill Amount: ' + this.bill[0].totalAmount, bold: true, fontSize: 18, alignment: 'left'
            },
            {
              text: 'Balance Due: ' + this.bill[0].balanceAmount, bold: true, fontSize: 18, alignment: 'right'
            }
          ]
        },
        '\n\n',
        { text: 'Order Summary \n\n', bold: true },
        { text: 'Sub Total: ' + this.bill[0].subtoal, style: 'sub_header' },
        { text: 'Taxes: ' + this.bill[0].taxAmount, style: 'sub_header' },
        { text: 'Total: ' + this.bill[0].total, style: 'sub_header' },
        { text: 'Amount Paid: ' + this.bill[0].paidAmount, style: 'sub_header' },
        { text: '----------------------------------------------------------------', style: 'sub_header' },
        { text: 'Balance Due: ' + this.bill[0].balanceAmount, style: 'sub_header' }
      ],
      styles: {
        header: {
          bold: true,
          fontSize: 20,
          alignment: 'left'
        },
        sub_header: {
          fontSize: 18,
          alignment: 'right'
        },
        url: {
          fontSize: 16,
          alignment: 'right'
        }
      },
      pageSize: 'A4',
      pageOrientation: 'portrait'
    };

    //step 2 : create a pdf 
    pdfmake.createPdf(docDefinition).getBuffer(function (buffer) { //
      let utf8 = new Uint8Array(buffer);
      let binaryArray = utf8.buffer;
      //step 3: save pdf file to device
      self.saveToDevice(binaryArray, "PurchaseHistory.pdf")
    });

  }


  //Function to save pdf to device
  saveToDevice(data: any, savefile: any) {
    let self = this;
    self.file.writeFile(self.file.externalDataDirectory, savefile, data, { replace: false }); //writing to file
    const toast = self.toastCtrl.create({ // creating toast
      message: 'File saved to your device',
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }



  EmailReceipt() { //function to email the receipt
    this.emailComposer.isAvailable().then((available: boolean) => {
      if (available) {
        //Now we know we can send
      }
    });

    let self = this;
    pdfmake.vfs = pdfFonts.pdfMake.vfs;
    //step 1: design page & add content
    var docDefinition = {
      content: [
        { text: 'Purchase History \n\n', style: 'header' },
        { text: this.bill[0].billNumber, style: 'header' },
        { text: 'Purchase Date: ' + this.bill[0].purchaseDate, style: 'header' },
        { text: this.bill[0].location + ' ' + this.bill[0].time, style: 'header' },
        '\n\n',
        {
          columns: [
            {
              text: 'Total Bill Amount: ' + this.bill[0].totalAmount, bold: true, fontSize: 18, alignment: 'left'
            },
            {
              text: 'Balance Due: ' + this.bill[0].balanceAmount, bold: true, fontSize: 18, alignment: 'right'
            }
          ]
        },
        '\n\n',
        { text: 'Order Summary \n\n', bold: true },
        { text: 'Sub Total: ' + this.bill[0].subtoal, style: 'sub_header' },
        { text: 'Taxes: ' + this.bill[0].taxAmount, style: 'sub_header' },
        { text: 'Total: ' + this.bill[0].total, style: 'sub_header' },
        { text: 'Amount Paid: ' + this.bill[0].paidAmount, style: 'sub_header' },
        { text: '----------------------------------------------------------------', style: 'sub_header' },
        { text: 'Balance Due: ' + this.bill[0].balanceAmount, style: 'sub_header' }
      ],
      styles: {
        header: {
          bold: true,
          fontSize: 20,
          alignment: 'left'
        },
        sub_header: {
          fontSize: 18,
          alignment: 'right'
        },
        url: {
          fontSize: 16,
          alignment: 'right'
        }
      },
      pageSize: 'A4',
      pageOrientation: 'portrait'
    };

    //step 2 : create a pdf 
    pdfmake.createPdf(docDefinition).getBuffer(function (buffer) {
      let utf8 = new Uint8Array(buffer);
      let binaryArray = utf8.buffer;
      self.saveToDevice(binaryArray, "PurchaseHistory.pdf")  //step 3: save pdf file to device
    });

    //step 4: email to given receipent
    let email = {
      to: 'shivani@fitnessforce.com',
      attachments: [
        self.file.externalDataDirectory + "PurchaseHistory.pdf"
      ],
      subject: 'Your Purchase History',
      body: 'Your Receipt...!!!',
      isHtml: true
    };

    // Send a text message using default options
    this.emailComposer.open(email);
  }

}
