import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PurchaseHistoryPage } from './purchase-history';
import { TranslateModule } from '@ngx-translate/core';
import { MomentModule } from 'angular2-moment';

@NgModule({
  declarations: [
    PurchaseHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(PurchaseHistoryPage),
    TranslateModule,
    MomentModule
  ],
})
export class PurchaseHistoryPageModule {}
