import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentOptionsPage } from './payment-options';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    PaymentOptionsPage
  ],
  imports: [
    IonicPageModule.forChild(PaymentOptionsPage),
    TranslateModule
  ],
})
export class PaymentOptionsPageModule { }
