import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams } from 'ionic-angular';
import { PaymentFailedPage } from '../../pages/pages';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';

declare var RazorpayCheckout: any;

@IonicPage()
@Component({
  selector: 'page-payment-options',
  templateUrl: 'payment-options.html',
})
export class PaymentOptionsPage {

  Paymentoptions = []; //ARRAY OF OBJECTS OF VARIOUS PAYMENT OPTIONS.

  public cartProducts; //FROM CART ITEM PAGE OR WAIVER SUCCESS PAGE.
  public additionalProducts; //FROM CART ITEM PAGE OR WAIVER SUCCESS PAGE.
  public totalProducts; //FROM CART ITEM PAGE OR WAIVER SUCCESS PAGE.
  public totalPrice; //FROM CART ITEM PAGE OR WAIVER SUCCESS PAGE.
  public discountAmount; //FROM CART ITEM PAGE OR WAIVER SUCCESS PAGE.
  public appFlowCart;
  public receivedWallet;
  public receivedWalletBoolean;
  public phonenumber = []; //TO STORE THE DIAL CODE AND ACTUAL PHONENUMBER SEPARATELY.

  //USER DETAILS FROM IONIC STORAGE.
  public userDetails = {
    memberID: "",
    firstName: "",
    middleName: "",
    lastName: "",
    emailAddress: "",
    phoneNumber: "",
    picture: "",
    memberStatus: "",
    memberType: "",
    companyID: "",
    tenantID: "",
    companyName: "",
    tenantName: "",
    tenantlocation: "",
    tenantLogo: "",
    tenantTermsOfserviceUrl: "",
    tenantPrivacyPolicyUrl: ""
  }

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    private storage: Storage,
    public translateService: TranslateService) {

    this.appFlowCart = this.navParams.get("appFlow"); //from app flow.
    this.cartProducts = this.navParams.get('cartProducts');
    this.additionalProducts = this.navParams.get('additionalProducts');
    this.totalProducts = this.navParams.get('totalProducts');
    this.totalPrice = this.navParams.get('totalPrice');
    this.discountAmount = this.navParams.get('discountAmount');

    this.receivedWallet = this.navParams.get('totalPrice');

    if (this.receivedWallet == 0) {
      this.receivedWallet = 0.01
    }

    this.receivedWalletBoolean = this.navParams.get('booleanWallet');
    console.log("Details From Cart Page");
    console.log(this.cartProducts);
    console.log(this.additionalProducts);
    console.log(this.totalProducts);
    console.log(this.totalPrice);
    console.log(this.discountAmount);
    this.storage.set('CartDetails', {
      'cartProducts': this.cartProducts,
      'additionalProducts': this.additionalProducts,
      'totalProducts': this.totalProducts,
      'totalPrice': this.totalPrice,
      'discountAmount': this.discountAmount
    });

    this.Paymentoptions = [
      {
        'paymentMode': 'Wallets',
        'subtext': '(Mobikwik, PayU, Freecarge)',
        'paymentModeIcon': '',
        'isSelected': false,
        'buttonText': '',
        'razorPayoptionName': 'wallet',
        'paymenticon': 'wallet.svg'
      },
      {
        'paymentMode': 'Credit / Debit Card',
        'paymentModeIcon': '',
        'isSelected': false,
        'buttonText': '',
        'razorPayoptionName': 'card',
        'paymenticon': 'creditcard.svg'
      },
      {
        'paymentMode': 'Netbanking',
        'paymentModeIcon': '',
        'isSelected': false,
        'buttonText': '',
        'razorPayoptionName': 'netbanking',
        'paymenticon': 'netbanking.svg'
      },
      {
        'paymentMode': 'EMI',
        'paymentModeIcon': '',
        'isSelected': false,
        'buttonText': '',
        'razorPayoptionName': 'emi',
        'paymenticon': 'emi.svg'
      }]
    console.log(this.Paymentoptions);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomeScreenPage');
    //FETCHING USER DETAILS FROM IONIC STORAGE.
    this.storage.get('User_Details_From_Selected_Center').then((user) => {
      this.userDetails = user;
      console.log("User Details");
      console.log(this.userDetails);
      //STORING THE DIAL CODE AND ACTUAL PHONE NUMBER SEPARATELY.
      this.phonenumber = this.userDetails.phoneNumber.split(' ');
    });
  }

  //SELECT THE PARTICULAR PAYMENT OPTION.
  toggel(i) {
    this.Paymentoptions[i].isSelected = !this.Paymentoptions[i].isSelected;
    if (this.Paymentoptions[i].isSelected == true) {
      this.storage.set('PaymentOption', this.Paymentoptions[i])
    }
    this.clearallExcept(i);
  }

  //MAKE REST OF THE OPTIONS UNSELECTED.
  clearallExcept(ex) {
    for (let i = 0; i < this.Paymentoptions.length; i++) {
      if (i !== ex) {
        this.Paymentoptions[i].isSelected = false;
      }
    }
  }

  //TO OPEN THE MODEL ON TRANSACTION FAIL.
  goto() {
    let TransactionFailedModal = this.modalCtrl.create(PaymentFailedPage);
    TransactionFailedModal.present();
  }

  //REDIRECTING TO THE RAZOR PAY PAGE OF SELECTED OPTION.
  makePayment(paymentOption) {
    var options = {
      description: 'Payment for Products in Cart',
      image: "https://png.pngtree.com/svg/20151115/pay_86048.png",
      currency: 'INR',
      key: 'rzp_test_z2d4UMo9n09efc',
      amount: this.totalPrice.toFixed() * 100,
      name: this.userDetails.firstName + " " + this.userDetails.middleName + " " + this.userDetails.lastName,
      prefill: {
        email: this.userDetails.emailAddress,
        contact: this.phonenumber[1],
        name: this.userDetails.firstName,
        method: paymentOption
      },
      theme: {
        color: '#53d0f2',
        emi_mode: true
      },
      modal: {
        ondismiss: () => {
          alert('dismissed')
        }
      }
    };

    //CALL BACK FUNCTION IN CASE THE TRANSACTION IS DONE SUCCESSFULLY.
    var successCallback = (success) => {
      console.log(success);
      this.navCtrl.setRoot("PaymentSuccessfulPurchasePage", {
        'success': success,
        'cartProducts': this.cartProducts,
        'additionalProducts': this.additionalProducts,
        'totalProducts': this.totalProducts,
        'totalPrice': this.totalPrice,
        'discountAmount': this.discountAmount,
        'appFlow': this.appFlowCart
      })
    };

    //CALL BACK FUNCTION IN CASE THE TRANSACTION IS FAILED.
    var CancelCallback = (error) => {
      console.log(error);
      let paymentFailedModal = this.modalCtrl.create("PaymentFailedPage", {
        'error': error,
        'cartProducts': this.cartProducts,
        'additionalProducts': this.additionalProducts,
        'totalProducts': this.totalProducts,
        'totalPrice': this.totalPrice,
        'discountAmount': this.discountAmount,
        'appFlow': this.appFlowCart,
        'userDetails': this.userDetails,
        'phonenumber': this.phonenumber
      });
      paymentFailedModal.present();
    };

    //https://api.razorpay.com/v1/payments/:id

    RazorpayCheckout.open(options, successCallback, CancelCallback);
  }

}
