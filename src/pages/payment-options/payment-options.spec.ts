// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from "@angular/http";
// import { Http, ConnectionBackend } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';
// import { PaymentOptionsPage } from './payment-options';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// describe('Payment Options Page - UI Part', () => {
//     let de: DebugElement;
//     let comp: PaymentOptionsPage;
//     let fixture: ComponentFixture<PaymentOptionsPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [PaymentOptionsPage],
//             imports: [
//                 IonicModule.forRoot(PaymentOptionsPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(PaymentOptionsPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Page component should be created.", () => expect(comp).toBeDefined());

//     it("Header should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".Header")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Header should contain navbar.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".NavigationBar")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Navbar should contain title.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".Title")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Navbar should contain sub-text.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".SubText")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Ion content should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".MainContent")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Ion content should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".EmptySpaceGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Ion content should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".EmptySpaceRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Ion content should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".EmptySpaceColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Ion content should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".EmptySpace")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Grid for Wallet Credit should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".WalletCreditGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Wallet Credit grid should contain heading row.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".WalletCreditHeadingRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Wallet Credit grid's heading row should contain column.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".WalletCreditHeadingColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Wallet Credit grid's heading column should contain wallet icon.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".WalletCreditHeading")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Wallet Credit grid's column should contain 'Wallet Credit' Text.", () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".WalletCreditHeadingIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Wallet Credit grid should contain credits list.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".CreditList")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Credit list should contain item divider", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".ItemDividerForCredit")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Credits should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".Credits")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Credits grid should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".CreditsGrid")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Credits Grid should contain row.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".CreditsRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Credits row should contain checkbox column.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".CheckBoxColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Checkbox column should contain check box.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".CheckBox")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Credits row should contain credits column.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".CreditsColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Credits subtext should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".CreditsSubText")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Grid for Other Payment Option should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".OtherPaymentOptionGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Other Payment Option grid should contain heading row.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".OtherPaymentOptionHeadingRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Other Payment Option grid's heading row should contain column.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".OtherPaymentOptionHeadingColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Other Payment Option grid's heading column should contain circle icon.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".OtherPaymentOptionHeadingIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Other Payment Option grid's column should contain 'Other Payment Options' Text.", () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".OtherPaymentOptionHeadingText")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Other Payment Option List should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentOptionsPage);
//         de = fixture.debugElement.query(By.css(".OptionsList")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Other Payment Option list should contain item divider", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".ItemDividerForOptions")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Other Payment Option should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".Options")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Other Payment Option grid should contain options grid.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".OptionsGrid")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Options grid should contain row.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".OptionsRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Options grid should contain column for options icon.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".OptionsIconColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Options icon column should contain options icon.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".OptionsIcon")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Options grid should contain column for options.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".OptionsColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Options sub text should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".OptionsSubText")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Payment Column should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".PaymentColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Selected check box on selecting particular payment mode should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".CheckIconOnSelection")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Options grid should contain column for check box.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".OptionsCheckBoxColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Check box column should contain check boxes.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".OptionsCheckBox")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("On selecting particular option additional grid should be displayed.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".PaymentOption")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("On selecting particular option additional grid should be displayed.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".PayWithOptionButtonGrid")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Pay with option button grid should contain row.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".PayWithOptionButtonRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Pay with option button should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".PayWithOptionButton")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Text below button should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentOptionsPage);
//             de = fixture.debugElement.query(By.css(".TextBelowButton")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });
// });

// describe('Payment Options Page - Methods Part', () => {
//     let de: DebugElement;
//     let comp: PaymentOptionsPage;
//     let fixture: ComponentFixture<PaymentOptionsPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [PaymentOptionsPage],
//             imports: [
//                 IonicModule.forRoot(PaymentOptionsPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(PaymentOptionsPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it('Ionviewdidload Method should be present.', () => {
//         expect(comp.ionViewDidLoad).toBeDefined(true);
//     });

//     it('Toggling Payment Option Selection Method should be present.', () => {
//         expect(comp.toggel).toBeDefined(true);
//     });

//     it('Clearing Method (for clearing rest of the Selection expect the one Selected recently) should be present.', () => {
//         expect(comp.clearallExcept).toBeDefined(true);
//     });

//     it('Go To Transaction Failed Modal method Should be present.', () => {
//         expect(comp.goto).toBeDefined(true);
//     });

//     it('Make Payment Method (Redirecting to RazorPay) should be present.', () => {
//         expect(comp.makePayment).toBeDefined(true);
//     });
// });

