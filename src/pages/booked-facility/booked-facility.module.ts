import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookedFacilityPage } from './booked-facility';

@NgModule({
  declarations: [
    BookedFacilityPage,
  ],
  imports: [
    IonicPageModule.forChild(BookedFacilityPage),
  ],
})
export class BookedFacilityPageModule {}
