import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import { Screenshot } from '@ionic-native/screenshot';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-booked-facility',
  templateUrl: 'booked-facility.html',
})
export class BookedFacilityPage {

  screen: any;
  state: boolean = false;

  public appointFull; //FROM DATE AND TIME PAGE.
  public FacilityName; //FROM DATE AND TIME PAGE.
  public FacilityImage; //FROM DATE AND TIME PAGE.
  public cartOrbooked; //FROM DATE AND TIME PAGE.
  public FacilityCourt; //FROM DATE AND TIME PAGE.
  public trainerIcon; //FROM DATE AND TIME PAGE.
  public Date; //FROM DATE AND TIME PAGE.
  public availableSlot; //FROM DATE AND TIME PAGE.
  public updated; //FROM DATE AND TIME PAGE.

  constructor(public navCtrl: NavController, public navParams: NavParams,public popoverCtrl: PopoverController,private screenshot: Screenshot,private socialSharing: SocialSharing,) {
    this.appointFull = this.navParams.get('appointFull');
    this.FacilityName = this.navParams.get("FacilityName");
    this.FacilityImage = this.navParams.get("FacilityImage");
    this.cartOrbooked = this.navParams.get('cartOrbooked');
    this.FacilityCourt = this.navParams.get("FacilityCourt");
    this.trainerIcon = this.navParams.get('trainerIcon');
    this.Date = this.navParams.get('Date');
    this.availableSlot = this.navParams.get('availableSlot');
    this.updated = this.navParams.get('updated');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookedFacilityPage');
  }

  screenShot() {
    this.screenshot.URI(80)
      .then((res) => {
        this.socialSharing.share('', '', res.URI, '')
          .then(() => { },
            () => {
              alert('SocialSharing failed');
            });
      },
        () => {
          alert('Screenshot failed');
        });
  }

  // Reset function we will use to hide the screenshot preview after 1 second
  reset() {
    var self = this;
    setTimeout(function () {
      self.state = false;
    }, 2000);
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create("FacilityPopoverPage", {
      'purpose': this.FacilityName,
      'icon': this.FacilityImage,
      'facilityCourt': this.FacilityCourt,
      'date': this.Date,
      'availableSlot': this.availableSlot,
    });
    popover.present({
      ev: myEvent
    });
  }

  GotoSchedule() {
    this.navCtrl.setRoot('SchedulingPage');
  }

}
