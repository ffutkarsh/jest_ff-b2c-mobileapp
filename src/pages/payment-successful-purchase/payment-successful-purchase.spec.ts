// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from "@angular/http";
// import { Http, ConnectionBackend } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';
// import { PaymentSuccessfulPurchasePage } from './payment-successful-purchase';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// describe('Payment Options Page - UI Part', () => {
//     let de: DebugElement;
//     let comp: PaymentSuccessfulPurchasePage;
//     let fixture: ComponentFixture<PaymentSuccessfulPurchasePage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [PaymentSuccessfulPurchasePage],
//             imports: [
//                 IonicModule.forRoot(PaymentSuccessfulPurchasePage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Page component should be created.", () => expect(comp).toBeDefined());

//     it("Header should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         de = fixture.debugElement.query(By.css(".Header")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Header should contain navbar.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         de = fixture.debugElement.query(By.css(".NavigationBar")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Navbar should contain menutoggle button.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         de = fixture.debugElement.query(By.css(".MenuToggleButton")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Menutoggle button should contain menutoggle icon.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         de = fixture.debugElement.query(By.css(".MenuToggleIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Navbar should contain title.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         de = fixture.debugElement.query(By.css(".Title")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Navbar should contain notification icon", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         de = fixture.debugElement.query(By.css(".NotificationIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Stars should be present", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".gridMembership")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Success grid should contain row.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".successGridRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Success grid should contain image tage in column.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".successImageCol")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Success grid should contain text column.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".SuccessGridColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Column should contain row for 'Membership Purchased Successfully'", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".SuccessRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Column should contain row for 'For Invoice'", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".InvoiceRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Product Details Grid  Devider should be present.", () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".ProductDetailsDevider")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Product Details Grid should contain list of products.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".ProductDetailsList")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("List of product should have header.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".ListHeader")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Product should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//             de = fixture.debugElement.query(By.css(".Products")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Products grid should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//             de = fixture.debugElement.query(By.css(".ProductsGrid")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Products row should  be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//             de = fixture.debugElement.query(By.css(".ProductsRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Products icon column should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//             de = fixture.debugElement.query(By.css(".ProductsIconColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Products icon should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//             de = fixture.debugElement.query(By.css(".ProductsIcon")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Products name column should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//             de = fixture.debugElement.query(By.css(".ProductsNameColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Products name should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//             de = fixture.debugElement.query(By.css(".ProductsName")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Product cost column should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//             de = fixture.debugElement.query(By.css(".ProductsCostColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Product cost should be present.", () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//             de = fixture.debugElement.query(By.css(".ProductsCost")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Saving grid should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".SavingGrid")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Saving row should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".SavingRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Saving column should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".SavingColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Heading column should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".SavingColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Query grid should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".QueryGrid")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Heading row should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".QuerysRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("For any other queries text should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".mailcallHeaderRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("CallMail row should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".CallMailRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Call column should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".CallColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Mail column should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".MailColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("GoToDashboard button grid should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".GoToDashboardButtonGrid")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("GoToDashboard button row should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".GoToDashboardButtonRow")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("GoToDashboard button should be present.", async () => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage);
//         fixture.whenStable().then(() => {
//             de = fixture.debugElement.query(By.css(".GoToDashboardButton")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });
// });

// describe('Payment Options Page - Methods Part', () => {
//     let de: DebugElement;
//     let comp: PaymentSuccessfulPurchasePage;
//     let fixture: ComponentFixture<PaymentSuccessfulPurchasePage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [PaymentSuccessfulPurchasePage],
//             imports: [
//                 IonicModule.forRoot(PaymentSuccessfulPurchasePage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(PaymentSuccessfulPurchasePage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it('Ionviewdidload Method should be present.', () => {
//         expect(comp.ionViewDidLoad).toBeDefined(true);
//     });

//     it('Go To Dashboard Method should be present.', () => {
//         expect(comp.gotoDashboard).toBeDefined(true);
//     });
// });
