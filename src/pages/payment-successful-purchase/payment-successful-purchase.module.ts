import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentSuccessfulPurchasePage } from './payment-successful-purchase';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    PaymentSuccessfulPurchasePage
  ],
  imports: [
    IonicPageModule.forChild(PaymentSuccessfulPurchasePage),
    TranslateModule
  ],
})
export class PaymentSuccessfulPurchasePageModule { }
