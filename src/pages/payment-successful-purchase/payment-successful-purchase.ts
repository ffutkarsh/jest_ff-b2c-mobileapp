import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DashboardPage } from '../../pages/pages';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-payment-successful-purchase',
  templateUrl: 'payment-successful-purchase.html',
})
export class PaymentSuccessfulPurchasePage {

  public cartProducts; //FROM PAYMENT OPTION PAGE.
  public additionalProducts; //FROM PAYMENT OPTION PAGE.
  public totalProducts; //FROM PAYMENT OPTION PAGE.
  public totalPrice; //FROM PAYMENT OPTION PAGE.
  public discountAmount; //FROM PAYMENT OPTION PAGE.
  public success; //FROM PAYMENT OPTION PAGE.
  public appFlowCart;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public translateService: TranslateService,
    private storage: Storage) {

    this.success = this.navParams.get('success');
    this.cartProducts = this.navParams.get('cartProducts');
    console.log('Cart Products');
    this.additionalProducts = this.navParams.get('additionalProducts');
    this.totalProducts = this.navParams.get('totalProducts');
    this.totalPrice = this.navParams.get('totalPrice');
    this.discountAmount = this.navParams.get('discountAmount');
    this.appFlowCart = this.navParams.get("appFlow"); //from app flow.

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentSuccessfulPurchasePage');
  }

  //REDIRECTING TO DASHBOARD PAGE ONCE THE PAYMENT IS DONE.


  gotoDashboard() {
    this.storage.remove('CartDetails');
    this.storage.remove('PaymentOption');
    this.storage.remove('CARTITEM');
    if (this.appFlowCart == true) {
      this.navCtrl.push("AppointmentBookedPage")
    }
    this.navCtrl.setRoot(DashboardPage);
  }

}
