import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuyMainPage } from './buy-main';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    BuyMainPage,
  ],
  imports: [
    IonicPageModule.forChild(BuyMainPage),
    TranslateModule
  ],
})
export class BuyMainPageModule {}
