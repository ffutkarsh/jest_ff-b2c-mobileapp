// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, ViewChild } from '@angular/core';
// import { BuyMainPage } from './buy-main';
// import { IonicModule, Platform, NavController,NavParams } from 'ionic-angular/index';
// import { StatusBar } from '@ionic-native/status-bar';
// import { SplashScreen } from '@ionic-native/splash-screen';
// import { Http, ConnectionBackend } from '@angular/http';
// import { IonicStorageModule, Storage } from '@ionic/storage';
// import { Grid, Col, Row } from 'ionic-angular/umd';
// import { HttpModule } from '@angular/http';


// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }


// describe('Buy Main Page UI Testing', () => {
//     let comp: BuyMainPage;
//     let fixture: ComponentFixture<BuyMainPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [BuyMainPage],
//             imports: [
//                 HttpModule,
//                 IonicModule.forRoot(BuyMainPage),
//                 IonicStorageModule.forRoot()
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
                
               
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(BuyMainPage);
//         comp = fixture.componentInstance;
//     });

//     it('Header should be present', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.header')).nativeElement;
//         expect(inputfield.className).toMatch('header');
//     });

//     it("Header text should match", async () => {
//         let fixture = TestBed.createComponent(BuyMainPage);
//         let element2 = fixture.debugElement.nativeElement.querySelector(".header");
//         expect(element2.textContent.trim()).toContain("Buy Membership");
//     });



//     it('Segment Slider should be present', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.men-seg')).nativeElement;
//         expect(inputfield.className).toMatch('men-seg');
//     });

//     it('Segments should be present', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.seg-style')).nativeElement;
//         expect(inputfield.className).toMatch('seg-style');
//     });

//     it('Footer View Cart Bar should be present', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.footrow')).nativeElement;
//         expect(inputfield.className).toMatch('footrow');
//     });


//     it('Footer View Cart Item Count should be present', () => {
//         let element2 = fixture.debugElement.nativeElement.querySelector(".chkoutinnerrow1");
//         expect(element2.textContent.trim()).toContain("2 ITEMS IN CART");
//     });

//     it('Footer View Cart Amount should be present', () => {
//         let element2 = fixture.debugElement.nativeElement.querySelector(".chkoutinnerrow2");
//         expect(element2.textContent.trim()).toContain("₹ 5000.00");
//     });

//     it('Footer View Cart Taxes should be present', () => {
//         let element2 = fixture.debugElement.nativeElement.querySelector(".chkoutinnerrow");
//         expect(element2.textContent.trim()).toMatch("Plus Taxes");
//     });


//    it('Footer View Cart Text should be present', () => {
//         let element2 = fixture.debugElement.nativeElement.querySelector(".vtxt");
//         expect(element2.textContent.trim()).toContain("View Cart");
//     });


   

//     });





