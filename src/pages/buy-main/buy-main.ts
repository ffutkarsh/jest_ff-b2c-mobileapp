import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { BuymembershipProvider } from '../../providers/buymembership/buymembership';
import { Storage } from '@ionic/storage';
import { count } from 'rxjs/operator/count';
import { CartItemsPage } from '../../pages/pages'
import { NewMyMembershipsPage } from '../new-my-memberships/new-my-memberships';
import { TranslateService } from '@ngx-translate/core';
@IonicPage()
@Component({
  selector: 'page-buy-main',
  templateUrl: 'buy-main.html',
})
export class BuyMainPage {
  public add: number = 0; //adding membership
  public count: number = 0; // allowing to add certain number of products
  public buyMembership = [];// to store buymembership response from server
  public addBoolean: boolean = false; //view cart button disabled,
  public buy;
  setofferLocally;

  constructor(
    public translateService: TranslateService,
    private toastCtrl: ToastController,
    private storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    public buymembershipprvd: BuymembershipProvider) {
    this.buy = ''; //storing purpose name
    this.fetchbuymembership(); //method to fetch membership from array

  }

  ionViewWillEnter() {
    this.buy = this.buyMembership[0].purposeName //storing purpose name
  }

  //fetching new purpose name
  console(val) {
    this.buy = val;
  }

  //method to fetch membership from array 
  fetchbuymembership() {
    this.buymembershipprvd.getBuyMembershipDetails().map(response => response).subscribe((response) => {
      this.buyMembership = response['buyMembership'];
    })
  }


  //on click of add button ;  i = package; z = membership
  addToCart(i, z) {
    if (this.count <= 8) { // allowing to add certain number of products
      this.buyMembership[z].purposePackage[i].status = !this.buyMembership[z].purposePackage[i].status;
      this.add += Number(this.buyMembership[z].purposePackage[i].packagePrice);
      this.count += 1;
      this.addBoolean = true;
    }
    else { //creating toast if max threshold limit crossed
      let toast = this.toastCtrl.create({
        message: 'Maximum items reached',
        duration: 3000,
        position: 'top'
      });
      toast.onDidDismiss(() => {
      });
      toast.present();
    }

    let obj = {
      membershipAmount: Number(this.buyMembership[z].purposePackage[i].packagePrice),
      type: "buymembership",
      purposeName: this.buyMembership[z].purposePackage[i].packageName,
      startduratin: "",
      purposelogo: "",
      quantity: 1
    };
    console.log(obj)

    this.storage.get('CARTITEM').then((val) => {
      this.setofferLocally = val;
    }).then(
      () => {
        if ((this.setofferLocally) || (this.setofferLocally = [])) {

          this.setofferLocally.push(obj);
          this.storage.set('CARTITEM', this.setofferLocally);
        }
      });

  }

  //remove particular membership having particular package ; i = package; z = membership
  removeFromCart(i, z) {
    this.buyMembership[z].purposePackage[i].status = !this.buyMembership[z].purposePackage[i].status;
    this.add -= Number(this.buyMembership[z].purposePackage[i].packagePrice);
    this.count -= 1;
    if (this.add == 0 && this.count == 0) {
      this.addBoolean = false;
    }

    this.storage.get('CARTITEM').then((val) => {
      val.splice(val.length - 1, 1);
      this.setofferLocally = val;
      this.storage.set('CARTITEM', this.setofferLocally);
    });
   
  }

  goToCart1() {
    if (this.add != 0 && this.count != 0) {
      this.navCtrl.push("CartItemsPage", {
        'membershipFlowCart': true
      });
   
    }
    
  }
}

