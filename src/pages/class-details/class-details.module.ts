import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ClassDetailsPage } from './class-details';
import { ClassesFlowApiProvider } from '../../providers/classes-flow-api/classes-flow-api';
import { Screenshot } from '@ionic-native/screenshot';

@NgModule({
  declarations: [
    ClassDetailsPage
  ],
  imports: [
    IonicPageModule.forChild(ClassDetailsPage),
    TranslateModule
  ],
  providers: [
    ClassesFlowApiProvider,
    Screenshot
  ]
})
export class ClassDetailsPageModule { }