import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import {
  NotificationlistPage,
  WithdrawModalPage,
  SeatSelectionMoadalPage
} from '../../pages/pages';
import { ClassesFlowApiProvider } from '../../providers/classes-flow-api/classes-flow-api';
import { Storage } from '@ionic/storage';
import { Screenshot } from '@ionic-native/screenshot';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-class-details',
  templateUrl: 'class-details.html',
})
export class ClassDetailsPage {

  public cardFetchedDetails; //CARD DETAILS FROM CLASSES PAGE.
  public classdetails = []; //CLASS DETAILS DESCRIPTION.
  public selectedClassDescription = {}; //STORING ALL THE SESSIONS OF THE SELECTED CATEGORY[PURPOSE]
  public PurposeSessions; //PURPOSE SESSIONS OF SELECTED CLASS.

  //TO CHECK THE STATUS OF CLASS.
  public joinButton: boolean;
  public withdrawbutton: boolean;
  public buyButton: boolean;

  public date; //TO STORE CURRENT DATE

  public state: boolean = false; //SAVE THE STATE OF SCREENSHOT PREVIEW.

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public translateService: TranslateService,
    public classesFlowApi: ClassesFlowApiProvider,
    private storage: Storage,
    private screenshot: Screenshot,
    private socialSharing: SocialSharing) {

    this.cardFetchedDetails = this.navParams.get('objectOfC');
    this.getClassDetails();
    this.PurposeSessions = this.navParams.get('PurposeSessions');

    if (this.cardFetchedDetails.MemberClassStatus == "Join") {
      this.joinButton = true;
      this.withdrawbutton = false;
      this.buyButton = false;
    }
    else if (this.cardFetchedDetails.MemberClassStatus == "Joined") {
      this.joinButton = false;
      this.withdrawbutton = true;
      this.buyButton = false;
    }
    else if (this.cardFetchedDetails.MemberClassStatus == "Buy") {
      this.joinButton = false;
      this.withdrawbutton = false;
      this.buyButton = true;
    }

    this.date = moment().format('ddd MMM DD YYYY');

  }

  ionViewDidLoad() { }

  /// for fetching response class details page
  getClassDetails() {
    this.classesFlowApi.getClassDetails().map((response) => response).subscribe((response) => {
      this.classdetails = response['ClassList'];
      for (let j = 0; j < this.classdetails.length; j++) {
        if (this.cardFetchedDetails.ClassId == this.classdetails[j].ClassId) {
          this.selectedClassDescription = this.classdetails[j];
        }
      }
    });
  }

  //NAVIGATING TO NOTIFICATION PAGE.
  goToNotification() {
    this.navCtrl.push(NotificationlistPage);
  }

  // Draws star rating as per the number (rating) passed
  starRating(rating: number) {
    let full;
    if (rating <= 5) {
      full = Math.floor(rating);
    }
    else {
      full = 5;
    }
    let status = [];
    for (let i = 0; i < full; i++) {
      status.push("fulle.svg"); // saves Full Star svg in array
    }
    if (rating - full == 0.5 && !(rating >= 5)) {
      status.push("halfe.svg"); // saves Half Star svg in array
    }
    if (status.length < 5) {
      let emp = 5 - status.length;
      for (let i = 0; i < emp; i++) {
        status.push("fullgrey.svg");// saves Empty Star svg in array
      }
    }
    return status; // Returns array so that it can be rendered using *ngFor
  }

  // Take a screenshot and save to phone storage
  screenShot() {
    this.screenshot.URI(80)
      .then((res) => {
        this.socialSharing.share('', '', res.URI, '')
          .then(() => { },
            () => {
              alert('SocialSharing failed');
            });
      },
        () => {
          alert('Screenshot failed');
        });
  }

  // Reset function we will use to hide the screenshot preview after 1 second
  reset() {
    var self = this;
    setTimeout(function () {
      self.state = false;
    }, 2000);
  }

  ///showing time format 
  showtime() {
    let start_Time = moment(this.cardFetchedDetails.StartTime, "hh:mm A");
    let startstring = start_Time.format("hh:mm A");
    let duration = (this.cardFetchedDetails.DurationEndtime + "").replace(" h", "");
    let array = duration.split(".");
    let result = start_Time.add(array[0], 'hours');
    result = start_Time.add(array[1], 'minutes');
    return startstring + " " + result.format("hh:mm A");
  }

  ///showing date format 
  showdate() {
    this.date = moment().format('MMM DD YYYY');
  }

  //SEAT SELECTION MODAL IF JOIN.
  gotojoin() {
    let profileModal = this.modalCtrl.create(SeatSelectionMoadalPage, {
      'buttonValue': "Join",
      'PurposeSessions': this.PurposeSessions
    });
    profileModal.present();
  }

  //WITHDRAW MODAL IF JOINED.
  gotowithdraw() {
    let WithdrawModal = this.modalCtrl.create(WithdrawModalPage);
    WithdrawModal.present();
  }

  //SEAT SELECTION MODAL WITH PURPOSE SELECTION AND THEN TO CART IF BUY AND JOIN.
  gotocart() {
    let profileModal = this.modalCtrl.create(SeatSelectionMoadalPage, {
      'buttonValue': "Buy&Join",
      'PurposeSessions': this.PurposeSessions,
      'PurposeName': this.cardFetchedDetails.PurposeName,
      'PurposeLogo':this.cardFetchedDetails.PurposeLogo
    });
    profileModal.present();
  }

}
