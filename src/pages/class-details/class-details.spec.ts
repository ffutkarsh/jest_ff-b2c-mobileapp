// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { ClassDetailsPage } from './class-details';
// import { ClassesFlowApiProvider } from '../../providers/classes-flow-api/classes-flow-api';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';
// import { Screenshot } from '@ionic-native/screenshot';
// import { SocialSharing } from '@ionic-native/social-sharing';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// // The describe function is for grouping related specs
// describe('class details Page UI Testing', () => {
//     let comp: ClassDetailsPage;
//     let fixture: ComponentFixture<ClassDetailsPage>;
//     let de: DebugElement;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 ClassDetailsPage
//             ],
//             imports: [
//                 IonicModule.forRoot(ClassDetailsPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 ClassesFlowApiProvider,
//                 Screenshot,
//                 SocialSharing
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(ClassDetailsPage); /////TESTBED CREATES THE OBJECT OF THE CLASSDETAILS  PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance;   //////CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//     });

//     it("Navbar to be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".classdetailMenuBar")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Header to be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".Header")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("title to be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".Title")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Notification Icon to be present", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".NotificationIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Content to be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".contentClassDetail")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("GRID FOR IMAGE TO BE be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".gridImage")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Background image to be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".cbackground")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Column for class level details to be present", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".userlevel")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("label to be present for text as beginner.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".top-left")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("label to be present for displaying class level.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".labelLevel")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Column for star count to be present", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".starcount")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("label for user count  to be present", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".usercount")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("div to be present for displaying users count.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".users")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it('image to be present for star rating.', () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(ClassDetailsPage);
//             de = fixture.debugElement.query(By.css(".stargeneral")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Row to be present for displaying logo image of class.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".rowClassIMG")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Avatar to be present for for displaying logo image of class.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".classico")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("image  to be present for for displaying logo image of class.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".classImage")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it('Method to move to next page', () => {
//         expect(comp.screenShot).toBeDefined();
//     });

//     it('Method to move to next page', () => {
//         expect(comp.gotowithdraw).toBeDefined();
//     });

//     it("icon for share should be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".iconShare")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("icon to be present for timer.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".icontimer")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("label to be present for class time.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".dateTime")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("row for class time should be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".rowTime")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Date for Location should be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".labelDate")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("name of center to be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".centername")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Row for location address should be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".rowLocation")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("label for address of location should be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".labelAddress")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("icon for location to be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".currenticon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("label for location name should be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".labelLocation")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("label for get Direction of address should be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".labelgetDirection")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Icon for Arrow should be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".iconArrow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Row for Capacity should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(ClassDetailsPage);
//             de = fixture.debugElement.query(By.css(".rowCapacity")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("label for Capacity should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(ClassDetailsPage);
//             de = fixture.debugElement.query(By.css(".labelcapacity")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("grid to be present for trainer details of class to be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".infotrainer")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Row for Trainer should be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".rowTrainer")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("label for trainer to be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".labeltrainer")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("column for trainer image should be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".colimgTrainer")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Column for description of trainer should be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".coldescTrainer")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("row for description of trainer should be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".trainerRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Name of trainer should be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".nameTrainer")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("image of trainer to be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".imageTrainer")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("icon of tag to be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".iconTag")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it('span tag should be present', () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(ClassDetailsPage);
//             de = fixture.debugElement.query(By.css(".tag")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("icon for description should be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".iconDesc")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Description label should be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".labeldesc")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Description should be present.", async () => {
//         fixture = TestBed.createComponent(ClassDetailsPage);
//         de = fixture.debugElement.query(By.css(".textDescription")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("grid for join Button should be present", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(ClassDetailsPage);
//             de = fixture.debugElement.query(By.css(".footbutton")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it('Text in Button should match to "JOIN"', async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(ClassDetailsPage);
//             de = fixture.debugElement.query(By.css(".buttonJoin")).nativeElement;
//             const Button: HTMLElement = fixture.debugElement.query(By.css('.buttonJoin')).nativeElement;
//             expect(Button.textContent).toContain("JOIN");
//         });
//     });

//     it("Row for button should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(ClassDetailsPage);
//             de = fixture.debugElement.query(By.css(".rowButton")).nativeElement;
//         });
//     });

// });