import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, LoadingController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { Geolocation } from '@ionic-native/geolocation';
import { LoginErrorFeedbackPage } from '../../pages/pages';
import {IpgeofetchProvider} from "../../providers/ipgeofetch/ipgeofetch"

@IonicPage()
@Component({
  selector: 'page-login-error',
  templateUrl: 'login-error.html',
})

export class LoginErrorPage {

  public fetchDialCode //FROM LOGIN PAGE.
  public fetchPhoneNumber; //FROM LOGIN PAGE.

  public tenantName; //ENTERING TENANT NAME;
  public name; //ENTERING CLIENT NAME.
  public emailAddress; //ENTERING EMAIL ADDRESS.
  public tenantlocation; //ENTERING TENANT LOCATION.

  public tenantNameRegex = /^[a-zA-Z0-9_ ]*$/;
  public nameRegex = /^[a-zA-Z_ ]*$/;
  public emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  public hasLocationAccess = false; //STORES STATUS OF LOCATION PERMISSION.

  public PhoneNumberEditable = false; //TO CHECK WHETHER CLICKED ON CHANGE OR NOT [FOR TOGGLING CONDITION CLICKED OR NOT CLICKED].

  @ViewChild('inputToFocus') inputToFocus;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public translateService: TranslateService,
    public alertCtrl: AlertController,
    private storage: Storage,
    private modalController: ModalController,
    private androidPermissions: AndroidPermissions,
    public loadingCtrl: LoadingController,
    private geolocation: Geolocation,
    public ipgeo:IpgeofetchProvider,
    private nativeGeocoder: NativeGeocoder) {

    this.fetchDialCode = this.navParams.get('defaultDialcode'); //FROM LOGIN PAGE.
    this.fetchPhoneNumber = this.navParams.get('phoneNumber'); //FROM LOGIN PAGE.

    this.tenantName = "";
    this.name = "",
      this.emailAddress = "";
    this.tenantlocation = "";

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginErrorPage');
    setTimeout(() => {
      this.inputToFocus.setFocus();
    }, 200)
  }

  //VALIDATING TENANT NAME.
  validateTenantName(tenantName) {
    return this.tenantNameRegex.test(String(tenantName).toLowerCase());
  }

  //VALIDATING NAME.
  validateName(name) {
    return this.nameRegex.test(String(name).toLowerCase());
  }

  //VALIDATING EMAIL.
  validateEmail(emailAddress) {
    return this.emailRegex.test(String(emailAddress).toLowerCase());
  }

  //LOCATION SELECTION MODAL.
  detectLocation() {

    // // Asking Permission For Android Location access
    // this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
    //   success => {
    //     console.log('Permission granted');
    //     this.hasLocationAccess = true;
    //   },
    //   err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
    // );

    // this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION]);

    // let options: NativeGeocoderOptions = {
    //   useLocale: true,
    //   maxResults: 5
    // };

    // // Showing Loader while GPS gets a lock
    // let loading = this.loadingCtrl.create({
    //   content: 'Fetching Location...'
    // });

    // if (this.hasLocationAccess) {
    //   loading.present();
    // }

    // this.geolocation.getCurrentPosition().then((resp) => {
    //   this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude, options)
    //     .then((result: NativeGeocoderReverseResult[]) => {
    //       console.log(JSON.stringify(result[0]));
    //       this.tenantlocation = result[0].subLocality;
    //       loading.dismiss(); // Close Loader after location found
    //     }
    //     )
    //     .catch((error: any) => console.log(error));
    // }).catch((error) => {
    //   console.log('Error getting location', error);
    // });

    this.ipgeo.getLocfromIP().subscribe(data => {
      this.tenantlocation = (data.city);
      });;

  }

  //GO TO LOGIN ERROR FEEDBACK PAGE.
  goToLoginErrorFeedback() {
    //TO CHECK WHETHER EMPTY FIELDS ARE THERE AND ACCORDINGLY SHOW ALERT TO THE USER. 
    if (this.tenantName == "" || this.name == "" || this.emailAddress == "" || this.tenantlocation == "") {
      const alert = this.alertCtrl.create({
        subTitle: "Please Enter All Fields.",
        buttons: ["OK"]
      });
      alert.present();
      return false;
    } //END OF 1ST IF OF SHOWALERT.
    if (!this.validateTenantName(this.tenantName)) {
      const alert = this.alertCtrl.create({
        subTitle: "Invalid Tenant Name",
        buttons: ["OK"]
      });
      alert.present();
      return false;
    } //END OF 2ND IF OF SHOWALERT.
    if (!this.validateName(this.name)) {
      const alert = this.alertCtrl.create({
        subTitle: "Invalid Name",
        buttons: ["OK"]
      });
      alert.present();
      return false;
    } //END OF 3RD IF OF SHOWALERT.
    if (!this.validateEmail(this.emailAddress)) {
      const alert = this.alertCtrl.create({
        subTitle: "Invalid Email Address",
        buttons: ["OK"]
      });
      alert.present();
      return false;
    }
    else {
      this.storage.set('LoginErrorFeedbackSubmitted', true); //FEEDBACK SUBMITTED.
      this.navCtrl.push(LoginErrorFeedbackPage, {
        'tenantName': this.tenantName,
        'name': this.name,
        'emailAddress': this.emailAddress,
        'tenantlocation': this.tenantlocation
      });
    }
  }

  //IF THE USER CLICKS ON THE CHANGE OPTION GIVEN.
  changeNumber() {
  
    if(this.fetchPhoneNumber == '')
    {
      alert('cannot be null')
    }
    else{
      this.PhoneNumberEditable = true; //CAN EDIT THE PHONE NUMBER IN THE TEXT BOX.
    }
  }

  //IF THE USER IS DONE EDITING THE NUMBER.
  doneNumber() {
    if(this.fetchPhoneNumber == '')
    {
      alert('cannot be null')
      this.fetchPhoneNumber = this.fetchPhoneNumber; //BIND THE EDITED PHONE NUMBER WITH THE "HELLO,....."
      
    }
    else{
      this.PhoneNumberEditable = false; //EDIT IS CLOSED [TEXT BOX IS CLOSED].
      this.fetchPhoneNumber = this.fetchPhoneNumber; //BIND THE EDITED PHONE NUMBER WITH THE "HELLO,....."
    }
   
   
 
  }

  //CHECK IF THE ENTERED CHARACTER IS A DIGIT.
  checkifNo(fetchPhoneNumber) {
    let reg = new RegExp('^\\d+$');
    return reg.test(fetchPhoneNumber);
  }

  //WILL SLICE THE INPUT IF IT IS CHARACTER.
  dynamic() {
    let a = this.fetchPhoneNumber.slice(-1);
    if (!this.checkifNo(a)) {
      this.fetchPhoneNumber = this.fetchPhoneNumber.substring(0, this.fetchPhoneNumber.length - 1);
    }
  }

}
