import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginErrorPage } from './login-error';
import { TranslateModule } from '@ngx-translate/core';
import { Geolocation } from '@ionic-native/geolocation';
import { AndroidPermissions } from '@ionic-native/android-permissions';

@NgModule({
  declarations: [
    LoginErrorPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginErrorPage),
    TranslateModule
  ],
  providers: [
    AndroidPermissions,
    Geolocation
  ]
})
export class LoginErrorPageModule { }
