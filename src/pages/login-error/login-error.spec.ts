// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from "@angular/http";
// import { Http, ConnectionBackend } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';
// import { LoginErrorPage } from './login-error';
// import { GeogetterProvider } from '../../providers/geogetter/geogetter';
// import { Geolocation } from '@ionic-native/geolocation';
// import { NativeGeocoder } from '@ionic-native/native-geocoder';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { AndroidPermissions } from '@ionic-native/android-permissions';
// import { OpenNativeSettings } from '@ionic-native/open-native-settings';
// import { NetworkProvider } from '../../providers/network/network';
// import { Network } from '@ionic-native/network';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// describe('Login error Page - UI Part', () => {
//     let de: DebugElement;
//     let comp: LoginErrorPage;
//     let fixture: ComponentFixture<LoginErrorPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [LoginErrorPage],
//             imports: [
//                 IonicModule.forRoot(LoginErrorPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 GeogetterProvider,
//                 Geolocation,
//                 NativeGeocoder,
//                 TranslateService,
//                 TranslateStore,
//                 AndroidPermissions,
//                 OpenNativeSettings,
//                 NetworkProvider,
//                 Network
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(LoginErrorPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Page component should be created.", () => expect(comp).toBeDefined());

//     it("Ion content should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".MainContent")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section grid should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".HeadingGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".HeadingGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".HeadingGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Section grid should contain the “Welcome” text.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".WelcomeText")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Hello grid should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".HelloGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Hello row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".HelloGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Hello column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".HelloGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Hello grid should contain the “Hello,” text.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".Hello")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Change Number column should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(LoginErrorPage);
//             de = fixture.debugElement.query(By.css(".ChangeOptionColumn")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Change text should be present.", async () => {
//         fixture.whenStable().then(() => {
//             fixture.detectChanges();
//             fixture = TestBed.createComponent(LoginErrorPage);
//             de = fixture.debugElement.query(By.css(".ChangeNumber")).nativeElement;
//             expect(de).toBeDefined();
//         });
//     });

//     it("Sub Heading grid should be present.'", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".SubHeadingGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Sub Heading row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".SubHeadingGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Sub Heading column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".SubHeadingGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Sub Heading should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".SubHeading")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Oops Emoji should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".OopsEmoji")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Oops Instruction grid should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".OopsInstructionGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Oops Instruction grid's row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".OopsInstructionGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Oops Instruction grid's column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".OopsInstructionGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Oops Instruction should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".OopsInstruction")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Form Heading grid should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".FormHeadingGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Form Heading grid's row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".FormHeadingGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Form Heading grid's column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".FormHeadingGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Form Heading should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".FormHeading")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Gym Name grid should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".GymNameGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Gym Name grid's row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".GymNameGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Gym Name grid's column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".GymNameGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Gym Name Item should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".GymNameItem")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Gym Name Label should be present..", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".GymNameLabel")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Gym Name Label Icon should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".GymNameLabelIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Gym Name Input should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".GymNameInput")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Name grid should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".ClientNameGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Name grid's row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".ClientNameGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Name grid's column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".ClientNameGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Name Item should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".ClientNameItem")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Name Label should be present..", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".ClientNameLabel")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Name Label Icon should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".ClientNameLabelIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Name Input should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".ClientNameInput")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Email grid should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".ClientEmailGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Email grid's row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".ClientEmailGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Email grid's column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".ClientEmailGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Email Item should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".ClientEmailItem")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Email Label should be present..", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".ClientEmailLabel")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Email Label Icon should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".ClientEmailLabelIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Client Email Input should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".ClientEmailInput")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Location grid should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".LocationGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Location grid's row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".LocationGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Location grid's column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".LocationGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Location Item should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".LocationItem")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Location Label should be present..", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".LocationLabel")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Location Label Icon should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".LocationLabelIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Location Input should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".LocationInput")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Detect Option Column should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".DetectColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Detect Option Item should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".DetectItem")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Detect Option Label should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".DetectLabel")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Detect Option should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".Detect")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Inform Centre Button Grid should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".InformCentreButtonGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Inform Centre Button Grid's row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".InformCentreButtonGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Inform Centre Button Grid's column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".InformCentreButtonGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Inform My Fitness Centre Button should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".InformCentreButton")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Footer Logo Grid should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".FooterLogoGrid")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Footer Logo Grid's row should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".FooterLogoGridRow")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Footer Logo Grid's column should be present.", async () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".FooterLogoGridColumn")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Footer Logo should be present.", () => {
//         fixture = TestBed.createComponent(LoginErrorPage);
//         de = fixture.debugElement.query(By.css(".FooterImage")).nativeElement;
//         expect(de).toBeDefined();
//     });

// });


// describe('Login error Page - Methods Part', () => {
//     let de: DebugElement;
//     let comp: LoginErrorPage;
//     let fixture: ComponentFixture<LoginErrorPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [LoginErrorPage],
//             imports: [
//                 IonicModule.forRoot(LoginErrorPage),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 ConnectionBackend,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 GeogetterProvider,
//                 Geolocation,
//                 NativeGeocoder,
//                 TranslateService,
//                 TranslateStore,
//                 AndroidPermissions,
//                 OpenNativeSettings,
//                 NetworkProvider,
//                 Network
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(LoginErrorPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Validating Tenant Name Test Case-1", async () => {
//         comp.tenantName = "ABC1";
//         expect(comp.validateTenantName(comp.tenantName)).toBe(true);
//     });

//     it("Validating Tenant Name Test Case-2", async () => {
//         comp.tenantName = "ABC 1";
//         expect(comp.validateTenantName(comp.tenantName)).toBe(true);
//     });

//     it("Validating Tenant Name Test Case-3", async () => {
//         comp.tenantName = "123A";
//         expect(comp.validateTenantName(comp.tenantName)).toBe(true);
//     });

//     it("Validating Tenant Name Test Case-4", async () => {
//         comp.tenantName = "123 A";
//         expect(comp.validateTenantName(comp.tenantName)).toBe(true);
//     });

//     it("Validating Tenant Name Test Case-5", async () => {
//         comp.tenantName = "123,A";
//         expect(comp.validateTenantName(comp.tenantName)).toBe(false);
//     });

//     it("Validating Tenant Name Test Case-6", async () => {
//         comp.tenantName = "ABC,1";
//         expect(comp.validateTenantName(comp.tenantName)).toBe(false);
//     });

//     it("Validating Name Test Case-1", async () => {
//         comp.name = "ABCD";
//         expect(comp.validateName(comp.name)).toBe(true);
//     });

//     it("Validating Name Test Case-2", async () => {
//         comp.name = "AB CD";
//         expect(comp.validateName(comp.name)).toBe(true);
//     });

//     it("Validating Name Test Case-3", async () => {
//         comp.name = "ABCD,";
//         expect(comp.validateName(comp.name)).toBe(false);
//     });

//     it("Validating Name Test Case-4", async () => {
//         comp.name = "ABCD,12";
//         expect(comp.validateName(comp.name)).toBe(false);
//     });

//     it("Validating Name Test Case-5", async () => {
//         comp.name = "ABCD12";
//         expect(comp.validateName(comp.name)).toBe(false);
//     });

//     it("Validating Name Test Case-6", async () => {
//         comp.name = "12ABCD";
//         expect(comp.validateName(comp.name)).toBe(false);
//     });

//     it("Validating Email Test Case-1", async () => {
//         comp.emailAddress = "ABCD";
//         expect(comp.validateEmail(comp.emailAddress)).toBe(false);
//     });

//     it("Validating Email Test Case-2", async () => {
//         comp.emailAddress = "AB CD";
//         expect(comp.validateEmail(comp.emailAddress)).toBe(false);
//     });

//     it("Validating Email Test Case-3", async () => {
//         comp.emailAddress = "ABCD,";
//         expect(comp.validateEmail(comp.emailAddress)).toBe(false);
//     });

//     it("Validating Email Test Case-4", async () => {
//         comp.emailAddress = "ABCD@";
//         expect(comp.validateEmail(comp.emailAddress)).toBe(false);
//     });

//     it("Validating Email Test Case-5", async () => {
//         comp.emailAddress = "ABCD12@gmail";
//         expect(comp.validateEmail(comp.emailAddress)).toBe(false);
//     });

//     it("Validating Email Test Case-6", async () => {
//         comp.emailAddress = "12AB@gmail.com";
//         expect(comp.validateEmail(comp.emailAddress)).toBe(true);
//     });

//     it("Location Detect Method to be defined", async () => {
//         expect(comp.detectLocation).toBeDefined();
//     });

//     it("GoToLoginErrorFeedback Method to be defined", async () => {
//         expect(comp.goToLoginErrorFeedback).toBeDefined();
//     });
// });