import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ReferralProvider } from '../../providers/providers';
import { SocialSharing } from '@ionic-native/social-sharing';


@IonicPage()
@Component({
  selector: 'page-myrefferal',
  templateUrl: 'myrefferal.html',
})
export class MyrefferalPage {
  referralResponse =        //// VARIABLE FOR STORING REFERRAL RESPONSE
    {
      "referralEarnings": "",
      "referralCode": "asas",
      "referralDescription": ""

    };

  constructor(public referral: ReferralProvider, private socialSharing: SocialSharing, public navCtrl: NavController, public navParams: NavParams) {
    this.getNewReferralDetails();  //method to fetch array
  }

  ionViewDidLoad() {
      //method to fetch array from json
    this.getNewReferralDetails();
  }


  /// FETCHING API RESPONSE FOR REFERRAL DETAILS 
  getNewReferralDetails() {
    this.referral.getReferralDetails().map((response) => response.json()).subscribe((response) => {
      this.referralResponse = response;
    });
  }


  ///METHOD FOR SHARING REFERRAL CODE
  share() {
    this.socialSharing.share("Your refferal code is " + this.referralResponse.referralCode, '', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAe1BMVEX///84gP80fv8se/8bdf8nef+hvf+7z/+ZuP/G1v9Vjv8xff8hd/8qev/j6/9/qP8Tc//2+P/x9f/T4P/q8P+Fq//L2v+mwf/Y4/9pmv+1y//09/9Chf+uxv/g6f+Qsv9zoP9glf9unf9Ykf+NsP+6zv9Niv8Ab/9AhP953P7HAAAHyUlEQVR4nO2bbXuiOhCGBRLUEiT4rq1abev6/3/hgQlIQlINrWvtnuf+sNdegDBPMklmJmmvBwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB8i9EwzlZPx+PTKht8bH/amluT91+54FGUhEEQhlHEBVttxj9t1e2YvDKeBC2SlO2HP23ZbRgemSVPEfJg89PWfZ/lUYRufSXR8de76ow1+qKUM4LXPpu80UP5ZD76YTu/yjaIankpC7LNfDTO8+fpZP1+ZLyQHpUPjVZScBb+Sn/9kPWAY6dFq5fG6zcm+8V/hlJ1qNj/iI3fYiBrffuJ6/5oVvyzledezu5s37d5Z8pyfnTqqzg245Qt72bbTdiJyu7FpadG5y4sRuXsXrbdhI3qwSi4PEt+cG3teLmTbTdhovomervy3CbVw5y7mHYbcmV4urr2YF/rw/DpHqbdiBWtg8n1BWCsj8PBHSy7EX2aZcKjx6OvTcwqp3/dsJtRreE+MWce1cuFvDjpPhYxjULhyo2eR6OW7umpjN+CSMbdPjIdTfOLD4ytT33y3HbZOSomH00O7cv5+qBC77fY8MfNUUqe0VdcfhoTRgfPZycmGJNh1jculw+qwfxxEOWXxH7TNMNyQPf18sLzZiWYEEw+LZ47CFxQF8pWA+bv5zQ4jOTKbLbKjMwVuIk0iiKujel1KOqAPuFCm54GonjyT6Fg0HwqbXzjgxW3I9m41jiTafPczl+hmhhbP+ifrVKmSXvizFecOV5XVj60lWR61IOEYkXiZ5MH5SfEZJnoi2zA6xldrUzN4Nkww6Q0uRRd6kwompHmIHmXQYt2LjGOi3bn62sKJ6ydUIesbixSGO5F64Fo71T4ytom+U4F72XHR6a/zdpfLR8xF/iFJPOuKJzISmCoyloEe9cUBtXVKK09MBCxQ+GhdoXSdVsvugJ9WBg9vnAILF79avyOmpTZ86OucFyVRCJ2fB/sVrWbsbWmkFSKcLbYZKLSKJ8thVViEIogi+P3oE4TfPLwkVrtjUuWixqGVdDa71hidIVPymSWqUk3jytPY2NDIT/O6f7zXl1J47bCKm5Oqwd7wxO9mZ88FK7LYW4mQm+f1KJCrj9FaUZiz2iaQhUrBdp0OEo0d6gVsmYWe1P3T22FL6oltLF0SIPQMf05mJWf4fo6NbGGdE2qD+1paX5oJyOaQmUX0/t5qoYTRXyVQq6tnWP1bTY1FQ5F0y5niezFrxJPHSb0pTv7pFwatPIlmuPt9aJRuFShhJkoqwwzis8KzdmKGjygFUVTuE+srxc/99LX66nX6Fc+7cJW4WJfSmFWpNUoVApYK/qgjtXum0N5Tq2SLgyFOZmUfrG8R0NYz9aXzom0clM9GKPmFpanNArJPaxocHHu+6oFjHBpqvp9Zyic0/9kl0CtYUyjSc9819ylTWEsm2QgtwKLRqG75VULlrpcClV30czXKFSVBZ/kzsG0fGPyql2JI4e0CmPMUGfYy0WjkNxDzFv31WRSLsAXFGaGQnrQMLIDI1Jodc1nCvW4ZnNFYX6WYkKXeReFO0fY5c39+/D5zn1IPmMYfmkc2p19aRzSi742Dk2F3xqHyme851J9yb86l9JyYjV9lY72OiicqEb52lxaBd6WZjfGoFLrofXZRqHy93ZwrtbDty4Klb9/dT3cd4hpwkj/paruWC9sFG7J2lZMo+ymhdVf4YFMMr5e+JCnQkoPfeNSPVBScamdIGpxqdrGkfpcU8el404K5+rqq/7kSoR+ST69xy+3CITucDQjOarCmkIVMAeskThVr1Yzlr/Cqq303GJVuJD06kZaLvzyQ2EMBMoPeXstMPPDfZUfzqrotc4PVVN1ULht54dqtzpqOa6bE73H2Av8JMc3dzVUjm+/z8jxq5UnYvvdYnAQVclJqkHRQWEvdub4st/zgL7jUadJzPWInDRxbOUYdZrleeM8SqNzHWagfdlTYS9z1Gk8S1FbV8HFrrXxVq5Lk75wtGGr1mYdXmnV2nwVFia13hRKR6XPCY3iq/XS1n01ifCejVUvNf0hatVLvRX2htysq3qm+L26mtJOZS/WvKvR224Wwq55B3UNLUi43DXOQjXvSJoK/5TXRKnQqnkXP6jbPeGiy8YQ9b7/vkXJgrtahXDuW7zIcl8iPRh+RfsW8cB4SU6bFXHp/Y59i14/O0kmGD98dNBXx9rMLgy69p6IKQ3TyK8gW/1kNLq89+TNeNo9QH2hocP9z6xVC/CNTL4DS7Xqe6cnr6qE5DuVPQIzmqWiqwcVFDvy6uTauY3Hgvw0SK3ZxsVA9bjXpvjjoKLTIPU4jlfFO9KOSB+beXVk6HTlgEW+V9HT5cNhD0l19DK8vGE1rDb5RIc95odhXYWi4vjpmcPpqtrR9dyafDSG5/OlK/f50qw6PBuwjidNHoZtnd8k7MV1RjiqY3qvrOwhyZtTA9Y577TOXdKXX3Tay2aj/a2FfVY/cB87+V3kmbxQ1U/Y/rf+HYLG6DONRZroPuL++xjHJ5G2SwaRSHf/QP+d2cZ7xihbL0i5kE+Df6T7dEbDxWCWZbPdov/P/fkhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgP8b/wEPX2Hbl+sM9gAAAABJRU5ErkJggg==', 'www.fitnessforce.com').then(() => {
    }).catch(() => {
    });
  }

}
