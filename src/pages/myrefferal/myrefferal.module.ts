import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyrefferalPage } from './myrefferal';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MyrefferalPage,
  ],
  imports: [
    IonicPageModule.forChild(MyrefferalPage),
    TranslateModule
  ],
})
export class MyrefferalPageModule {}
