import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoPlayingPage } from './video-playing';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    VideoPlayingPage,
  ],
  imports: [
    IonicPageModule.forChild(VideoPlayingPage),
    TranslateModule
  ],
})
export class VideoPlayingPageModule {}
