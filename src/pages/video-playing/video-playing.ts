import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-video-playing',
  templateUrl: 'video-playing.html',
})
export class VideoPlayingPage {
  //public videoDetails;  
  public showDesc = false;
  public videoDetails =  { //object to store default keys
    "tenantId": "",
    "tenantName": "",
    "VideoThumbnail": "gym.jpg",
    "VideoId":"14156654",
    "VideoTitle":"he",
    "VideoDescription":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged",
    "Videocategory":"technology",
    "VideoURL":"https://player.vimeo.com/external/85569724.sd.mp4?s=43df5df0d733011263687d20a47557e4",
    "uploadDate":"24-12-2018",
    "Videolength":12,
    "viewCount":1671,
    "Tags": ["technology","science","entertainment","fitness","entertainment","fitness"]
}
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.videoDetails = this.navParams.get('item');
  }

  //to show description text
  toggleD()
  {
    this.showDesc = ! this.showDesc;   
  }

  
}
