import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentModalPage } from './appointment-modal';
import {FetcherProvider} from '../../providers/fetcher/fetcher';
import { TranslateModule } from '@ngx-translate/core';
  
@NgModule({
  declarations: [
    AppointmentModalPage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentModalPage),
    TranslateModule
  ],
  providers:[
    FetcherProvider
  ]
})
export class AppointmentModalPageModule {}
