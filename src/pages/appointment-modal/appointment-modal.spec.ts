// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, ViewChild } from '@angular/core';
// import { AppointmentModalPage } from './appointment-modal';
// import { IonicModule, Platform, NavController,NavParams ,AlertController,ViewController } from 'ionic-angular/index';
// import { StatusBar } from '@ionic-native/status-bar';
// import { SplashScreen } from '@ionic-native/splash-screen';
// import { Http, ConnectionBackend } from '@angular/http';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { IonicStorageModule, Storage } from '@ionic/storage';
// import { Grid, Col, Row } from 'ionic-angular/umd';
// import { HttpModule } from '@angular/http';
// import {FetcherProvider} from '../../providers/fetcher/fetcher'
// import { ScheduleAuthProvider } from '../../providers/schedule-auth/schedule-auth';
 
// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// export class ViewControllerMock {

//     public readReady: any = {
//         emit(): void {

//         },
//         subscribe(): any {

//         }
//     };

//     public writeReady: any = {
//         emit(): void {

//         },
//         subscribe(): any {

//         }
//     };

//     public contentRef(): any {
//         return new Promise(function (resolve: Function): void {
//             resolve();
//         });
//     }

//     public didEnter(): any {
//         return new Promise(function (resolve: Function): void {
//             resolve();
//         });
//     }

//     public didLeave(): any {
//         return new Promise(function (resolve: Function): void {
//             resolve();
//         });
//     }

//     public onDidDismiss(): any {
//         return new Promise(function (resolve: Function): void {
//             resolve();
//         });
//     }

//     public onWillDismiss(): any {
//         return new Promise(function (resolve: Function): void {
//             resolve();
//         });
//     }

//     public willEnter(): any {
//         return new Promise(function (resolve: Function): void {
//             resolve();
//         });
//     }

//     public willLeave(): any {
//         return new Promise(function (resolve: Function): void {
//             resolve();
//         });
//     }

//     public willUnload(): any {
//         return new Promise(function (resolve: Function): void {
//             resolve();
//         });
//     }

//     public dismiss(): any {
//         return true;
//     }

//     public enableBack(): any {
//         return true;
//     }

//     public getContent(): any {
//         return true;
//     }

//     public hasNavbar(): any {
//         return true;
//     }

//     public index(): any {
//         return true;
//     }

//     public isFirst(): any {
//         return true;
//     }

//     public isLast(): any {
//         return true;
//     }

//     public pageRef(): any {
//         return true;
//     }

//     public setBackButtonText(): any {
//         return true;
//     }

//     public showBackButton(): any {
//         return true;
//     }

//     public _setHeader(): any {
//         return true;
//     }

//     public _setIONContent(): any {
//         return true;
//     }

//     public _setIONContentRef(): any {
//         return true;
//     }

//     public _setNavbar(): any {
//         return true;
//     }

//     public _setContent(): any {
//         return true;
//     }

//     public _setContentRef(): any {
//         return true;
//     }

//     public _setFooter(): any {
//         return true;
//     }

// }

// // Mock Object used to test functions
// let testjson = [
//     {
//       "PurposeName": "Kick-boxing",
//       "PurposeId": " ",
//       "PurposeIcon": "kickboxing.svg",
//     "PurposeDiscription": " " ,
//     "PurposeDuration":" ",
//       "PostSessionMaintenanceTime": "",
//       "SpecialMessage": "",
//       "membermanagable": true,
//       "BookedSession": "20",
//       "TotalSession": "20",
//       "SessionOwner": " ",
//       "IfNoSessionOwnerLetMemberChooseSessionOwner": " ",
//       "PurposeDescription": " ",
//       "PurposeColour": " ",
//       "MaxDailySessionBookingLimitReached-": " "
//     },
//     {
//       "PurposeName": "aerobics",
//       "PurposeId": " ",
//       "PurposeIcon": "aerobics.svg",
//     "PurposeDiscription": " " ,
//     "PurposeDuration":" ",
//     "PostSessionMaintenanceTime": "",
//       "SpecialMessage": "jhvvkvikvc",
//       "membermanagable": true,
//       "BookedSession": "20",
//       "TotalSession": "20",
//       "SessionOwner": " ",
//       "IfNoSessionOwnerLetMemberChooseSessionOwner": " ",
//       "PurposeDescription": " ",
//       "PurposeColour": " ",
//       "MaxDailySessionBookingLimitReached-": " "
//     },
//     {
//       "PurposeName": "football",
//       "PurposeId": " ",
//       "PurposeIcon": "kickboxing.svg",
//     "PurposeDiscription": " " ,
//     "PurposeDuration":" ",
//       "PostSessionMaintenanceTime": "",
//       "SpecialMessage": "dsfgjkkgf",
//       "membermanagable": true,
//       "BookedSession": "0",
//       "TotalSession": "20",
//       "SessionOwner": " ",
//       "IfNoSessionOwnerLetMemberChooseSessionOwner": " ",
//       "PurposeDescription": " ",
//       "PurposeColour": " ",
//       "MaxDailySessionBookingLimitReached-": " "
//     },
//     {
//       "PurposeName": "cricket",
//       "PurposeId": " ",
//       "PurposeIcon": "kickboxing.svg",
//     "PurposeDiscription": " " ,
//     "PurposeDuration":" ",
//       "PostSessionMaintenanceTime": "",
//       "SpecialMessage": "vdffhdf",
//       "membermanagable": true,
//       "BookedSession": "10",
//       "TotalSession": "20",
//       "SessionOwner": " ",
//       "IfNoSessionOwnerLetMemberChooseSessionOwner": " ",
//       "PurposeDescription": " ",
//       "PurposeColour": " ",
//       "MaxDailySessionBookingLimitReached-": " "
//     },
//     {
//       "PurposeName": "Kick-boxing",
//       "PurposeId": " ",
//       "PurposeIcon": "kickboxing.svg",
//     "PurposeDiscription": " " ,
//     "PurposeDuration":" ",
//       "PostSessionMaintenanceTime": "",
//       "SpecialMessage": "",
//       "membermanagable": false,
//       "BookedSession": "20",
//       "TotalSession": "20",
//       "SessionOwner": " ",
//       "IfNoSessionOwnerLetMemberChooseSessionOwner": " ",
//       "PurposeDescription": " ",
//       "PurposeColour": " ",
//       "MaxDailySessionBookingLimitReached-": " "
//     },
//     {
//       "PurposeName": "Kick-boxing",
//       "PurposeId": " ",
//       "PurposeIcon": "kickboxing.svg",
//     "PurposeDiscription": " " ,
//     "PurposeDuration":" ",
//       "PostSessionMaintenanceTime": "",
//       "SpecialMessage": "",
//       "membermanagable": true,
//       "BookedSession": "20",
//       "TotalSession": "20",
//       "SessionOwner": " ",
//       "IfNoSessionOwnerLetMemberChooseSessionOwner": " ",
//       "PurposeDescription": " ",
//       "PurposeColour": " ",
//       "MaxDailySessionBookingLimitReached-": " "
//     },
//     {
//       "PurposeName": "Kick-boxing",
//       "PurposeId": " ",
//       "PurposeIcon": "kickboxing.svg",
//     "PurposeDiscription": " " ,
//     "PurposeDuration":" ",
//       "PostSessionMaintenanceTime": "",
//       "SpecialMessage": "",
//       "membermanagable": false,
//       "BookedSession": "20",
//       "TotalSession": "20",
//       "SessionOwner": " ",
//       "IfNoSessionOwnerLetMemberChooseSessionOwner": " ",
//       "PurposeDescription": " ",
//       "PurposeColour": " ",
//       "MaxDailySessionBookingLimitReached-": " "
//     },
//     {
//       "PurposeName": "Kick-boxing",
//       "PurposeId": " ",
//       "PurposeIcon": "kickboxing.svg",
//     "PurposeDiscription": " " ,
//     "PurposeDuration":" ",
//       "PostSessionMaintenanceTime": "",
//       "SpecialMessage": "",
//       "membermanagable": true,
//       "BookedSession": "1",
//       "TotalSession": "20",
//       "SessionOwner": " ",
//       "IfNoSessionOwnerLetMemberChooseSessionOwner": " ",
//       "PurposeDescription": " ",
//       "PurposeColour": " ",
//       "MaxDailySessionBookingLimitReached-": " "
//     },
//     {
//       "PurposeName": "Kick-boxing",
//       "PurposeId": " ",
//       "PurposeIcon": "kickboxing.svg",
//     "PurposeDiscription": " " ,
//     "PurposeDuration":" ",
//       "PostSessionMaintenanceTime": "",
//       "SpecialMessage": "",
//       "membermanagable": true,
//       "BookedSession": "20",
//       "TotalSession": "20",
//       "SessionOwner": " ",
//       "IfNoSessionOwnerLetMemberChooseSessionOwner": " ",
//       "PurposeDescription": " ",
//       "PurposeColour": " ",
//       "MaxDailySessionBookingLimitReached-": " "
//     }
    
    
//     ]



// describe('Appointment Modal UI Testing', () => {
//     let comp: AppointmentModalPage;
//     let fixture: ComponentFixture<AppointmentModalPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [AppointmentModalPage],
//             imports: [
//                 HttpModule,
//                 IonicModule.forRoot(AppointmentModalPage),
//                 IonicStorageModule.forRoot(),
//             TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 FetcherProvider,
//                 TranslateService,
//                 {provide: ViewController, useClass: ViewControllerMock},
//                 TranslateStore,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 AlertController,
//                 ScheduleAuthProvider
                
               
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(AppointmentModalPage);
//         comp = fixture.componentInstance;
//         comp.appoint= ['','','',''];
        
//     });

  


//     it('Heading should be present and match for Action Sheet', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.Header')).nativeElement;
//         expect(inputfield.className).toMatch('Header');
//     });

//     it('List should be present', () => {
        
//         fixture.detectChanges();
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.customcard')).nativeElement;
//         expect(inputfield.className).toMatch('customcard');
//     });

//     it('Card Text should match', () => {
       
//         fixture.detectChanges();
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.aname')).nativeElement;
//         expect(inputfield.className).toMatch('aname');
//     });

//     it('Buy button should be present', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.Header')).nativeElement;
//         expect(inputfield.className).toMatch('Header');
//     });

//     it('Session text should be present', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.Header')).nativeElement;
//         expect(inputfield.className).toMatch('Header');
//     });


        


//     });



//     describe('Appointment Modal Logic Testing', () => {
//         let comp: AppointmentModalPage;
//         let fixture: ComponentFixture<AppointmentModalPage>;
    
//         beforeEach(async(() => {
//             TestBed.configureTestingModule({
//                 declarations: [AppointmentModalPage],
//                 imports: [
//                     HttpModule,
//                     IonicModule.forRoot(AppointmentModalPage),
//                     IonicStorageModule.forRoot(),
//                     TranslateModule.forChild()
//                 ],
//                 providers: [
//                     NavController,
//                     FetcherProvider,
//                     { provide: NavParams, useClass: NavParamsMock },
//                     AlertController,
//                     {provide: ViewController, useClass: ViewControllerMock},
//                     TranslateService,
//                 TranslateStore,
//                     ScheduleAuthProvider
                    
                   
//                 ]
//             });
//         }));
    
//         beforeEach(() => {
//             fixture = TestBed.createComponent(AppointmentModalPage);
//             comp = fixture.componentInstance;
//             comp.appoint= ['','','',''];
            
//         });
    
      
    
    
//         // it('Check if membership available', () => {
           
//         //    expect(comp.checkMemebership(testjson[0])).toBe(false);
//         // });

//         // it('Compare Used and Total Sessions', () => {
           
//         //     expect(comp.checkMemebership(testjson[1]) != 0).toBe(true);
//         //  });
    
//          it('Buy Button Status depending on conditions - 1', () => {
//             comp.checkBuyButton(testjson);
//             expect(comp.buybuttonstatus[3]).toBe(false);
//          });

//          it('Buy Button Status depending on conditions - 2', () => {
//             comp.checkBuyButton(testjson);
//             expect(comp.buybuttonstatus[2]).toBe(false);
//          });
    
//          it('Buy Button Status depending on conditions - 3', () => {
//             comp.checkBuyButton(testjson);
//             expect(comp.buybuttonstatus[3]).toBe(false);
//          });

        

//          it('Check if non - member managable', () => {
//             comp.checkBuyButton(testjson);
//             expect(testjson[4].membermanagable).toBe(false);
//          });

        

//          it('Check if non - member managable', () => {
//              comp.closeModaly;
//             expect(comp.closeModaly).toBeDefined;
//          });

         

//         //  it('Check if special alert is present for selected purpose - 1', () => {
//         //     expect(comp.checkifhasSpecialAlert(testjson[4])).toBe(true);
//         //  });

//          it('Check if special alert has a msg String', () => {
//             expect(testjson[3].SpecialMessage.length !=0).toBe(true);
//          });
        
//         //  it('Check if special alert is present for selected purpose - 2', () => {
//         //     expect(comp.checkifhasSpecialAlert(testjson[3])).toBe(false);
//         //  });
//         it('check if cartOrbooked boolean is false', () => {
//             expect(comp.cartOrbooked).toBeFalsy;
//         })
    
//          it('Check if received any empty value from server and use placeholder instead', () => {
//             expect(comp.emptyValueChecker).toBeDefined();
//          });
//          });





