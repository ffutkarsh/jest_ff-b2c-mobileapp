import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import * as moment from 'moment';
import { ScheduleAuthProvider } from '../../providers/schedule-auth/schedule-auth';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-appointment-modal',
  templateUrl: 'appointment-modal.html',
})
export class AppointmentModalPage {

  public purposeList; //TO STORE PURPOSE LIST.
  public buybuttonstatus = []; //VARIABLE TO STORE WHETHER PURPOSE CAN BE BOUGHT OR NOT.
  public temp; //VARIABLE FOR STORING DATABASE VALUE TEMPORARILY.
  public time = moment().format("D"); //VARIABLE TO STORE TODAYS DATE.
  public cartOrbooked: boolean = false; //boolean from navparams for cart or repeat view condition 

  constructor(
    private storage: Storage,
    public viewCtrl: ViewController,
    public scheduleAuthService: ScheduleAuthProvider,
    public navCtrl: NavController,
    public translateService: TranslateService,
    public navParams: NavParams,
    public alertCtrl: AlertController) {

    this.purposeList = navParams.get("purposeApiResponse");
    console.log("PURPOSE API RESPONSE FROM DASHBOARD");
    console.log(this.purposeList);
    this.emptyValueChecker(); // Method used to set placeholders in case server does not send data
    this.checkMM(); // Removes non - member managable task from the list

  }

  ionViewDidLoad() { }

  // Method used to set placeholders in case server does not send data data . Eg: icon , special msg
  emptyValueChecker() {
    for (let i = 0; i < this.purposeList.length; i++) {
      if (this.purposeList[i].PurposeIcon == "") {
        this.purposeList[i].PurposeIcon = ""; // Setting a default icon if icon not present
      }
    }
  }

  // Removes non - member managable task from the list
  checkMM() {
    for (let i = 0; i < this.purposeList.length; i++)
      if (this.purposeList[i].membermanagable == false) {
        this.purposeList.splice(i, 1); // Removing such entries which are not supposed to be bought
      }
    this.checkBuyButton(this.purposeList);
  }

  // Method to Form array conataining final state of Button on the appointment list based on conditions
  checkBuyButton(objarray) {
    for (let i = 0; i < objarray.length; i++) {
      // Checking if all sessions used up
      if (this.comparesession(objarray[i]) != 0) {
        this.buybuttonstatus.push(false);
      }
      else {
        this.buybuttonstatus.push(true);
      }
    }
  }

  // Calculated difference between Total and Used Sessions
  comparesession(obj) {
    let diff = obj.TotalSession - obj.BookedSession;
    return diff;
  }

  //close modal when clicked outside the modal content area i.e the backdrop area
  closeModaly(e) {
    if ((e.path[0].className) == 'scroll-content') {
      this.viewCtrl.dismiss();
    }
    if ((e.path[0].className) == 'button-inner') {
      this.navCtrl.pop();
    }
  }

  // Moving to next page - Choose Trainer
  gototrainer(i) {

    // Checks if Buy Button is enabled
    if (this.buybuttonstatus[i] == true) {
      // Checking if has Special Alert Condition
      if (this.purposeList[i].SpecialMessage.length != 0) {
        this.showSpecialAlert(i); // Show alert msg from the JSON
      }
      else {
       
        // Sending Purpose via nav Params
        this.navCtrl.push('SpecificorAnyTrainerPage', {
          'appointFull': this.purposeList[i],
          'purpose': this.purposeList[i].PurposeName,
          'purposeTotalSessions': this.purposeList[i].TotalSession,
          'icon': this.purposeList[i].PurposeIcon,
          'cartOrbooked': this.cartOrbooked = true
        });
      }
    }
  
    this.scheduleAuthService.fromSchedulepage();
  }

  //navigating to select trainer with get the object for daily maximum allowed validations from the ionic localstorage 
  gototrainer2(i) {

    this.storage.get(this.time).then((val) => {
      this.temp = val;
      if (this.temp == null) {
            this.navCtrl.push('SpecificorAnyTrainerPage', {
          'appointFull': this.purposeList[i],
          'purpose': this.purposeList[i].PurposeName,
          'purposeTotalSessions': this.purposeList[i].TotalSession,
          'icon': this.purposeList[i].PurposeIcon,
          'cartOrbooked': this.cartOrbooked,
        })
        
     
      }
      else {
        var j;
        var f = 0;
        for (j = 0; j < this.temp.length; j++) {
          // if (this.temp[j].PurposeName == this.purposeList[i].PurposeName) {
          //   f += 1;
          // }
        }
        if (f >= 3) {
          this.maxAlert();
        }
        else {
         
            this.navCtrl.push('SpecificorAnyTrainerPage', {
              'appointFull': this.purposeList[i],
              'purpose': this.purposeList[i].PurposeName,
              'purposeTotalSessions': this.purposeList[i].TotalSession,
              'icon': this.purposeList[i].PurposeIcon,
              'cartOrbooked': this.cartOrbooked,
            
           
          })
       
        }
      }
    });
  }

  // Present Dialog that shows the special condition (from JSON)
  showSpecialAlert(index) {
    const alert = this.alertCtrl.create({
      title: 'Please Note that :',
      subTitle: this.purposeList[index].SpecialMessage,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.push('SpecificorAnyTrainerPage',
              {
                'purpose': this.purposeList[index].PurposeName,
                'purposeTotalSessions': this.purposeList[index].TotalSession,
                'icon': this.purposeList[index].PurposeIcon,
                'cartOrbooked': this.cartOrbooked = true,
              }
            )
          }
        },
      ]
    });
    alert.present();
  }

  //alert for maximum booked sessions daily
  maxAlert() {
    const alert = this.alertCtrl.create({
      title: ' Alert:',
      subTitle: 'Maximum allowed sessions exceeded for the day',
      buttons: ['Dismiss']
    });
    alert.present();
  }

  ionViewDidLeave()
  {
   
  }
}
