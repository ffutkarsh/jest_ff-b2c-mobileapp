import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions, DestinationType } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { TranslateService } from '@ngx-translate/core';
import { Base64 } from '@ionic-native/base64';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';

@IonicPage()
@Component({
  selector: 'page-employee-modal',
  templateUrl: 'employee-modal.html',
})
export class EmployeeModalPage {
  public fileData: any;
  public path: string;  //storing string path

  constructor(public navCtrl: NavController,
    public camera: Camera,
    public crop: Crop,
    private fileChooser: FileChooser,
    private filePath: FilePath,
    private base64: Base64,
    public translateService: TranslateService,

    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EmployeeModalPage');
  }

  // Method to close the modal
  closeModal() {
    this.navCtrl.pop();
  }

  //method to open camera and crop image
  cropDOC() {

    let option: CameraOptions = { //setting quality, height,width
      quality: 100,
      targetWidth: 1000,
      targetHeight: 1000,
      destinationType: this.camera.DestinationType.NATIVE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(option).then(url => { //open camera
      this.path = url;
      this.crop.crop(this.path, option).then(newImageUrl => { //crop image
        this.path = newImageUrl;
        if (this.path) {
          // convert your file in base64 format
          this.base64.encodeFile(this.path)
            .then((base64File: string) => {
              this.fileData = base64File; 
              console.log(this.fileData);
              alert('File uploaded successfully');  ////store in variable
            }, (err) => {
              alert('err' + JSON.stringify(err));
            });
        }
      }, err => {
       // alert("Error" + err);
      });
     // alert("file uploaded")
    }, err => {
    //  alert("Error" + err);
    });

  }




  fileChoose(){
    // choose your file from the device
	this.fileChooser.open().then(uri => {
		// alert('uri'+JSON.stringify(uri));
        // get file path
		this.filePath.resolveNativePath(uri)
		.then(file => {
			// alert('file'+JSON.stringify(file));
			let filePath: string = file;
			if (filePath) {
                // convert your file in base64 format
				this.base64.encodeFile(filePath)
                .then((base64File: string) => {
			
        this.fileData=base64File;   ////store in variable
        console.log(this.fileData);
        alert('File uploaded successfully');
				}, (err) => {
					alert('err'+JSON.stringify(err));
				});
			}
		})
		.catch(err => console.log(err));
	})
	.catch(e => alert('uri'+JSON.stringify(e)));
  }
}
