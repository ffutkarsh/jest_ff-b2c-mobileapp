import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmployeeModalPage } from './employee-modal';
import { TranslateModule } from '@ngx-translate/core';
import { Crop } from '@ionic-native/crop';
import { Base64 } from '@ionic-native/base64';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
@NgModule({
  declarations: [
    EmployeeModalPage,
  ],
  imports: [
    IonicPageModule.forChild(EmployeeModalPage),
    TranslateModule

  ],
  providers:
  [
    Crop  ,Base64,FileChooser,FilePath
  ]
})
export class EmployeeModalPageModule {}
