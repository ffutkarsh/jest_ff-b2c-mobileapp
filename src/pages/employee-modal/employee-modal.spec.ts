// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, style } from '@angular/core';
// import { EmployeeModalPage } from './employee-modal';

// import { IonicModule, Platform, NavController, NavParams, ViewController, Item } from 'ionic-angular/index';
// import { HttpModule } from '@angular/http';
// import { Camera, CameraOptions, DestinationType } from '@ionic-native/camera';
// import { Crop } from '@ionic-native/crop';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { mockApp, mockConfig, mockPlatform, mockView } from "ionic-angular/util/mock-providers";
// import { Base64 } from '@ionic-native/base64';
// import { FileChooser } from '@ionic-native/file-chooser';
// import { FilePath } from '@ionic-native/file-path';
// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }
// //The describe function is for grouping related specs
// describe('---------EMPLOYEE MODAL PAGE UI----------', () => {
//     let de: DebugElement;
//     let comp: EmployeeModalPage;
//     let fixture: ComponentFixture<EmployeeModalPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [EmployeeModalPage],
//             imports: [
//                 HttpModule,
//                 IonicModule.forRoot(EmployeeModalPage),
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 {
//                     provide: ViewController, useValue: mockView()
//                 },
//                 TranslateService,
//                   TranslateStore,
//                   Base64,FileChooser,FilePath,  
//                 NavController, Camera, Crop, { provide: NavParams, useClass: NavParamsMock },],
//         });
//     }));

//     beforeEach(() => {
//         // create component and test fixture
//         fixture = TestBed.createComponent(EmployeeModalPage);
//         // get test component from the fixture
//         comp = fixture.componentInstance;
//     });
//     //Creating the main Component
//     it('should create component', () => expect(comp).toBeDefined());
//      //checking the content
//     it('checking the main content using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.mainContent')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('mainContent');
//     });
//       //checking the sub content
//       it('checking the sub content using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.modal')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('modal');
//     });

//      //checking the sub content
//      it('checking the dib containing attach document header using class name', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.Header')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('Header');
//     });

//      //checking the main list 
//      it('checking the list holding items', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.list1')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('list1');
//     });

//        //checking the list items
//        it('checking the list items', () => {
//         const contentClass: HTMLInputElement = fixture.debugElement.query(By.css('.listItem')).nativeElement;
//         expect(contentClass.getAttribute('class')).toMatch('listItem');
//     });

      

// });