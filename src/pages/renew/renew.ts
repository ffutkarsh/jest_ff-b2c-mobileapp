import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-renew',
  templateUrl: 'renew.html',
})
export class RenewPage {

  description1; //var to save description boolean
  description2; //var to save description boolean
  home;
  passport; 
  public renewMembership = [];    // array to store details of renew membership
  public renewData: any = [];     //  variable for pushing data by navparams
  public renewFlag: boolean;       /// variable for pushing data by navparams
  public selectedRenewMembership = [];      ///  array to store details of renew membership

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams: NavParams) {

    this.description1 = false; //toggle variable
    this.description2 = false; //toggle variable
    this.home = "Home Membership gives you access to the branch for which you purchased the membership at a more affordable rate. Best suited for people who regularly use facility at only 1 location";
    this.passport = "Passport Membership gives you access to the branch for which you purchased the membership at a more affordable rate. Best suited for people who use facility at multiple location";

  }

  ionViewWillEnter() {

    this.renewFlag = (this.navParams.get("renewflag"));   
    this.renewData.push(this.navParams.get("renew"));  


    this.renewMembership = [      // array to store details of renew membership
      {
        "monthDuration": "3",
        "homeRate": "8000",
        "passportRate": "11000",
        "statushome": false,
        "statuspass": false
      },
      {
        "monthDuration": "6",
        "homeRate": "14000",
        "passportRate": "19000",
        "statushome": false,
        "statuspass": false
      },
      {
        "monthDuration": "9",
        "homeRate": "20000",
        "passportRate": "25000",
        "statushome": false,
        "statuspass": false
      }
    ]
  }

  ionViewDidLoad() {   
     }

   /// showing  details of home rate
  expandDescription1() {
    this.description1 = !this.description1;
  }

  /// closing  details of home rate
  expandDescription2() {
    this.description2 = !this.description2;
  }

   /// METHOD FOR opening renew Home rate model;
  openHome(R,selected) {
    for (let i = 0; i < this.renewMembership.length; i++) {
      if (this.renewMembership[i].statuspass == true) {
        this.renewMembership[i].statuspass = false
      }
    }
    for (let i = 0; i < this.renewMembership.length; i++) {
      if (i == selected) {
        this.renewMembership[i].statushome = true;
        this.selectedRenewMembership = this.renewMembership[i];
        let renewModal = this.modalCtrl.create("RenewHomeActionPage", { "renew": R ,'renewJsonData':this.renewData});
        renewModal.present();
      }
      else
        this.renewMembership[i].statushome = false;
    }
  }

       /// METHOD FOR opening renew passport rate model;
  openPassport(R,selected) { 
    for (let i = 0; i < this.renewMembership.length; i++) {
      if (this.renewMembership[i].statushome == true) {
        this.renewMembership[i].statushome = false;
      }
    }
    for (let i = 0; i < this.renewMembership.length; i++) {
      if (i == selected) {
        this.renewMembership[i].statuspass = true;
        this.selectedRenewMembership = this.renewMembership[i];
        let renewModal = this.modalCtrl.create("RenewActionModalPage", { "renew": R ,'renewJsonData':this.renewData});
        renewModal.present();
      }
      else
        this.renewMembership[i].statuspass = false;
    }
  }

}
