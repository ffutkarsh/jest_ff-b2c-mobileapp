import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RenewPage } from './renew';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    RenewPage,
  ],
  imports: [
    IonicPageModule.forChild(RenewPage),
    TranslateModule
  ],
})
export class RenewPageModule {}
