import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';
import  { VideoListPage } from '../../pages/pages';
import { VideoCategoryProvider } from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-video-category',
  templateUrl: 'video-category.html',
})
export class VideoCategoryPage {

  public videoCategory = {}; //STORING API RESPONSE OF VIDEO CATEGORIES.
  public myInput: any; //VARIABLE TO BIND THE STRING WRITTEN IN SEARCH BAR TO NG-MODEL.

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,        
    public translateService: TranslateService,
    public videocategory: VideoCategoryProvider) {

  }

  ionViewDidLoad() {   
    this.getVideoCategory();
  }

  //API RESPONSE FOR VIDEO CATEGORIES.
  getVideoCategory() {
    this.videocategory.getVideoCategory().map((response) => response).subscribe((response) => {
      this.videoCategory = response;     
    });
  }

  //HANDLES THE BACKPRESSED OR DELETE AND DISPLAYS THE LIST ACCORDING TO CURRENT SEARCH TEXT.
  onBackPress(event) {
    if (event.keyCode == 8 || event.keyCode == 46) {     
      this.getVideoCategory();
    }
  }

  //TAKES THE INPUT AND BASED ON IT FILTERS THE LIST OF VIDEO CATEGORIES.
  onInput($event) {
    this.setFilteredItems(this.myInput)
  }

  //FILTERS THE LIST OF VIDEO CATEGORIES BY MODIFYING THE LIST OF VIDEO CATEGORIES AS PER INPUT.
  setFilteredItems(term) {
    this.videoCategory['VideoCategory'] = this.filterItemsSearch(term);
  }

  //MODIFIES THE VIDEO CATEGORIES LIST FROM THE SEARCH TEXT.
  filterItemsSearch(searchText) {

    if (!this.videoCategory['VideoCategory']) return [];

    if (!searchText) return this.videoCategory['VideoCategory'];

    searchText = searchText.toLowerCase();

    return this.videoCategory['VideoCategory'].filter(it => {
      return it.purposeName.toLowerCase().includes(searchText);
    });

  }


  //go to video list
  goToList()
  {
     this.navCtrl.push(VideoListPage);
  }

}
