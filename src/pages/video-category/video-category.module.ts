import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoCategoryPage } from './video-category';
import { VideoCategoryProvider } from '../../providers/video-category/video-category';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    VideoCategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(VideoCategoryPage),
    TranslateModule
  ],
  providers: [
    VideoCategoryProvider,
    
  ]
})
export class VideoCategoryPageModule { }
