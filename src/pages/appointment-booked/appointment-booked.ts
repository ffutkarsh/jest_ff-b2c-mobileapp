import { Component ,ViewChild} from '@angular/core';
import { ViewController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { IonicPage, NavController, NavParams,App,Nav,Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FetcherProvider } from '../../providers/fetcher/fetcher';
import { RepeatBookingAuthProvider } from '../../providers/repeat-booking-auth/repeat-booking-auth';
import { PopoverController } from 'ionic-angular';
import { CartItemsProvider } from '../../providers/providers';
import { RepeatBookingProvider } from '../../providers/repeat-booking/repeat-booking';
import * as moment from 'moment';
import { Screenshot } from '@ionic-native/screenshot';
import { SocialSharing } from '@ionic-native/social-sharing';
import { SchedulingPage} from '../../pages/pages'
import { MenuController } from 'ionic-angular';
@IonicPage()

@Component({
  selector: 'page-appointment-booked',
  templateUrl: 'appointment-booked.html',
})
export class AppointmentBookedPage {
  @ViewChild(Nav) nav: Nav;
  public appointFull; //FROM DATE AND TIME PAGE.
  public purpose; //FROM DATE AND TIME PAGE.
  public purposeTotalSessions; //FROM DATE AND TIME PAGE.
  public icon; //FROM DATE AND TIME PAGE.
  public cartOrbooked; //FROM DATE AND TIME PAGE.
  public trainername; //FROM DATE AND TIME PAGE.
  public trainerType; //FROM DATE AND TIME PAGE.
  public trainerIcon; //FROM DATE AND TIME PAGE.
  public mastertrainerlist; //FROM DATE AND TIME PAGE.
  public trianerrating; //FROM DATE AND TIME PAGE.
  public ratingsurveycount; //FROM DATE AND TIME PAGE.
  public date; //FROM DATE AND TIME PAGE.
  public availableSlot; //FROM DATE AND TIME PAGE.
  public updated; //FROM DATE AND TIME PAGE.

  appFromCart;
  public cartItems = [];
  public time = moment().format("D"); //TIME.
  public init = [];

  cartObj = [];
  screen: any; //holding filepath  
  bookedDetails;

  apptype; //VARIABLE TO STORE THE APPOINTMENT TYPE "ONLY ONCE" OR "REPEATS".
  repeattime; //VARIABLE TO STORE THE REPEAT TIME IF "REPEATS" IS SELECTED.
  totalsessions; //VARIABLE TO STORE THE total sessions.

  updatedFromDate;
  state: boolean = false;
  objFromCartProvider;

  dummyResponse = {
    'appointFull': "",
    'purpose': "",
    'purposeTotalSessions': "",
    'icon': "",
    'cartOrbooked': "",
    'trainername': "",
    'trainerType': "",
    'trainerIcon': "",
    'mastertrainerlist': "",
    'trainerrating': "",
    'ratingsurveycount': "",
    'date': "",
    'availableSlot': "",
    'updated': "",
    'apptype': "",
    'repeattime': 0
  }; // Variable to mimic response from the server

  public timer;

  private tick: number; //TIMER

  public appointmentdetail; // USED TO SAVE VALUES IN LOCAL STORAGE.

  constructor(
    public platform: Platform,
    public app: App,
    public menuCtrl: MenuController,
    public repeatbooking: RepeatBookingProvider,
    public popoverCtrl: PopoverController,
    private screenshot: Screenshot,
    public cartItemsService: CartItemsProvider,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public repeatAuthService: RepeatBookingAuthProvider,
    public fetcher: FetcherProvider,
    public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage,
    private socialSharing: SocialSharing,

    ) {

    this.appointFull = this.navParams.get('appointFull');
    this.purpose = this.navParams.get("purpose");
    this.purposeTotalSessions = this.navParams.get("purposeTotalSessions");
    this.icon = this.navParams.get("icon");
    this.cartOrbooked = this.navParams.get('cartOrbooked');
    this.trainername = this.navParams.get("trainername");
    this.trainerType = this.navParams.get("trainerType");
    this.trainerIcon = this.navParams.get('trainerIcon');
    this.mastertrainerlist = this.navParams.get('mastertrainerlist');
    this.trianerrating = this.navParams.get('trainerrating');
    this.ratingsurveycount = this.navParams.get('ratingsurveycount');
    this.date = this.navParams.get('date');
    this.availableSlot = this.navParams.get('availableSlot');
    this.updated = this.navParams.get('updated');

    this.apptype = this.navParams.get("apptype");; //RECEIVING APPOINTMENT TYPE. "ONLY ONCE" OR "REPEATS".
    this.repeattime = this.navParams.get("repeattime");; //RECEIVING REPEAT TIME "NO. OF APPOINTMENTS.". 

    this.dummySuccessBooking();
    this.cartObj = this.cartItemsService.tempItem;
    this.fetch();
    this.fetchCartItems(this.cartObj) //calling cart items provider method

    this.appFromCart = false;
    this.objFromCartProvider = this.cartItemsService.tempObj
    if (this.cartItemsService.tempObj == undefined) {
      this.appFromCart = false;
    }
    else if (this.cartItemsService.tempObj.fromCart == true) {
      this.appFromCart = true;
    }

  }

  ionViewDidLoad() {
    this.fetchCartItems(this.cartObj) //calling cart items provider method
    this.getData();
    this.startTimer(5);
  }

  //from json using its provider -- CART ITEMS
  fetchCartItems(cartObj) {
    console.log(cartObj, "CART ITEMS...APPBOOKED")
    for (var index = 0; index < cartObj.length; index++) {
      this.cartItems.push(cartObj)
    }
  }

  getData() {
    this.fetcher.getBookedOrder().map((response) => response).subscribe((response) => {
      if (this.dummyResponse.icon == "") {
        this.dummyResponse.icon = "assets/icon/sarah-avatar.png.jpeg";
      }
      if (this.dummyResponse.icon == "") {
        this.dummyResponse.icon = "assets/img/PT/cardio.svg";
      }
    });
  }

  startTimer(timeoutt) {
    // this.timer = TimerObservable.create(100, 1000).subscribe(t => {
    //   this.tick = timeoutt - t;
    //   if (this.tick < 0) {
    //     this.timer.unsubscribe();
    //     this.tick = null;
    //     this.GotoSchedule();
    //   }
    // });
  }

  dummySuccessBooking() {
    let currentbooking = this.appointFull;
    this.storage.get(this.time).then((val) => {
      if (val != null)
        this.init = val;
    }).then(() => {
      this.init.push(currentbooking);
      this.storage.set(this.time, this.init).then((val) => {
        this.storage.get(this.time).then((val) => {
          console.log("from DB", val);
        });
      });
    });
  }

  fetch() {
    this.repeatbooking.repeatBookingDetails().map(response => response).subscribe((response) => {
      this.bookedDetails = response['bookedResponse'];
   
      console.log(this.bookedDetails, "this is repeat booking response")
    })
  }

  screenShot() {
    this.screenshot.URI(80)
    .then((res) => {
      this.socialSharing.share('', '', res.URI,''  )
       .then(() => {},
         () => { 
           alert('SocialSharing failed');
         });
       },
      () => {
      alert('Screenshot failed');
      });
    // alert('screenshot captured')
    // this.screenshot.save('jpg', 80, 'myscreenshot.jpg').then(res => {
    //   this.screen = res.filePath;
    //   this.state = true;
    //   this.reset();
    //   // Share via email
    //   this.socialSharing.share('message', 'subject', this.screen, 'url').then(() => {
    //     console.log("Sharing Success");
    //   }).catch(() => {
    //     console.log("Sharing Failure");
    //   });
    // });
  }

  // Reset function we will use to hide the screenshot preview after 1 second
  reset() {
    var self = this;
    setTimeout(function () {
      self.state = false;
    }, 2000);
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create("AppointmentPopoverPage", {
      'appointFull': this.appointFull,
      'purpose': this.purpose,
      'purposeTotalSessions': this.purposeTotalSessions,
      'icon': this.icon,
      'cartOrbooked': this.cartOrbooked,
      'trainername': this.trainername,
      'trainerType': this.trainerType,
      'trainerIcon': this.trainerIcon,
      'mastertrainerlist': this.mastertrainerlist,
      'trainerrating': this.trianerrating,
      'ratingsurveycount': this.ratingsurveycount,
      'date': this.date,
      'availableSlot': this.availableSlot,
      'updated': this.updated,
      'apptype': this.apptype,
      'repeattime': this.repeattime
    });
    popover.present({
      ev: myEvent
    });
  }

  starRating(rating: number) {
    let full;
    if (rating <= 5) {
      full = Math.floor(rating);
    }
    else {
      full = 5;
    }
    let status = [];
    for (let i = 0; i < full; i++) {
      status.push("fulle.svg"); // saves Full Star svg in array
    }
    if (rating - full == 0.5 && !(rating >= 5)) {
      status.push("halfe.svg"); // saves Half Star svg in array
    }
    if (status.length < 5) {
      let emp = 5 - status.length;
      for (let i = 0; i < emp; i++) {
        status.push("fullgrey.svg");// saves Empty Star svg in array
      }
    }
    return status; // Returns array so that it can be rendered using *ngFor
  }

  GotoSchedule() {
    this.storelocally();
   
   this.navCtrl.push('SchedulingPage',{
'scheduleKey' : true
   })
   
 // this.navCtrl.setRoot('SchedulingPage')
  
  }

  
  //SAVES DATA TO LOCAL DATABASE.
  storelocally() {
    this.appointmentdetail = {
      'appointFull': this.appointFull,
      'purpose': this.purpose,
      'purposeTotalSessions': this.purposeTotalSessions,
      'icon': this.icon,
      'cartOrbooked': this.cartOrbooked,
      'trainername': this.trainername,
      'trainerType': this.trainerType,
      'trainerIcon': this.trainerIcon,
      'mastertrainerlist': this.mastertrainerlist,
      'trainerrating': this.trianerrating,
      'ratingsurveycount': this.ratingsurveycount,
      'date': this.date,
      'availableSlot': this.availableSlot,
      'updated': this.updated,
      'apptype': this.apptype,
      'repeattime': this.repeattime
    }
    this.storage.set('appointmentdetails', this.appointmentdetail).then((val) => {
      this.storage.get('appointmentdetails').then((val) => {
        console.log(val);
      });
    });
  } //SAVE OBJECT ON LOCAL STORAGE , FEEDBACK DETAILS: FEEDBACK TYPE, FEEDBACK TEXT, PHONENUMBER, EMAILID.

  ionViewWillEnter() {
    this.objFromCartProvider = this.cartItemsService.tempObj
    if (this.cartItemsService.tempObj == undefined) {
      this.appFromCart = false;
    }
    else if (this.cartItemsService.tempObj.fromCart == true) {
      this.appFromCart = true;
    }


    
  }

  // Method used to check for navigation gaurd logic
  ionViewCanEnter() {
    this.updatedFromDate = this.navParams.get("updated");
    console.log("in ionViewCanEnter");
  }

}
