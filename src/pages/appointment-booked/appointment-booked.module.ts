import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentBookedPage } from './appointment-booked';
import {FetcherProvider} from '../../providers/fetcher/fetcher';

@NgModule({
  declarations: [
    AppointmentBookedPage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentBookedPage),
  ],
  providers : [
    FetcherProvider
  ]
})
export class AppointmentBookedPageModule {}
