// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, ViewChild } from '@angular/core';
// import { AppointmentBookedPage } from './appointment-booked';
// import { IonicModule, Platform, NavParams, NavController } from 'ionic-angular/index';
// import { Http, ConnectionBackend, HttpModule } from '@angular/http';
// import { IonicStorageModule, Storage } from '@ionic/storage';
// import { FetcherProvider } from '../../providers/fetcher/fetcher';
// import { RepeatBookingAuthProvider } from '../../providers/repeat-booking-auth/repeat-booking-auth';


// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// let dummyResponse =
// {
//     "purpose": "PT",
//     "purposeIcon": "assets/img/PT/cardio.svg",
//     "trainer": "Raphen Reuben",
//     "date": "06 Sep 2018",
//     "icon": "assets/icon/sarah-avatar.png.jpeg",
//     "availableSlot": "11:00 AM -12:00 PM",
//     "apptype": "Repeats",
//     "repeattime": 11,
//     "venue": "Studio 10 x 10"
// }

// export function provideStorage() { return new Storage(); }

// describe('Appointment Booked - UI Part', () => {
//     let de: DebugElement;
//     let comp: AppointmentBookedPage;
//     let fixture: ComponentFixture<AppointmentBookedPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [AppointmentBookedPage],
//             imports: [
//                 IonicModule.forRoot(AppointmentBookedPage),
//                 IonicModule.forRoot(ViewChild),
//                 IonicStorageModule.forRoot(),
//                 HttpModule
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 { provide: Storage },
//                 NavController, { provide: NavParams, useClass: NavParamsMock },
//                 FetcherProvider,
//                 ConnectionBackend,
//                 RepeatBookingAuthProvider
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(AppointmentBookedPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     //APPOINTMENT BOOKED PAGE MENU BAR TEST CASE-1.
//     it("Menu Bar to be present.", async () => {
//         fixture = TestBed.createComponent(AppointmentBookedPage);
//         de = fixture.debugElement.query(By.css(".repeatBookingMenuBar")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     //APPOINTMENT BOOKED PAGE MENU BAR TEST CASE-2.
//     it("Title of Menu bar to be present.", async () => {
//         const title: HTMLElement = fixture.debugElement.query(By.css('.menuBarTitle')).nativeElement;
//         expect(title.textContent).toContain("Appointment Booking");
//     });

//     //APPOINTMENT BOOKED PAGE CHECK ICON TEST CASE-1.
//     it("Green Check Mark  To be Present   ", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css('.checkIcon')).nativeElement;
//         expect(element2).toBeDefined();
//     });

//     //APPOINTMENT BOOKED PAGE CHECK ICON TEST CASE-2.
//     it("Green Check Mark  To be Centered   ", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css('.checkIconContainer')).nativeElement;
//         expect(element2.getAttribute('text-center')).toBeDefined()
//     });

//     //APPOINTMENT BOOKED PAGE CHECK ICON TEST CASE-3.
//     it("Green Check Mark  To be have padding    ", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css('.checkIconContainer')).nativeElement;
//         expect(element2.getAttribute('padding-top')).toBeDefined()
//     });

//     //APPOINTMENT BOOKED PAGE CHECK ICON TEST CASE-4.
//     it("Green Check Mark  To be Check Mark   ", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css('.checkIcon')).nativeElement;
//         expect(element2.getAttribute('name')).toMatch('md-checkmark-circle');
//     });

//     //APPOINTMENT BOOKED PAGE "APPOINTMENT BOOKED" TEXT TEST CASE-1.
//     it("Sucessfull Booking to be shown ", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css(".suceesfullBooking")).nativeElement;
//         expect(element2.textContent).toMatch('Appointment Booked');
//     });

//     //APPOINTMENT BOOKED PAGE "APPOINTMENT BOOKED" TEXT TEST CASE-2.
//     it("Sucessfull Booking to be Centered   ", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css('.suceesfullBookingcontainer')).nativeElement;
//         expect(element2.getAttribute('text-center')).toBeDefined()
//     });

//     //APPOINTMENT BOOKED PAGE "APPOINTMENT BOOKED" TEXT TEST CASE-3.
//     it("Sucessfull Booking to be have padding Bottom ", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css(".suceesfullBookingcontainer")).nativeElement;
//         expect(element2.getAttribute('padding-top')).toBeDefined()
//     });

//     //APPOINTMENT DETAILS TEST CASE-1
//     it("Date and Time Icon Should be Present", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css(".DateAndTimeIcon")).nativeElement;
//         expect(element2).toBeDefined();
//     });

//     //APPOINTMENT DETAILS TEST CASE-2
//     it("Sucessfull Booking Date and Time to be shown ", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css(".AppointmentDateAndTime")).nativeElement;
//         expect(element2).toBeDefined();
//     });

//     //APPOINTMENT DETAILS TEST CASE-3
//     it("Purpose Icon Should be Present", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css(".PurposeIcon")).nativeElement;
//         expect(element2).toBeDefined();
//     });

//     //APPOINTMENT DETAILS TEST CASE-4
//     it("Sucessfull Booking Purpose to be shown ", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css(".AppointmentPurpose")).nativeElement;
//         expect(element2).toBeDefined();
//     });

//     //APPOINTMENT DETAILS TEST CASE-5
//     it("Trainer Icon Should be Present", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css(".TrainerIcon")).nativeElement;
//         expect(element2).toBeDefined();
//     });

//     //APPOINTMENT DETAILS TEST CASE-6
//     it("Trainer Avatar Should be Present", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css(".TrainerAvatar")).nativeElement;
//         expect(element2).toBeDefined();
//     });

//     //APPOINTMENT DETAILS TEST CASE-7
//     it("Sucessfull Booking Trainer Name to be shown", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css(".AppointmentTrainer")).nativeElement;
//         expect(element2).toBeDefined();
//     });

//     //APPOINTMENT DETAILS TEST CASE-8
//     it("Venue Icon Should be Present", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css(".VenueIcon")).nativeElement;
//         expect(element2).toBeDefined();
//     });

//     //APPOINTMENT DETAILS TEST CASE-9
//     it("Sucessfull Booking Venue to be shown ", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css(".AppointmentVenue")).nativeElement;
//         expect(element2).toBeDefined();
//     });

//     //APPOINTMENT DETAILS TEST CASE-10
//     it("Appointment Status Icon Should be Present", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css(".AppointmentStatusIcon")).nativeElement;
//         expect(element2).toBeDefined();
//     });

//     //APPOINTMENT DETAILS TEST CASE-11
//     it("Sucessfull Booking Appointment Status to be shown ", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css(".AppointmentStatus")).nativeElement;
//         expect(element2).toBeDefined();
//     });

//     //SCHEDULE BUTTON TEST CASE
//     it("Sucessfull Booking  Button to schedule  to be shown ", async () => {
//         let fixture = TestBed.createComponent(AppointmentBookedPage);
//         let element2 = fixture.debugElement.query(By.css(".donebutton")).nativeElement;
//         expect(element2).toBeDefined();
//     });

// });

// describe('Appointment Booked Logic Tests', () => {
//     let de: DebugElement;
//     let comp: AppointmentBookedPage;
//     let fixture: ComponentFixture<AppointmentBookedPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [AppointmentBookedPage],
//             imports: [
//                 IonicModule.forRoot(AppointmentBookedPage),
//                 IonicModule.forRoot(ViewChild),
//                 IonicStorageModule.forRoot(),
//                 HttpModule
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 { provide: Storage },
//                 NavController, { provide: NavParams, useClass: NavParamsMock },
//                 FetcherProvider,
//                 ConnectionBackend,
//                 RepeatBookingAuthProvider
//             ]
//         });
//     }));

//     beforeEach(() => {

//         fixture = TestBed.createComponent(AppointmentBookedPage); //TESTBED CREATES THE OBJECT OF THE LOGIN PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance; //CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//         de = fixture.debugElement.query(By.css("foot")); //VARIABLE USED FOR DEBUGGING THE QUERY SELECTED ELEMENT.
//     });

//     it("Response Fetcher Method should be present", async () => {
//         expect(comp.getData).toBeDefined();
//     });

//     it("Check if purpose is present", async () => {
//         expect(dummyResponse.purpose).toBeDefined();
//     });

//     it("Check if purpose icon is present", async () => {
//         expect(dummyResponse.purposeIcon).toBeDefined();
//     });

//     it("Check if trainer is present", async () => {
//         expect(dummyResponse.trainer).toBeDefined();
//     });

//     it("Check if date is present", async () => {
//         expect(dummyResponse.date).toBeDefined();
//     });

//     it("Check if icon is present", async () => {
//         expect(dummyResponse.icon).toBeDefined();
//     });

//     it("Check if availableSlot is present", async () => {
//         expect(dummyResponse.availableSlot).toBeDefined();
//     });

//     it("Check if apptype is present", async () => {
//         expect(dummyResponse.apptype).toBeDefined();
//     });

//     it("Check if repeattime is present", async () => {
//         expect(dummyResponse.repeattime).toBeDefined();
//     });

//     it("Check if venue is present", async () => {
//         expect(dummyResponse.venue).toBeDefined();
//     });

//     it("Reverse Timer Method should be present", async () => {
//         expect(comp.startTimer).toBeDefined();
//     });

// });






