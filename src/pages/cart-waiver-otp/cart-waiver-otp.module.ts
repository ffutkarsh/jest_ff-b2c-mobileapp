import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CartWaiverOtpPage } from './cart-waiver-otp';

@NgModule({
  declarations: [
    CartWaiverOtpPage,
  ],
  imports: [
    IonicPageModule.forChild(CartWaiverOtpPage),
  ],
})
export class CartWaiverOtpPageModule {}
