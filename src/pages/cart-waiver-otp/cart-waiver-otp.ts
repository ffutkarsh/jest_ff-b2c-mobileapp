import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Platform, AlertController, LoadingController } from 'ionic-angular';
import { CenterSelectAuthProvider } from '../../providers/center-select-auth/center-select-auth';
import { DashboardFlowAuthProvider } from '../../providers/dashboard-flow-auth/dashboard-flow-auth';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { WaiverTermsProvider } from '../../providers/waiver-terms/waiver-terms';
import { ViewController } from 'ionic-angular';

declare var SMS: any;

@IonicPage()
@Component({
  selector: 'page-cart-waiver-otp',
  templateUrl: 'cart-waiver-otp.html',
})
export class CartWaiverOtpPage {

  public otp11;
  public otp12;
  public otp13;
  public otp14;
  public otp15;
  public otp16;
  public timer;
  public responseOTP;
  public tickWithbracket;//timer value with ()
  public focused: number; // Used to save index of currently focussed OTP field
  public tick: number; //TIMER
  public isButtondisabled; //button disabled
  public resendState;
  digits: number;// describe no of digits
  public receivedPhoneNo = "2134323";// Save Phone Number Received from navParams

  public cartProducts;
  public additionalProducts;
  public totalProducts;
  public totalPrice;
  public discountAmount;
  public appFlowCart;

  @ViewChild('op11') myInput11;
  @ViewChild('op12') myInput12;
  @ViewChild('op13') myInput13;
  @ViewChild('op14') myInput14;
  @ViewChild('op15') myInput15;
  @ViewChild('op16') myInput16;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public events: Events, public platform: Platform,    public viewCtrl: ViewController,
    public centerAuthService: CenterSelectAuthProvider,
    public dashboardFlowAuthService: DashboardFlowAuthProvider,
    private alertCtrl: AlertController,
    public waiverterms: WaiverTermsProvider,
    public loadingCtrl: LoadingController) {

    this.isButtondisabled = true; // Disabling Submit Button
    this.GetWaiverTermsContent();
    this.appFlowCart = this.navParams.get("appFlow"); //from app flow.
    this.cartProducts = this.navParams.get('cartProducts');
    this.additionalProducts = this.navParams.get('additionalProducts');
    this.totalProducts = this.navParams.get('totalProducts');
    this.totalPrice = this.navParams.get('totalPrice');
    this.discountAmount = this.navParams.get('discountAmount');

  }

  closeModal(e)
  {
    if((e.path[0].className) == 'scroll-content')
    {
      this.viewCtrl.dismiss()
    }

  }

  // Method to start reverse timer for 'n' seconds
  startTimer(timeoutt) {
    this.timer = TimerObservable.create(100, 1000).subscribe(t => {
      this.tick = timeoutt - t;
      this.tickWithbracket = "(" + this.tick + ")";
      if (this.tick <= 0) {
        this.timer.unsubscribe();//method to stop timer 
        this.tick = null;
        this.tickWithbracket = "";
      }
    });
  }

  otpconcat() {
    return ("" + this.otp11 + this.otp12 + this.otp13 + this.otp14 + this.otp15 + this.otp16);
  } // Returns the concatinated OTP as String

  public saveFocus(i) {
    this.focused = i;
  } // Used to save Focus 

  checkSubEnable() {
    console.log(this.otpconcat());
    if (this.otpconcat().length == 6) {
      this.isButtondisabled = false;
      console.log("length " + this.otpconcat().length)
    }
    else {
      this.isButtondisabled = true;
    }
  }

  checkifNo() {
    let reg = new RegExp('^\\d+$');
    //return reg.test(""+this.otpconcat);
    if (!reg.test(this.otpconcat())) {
      this.showInvalidAlert();
      return false;
    }
    return true;
  } // Checks if invalid input entered in OTP Fields

  showInvalidAlert() {
    const alert = this.alertCtrl.create({
      title: 'Sorry',
      subTitle: 'Invalid OTP',
      buttons: ['OK']
    });
    //for otp6
    if (this.digits == 6) {
      this.otp11 = ""; this.otp12 = ""; this.otp13 = ""; this.otp14 = ""; this.otp15 = ""; this.otp16 = "";
    }
    alert.present();
  }   // Shows Alert 

  // Loader to display while verifying OTP
  presentLoadingText() {
    let loading = this.loadingCtrl.create({
      content: 'Verifying Please Wait...'
    });
    loading.present();
    setTimeout(() => {
      loading.dismiss();
      this.LoginEvent();

      this.navCtrl.pop();
      this.navCtrl.push('WaiverSuccessPage',
        {
          'phoneNumber': this.receivedPhoneNo,
          'otp': this.otpconcat(),
          'cartProducts': this.cartProducts,
          'additionalProducts': this.additionalProducts,
          'totalProducts': this.totalProducts,
          'totalPrice': this.totalPrice,
          'discountAmount': this.discountAmount,
          'appFlow': this.appFlowCart
        }
      ).then(()=>
      {
        this.navCtrl.remove(this.navCtrl.getPrevious().index);
        this.navCtrl.remove(this.navCtrl.getPrevious().index -1);
      }
      );
    }, 2000);
  }  // Shows loader while contacting to server 

  LoginEvent() {
    this.events.publish('User Verified and Logged In', Date.now());
  }

  // WaiverSuccessPage
  submit() {
    this.centerAuthService.fromOtppage();
    this.dashboardFlowAuthService.fromOtpToDashboard()
    console.log("FR0M OTP TO DASHBOARD-----------" + this.dashboardFlowAuthService.authenticated1())
    if (this.checkifNo()) {
      console.log("Good to go to new Page");
      this.presentLoadingText();
    }
  } // Submit Button Action

  sendd() {
    this.platform.ready().then((readySource) => {
      let otp = Math.floor(Math.random() * 999999) + 1;
      if (SMS) SMS.sendSMS("+919869081205", "Hello, Testing your OTP is " + otp + ".This will expire soon.", () => {
        console.log("Sent");
      }, Error => {
        console.log("Error occurs")
      });
    });
  }

  resendOTP() {
    this.resendState = true;
    if (this.tick > 0) {
      console.log('please wait');
    }
    else {
      this.sendd();
      this.startTimer(12);
    }
    // this.sendd();
  }

  setFocusOn(i) {
    if (i == 0) {
      this.myInput11.setFocus();
    }
    if (i == 1) {
      this.myInput12.setFocus();
    }
    if (i == 2) {
      this.myInput13.setFocus();
    }
    if (i == 3) {
      this.myInput14.setFocus();
    }
    if (i == 4) {
      this.myInput15.setFocus();
    }
    if (i == 5) {
      this.myInput16.setFocus();
    }
  }

  backwards(id) {
    if (id == 1) {
      if (this.otp12.length == 0) {
        this.setFocusOn(0);
      }
    }

    if (id == 2) {
      if (this.otp13.length == 0) {
        this.setFocusOn(1);
      }
    }

    if (id == 3) {
      if (this.otp14.length == 0) {
        this.setFocusOn(2);
      }
    }

    if (id == 4) {
      if (this.otp15.length == 0) {
        this.setFocusOn(3);
      }
    }

    if (id == 5) {
      if (this.otp16.length == 0) {
        this.setFocusOn(4);
      }
    }
  }

  checkNo(phoneNumber) {
    let reg = new RegExp('^\\d+$');
    return reg.test(phoneNumber);
  }

  switchType(event, id) {
    this.checkSubEnable();
    if (event.keyCode == 8 || event.keyCode == 46) {
      this.backwards(id);
    }
    else {
      if (id == 0) {
        if (this.otp11.length != 0) {
          let a = this.otp11.slice(-1);
          if (!this.checkNo(a)) {
            this.otp11 = "";
          }
          else {
            this.setFocusOn(1);
          }
        }
      }
      if (id == 1) {
        if (this.otp12.length != 0) {
          let a = this.otp12.slice(-1);
          if (!this.checkNo(a)) {
            this.otp12 = "";
          }
          else {
            this.setFocusOn(2);
          }
        }
        if (this.otp11.length == 0) {
          this.otp12 = "";
          this.setFocusOn(0);
        }
      }
      if (id == 2) {
        if (this.otp13.length != 0) {
          let a = this.otp13.slice(-1);
          if (!this.checkNo(a)) {
            this.otp13 = "";
          }
          else {
            this.setFocusOn(3);
          }
        }
        if (this.otp12.length == 0) {
          this.otp13 = "";
          this.setFocusOn(1);
        }
      }
      if (id == 3) {
        if (this.otp14.length != 0) {
          let a = this.otp14.slice(-1);
          if (!this.checkNo(a)) {
            this.otp14 = "";
          }
          else {
            this.setFocusOn(4);
          }
        }
        if (this.otp13.length == 0) {
          this.otp14 = "";
          this.setFocusOn(2);
        }
      }
      if (id == 4) {
        if (this.otp15.length != 0) {
          let a = this.otp15.slice(-1);
          if (!this.checkNo(a)) {
            this.otp15 = "";
          }
          else {
            this.setFocusOn(5);
          }
        }
        if (this.otp14.length == 0) {
          this.otp15 = "";
          this.setFocusOn(3);
        }
      }
      if (id == 5) {
        if (this.otp16.length != 0) {
          let a = this.otp16.slice(-1);
          if (!this.checkNo(a)) {
            this.otp16 = "";
            this.checkSubEnable();
          }
        }
        if (this.otp15.length == 0) {
          this.otp16 = "";
          this.setFocusOn(4);
        }
      }
    }
  } // Used to automatically switch focus on next input while entering OTP

  ionViewDidLoad() {
    this.startTimer(12);
    console.log(this.tickWithbracket);
  }

  ////----------------
  GetWaiverTermsContent() {
    this.waiverterms.getWaiverOtp().map((response) => response).subscribe((response) => {
      this.responseOTP = response;
      console.log(this.responseOTP);
    });
  }

}// Used to automatically start resend otp timer at the page load


