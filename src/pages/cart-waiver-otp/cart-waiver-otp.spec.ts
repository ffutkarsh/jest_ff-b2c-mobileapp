// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, ViewChild } from '@angular/core';
// import { CartWaiverOtpPage } from './cart-waiver-otp';
// import { Events, IonicModule, Platform, NavController, NavParams } from 'ionic-angular/index';
// import { StatusBar } from '@ionic-native/status-bar';
// import { SplashScreen } from '@ionic-native/splash-screen';
// import { TranslateService } from '@ngx-translate/core';
// import { OtpHelperProvider } from '../../providers/otp-helper/otp-helper';
// import { Http, ConnectionBackend } from '@angular/http';
// import { IonicStorageModule, Storage } from '@ionic/storage';
// import { Grid, Col, Row } from 'ionic-angular/umd';
// import { AndroidPermissions } from '@ionic-native/android-permissions';
// import { HttpModule } from '@angular/http';
// import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
// import { DashboardFlowAuthProvider } from '../../providers/dashboard-flow-auth/dashboard-flow-auth';
// import { CenterSelectAuthProvider } from '../../providers/center-select-auth/center-select-auth';

// class NavParamsMock {
// public get(key): any {

//          if (key === 'combinedata') {

//              return {
//                  combinedata: {
//                      'domain': '',
//                      'phone': '',
//                      'otp': ''
//                  }
//              }
//          }
//      }
//  }



// describe('Waiver OTP Page UI Testing', () => {
//     let comp: CartWaiverOtpPage;
//      let fixture: ComponentFixture<CartWaiverOtpPage>;

//      beforeEach(async(() => {
//          TestBed.configureTestingModule({
//              declarations: [CartWaiverOtpPage],
//              imports: [
//                  HttpModule,
//                  IonicModule.forRoot(CartWaiverOtpPage),
//                  IonicStorageModule.forRoot()
//              ],
//              providers: [
//                  NavController,
//                  AndroidPermissions,
//                  TranslateService,
//                  OtpHelperProvider,
//                  { provide: NavParams, useClass: NavParamsMock },
//                  AuthServiceProvider,
//                  DashboardFlowAuthProvider,
//                  CenterSelectAuthProvider,
//                  Events
//              ]
//          });
//      }));

//      beforeEach(() => {
//          fixture = TestBed.createComponent(CartWaiverOtpPage);
//          comp = fixture.componentInstance;
//      });

//      it('MainContent classname', () => {
//          const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.main-content')).nativeElement;
//          console.log(inputfield.className);
//          expect(inputfield.className).toMatch('main-content');
//      });

//      it('Modal classname', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.Modal')).nativeElement;
//         console.log(inputfield.className);
//         expect(inputfield.className).toMatch('Modal');
//     });

//     it(" Header Text ", async () => {
//         let fixture = TestBed.createComponent(CartWaiverOtpPage);
//         let element2 = fixture.debugElement.nativeElement.querySelector(".header");
//         console.log(element2);
//         expect(element2.textContent.trim()).toContain("Enter OTP");
//     });

//     it('OTP description classname', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.Otp-Desc')).nativeElement;
//         console.log(inputfield.className);
//         expect(inputfield.className).toMatch('Otp-Desc');
//     });

//     it(" Otp description Text ", async () => {
//         let fixture = TestBed.createComponent(CartWaiverOtpPage);
//         let element2 = fixture.debugElement.nativeElement.querySelector(".Otp-Desc");
//         console.log(element2);
//         expect(element2.textContent.trim()).toContain( "OTP has been sent to mobile number 981212****." 
//         );
//     });

//     it(" Otp description Text ", async () => {
//         let fixture = TestBed.createComponent(CartWaiverOtpPage);
//         let element2 = fixture.debugElement.nativeElement.querySelector(".Otp-Desc");
//         console.log(element2);
//         expect(element2.textContent.trim()).toContain(  
//         "For signing waiver release form."
//      );
//     });

//     it(" Otp description Text ", async () => {
//         let fixture = TestBed.createComponent(CartWaiverOtpPage);
//         let element2 = fixture.debugElement.nativeElement.querySelector(".Otp-Desc");
//         console.log(element2);
//         expect(element2.textContent.trim()).toContain(  
//       "Please enter the same in the space provided below.");
//     });

//     it('paddingGrid classname', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.paddingGrid')).nativeElement;
//         console.log(inputfield.className);
//         expect(inputfield.className).toMatch('paddingGrid');
//     });

//     it('OTP row classname', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.OTProw')).nativeElement;
//         console.log(inputfield.className);
//         expect(inputfield.className).toMatch('OTProw');
//     });
    

//      it("Max Length for OTP input 1", async () => {
//          let fixture = TestBed.createComponent(CartWaiverOtpPage);
//          let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel1");
//          expect(element1.getAttribute("maxlength")).toContain("1");
//      });

//      it("OTP input type is tel for input 1", async () => {
//         let fixture = TestBed.createComponent(CartWaiverOtpPage);
//         let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel1");
//         expect(element1.getAttribute("type")).toContain("tel");
//     });


//      it("Max Length for OTP input 2", async () => {
//          let fixture = TestBed.createComponent(CartWaiverOtpPage);
//          let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel2");
//          expect(element1.getAttribute("maxlength")).toContain("1");
//      });

//      it("OTP input type is tel for input 2", async () => {
//         let fixture = TestBed.createComponent(CartWaiverOtpPage);
//         let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel1");
//         expect(element1.getAttribute("type")).toContain("tel");
//     });

//      it("Max Length for OTP input 3", async () => {
//          let fixture = TestBed.createComponent(CartWaiverOtpPage);
//          let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel3");
//          expect(element1.getAttribute("maxlength")).toContain("1");
//      });

//      it("OTP input type is tel for input 3", async () => {
//         let fixture = TestBed.createComponent(CartWaiverOtpPage);
//         let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel1");
//         expect(element1.getAttribute("type")).toContain("tel");
//     });

//      it("Max Length for OTP input 4", async () => {
//          let fixture = TestBed.createComponent(CartWaiverOtpPage);
//          let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel4");
//          expect(element1.getAttribute("maxlength")).toContain("1");
//      });

//      it("OTP input type is tel for input 4", async () => {
//         let fixture = TestBed.createComponent(CartWaiverOtpPage);
//         let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel1");
//         expect(element1.getAttribute("type")).toContain("tel");
//     });

     
//      it("Max Length for OTP input 5", async () => {
//          let fixture = TestBed.createComponent(CartWaiverOtpPage);
//          let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel5");
//          expect(element1.getAttribute("maxlength")).toContain("1");
//      });

//      it("OTP input type is tel for input 5", async () => {
//         let fixture = TestBed.createComponent(CartWaiverOtpPage);
//         let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel1");
//         expect(element1.getAttribute("type")).toContain("tel");
//     });

//      it("Max Length for OTP input 6", async () => {
//          let fixture = TestBed.createComponent(CartWaiverOtpPage);
//          let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel6");
//          expect(element1.getAttribute("maxlength")).toContain("1");
//      });

//      it("OTP input type is tel for input 6", async () => {
//         let fixture = TestBed.createComponent(CartWaiverOtpPage);
//         let element1 = fixture.debugElement.nativeElement.querySelector(".otplabel1");
//         expect(element1.getAttribute("type")).toContain("tel");
//     });
    

//     it('Submit button classname', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.subbutton')).nativeElement;
//         console.log(inputfield.className);
//         expect(inputfield.className).toMatch('subbutton');
//     });
//   //dont include {{ tickWithbracket }}
//     //it(" Resend OTP text ", async () => {
//       //  let fixture = TestBed.createComponent(CartWaiverOtpPage);
//       //  let element2 = fixture.debugElement.nativeElement.querySelector(".URL_Section");
//       //  console.log(element2);
//       //  expect(element2.textContent.trim()).toContain(`{{ tickWithbracket }}Resend OTP`);
//  //   });
// });


//  describe('Waiver OTP Page Logic Testing', () => {
//      let component: CartWaiverOtpPage;
//      let fixture: ComponentFixture<CartWaiverOtpPage>;
//      let otpService: OtpHelperProvider;
//      let timerCallback: any;
//      let setFocus: any;
//      let authService : AuthServiceProvider;
//      let dashboardflowauthService : DashboardFlowAuthProvider;

//      beforeEach(() => {
//          this.receivedPhoneNo= "2134323";
//          TestBed.configureTestingModule({
//              declarations: [CartWaiverOtpPage],
//              imports: [
//                  HttpModule,
//                  IonicModule.forRoot(CartWaiverOtpPage),
//                  IonicModule.forRoot(ViewChild),
//                  IonicStorageModule.forRoot()
//              ],
//              providers: [OtpHelperProvider, NavController,
//                  AndroidPermissions,
//                  { provide: NavParams, useClass: NavParamsMock },
//                  AuthServiceProvider,
//                  DashboardFlowAuthProvider,
//                  CenterSelectAuthProvider,
//                  Events
//              ]
//          });
//      });

//      beforeEach(() => {
//          // create component and test fixture
//          fixture = TestBed.createComponent(CartWaiverOtpPage);

//          // get test component from the fixture
//          component = fixture.componentInstance;

//          // UserService provided to the TestBed
//           authService = TestBed.get(OtpHelperProvider);
//        otpService = fixture.debugElement.injector.get(OtpHelperProvider);
//       authService = fixture.debugElement.injector.get(AuthServiceProvider);
//       dashboardflowauthService = fixture.debugElement.injector.get(DashboardFlowAuthProvider);
//      });

//      afterEach(() => {
//          otpService = null;
//      });

//      it('OTP Help Provider should be created', () => {
//          expect(otpService).toBeDefined();
//      });

//      it('Auth service should be defined', () => {
//         expect(authService).toBeDefined();
//     });

//     it('Dashboard auth flow service should be defined', () => {
//         expect(dashboardflowauthService).toBeDefined();
//     });

//      it('Otp should Concat ', () => {
//         component.otp11 = 1; component.otp12 = 2; component.otp13 = 3; component.otp14 = 4; component.otp15 = 5; component.otp16 = 6; // Mimicking User input
//          expect(component.otpconcat()).toContain('123456');
//      });

//      it("Focus on a field test", () => {
//          component.saveFocus(0);
//          expect(component.focused == 0).toBe(true);
//      });

//      it("Focus on a field test", () => {
//         component.saveFocus(1);
//         expect(component.focused == 1).toBe(true);
//     });

//     it("Focus on a field test", () => {
//         component.saveFocus(2);
//         expect(component.focused == 2).toBe(true);
//     });

//     it("Focus on a field test", () => {
//         component.saveFocus(3);
//         expect(component.focused == 3).toBe(true);
//     });

//     it("Focus on a field test", () => {
//         component.saveFocus(4);
//         expect(component.focused == 4).toBe(true);
//     });

//     it("Focus on a field test", () => {
//         component.saveFocus(5);
//         expect(component.focused == 5).toBe(true);
//     });

  

// //     // edited by sayli 
// //     it("set Focus on", () => {
// //         let component = fixture.componentInstance;
// //         expect(document.activeElement).toBeTruthy();
// //         console.log("testing" + document.activeElement)
// //     });

// //     //try to check focused element
// //     it('focuses the element on mount', () => {
// //         const elem: HTMLElement = fixture.debugElement.query(By.css('.otplabel1')).nativeElement;
// //         expect(elem.focus).toBeTruthy();
// //     }); //It fails on toBeFalsy

//     it("Focus Forward on input", () => {
//          component.saveFocus(3);
//          console.log(component.focused);
//         expect(component.focused).toMatch("3");
//     });

//     it("Focus Backward on backspace or Delete", () => {
//        component.saveFocus(2);
//         console.log(component.focused);
//         expect(component.focused).toMatch("2");
//     });

//     it("Enable Button on all six OTP Fields Filled", () => {
//          component.otp11 = 5;component.otp12 = 5; component.otp13 = 5; component.otp14 = 5;component.otp15 = 5;component.otp16 = 5;
//          component.checkSubEnable() ; // Mimicking User input
//         expect(component.isButtondisabled).toBe(false);
//     });
 
//     // it("Check if input is number only", () => {
//     //      component.otp11 = 5; component.otp12 = 5; component.otp13 = 5; component.otp14 = 5; component.otp15= 5; component.otp15=5; // Mimicking User input
//     //      component.otpconcat();
//     //      expect(component.checkifNo()).toBe(true);
//     //  });
 
//      it("ReceivedPhoneNo is '213432'", () => {
//          expect(component.receivedPhoneNo).toMatch("2134323");
//      }); // Checking is button Disabled

 

//     it("Check resend state on resend OTP", () => {
//         component.resendOTP();  
//         expect(component.resendState).toBe(true);
//     }); 

// //     it("OTP ReadListSMS", () => {
// //         expect(component.ReadListSMS).toBeTruthy();
// //     });

 
//      it("Make OTP Field empty if non number input", () => {
//          component.otp11 = 'a'// Mimicking User input
//         let a = component.otp11.slice(-1);
//         if (!component.checkNo(a)) {
//             component.otp11 = "";
//         }
//         expect(component.otp11).toBe("");
//     });

//     // it("resendOTP disable if timer is on", () => {
//     // component.tick =5 ; 
//     // expect(component.resendOTP()).toBe(false)

//     // }
//     // ) 


    
//  });


