import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events, AlertController } from 'ionic-angular';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import {
  NotificationlistPage,
  LocationModalPage,
  FilterPage,
  ClassDetailsPage
} from '../../pages/pages';
import { ClassesFlowApiProvider } from '../../providers/classes-flow-api/classes-flow-api';
import { Storage } from '@ionic/storage';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Geolocation } from '@ionic-native/geolocation';
import {
  NativeGeocoder,
  NativeGeocoderOptions
} from '@ionic-native/native-geocoder';
import { NetworkProvider } from '../../providers/network/network';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { Subject } from 'rxjs/Subject';
import { Slides } from 'ionic-angular';
import { extendMoment } from 'moment-range';
import {IpgeofetchProvider} from "../../providers/ipgeofetch/ipgeofetch";
const momentt = extendMoment(moment);

@IonicPage()
@Component({
  selector: 'page-classes',
  templateUrl: 'classes.html',
})
export class ClassesPage {

  @ViewChild(Slides) slides: Slides; //THIS IS USED FOR CREATING SLIDES.

  public classList = []; //STORING API RESPONSE OF CLASSES.


  public PurposeSessionsList = []; //STORING API RESPONSE OF PURPOSE SESSIONS.

  public currentSelectedLoc = "Search Location"; //BY DEFAULT PLACEHOLDER IN SEARCH BAR.
  public latnow = 19.119860; //LATITUDE OF CURRENT LOCATION. [HARDCODED]
  public longnow = 72.883461; //LONGITUDE OF CURRENT LOCATION. [HARDCODED]

  public now: any; //TO STORE CURRENT DATE.
  public before: any; //TO STORE YESTERDAY'S DATE.
  public beforethat: any; //TO STORE DAY BEFORE YESTERDAY'S DATE.
  public calcD: any //TO STORE FUTURE DATE WITH THE INTERVAL OF 1 DAY.
  public dates = []; //ARRAY TO STORE DATES.

  public beforeDays = []; //TO STORE DAYS DEFORE.
  public afterDays = []; //TO STORE DAYS AFTER.
  public currentUpcomingDayAfterWeek; //TO STORE UPCOMING DAY AFTER A WEEK.

  public YearArray = []; //TO STORE THE YEAR'S DATE.

  TotalSeriesArray: any; //TO STORE SERIES OF TIME WITH THE INTERVAL OF HALF AN HOURS.

  public knobValue = { lower: 15, upper: 32 }; // Initial values of dual knob (ion range)
  public leftknob; // Store value of left knob
  public rightknob; // Store value of right knob
  public startTime;  // Saves String of moment time object
  public endTime;  // Saves String of moment time object
  public hours;  // Saves all possible hours of day according to given interval

  public categories = []; //TO FETCH UNIQUE CATEGORIES[PURPOSES] FROM RESPONSE.
  public classesVisible = []; //TO STORE SORTED ARRAY OF CLASSES BASED ON CATEGORIES[PURPOSES]
  public selectedCategory = 0; //SAVE SELECTED SEGMENT - [BY DEFAULT FIRST SEGMENT WILL BE SELECTED ALWAYS]
  public selectedPurposeSessions = {}; //STORING ALL THE SESSIONS OF THE SELECTED CATEGORY[PURPOSE]

  //USED FOR NETWORK ABSENCE.
  public myObservable = new Subject<boolean>();
  public networkAvailable;
  public networkAlert;

  public locationMethodFlag = false; // Variable used to switch location fetching method
                                     // Use FALSE for IP Based Location
                                     // Use TRUE for GPS Based Location (Not working on higher Android API Level)

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalctrl: ModalController,
    private modalController: ModalController,
    public events: Events,
    private alertCtrl: AlertController,
    public translateService: TranslateService,
    public classesFlowApi: ClassesFlowApiProvider,
    private storage: Storage,
    private androidPermissions: AndroidPermissions,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    public networkProvider: NetworkProvider,
    public ipgeo:IpgeofetchProvider,
    private openNativeSettings: OpenNativeSettings) {

    this.getClassesList();


    this.getPurposeSessions();

    this.getCurrentLoc();

    this.now = moment(); //find date
    this.before = moment(this.now).subtract(1, 'days'); //yesterday
    this.beforethat = moment(this.before).subtract(1, 'days'); //day before yesterday

    let tempNow = {
      'isColor': '#69C258',
      'textColor': 'white',
      'date': this.now //changing todays color
    }

    let temp = {
      'isColor': 'transparent',
      'textColor': '#E5E5E5',
      'date': this.before, //changing yesterdays color
    }

    let temp2 = {
      'isColor': 'transparent',
      'textColor': '#E5E5E5',
      'date': this.beforethat  //changing day before yesterdays color
    }

    this.dates = new Array; //today's date in array

    this.dates.push(temp2); //pushing day before ysterday in array
    this.dates.push(temp); //pushing yesterday date in array
    this.dates.push(tempNow); //pushing todays date in array

    this.calcD = this.now; //calculating future dates

    for (let i = 3; i <= 360; i++) {
      this.calcD = moment(this.calcD).add(1, 'days'); //adding +1 as date
      let calc = {
        'isColor': 'transparent',
        'textColor': 'black', //changing current dates color
        'date': this.calcD
      }
      this.dates.push(calc); //pushing to date array
    }

    this.beforeDays = [];
    this.afterDays = [];

    this.YearArray = [];

    let day = moment(this.now).format('dddd');

    if (day == 'Monday') {
      this.computePrevious(this.beforeDays, 1);
    }
    else if (day == 'Tuesday') {
      this.computePrevious(this.beforeDays, 2);
    }
    else if (day == 'Wednesday') {
      this.computePrevious(this.beforeDays, 3);
    }
    else if (day == 'Thursday') {
      this.computePrevious(this.beforeDays, 4);
    }
    else if (day == 'Friday') {
      this.computePrevious(this.beforeDays, 5);
    }
    else if (day == 'Saturday') {
      this.computePrevious(this.beforeDays, 6);
    }
    else {

    }

    this.beforeDays = this.beforeDays.reverse(); //reverse to start from sunday

    this.computeNext(this.afterDays, this.beforeDays.length);

    this.currentUpcomingDayAfterWeek = this.afterDays[this.afterDays.length - 1];

    let beForeLength = this.beforeDays.length;
    let afterLength = this.afterDays.length;
    let FirstWeek = [];
    let increment = 0;

    while (beForeLength > 0) {
      let temp = {
        'isColor': 'transparent',
        'textColor': '#E5E5E5',
        'date': this.beforeDays[increment] //changing yesterdays color
      }
      FirstWeek.push(temp);
      increment = increment + 1;
      beForeLength = beForeLength - 1;
    }

    let afterCounter = Math.abs(afterLength - beForeLength);
    let increment2 = 0;
    let tempafter;

    while (afterCounter > 0) {
      if (increment2 == 0) {
        tempafter = {
          'isColor': '#8FDCE7',
          'textColor': '#8FDCE7',
          'date': this.afterDays[increment2] //changing yesterdays color
        }
      } else {
        tempafter = {
          'isColor': 'transparent',
          'textColor': 'black',
          'date': this.afterDays[increment2] //changing yesterdays color
        }
      }
      FirstWeek.push(tempafter);
      increment2 = increment2 + 1;
      afterCounter = afterCounter - 1;
    }

    this.YearArray.push(FirstWeek);

    this.computeYear(this.currentUpcomingDayAfterWeek);

    this.TotalSeriesArray = [];
    this.generateSeries();

    this.leftknob = ((this.knobValue.lower / 48) * 100) - 2; // Sets intial value to knob
    this.rightknob = ((this.knobValue.upper / 48) * 100) - 1; // Sets intial value to knob
    this.hours = this.generateHours(); // Generates all possible hours of day according to given interval
    this.sliderChanger(); // Sets intial time string to knob pins

    this.networkProvider.initializeNetworkEvents();
    this.networkChangesSubscribe();
    this.networkAvailable = (this.networkProvider.isConnected() ? true : false);
    this.myObservable.next(this.networkAvailable);
    this.myObservable.subscribe(val => {
      if (!val) {
        this.showNetworkAlert();
      }
    });

  }

  ionViewDidLeave() {
    this.networkChangesUnsubscribe();
  }

  // Fetches all the class data
  getClassesList() {
    this.classesFlowApi.getClassList().map((response) => response).subscribe((response) => {
      this.classList = response['ClassList'];
      this.addDistance();
      let allpurpose = [];
      this.classList.forEach(element => {
        allpurpose.push(element.PurposeName)
      });
      this.categories = this.unique(allpurpose); // Fetching all unique category
      this.selectedCategory = 0; // Default selection first category
      this.sortByPurpose(0); // Filtering Default selection first category
    });
  }

  addDistance() {
    let latlong = [];
    for (let i = 0; i < this.classList.length; i++) {
      let coordinates = this.classList[i].CentreLocation;
     
    
       latlong = coordinates.split(",");
       console.log(latlong);
       this.classList[i].dist = this.distBetweenPlaces(latlong[0],latlong[1], this.latnow,this.longnow);
     
    if(!this.locationMethodFlag)
    {
      this.ipgeo.reverseGeo(latlong[0], latlong[1]).subscribe(data => {
        console.log(data);
        if(data.address.city)
        {
          this.classList[i].classlocation = data.address.city;
        }
        else
        if(data.address.village)
        {
          this.classList[i].classlocation = data.address.village;
        }
        else
        {
          this.classList[i].classlocation = data.address.town;
        }
        
      });
    }
    else
    {
     // let rand = this.templocs[Math.floor(Math.random() * this.templocs.length)];
     // this.classList[i].dist = rand.dist;
       let options: NativeGeocoderOptions = {
         useLocale: true,
         maxResults: 5
     };
    
     this.nativeGeocoder.reverseGeocode(latlong[0], latlong[1], options)
       .then((result) =>{ console.log(JSON.stringify(result[0]));
        this.classList[i].classlocation = result[0].locality;
      })
       .catch((error: any) => console.log(error));
      
    }
    
    }
  }

  // Takes input array with duplicate values and returns array with unique values
  unique(inp) {
    let uniqueValues = new Set(inp);  // Using Set to get all unique values
    let array = Array.from(uniqueValues);
    return array; // Array of unique values
  }

  // Method to only show selected purpose/ category
  // Takes input index of currently select purpose/ category segment
  sortByPurpose(p) {
    let result = this.classList.filter(c => c.PurposeName === this.categories[p] && this.checkIfBetweenTimeSlot(c));
    // Cehck if category matches and Class time lies between time selected using dual knobs
    this.classesVisible = result;
    this.classesVisible = this.sortNearBy(this.classesVisible);
    this.selectedCategory = p; // Saving selected Category index in variable selectedCategory
    for (let j = 0; j < this.PurposeSessionsList.length; j++) {
      if (this.categories[p] == this.PurposeSessionsList[j].purposeName) {
        this.selectedPurposeSessions = this.PurposeSessionsList[j];
      }
    }
  }

  //Checks if Class time lies between time selected using dual knobs
  // takes input object of single class
  checkIfBetweenTimeSlot(c) {
    let time = moment(c.StartTime, "hh:mm A"), // Getting Class time for given class
      beforeTime = moment(this.startTime, "hh:mm A"), // Selected start time according to left knob
      afterTime = moment(this.endTime, "hh:mm A").add(1, 'seconds'); // Selected start time according to left knob , Also adding "1 second" to filter inclusive of boundary values
    // When both knob position are the same i.e. overlapping knobs 
    if (this.startTime === this.endTime) {
      beforeTime = moment(this.startTime, "hh:mm A").subtract(1, 'seconds'); // Substracting "1 second" to filter inclusive of boundary values
      afterTime = moment(this.endTime, "hh:mm A").add(1, 'seconds'); // Adding "1 second" to filter inclusive of boundary values
    }
    if (time.isBetween(beforeTime, afterTime)) {
      return true; // Returns true if class lies between time selected using dual knobs
    }
    else {
      return false; // Returns true if class does Not lie between time selected using dual knobs
    }
  }

  sortNearBy(locarray) {
    return locarray.sort(function (a, b) {
      return a.dist - b.dist;
    })
  }

  getPurposeSessions() {
    this.classesFlowApi.getPurposeSessions().map((response) => response).subscribe((response) => {
      this.PurposeSessionsList = response['PurposeSessions'];
    });
  }

  //Method to get current Location
  getCurrentLoc() {

    if(!this.locationMethodFlag)
    {
      this.ipgeo.getLocfromIP().subscribe(data => {
        let latlong = data.loc.split(",");
        //console.log("latlong",latlong);
        this.latnow = latlong[0];
        this.longnow = latlong[1];
        });
    }
    else
    {
    // Asking Permission For Android Location access
     this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      success => console.log('Permission granted'),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
    );
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION]);
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latnow = resp.coords.latitude;
      this.longnow = resp.coords.longitude;
    }).catch((error) => {
      console.log('Error getting location', error);
    });
 

    }
    


  }

  computePrevious(arr, count) {
    for (let i = 0; i < count; i++) {
      let addDay = moment(this.now).subtract(i + 1, 'days');
      arr.push(addDay);
    }
  }

  computeNext(arr, count) {
    for (let i = 0; i < 7 - count; i++) {
      let addDay = moment(this.now).add(i, 'days');
      arr.push(addDay);
    }
  }

  computeYear(date) {
    //Logic For First week 
    let TempArray = []
    let Dateholder;
    for (let j = 0; j < 52; j++) {
      for (let i = 1; i <= 7; i++) {
        let DatetoBeAdded = moment(date).add(i, 'days');
        let temp = {
          'isColor': 'transparent',
          'textColor': 'black',
          'date': DatetoBeAdded, //changing yesterdays color
        }
        TempArray.push(temp);
        Dateholder = DatetoBeAdded;
      }
      this.YearArray.push(TempArray)
      TempArray = []
      date = Dateholder;
    }
  }

  generateSeries() {
    let currentCount = moment.duration({ 'minutes': 30 });
    for (let i = 0; i < 4; i++) {
      let seriesarray = [];
      for (let j = 0; j < 5; j++) {
        let duration = currentCount.clone();
        seriesarray.push(duration)
        duration.add(30, 'minutes');
        currentCount = duration.clone();
      }
      this.TotalSeriesArray.push(seriesarray);
    }
  }

  // Sets time string to knob pins , Fires every time any knob position is changed
  sliderChanger() {
    if (this.knobValue.lower > 0 && this.knobValue.lower <= 41) {
      this.leftknob = ((this.knobValue.lower / 48) * 100) - 2; // Calculating position for Left knob Time String
    }
    if (this.knobValue.upper < 41) {
      this.rightknob = ((this.knobValue.upper / 48) * 100) - 1; // Calculating position for Left knob Time String
    }
    this.startTime = this.hours[this.knobValue.lower]; // Setting Time String for Left knob
    this.endTime = this.hours[this.knobValue.upper]; // Setting Time String for Right knob
    this.sortByPurpose(this.selectedCategory); // Updating List as per selected purpose and time slot
  }

  // Generates all possible hours of day according to given interval
  generateHours() {
    let result = []; //empty array to store result
    let start = moment("12:00 AM", "hh:mm A"); // Starting at 12:00 AM
    // Generating Slots with 1/2 hour interval
    for (let i = 0; i <= 47; i++) {
      let mdate = start.format("hh:mm A");
      result.push(mdate);
      mdate = start.add(30, 'minutes').format("hh:mm A");
    }
    result.push("11:59 PM"); // Last Entry to array
    return result;
  }

  networkChangesSubscribe() {
    // Offline event
    this.events.subscribe('network:offline', () => {  //method to check internet
      this.myObservable.next(false);
      this.networkAvailable = false;
    });
    // Online event
    this.events.subscribe('network:online', () => {
      this.myObservable.next(true);
      this.networkAvailable = true;
    });
  }

  showNetworkAlert() {
    this.networkAlert = this.alertCtrl.create({
      title: 'No Internet Connection',
      message: 'Please check your internet connection.',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Retry',
          handler: () => {
            if (this.networkAvailable == false) {
              this.showNetworkAlert();
            }
          }
        },
        {
          text: 'Open Settings',
          handler: () => {

            this.networkAlert.dismiss().then(() => {
              this.showSettings();
              if (this.networkAvailable == false) {
                this.showNetworkAlert();
              }
            })
          }
        }
      ]
    });
    this.networkAlert.present();
  }

  showSettings() {
    if ((<any>window).cordova && (<any>window).cordova.plugins.settings) {
      (<any>window).cordova.plugins.settings.open("settings", function () { //OPENED SETTINGS

      },
        function () { //FAILED TO OPEN SETTINGS

        }
      );
    } else { //OPEN NATIVE SETTINGS IS NOT ACTIVE

    }
  }

  networkChangesUnsubscribe() {
    this.events.unsubscribe('network:offline');
    this.events.unsubscribe('network:online');
  }

  goToNotification() {
    this.navCtrl.push(NotificationlistPage);
  }

  // Go to Location Selection Modal
  locationSelection() {
    this.networkChangesUnsubscribe();
    let modal = this.modalController.create(LocationModalPage);
    modal.onDidDismiss((selected) => {
      if (typeof selected !== "undefined") {
        this.currentSelectedLoc = selected;
      }
      this.networkChangesSubscribe();
    }
    )
    modal.present();
  }

  goToFilter() {
    this.navCtrl.push(FilterPage);
  }

  slidePrev() {
    this.slides.slidePrev();
  }

  selectDateNew(item) {
    for (let j = 0; j < 53; j++) {
      if (j == 0) {
        for (let i = this.beforeDays.length; i < 7; i++) {
          if (this.YearArray[j][i].date == item.date) {
            this.YearArray[j][i].isColor = '#8FDCE7';
            this.YearArray[j][i].textColor = '#8FDCE7';
          }
          else {
            this.YearArray[j][i].isColor = 'transparent';
            this.YearArray[j][i].textColor = 'black';
          }
        }
      }
      else {
        for (let i = 0; i < 7; i++) {
          if (this.YearArray[j][i].date == item.date) {
            this.YearArray[j][i].isColor = '#8FDCE7';
            this.YearArray[j][i].textColor = '#8FDCE7';
          } else {
            this.YearArray[j][i].isColor = 'transparent';
            this.YearArray[j][i].textColor = 'black';
          }
        }
      }
    }
  }

  slideNext() {
    this.slides.slideNext();
  }

  goToClassDetail(c) {
    this.navCtrl.push(ClassDetailsPage, {
      'objectOfC': c,
      'PurposeSessions': this.selectedPurposeSessions
    });
  }

  // Draws star rating as per the number (rating) passed
  starRating(rating: number) {
    let full;
    if (rating <= 5) {
      full = Math.floor(rating);
    }
    else {
      full = 5;
    }
    let status = [];
    for (let i = 0; i < full; i++) {
      status.push("fulle.svg"); // saves Full Star svg in array
    }
    if (rating - full == 0.5 && !(rating >= 5)) {
      status.push("halfe.svg"); // saves Half Star svg in array
    }
    if (status.length < 5) {
      let emp = 5 - status.length;
      for (let i = 0; i < emp; i++) {
        status.push("fullgrey.svg");// saves Empty Star svg in array
      }
    }
    return status; // Returns array so that it can be rendered using *ngFor
  }

  distBetweenPlaces(lat1,long1,lat2,long2)
  {
    let radlat1 = Math.PI * lat1/180;
    let radlat2 = Math.PI * lat2/180;
    let radlon1 = Math.PI * long1/180;
    let radlon2 = Math.PI * long2/180;
    let theta = long1-long2;
    let radtheta = Math.PI * theta/180
    let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180/Math.PI
    dist = dist * 60 * 1.1515 * 1.609344;
    return  Math.round(dist * 100) / 100;
     
  }
  
}
