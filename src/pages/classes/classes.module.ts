import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ClassesPage } from './classes';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Geolocation } from '@ionic-native/geolocation';
import { MomentModule } from 'angular2-moment';

@NgModule({
  declarations: [
    ClassesPage
  ],
  imports: [
    IonicPageModule.forChild(ClassesPage),
    TranslateModule,
    MomentModule
  ],
  providers: [
    AndroidPermissions,
    Geolocation
  ]
})
export class ClassesPageModule { }
