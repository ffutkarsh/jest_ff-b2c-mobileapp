// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement } from '@angular/core';
// import { IonicModule, NavController, NavParams } from 'ionic-angular/index';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { ClassesPage } from './classes';
// import { ClassesFlowApiProvider } from '../../providers/classes-flow-api/classes-flow-api';
// import { IonicStorageModule } from '@ionic/storage';
// import { HttpModule } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';
// import { AndroidPermissions } from '@ionic-native/android-permissions';
// import { Geolocation } from '@ionic-native/geolocation';
// import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
// import { NetworkProvider } from '../../providers/network/network';
// import { Network } from '@ionic-native/network';
// import { OpenNativeSettings } from '@ionic-native/open-native-settings';
// import { MomentModule } from 'angular2-moment';

// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// let dummyData = [
//     {
//         "ClassId": "1",
//         "ClassLevel": "Beginner",
//         "Date": "03 Jan",
//         "StartTime": "08:30 AM",
//         "DurationEndtime": "1.30 h",
//         "Rating": "3",
//         "Ratingcount": "1540",
//         "CenterId": " ",
//         "CenterName": "Fitness Time Ladies",
//         "CenterAddress": "101,Abdulla bin Aziz street,mumzar,opposite to Ministry of Finance",
//         "CentreLocation": "18.520430,73.856743",
//         "CapicityStatus": "Available",
//         "MemberClassStatus": "Join",
//         "UserId": " ",
//         "UserName": "Elton David",
//         "UserImage": "assets/img/sarah-avatar.png.jpeg",
//         "PurposeId": "123",
//         "PurposeName": "Kickboxing",
//         "PurposeLogo": "assets/img/PT/kickboxing.svg",
//         "BackgroundImage": "assets/img/fotoram.jpg",
//         "Tags": [
//             "Cardio",
//             "High Intensity Dance"
//         ]
//     },
//     {
//         "ClassId": "2",
//         "ClassLevel": "Beginner",
//         "Date": "04 Jan",
//         "StartTime": "07:00 PM",
//         "DurationEndtime": "1.30 h",
//         "Rating": "3.5",
//         "Ratingcount": "1540",
//         "CenterId": " ",
//         "CenterName": "Fitness Time Ladies",
//         "CenterAddress": "101,Abdulla bin Aziz street,mumzar,opposite to Ministry of Finance",
//         "CentreLocation": "18.520430,73.856743",
//         "CapicityStatus": "Available",
//         "MemberClassStatus": "Join",
//         "UserId": " ",
//         "UserName": "Elton David",
//         "UserImage": "assets/img/sarah-avatar.png.jpeg",
//         "PurposeId": " ",
//         "PurposeName": "Zumba",
//         "PurposeLogo": "assets/img/zmb.png",
//         "BackgroundImage": "assets/img/fotoram.jpg",
//         "Tags": [
//             "Cardio",
//             "High Intensity Dance"
//         ]
//     },
//     {
//         "ClassId": "3",
//         "ClassLevel": "Beginner",
//         "Date": "05 Jan",
//         "StartTime": "08:00 PM",
//         "DurationEndtime": "1.30 h",
//         "Rating": "3",
//         "Ratingcount": "1540",
//         "CenterId": " ",
//         "CenterName": "Fitness Time Ladies",
//         "CenterAddress": "101,Abdulla bin Aziz street,mumzar,opposite to Ministry of Finance",
//         "CentreLocation": "18.520430,73.856743",
//         "CapicityStatus": "Available",
//         "MemberClassStatus": "Join",
//         "UserId": " ",
//         "UserName": "Elton David",
//         "UserImage": "assets/img/sarah-avatar.png.jpeg",
//         "PurposeId": "123",
//         "PurposeName": "Zumba",
//         "PurposeLogo": "assets/img/zmb.png",
//         "BackgroundImage": "assets/img/fotoram.jpg",
//         "Tags": [
//             "Cardio",
//             "High Intensity Dance"
//         ]
//     },
//     {
//         "ClassId": "4",
//         "ClassLevel": "Beginner",
//         "Date": "06 Jan",
//         "StartTime": "09:00 PM",
//         "DurationEndtime": "1.30 h",
//         "Rating": "3.5",
//         "Ratingcount": "1540",
//         "CenterId": " ",
//         "CenterName": "Fitness Time Ladies",
//         "CenterAddress": "101,Abdulla bin Aziz street,mumzar,opposite to Ministry of Finance",
//         "CentreLocation": "18.520430,73.856743",
//         "CapicityStatus": "Available",
//         "MemberClassStatus": "Join",
//         "UserId": " ",
//         "UserName": "Elton David",
//         "UserImage": "assets/img/sarah-avatar.png.jpeg",
//         "PurposeId": " ",
//         "PurposeName": "Aerobics",
//         "PurposeLogo": "assets/img/PT/aerobics.svg",
//         "BackgroundImage": "assets/img/fotoram.jpg",
//         "Tags": [
//             "Cardio",
//             "High Intensity Dance"
//         ]
//     },
//     {
//         "ClassId": "5",
//         "ClassLevel": "Advance",
//         "Date": "07 Jan",
//         "StartTime": "05:00 AM",
//         "DurationEndtime": "1.30 h",
//         "Rating": "4",
//         "Ratingcount": "1540",
//         "CenterId": " ",
//         "CenterName": "Fitness Time Ladies",
//         "CenterAddress": "101,Abdulla bin Aziz street,mumzar,opposite to Ministry of Finance",
//         "CentreLocation": "18.520430,73.856743",
//         "CapicityStatus": "Available",
//         "MemberClassStatus": "Joined",
//         "UserId": " ",
//         "UserName": "Elton David",
//         "UserImage": "assets/img/sarah-avatar.png.jpeg",
//         "PurposeId": "123",
//         "PurposeName": "Nutrition",
//         "PurposeLogo": "assets/img/zmb.png",
//         "BackgroundImage": "assets/img/fotoram.jpg",
//         "Tags": [
//             "Cardio",
//             "High Intensity Dance"
//         ]
//     },
//     {
//         "ClassId": "6",
//         "ClassLevel": "Beginner",
//         "Date": "08 Jan",
//         "StartTime": "09:00 AM",
//         "DurationEndtime": "1.30 h",
//         "Rating": "3.5",
//         "Ratingcount": "1540",
//         "CenterId": " ",
//         "CenterName": "Fitness Time Ladies",
//         "CenterAddress": "101,Abdulla bin Aziz street,mumzar,opposite to Ministry of Finance",
//         "CentreLocation": "18.520430,73.856743",
//         "CapicityStatus": "Available",
//         "MemberClassStatus": "Buy",
//         "UserId": " ",
//         "UserName": "Elton David",
//         "UserImage": "assets/img/sarah-avatar.png.jpeg",
//         "PurposeId": " ",
//         "PurposeName": "Nutrition",
//         "PurposeLogo": "assets/img/zmb.png",
//         "BackgroundImage": "assets/img/fotoram.jpg",
//         "Tags": [
//             "Cardio",
//             "High Intensity Dance"
//         ]
//     },
//     {
//         "ClassId": "7",
//         "ClassLevel": "Beginner",
//         "Date": "09 Jan",
//         "StartTime": "07:00 AM",
//         "DurationEndtime": "1.30 h",
//         "Rating": "4.5",
//         "Ratingcount": "1540",
//         "CenterId": " ",
//         "CenterName": "Fitness Time Ladies",
//         "CenterAddress": "101,Abdulla bin Aziz street,mumzar,opposite to Ministry of Finance",
//         "CentreLocation": "18.520430,73.856743",
//         "CapicityStatus": "Available",
//         "MemberClassStatus": "Join",
//         "UserId": " ",
//         "UserName": "Elton David",
//         "UserImage": "assets/img/sarah-avatar.png.jpeg",
//         "PurposeId": "123",
//         "PurposeName": "Kickboxing",
//         "PurposeLogo": "assets/img/zmb.png",
//         "BackgroundImage": "assets/img/fotoram.jpg",
//         "Tags": [
//             "Cardio",
//             "High Intensity Dance"
//         ]
//     },
//     {
//         "ClassId": "8",
//         "ClassLevel": "Advance",
//         "Date": "10 Jan",
//         "StartTime": "11:00 AM",
//         "DurationEndtime": "1.30 h",
//         "Rating": "3.5",
//         "Ratingcount": "1540",
//         "CenterId": " ",
//         "CenterName": "Fitness Time Ladies",
//         "CenterAddress": "101,Abdulla bin Aziz street,mumzar,opposite to Ministry of Finance",
//         "CentreLocation": "18.520430,73.856743",
//         "CapicityStatus": "Available",
//         "MemberClassStatus": "Joined",
//         "UserId": " ",
//         "UserName": "Elton David",
//         "UserImage": "assets/img/sarah-avatar.png.jpeg",
//         "PurposeId": " ",
//         "PurposeName": "Kickboxing",
//         "PurposeLogo": "assets/img/zmb.png",
//         "BackgroundImage": "assets/img/fotoram.jpg",
//         "Tags": [
//             "Cardio",
//             "High Intensity Dance"
//         ]
//     },
//     {
//         "ClassId": "9",
//         "ClassLevel": "Beginner",
//         "Date": "11 Jan",
//         "StartTime": "03:45 PM",
//         "DurationEndtime": "1.30 h",
//         "Rating": "3.5",
//         "Ratingcount": "1540",
//         "CenterId": " ",
//         "CenterName": "Fitness Time Ladies",
//         "CenterAddress": "101,Abdulla bin Aziz street,mumzar,opposite to Ministry of Finance",
//         "CentreLocation": "18.520430,73.856743",
//         "CapicityStatus": "Available",
//         "MemberClassStatus": "Joined",
//         "UserId": " ",
//         "UserName": "Elton David",
//         "UserImage": "assets/img/sarah-avatar.png.jpeg",
//         "PurposeId": "123",
//         "PurposeName": "Massage",
//         "PurposeLogo": "assets/img/zmb.png",
//         "BackgroundImage": "assets/img/fotoram.jpg",
//         "Tags": [
//             "Cardio",
//             "High Intensity Dance"
//         ]
//     },
//     {
//         "ClassId": "10",
//         "ClassLevel": "Advance",
//         "Date": "12 Jan",
//         "StartTime": "06:00 AM",
//         "DurationEndtime": "1.30 h",
//         "Rating": "2.5",
//         "Ratingcount": "1540",
//         "CenterId": " ",
//         "CenterName": "Fitness Time Ladies",
//         "CenterAddress": "101,Abdulla bin Aziz street,mumzar,opposite to Ministry of Finance",
//         "CentreLocation": "18.520430,73.856743",
//         "CapicityStatus": "Available",
//         "MemberClassStatus": "Buy",
//         "UserId": " ",
//         "UserName": "Elton David",
//         "UserImage": "assets/img/sarah-avatar.png.jpeg",
//         "PurposeId": " ",
//         "PurposeName": "Massage",
//         "PurposeLogo": "assets/img/zmb.png",
//         "BackgroundImage": "assets/img/fotoram.jpg",
//         "Tags": [
//             "Cardio",
//             "High Intensity Dance"
//         ]
//     },
//     {
//         "ClassId": "11",
//         "ClassLevel": "Beginner",
//         "Date": "13 Jan",
//         "StartTime": "03:00 PM",
//         "DurationEndtime": "1.30 h",
//         "Rating": "3.5",
//         "Ratingcount": "1540",
//         "CenterId": " ",
//         "CenterName": "Fitness Time Ladies",
//         "CenterAddress": "101,Abdulla bin Aziz street,mumzar,opposite to Ministry of Finance",
//         "CentreLocation": "18.520430,73.856743",
//         "CapicityStatus": "Available",
//         "MemberClassStatus": "Join",
//         "UserId": " ",
//         "UserName": "Elton David",
//         "UserImage": "assets/img/sarah-avatar.png.jpeg",
//         "PurposeId": "123",
//         "PurposeName": "P T",
//         "PurposeLogo": "assets/img/zmb.png",
//         "BackgroundImage": "assets/img/fotoram.jpg",
//         "Tags": [
//             "Cardio",
//             "High Intensity Dance"
//         ]
//     },
//     {
//         "ClassId": "12",
//         "ClassLevel": "Advance",
//         "Date": "14 Jan",
//         "StartTime": "02:00 PM",
//         "DurationEndtime": "1.30 h",
//         "Rating": "3.5",
//         "Ratingcount": "1540",
//         "CenterId": " ",
//         "CenterName": "Fitness Time Ladies",
//         "CenterAddress": "101,Abdulla bin Aziz street,mumzar,opposite to Ministry of Finance",
//         "CentreLocation": "18.520430,73.856743",
//         "CapicityStatus": "Available",
//         "MemberClassStatus": "Buy",
//         "UserId": " ",
//         "UserName": "Elton David",
//         "UserImage": "assets/img/sarah-avatar.png.jpeg",
//         "PurposeId": " ",
//         "PurposeName": "P T",
//         "PurposeLogo": "assets/img/zmb.png",
//         "BackgroundImage": "assets/img/fotoram.jpg",
//         "Tags": [
//             "Cardio",
//             "High Intensity Dance"
//         ]
//     }
// ]

// describe('Classes Page UI Testing', () => {
//     let comp: ClassesPage;
//     let fixture: ComponentFixture<ClassesPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 ClassesPage
//             ],
//             imports: [
//                 IonicModule.forRoot(ClassesPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 MomentModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 ClassesFlowApiProvider,
//                 AndroidPermissions,
//                 Geolocation,
//                 NativeGeocoder,
//                 NetworkProvider,
//                 Network,
//                 OpenNativeSettings
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(ClassesPage);
//         comp = fixture.componentInstance;
//     });

//     // Header Section Tests
//     it('Header should be present', () => {
//         const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.navbarclass')).nativeElement;
//         expect(inputfield.className).toMatch('navbarclass');
//     });

//     // Location Bar Tests
//     it('Area to display location should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.location')).nativeElement;
//             expect(inputfield.className).toMatch('location');
//         });
//     });

//     it('Area to display filter icon should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.filtericon')).nativeElement;
//             expect(inputfield.className).toMatch('filtericon');
//         });
//     });

//     it('Filter icon should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.sortico')).nativeElement;
//             expect(inputfield.className).toMatch('sortico');
//         });
//     });

//     it('Location should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.location')).nativeElement;
//             expect(inputfield.className).toMatch('location');
//         });
//     });

//     // Segment Section Tests
//     it('Segment should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.men-seg')).nativeElement;
//             expect(inputfield.className).toMatch('men-seg');
//         });
//     });

//     it('Selected Buttons Class inside Segment should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.selectedButton')).nativeElement;
//             expect(inputfield.className).toMatch('selectedButton');
//         });
//     });

//     it('Un Selected Buttons Class inside Segment should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.classbuttons')).nativeElement;
//             expect(inputfield.className).toMatch('classbuttons');
//         });
//     });

//     // Date Slider Tests
//     it('Date Slider  should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.dateslider')).nativeElement;
//             expect(inputfield.className).toMatch('dateslider');
//         });
//     });

//     // Time Range Tests
//     it('Time Slider (ion-range) should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.timeslider')).nativeElement;
//             expect(inputfield.className).toMatch('timeslider');
//         });
//     });

//     //  Class Cards Section Tests
//     it('Class Cards grid should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.cardgrid')).nativeElement;
//             expect(inputfield.className).toMatch('cardgrid');
//         });
//     });

//     it('Class Cards should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.ccard')).nativeElement;
//             expect(inputfield.className).toMatch('ccard');
//         });
//     });

//     it('Class Cards start time should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.strttime')).nativeElement;
//             expect(inputfield.className).toMatch('strttime');
//         });
//     });

//     it('Class Cards Status should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.cardlabeljoin')).nativeElement;
//             expect(inputfield.className).toMatch('cardlabeljoin');
//         });
//     });

//     it('Class Cards time duration should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.classduration')).nativeElement;
//             expect(inputfield.className).toMatch('classduration');
//         });
//     });

//     it('Class Cards icon image should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.classico')).nativeElement;
//             expect(inputfield.className).toMatch('classico');
//         });
//     });

//     it('Class Cards distance should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.c3')).nativeElement;
//             expect(inputfield.className).toMatch('c3');
//         });
//     });

//     it('Class Cards trainer name should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.trainername')).nativeElement;
//             expect(inputfield.className).toMatch('trainername');
//         });
//     });

//     it('Class Cards trainer icon should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.trainericon')).nativeElement;
//             expect(inputfield.className).toMatch('trainericon');
//         });
//     });

//     it('Class Cards trainer full or not status should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.c1')).nativeElement;
//             expect(inputfield.className).toMatch('c1');
//         });
//     });

//     it('Rating Bar inside card should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.stargeneral')).nativeElement;
//             expect(inputfield.className).toMatch('stargeneral');
//         });
//     });

//     it('Rating Bar User Count inside card should be present', () => {
//         fixture.whenStable().then(() => {
//             // after something in the component changes, you should detect changes
//             fixture.detectChanges();
//             // everything else in the beforeEach needs to be done here.
//             const inputfield: HTMLInputElement = fixture.debugElement.query(By.css('.usercount')).nativeElement;
//             expect(inputfield.className).toMatch('usercount');
//         });
//     });
// });

// describe('Classes Page Logic Testing', () => {
//     let comp: ClassesPage;
//     let fixture: ComponentFixture<ClassesPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 ClassesPage
//             ],
//             imports: [
//                 IonicModule.forRoot(ClassesPage),
//                 TranslateModule.forChild(),
//                 IonicStorageModule.forRoot(),
//                 HttpModule,
//                 HttpClientModule,
//                 MomentModule
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,
//                 ClassesFlowApiProvider,
//                 AndroidPermissions,
//                 Geolocation,
//                 NativeGeocoder,
//                 NetworkProvider,
//                 Network,
//                 OpenNativeSettings
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(ClassesPage);
//         comp = fixture.componentInstance;
//     });

//     it('Sliding ion-range knobs - 1', () => {
//         comp.sliderChanger();
//         expect(comp.leftknob).toBeDefined();
//     });

//     it('Sliding ion-range knobs - 2', () => {
//         comp.sliderChanger();
//         expect(comp.rightknob).toBeDefined();
//     });

//     it('Generate all possible hours of day according to given interval', () => {
//         let res = comp.generateHours();
//         expect(res.length).toBe(49);
//     });

//     it('Rating Bar as per the number(rating) passed', () => {
//         let res = comp.starRating(2.5);
//         expect(res).toContain("fulle.svg");
//     });

//     it('Method to show location Modal', () => {
//         expect(comp.locationSelection).toBeDefined();
//     });

//     it('Fetching Data From JSON file', () => {
//         expect(comp.getClassesList).toBeDefined();
//     });

//     it('Function that returns array of unique values', () => {
//         let input = [1, 1, 1, 2, 3, 3, 4, 4, 5, 5, 5];  // Dummy data for testing
//         expect(comp.unique(input).length).toBe(5);
//     });

//     it('Sorting by Purpose', () => {
//         // Dummy data for testing
//         comp.categories = ["Aerobics", "Test"];
//         comp.classList = dummyData;
//         comp.sortByPurpose(1);
//         expect(comp.classesVisible).not.toBe(null);
//     });

//     it('Check If given time is Between selected TimeSlot - 1', () => {
//         // Dummy data for testing
//         comp.startTime = "05:00 PM";
//         comp.endTime = "07:00 PM";
//         expect(comp.checkIfBetweenTimeSlot(dummyData[0])).toBe(false);
//     });

//     it('Check If given time is Between selected TimeSlot - 2', () => {
//         // Dummy data for testing
//         comp.startTime = "09:00 AM";
//         comp.endTime = "11:00 AM";
//         expect(comp.checkIfBetweenTimeSlot(dummyData[0])).toBe(false);
//     });

//     it('Method to fetch location co-ordinates', () => {
//         expect(comp.getCurrentLoc).toBeDefined();
//     });

//     it('Method to move to next page', () => {
//         expect(comp.goToClassDetail).toBeDefined();
//     });

//     it('Method to Add Calculated Distance to Class List Array', () => {
//         comp.classList = dummyData; // Setting Dummy Data
//         comp.addDistance(); // Adding Dist attribute to the class list
//         expect(comp.classList[0].dist.length != 0).toBe(true); // Checking if clas list has "dist" attribute
//     });

//     it('Method to Class List according Distance - to nearest first', () => {
//         comp.classList = dummyData; // Setting Dummy Data
//         comp.addDistance(); // Adding Dist attribute to the class list
//         comp.classesVisible = comp.sortNearBy(comp.classList); // Sorting according Distance - to nearest first
//         expect(comp.classesVisible[0].dist < comp.classesVisible[1].dist).toBe(false); // Checking if first class distance is less than second class
//     });
// });