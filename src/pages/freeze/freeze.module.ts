import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FreezePage } from './freeze';
import { TranslateModule } from '@ngx-translate/core';
import { MomentModule } from 'angular2-moment';

@NgModule({
  declarations: [
    FreezePage,
  ],
  imports: [
    IonicPageModule.forChild(FreezePage),
    MomentModule
  ],
})
export class FreezePageModule {}
