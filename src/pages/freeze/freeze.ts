import { Component, ViewChildren, QueryList } from '@angular/core';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, LoadingController, NavParams } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { ChooseTrainerAuthProvider } from '../../providers/choose-trainer-auth/choose-trainer-auth';
import { DateTimeAuthProvider } from '../../providers/date-time-auth/date-time-auth';
import { extendMoment } from 'moment-range';
import { Storage } from '@ionic/storage';

const momentt = extendMoment(moment);

@IonicPage()
@Component({
    selector: 'page-freeze',
    templateUrl: 'freeze.html',
})
export class FreezePage {

    @ViewChildren(Slides) slides: QueryList<Slides>

    now: any;
    beforeDays: any;
    afterDays: any;
    YearArray: any;
    currentUpcomingDayAfterWeek: any;
    YearArr: any;
    fromDate: any;
    toDate: any;
    YearArrayTo: any;
    purpose;
   public  setofferLocally =[];
    constructor(
        public dateTimeAuthService: DateTimeAuthProvider,
        public chooseTrainerAuthService: ChooseTrainerAuthProvider,
        public modalCtrl: ModalController,
        public viewCtrl: ViewController,
        public popoverCtrl: PopoverController,
        public translateService: TranslateService,
        public loadingCtrl: LoadingController,
        public navCtrl: NavController,
        public navParams: NavParams,
        private storage: Storage
    ) {

        this.YearArrayTo = [];

        this.computeMonthofCalander();

        this.purpose = this.navParams.get("freeze");
        console.log("freez data", this.purpose)

        this.YearArrayTo = [...this.YearArr];

        this.now = moment(); //find date
        let day = moment(this.now).format('dddd');

        this.beforeDays = [];
        this.afterDays = [];

        this.YearArray = [];

        if (day == 'Monday') {
            this.computePrevious(this.beforeDays, 1);
        }
        else if (day == 'Tuesday') {
            this.computePrevious(this.beforeDays, 2);
        }
        else if (day == 'Wednesday') {
            this.computePrevious(this.beforeDays, 3);
        }
        else if (day == 'Thursday') {
            this.computePrevious(this.beforeDays, 4);
        }
        else if (day == 'Friday') {
            this.computePrevious(this.beforeDays, 5);
        }
        else if (day == 'Saturday') {
            this.computePrevious(this.beforeDays, 6);
        }
        else {
            console.log('start');
        }

        //reverse to start from sunday
        this.beforeDays = this.beforeDays.reverse();
        this.computeNext(this.afterDays, this.beforeDays.length);
        this.currentUpcomingDayAfterWeek = this.afterDays[this.afterDays.length - 1];

        //Last Day 
        let beForeLength = this.beforeDays.length
        let FirstWeek = [];
        let afterLength = this.afterDays.length;
        let increment = 0;

        while (beForeLength > 0) {
            let temp = {
                'isColor': 'transparent',
                'textColor': '#E5E5E5',
                'date': this.beforeDays[increment], //changing yesterdays color
            }
            FirstWeek.push(temp);
            increment = increment + 1;
            beForeLength = beForeLength - 1;
        }

        let afterCounter = Math.abs(afterLength - beForeLength);
        let increment2 = 0;
        let tempafter;
        while (afterCounter > 0) {

            if (increment2 == 0) {
                tempafter = {
                    'isColor': '#8FDCE7',
                    'textColor': '#8FDCE7',
                    'date': this.afterDays[increment2], //changing yesterdays color
                }
            } else {
                tempafter = {
                    'isColor': 'transparent',
                    'textColor': 'black',
                    'date': this.afterDays[increment2], //changing yesterdays color
                }
            }
            FirstWeek.push(tempafter);
            increment2 = increment2 + 1;
            afterCounter = afterCounter - 1;
        }

        //First Week;
        this.YearArray.push(FirstWeek);
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad FreezePage');
    }

    ionViewWillEnter() {
        this.purpose = this.navParams.get("freeze");
        this.presentLoading();
    }

    presentLoading() {  //to present loading after button press
        const loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 500
        });
        loader.present();
    }

    computeMonthofCalander() {
        let Year = moment().format('YYYY');
        let date = moment();
        let MonthNumber = moment().format('MM');
        let YearArrT;
        let temArr;

        YearArrT = [];
        this.YearArr = [];

        for (let i = 0; i < 12; i++) {
            temArr = this.getDaysArray(Year, MonthNumber);
            date = moment(date).add(1, 'month');
            Year = moment(date).format('YYYY');
            MonthNumber = moment(date).format('MM');
            YearArrT.push(temArr);
            temArr = [];
        }

        this.YearArr = YearArrT;
    }

    getDaysArray = (year, month) => {
        const names = Object.freeze(['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat']);
        const date = new Date(year, month - 1, 1);
        const result = [];
        while (date.getMonth() == month - 1) {
            let tempafter = {
                'isColor': 'transparent',
                'textColor': 'black',
                'date': moment(date), //changing yesterdays color
            }
            result.push(tempafter);
            date.setDate(date.getDate() + 1);
        }
        return result;
    }

    computePrevious(arr, count) {
        for (let i = 0; i < count; i++) {
            let addDay = moment(this.now).subtract(i + 1, 'days');
            arr.push(addDay);
        }
    }

    computeNext(arr, count) {
        for (let i = 0; i < 7 - count; i++) {
            let addDay = moment(this.now).add(i, 'days');
            arr.push(addDay);
        }
    }

    slidePrev(index: number) {
        this.slides.toArray()[index].slidePrev();
    }

    slideNext(index: number) {
        this.slides.toArray()[index].slideNext();
    }

    selectDate(date, type) {
        if (type == 'from') {
            this.resetColor();
        }
        if (type == 'to' && ((this.fromDate > date.date) || (this.fromDate.length == 0))) {
            return;

        }

        // console.log('from:' + type);
        // console.log(moment(date.date));
        // console.log("Afterrr Clickkkk");
        for (let i = 0; i < 12; i++) {
            for (let j = 0; j < this.YearArr[i].length; j++) {
                //  console.log(moment(this.YearArr[i][j].date).format('L'));
                if (type == 'from') {

                    if (this.YearArr[i][j].date == date.date) {
                        this.YearArr[i][j].isColor = '#5FBFCD';
                        this.YearArr[i][j].textColor = '#fff';
                        this.fromDate = this.YearArr[i][j].date;
                    }
                }
                else if (type == 'to') {
                    if (this.YearArr[i][j].date == date.date) {
                        this.toDate = this.YearArr[i][j].date;
                        this.YearArr[i][j].isColor = '#5FBFCD';
                        this.YearArr[i][j].textColor = '#fff';
                        //    console.log("from ..........to", this.fromDate, this.toDate);
                        this.colorDates(this.fromDate, this.toDate);
                    }

                }
            }
        }
    }


    resetColor() {
        for (let i = 0; i < 12; i++) {
            for (let j = 0; j < this.YearArr[i].length; j++) {

                this.YearArr[i][j].isColor = 'transparent';
                this.YearArr[i][j].textColor = 'black';

            }
        }
    }

    colorDates(from, to) {
        let range = momentt().range(momentt(from).add(1, 'days'), momentt(to).subtract(1, 'days'));
        for (let i = 0; i < 12; i++) {
            for (let j = 0; j < this.YearArr[i].length; j++) {
                if (range.contains(this.YearArr[i][j].date)) {
                    this.YearArr[i][j].isColor = '#B7DDE3';
                    this.YearArr[i][j].textColor = '#000';
                }
                else
                    if (this.YearArr[i][j].date == this.fromDate || this.YearArr[i][j].date == this.toDate) {
                        this.YearArr[i][j].isColor = '#5FBFCD';
                        this.YearArr[i][j].textColor = '#fff';
                        //      console.log("excepttt",this.YearArr[i][j].date);
                    }
                    else {
                        this.YearArr[i][j].isColor = 'transparent';
                        this.YearArr[i][j].textColor = '#000';
                    }
            }
        }
    }

    goToCart() {

        let obj = {
            freezAmount:this.purpose.RecurringAmount,
            type:"freeze",
            freezepurposeName: this.purpose.purpose,
            freezefromDate: "",
            freezetoDate: "",
            freezedurationinDays:(this.toDate.diff(this.fromDate, 'days') + 1)
        };
        console.log(obj)

        this.storage.get('CARTITEM').then((val) => {
            this.setofferLocally = val;
         }).then(
           () => {
             if ((this.setofferLocally) || (this.setofferLocally = [])) {
       
               this.setofferLocally.push(obj);
               this.storage.set('CARTITEM', this.setofferLocally);
     
             }
           });


             this.navCtrl.push("CartItemsPage")

        // this.navCtrl.push("CartItemsPage", { 
        //     'freezeFlow': true,
        //     'freezeDetails': {
        //         type:"freeze",
        //         freezepurposeName: this.purpose.purpose,
        //         freezefromDate: this.fromDate,
        //         freezetoDate: this.toDate,
        //         freezedurationinDays: (this.toDate.diff(this.fromDate, 'days') + 1)
        //     }
        // })
    }

}
