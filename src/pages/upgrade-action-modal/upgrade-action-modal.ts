import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { FetcherProvider } from '../../providers/fetcher/fetcher';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-upgrade-action-modal',
  templateUrl: 'upgrade-action-modal.html',
})
export class UpgradeActionModalPage {
  upgradeMembership:any
  total:number;
  city=[];
  cities:any;
  newcost:any;
  oldcost:any;
  month;
  upgradeData;
  setofferLocally;

  constructor(public navCtrl: NavController, public navParams: NavParams,public fetcher: FetcherProvider,public viewCtrl: ViewController,private storage: Storage) {
    this.city=[];
    this.getcity();
    this.newcost=11000;
    this.oldcost=5000;
    this.upgradeMembership=navParams.get("upgrade");
    this.upgradeData = this.navParams.get("upgradeJsonData");
    this.upgradeData = this.navParams.get("upgradeJsonData")[0];
console.log(" this.upgradeData", this.upgradeData)
 
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpgradeActionModalPage');
  }

  totalcost() {
    this.total = Number(this.upgradeMembership.passportRate) - Number(this.oldcost); 
    // console.log("total----"+this.total);   
    return this.total;
    
  }
  getcity() {
    this.fetcher.getCities().map((response) => response.json()).subscribe((response) => {
      this.city = response;
      console.log("citylist------",this.city);      
    });
  }

  closeModal()
  {
    // this.navCtrl.pop()
    this.viewCtrl.dismiss();
  }


  goToCart() {
    
    let upData;
    if(this.upgradeData.frozenstatus){

      upData = {

        expiry:this.upgradeData.expiry,
        frozenicon:this.upgradeData.frozenicon,
        frozenstatus:this.upgradeData.frozenstatus, 
        location:this.upgradeData.location,
        purposedescription:this.upgradeData.purposedescription,
        totalCost: this.totalcost(),
        totalSessions:this.upgradeData.totalSessions,
        usedSessions:this.upgradeData.usedSessions,
        quantity : 1,
        type : "upgradeFrozen"
      }

    }else{

    upData = {
      purposedescription:this.upgradeData.purposedescription,
      contractName: this.upgradeData.contractName,
      contractStatus: this.upgradeData.contractStatus,
      endDate: this.upgradeData["end-date"],
      location: this.upgradeData.location,
      package:this.upgradeData.package,
      purpose: this.upgradeData.purpose,
      purposelogo: this.upgradeData.purposelogo,
      startDate: this.upgradeData["start-date"],
      upgradeAmount : this.totalcost(),
      quantity : 1,
      type : "upgradeContract"
      
    }
 
  }

console.log("upData",upData);
    this.storage.get('CARTITEM').then((val) => {
       this.setofferLocally = val;
    }).then(
      () => {
        if ((this.setofferLocally) || (this.setofferLocally = [])) {
  
          this.setofferLocally.push(upData);
          this.storage.set('CARTITEM', this.setofferLocally);

        }
      });


    this.navCtrl.push("CartItemsPage",
      {
        'upgradeFlow': true,"upgrdeTotalcost":this.totalcost(),'upgradeCartData':this.navParams.get("upgradeJsonData")
      });
  }
}
