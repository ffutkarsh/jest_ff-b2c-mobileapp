import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpgradeActionModalPage } from './upgrade-action-modal';
import { FetcherProvider } from '../../providers/fetcher/fetcher';

@NgModule({
  declarations: [
    UpgradeActionModalPage,
  ],
  imports: [
    IonicPageModule.forChild(UpgradeActionModalPage),
  ],
  providers:[   
    FetcherProvider   
  ]
})
export class UpgradeActionModalPageModule {}
