import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ClassesPage } from '../classes/classes';

@IonicPage()
@Component({
  selector: 'page-emptyclasses',
  templateUrl: 'emptyclasses.html',
})
export class EmptyclassesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EmptyclassesPage');
  }

  goToClasslist(){
    this.navCtrl.push(ClassesPage);
  }

}
