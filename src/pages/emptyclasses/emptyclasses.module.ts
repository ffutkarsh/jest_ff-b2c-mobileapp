import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmptyclassesPage } from './emptyclasses';

@NgModule({
  declarations: [
    EmptyclassesPage,
  ],
  imports: [
    IonicPageModule.forChild(EmptyclassesPage),
  ],
})
export class EmptyclassesPageModule {}
