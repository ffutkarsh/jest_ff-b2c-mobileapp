// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, ViewChild } from '@angular/core';
// import { EmptyclassesPage } from './emptyclasses';
// import { IonicModule, Platform, NavController, NavParams } from 'ionic-angular/index';
// import { StatusBar } from '@ionic-native/status-bar';
// import { SplashScreen } from '@ionic-native/splash-screen';
// import { Http, ConnectionBackend } from '@angular/http';
// import { IonicStorageModule, Storage } from '@ionic/storage';
// import { Grid, Col, Row } from 'ionic-angular/umd';
// import { HttpModule } from '@angular/http';
// import { TranslatePipe } from '@ngx-translate/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { TranslateStore } from "@ngx-translate/core/src/translate.store";
// import { TranslateService } from '@ngx-translate/core';


// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }

// // The describe function is for grouping related specs
// describe('empty class details Page UI Testing', () => {
//     let comp: EmptyclassesPage;
//     let fixture: ComponentFixture<EmptyclassesPage>;
//     let de: DebugElement;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [EmptyclassesPage],
//             imports: [
//                 HttpModule,
//                 IonicModule.forRoot(EmptyclassesPage),
//                 IonicStorageModule.forRoot(),
//                 TranslateModule.forChild()
//             ],
//             providers: [
//                 NavController,
//                 { provide: NavParams, useClass: NavParamsMock },
//                 TranslateService,
//                 TranslateStore,            ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(EmptyclassesPage); /////TESTBED CREATES THE OBJECT OF THE CLASSDETAILS  PAGE AND ASSIGNS IT TO FIXTURE WHICH HANDLES THE HTML PART.
//         comp = fixture.componentInstance;   //////CREATES THE INSTANCE OF THAT OBJECT AND ASSIGNS IT TO COMP VARIABLE TO ACCESS THE SERVICES.
//     });

//     ///// Test Cases for Header Part
//         it("Header to be present.", async () => {
//         fixture = TestBed.createComponent(EmptyclassesPage);
//         de = fixture.debugElement.query(By.css(".Header")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Navbar to be present.", async () => {
//         fixture = TestBed.createComponent(EmptyclassesPage);
//         de = fixture.debugElement.query(By.css(".emptyclassMenuBar")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("title to be present.", async () => {
//         fixture = TestBed.createComponent(EmptyclassesPage);
//         de = fixture.debugElement.query(By.css(".title")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("image for Notification icon  to be present.", async () => {
//         fixture = TestBed.createComponent(EmptyclassesPage);
//         de = fixture.debugElement.query(By.css(".NotificationIcon")).nativeElement;
//         expect(de).toBeDefined();
//     });    
    
//     it("button for Notification icon  to be present.", async () => {
//         fixture = TestBed.createComponent(EmptyclassesPage);
//         de = fixture.debugElement.query(By.css(".buttonNotification")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     ///// Test cases for Content Part
//     it("Content to be present.", async () => {
//         fixture = TestBed.createComponent(EmptyclassesPage);
//         de = fixture.debugElement.query(By.css(".contentclass")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("grid for Content to be present.", async () => {
//         fixture = TestBed.createComponent(EmptyclassesPage);
//         de = fixture.debugElement.query(By.css(".contentgrid")).nativeElement;
//         expect(de).toBeDefined();
//     });
    
//     it("grid for label to be present.", async () => {
//         fixture = TestBed.createComponent(EmptyclassesPage);
//         de = fixture.debugElement.query(By.css(".labelgrid")).nativeElement;
//         expect(de).toBeDefined();
//     });
        
//     it("row for label to be present.", async () => {
//         fixture = TestBed.createComponent(EmptyclassesPage);
//         de = fixture.debugElement.query(By.css(".labelrow")).nativeElement;
//         expect(de).toBeDefined();
//     });
        
//     it("label to be present.", async () => {
//         fixture = TestBed.createComponent(EmptyclassesPage);
//         de = fixture.debugElement.query(By.css(".labelclass")).nativeElement;
//         expect(de).toBeDefined();
//     });
        
//     it("description for Content to be present.", async () => {
//         fixture = TestBed.createComponent(EmptyclassesPage);
//         de = fixture.debugElement.query(By.css(".labeldesc")).nativeElement;
//         expect(de).toBeDefined();
//     });    
    
//     it("grid for join button to be present.", async () => {
//         fixture = TestBed.createComponent(EmptyclassesPage);
//         de = fixture.debugElement.query(By.css(".joingrid")).nativeElement;
//         expect(de).toBeDefined();
//     });
    
//     it("grid for join button to be present.", async () => {
//         fixture = TestBed.createComponent(EmptyclassesPage);
//         de = fixture.debugElement.query(By.css(".rowbutton")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("grid for join button to be present.", async () => {
//         fixture = TestBed.createComponent(EmptyclassesPage);
//         de = fixture.debugElement.query(By.css(".colbutton")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("join button to be present.", async () => {
//         fixture = TestBed.createComponent(EmptyclassesPage);
//         de = fixture.debugElement.query(By.css(".joinclassbutton")).nativeElement;
//         expect(de).toBeDefined();
//     });

//      // Test Cases for click function of share icon
//     it('Method to move to next page', () => {
//         expect(comp.goToClasslist).toBeDefined();
//     });

// });