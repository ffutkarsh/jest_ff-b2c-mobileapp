import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MainPage } from '../../pages/pages';

@IonicPage()
@Component({
  selector: 'page-logoutconfirm',
  templateUrl: 'logoutconfirm.html',
})
export class LogoutconfirmPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage) { }

  clearData() {
    this.storage.remove('USERDETAILS');
    this.storage.remove('loggedIn');
    this.storage.remove('User_Details_From_Selected_Center');
    this.navCtrl.setRoot(MainPage); //REDIRECTING TO LOGIN PAGE.
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LogoutconfirmPage');
  }

  closeModal() {
    this.navCtrl.setRoot("DashboardPage");
  }

}
