import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogoutconfirmPage } from './logoutconfirm';

@NgModule({
  declarations: [
    LogoutconfirmPage,
  ],
  imports: [
    IonicPageModule.forChild(LogoutconfirmPage),
  ],
})
export class LogoutconfirmPageModule {}
