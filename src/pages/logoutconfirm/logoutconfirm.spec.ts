// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, ViewChild } from '@angular/core';
// import { LogoutconfirmPage } from './logoutconfirm';
// import { IonicModule, Platform, NavParams, NavController } from 'ionic-angular/index';
// import { Http, ConnectionBackend, HttpModule } from '@angular/http';
// import { IonicStorageModule, Storage } from '@ionic/storage';


// class NavParamsMock {
//     static returnParam = null;
//     public get(key): any {
//         if (NavParamsMock.returnParam) {
//             return NavParamsMock.returnParam
//         }
//         return 'default';
//     }
//     static setParams(value) {
//         NavParamsMock.returnParam = value;
//     }
// }



// export function provideStorage() { return new Storage(); }

// describe('Appointment Booked - UI Part', () => {
//     let de: DebugElement;
//     let comp: LogoutconfirmPage;
//     let fixture: ComponentFixture<LogoutconfirmPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [LogoutconfirmPage],
//             imports: [
//                 IonicModule.forRoot(LogoutconfirmPage),
//                 IonicModule.forRoot(ViewChild),
//                 IonicStorageModule.forRoot(),
//                 HttpModule
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 { provide: Storage },
//                 NavController, { provide: NavParams, useClass: NavParamsMock },
//                 ConnectionBackend
//             ]
//         });
//     }));

//     beforeEach(() => {
//         fixture = TestBed.createComponent(LogoutconfirmPage); 
//         comp = fixture.componentInstance; 
//         de = fixture.debugElement.query(By.css("foot"));
//     });

    
    
//     it("Content to be present.", async () => {
//         fixture = TestBed.createComponent(LogoutconfirmPage);
//         de = fixture.debugElement.query(By.css(".main-content")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Dialog area to be present.", async () => {
//         fixture = TestBed.createComponent(LogoutconfirmPage);
//         de = fixture.debugElement.query(By.css(".modal")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading to be present.", async () => {
//         fixture = TestBed.createComponent(LogoutconfirmPage);
//         de = fixture.debugElement.query(By.css(".Heading")).nativeElement;
//         expect(de).toBeDefined();
//     });

//     it("Heading Should Match", async () => {
//         const title: HTMLElement = fixture.debugElement.query(By.css('.Heading')).nativeElement;
//         expect(title.textContent).toContain("Confirm Logout");
//     });


    
//     it("Description to be present.", async () => {
//         fixture = TestBed.createComponent(LogoutconfirmPage);
//         de = fixture.debugElement.query(By.css(".reason")).nativeElement;
//         expect(de).toBeDefined();
//     });


//     it("Description Should Match", async () => {
//         const title: HTMLElement = fixture.debugElement.query(By.css('.reason')).nativeElement;
//         expect(title.textContent).toContain("Do you really want to logout from the application ?");
//     });

   
   


// });

// describe('Appointment Booked Logic Tests', () => {
//     let de: DebugElement;
//     let comp: LogoutconfirmPage;
//     let fixture: ComponentFixture<LogoutconfirmPage>;

//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [LogoutconfirmPage],
//             imports: [
//                 IonicModule.forRoot(LogoutconfirmPage),
//                 IonicModule.forRoot(ViewChild),
//                 IonicStorageModule.forRoot(),
//                 HttpModule
//             ],
//             providers: [
//                 NavController,
//                 Http,
//                 { provide: Storage },
//                 NavController, { provide: NavParams, useClass: NavParamsMock },
//                 ConnectionBackend
//             ]
//         });
//     }));

//     beforeEach(() => {

//         fixture = TestBed.createComponent(LogoutconfirmPage); 
//         comp = fixture.componentInstance;
       
//     });

   

//     it("Check if purpose is present", async () => {
//         expect(comp.clearData).toBeDefined();
//     });


 

// });