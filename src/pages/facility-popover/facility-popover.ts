import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-facility-popover',
  templateUrl: 'facility-popover.html',
})
export class FacilityPopoverPage {

  public purpose; //FROM BOOKED FACILITY PAGE OR FACILITY BOOKED DETAILS PAGE.
  public icon; //FROM BOOKED FACILITY PAGE OR FACILITY BOOKED DETAILS PAGE.
  public facilityCourt; //FROM BOOKED FACILITY PAGE OR FACILITY BOOKED DETAILS PAGE.
  public date; //FROM BOOKED FACILITY PAGE OR FACILITY BOOKED DETAILS PAGE.
  public availableSlot; //FROM BOOKED FACILITY PAGE OR FACILITY BOOKED DETAILS PAGE.

  constructor(
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams) {

    this.purpose = this.navParams.get("purpose");
    this.icon = this.navParams.get("icon");
    this.facilityCourt = this.navParams.get("trainername");
    this.date = this.navParams.get("date");
    this.availableSlot = this.navParams.get("availableSlot");

  }

  ionViewDidLoad() { }

  gotoDateTime() {
    this.navCtrl.push("FacilityDateTimePage", {
      'booleanFromEdit': true,
      'purpose': this.purpose,
      'icon': this.icon,
      'facilityCourt': this.facilityCourt,
      'date': this.date,
      'availableSlot': this.availableSlot,
    });
  }

  cancelApointment() {
    this.viewCtrl.dismiss();
    let myModal = this.modalCtrl.create("CancelFacilityModalPage");
    myModal.present();
  }

}
