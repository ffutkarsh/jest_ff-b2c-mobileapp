import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FacilityPopoverPage } from './facility-popover';

@NgModule({
  declarations: [
    FacilityPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(FacilityPopoverPage),
  ],
})
export class FacilityPopoverPageModule {}
